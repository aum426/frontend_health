webpackJsonp([0],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeeklyAchivementPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_weekly_achievement_weekly_achievement__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_setting_profile_setting__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { SettingsProvider } from '../../providers/settings/settings';

// import { analyzeAndValidateNgModules } from '@angular/compiler';

var WeeklyAchivementPage = /** @class */ (function () {
    function WeeklyAchivementPage(weeklyAchievement, navCtrl, navParams, nav
        // private settings: SettingsProvider
    ) {
        this.weeklyAchievement = weeklyAchievement;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nav = nav;
        // mockupData: any;
        this.nextLevel = [];
        var me = this;
        this.yourLevel = {
            levelNo: '',
            levelName: '',
            levelImage: '',
            lsInfo: []
        };
        this.weeklyAchievement.getWeeklyAchievement().then(function (data) {
            console.log('xxx ', data, '--- ', data != undefined);
            if (data.length > 0) {
                me.yourLevel = data[0];
                console.log(data[0]);
                for (var i = 1; i < data.length; i++) {
                    console.log('==>', data[i]);
                    me.nextLevel.push(data[i]);
                }
            }
        });
    }
    WeeklyAchivementPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WeeklyAchivementPage');
    };
    WeeklyAchivementPage.prototype.btnBack = function () {
        console.log(this.navParams, ' ', this.navParams.data.sourcePage);
        if (this.navParams.data.sourcePage == 'thisWeek') {
            console.log('this ');
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__profile_setting_profile_setting__["a" /* ProfileSettingPage */], {}, { animate: true, direction: 'back' });
        }
    };
    WeeklyAchivementPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-weekly-achivement',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/weekly-achivement/weekly-achivement.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>Weekly Achievement</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n\n  <div class="topHeader">\n    <div class="topCurve">\n      <div class="textTitleLevel">Your Level</div>\n    </div>\n    <div class="cardFloat">\n      <ion-card class="cardPadding">\n        <ion-row no-margin no-padding>\n          <ion-col col-5 no-margin no-padding class="center-center">\n            <img src="{{yourLevel.levelImage}}" class="imgLevel">\n          </ion-col>\n          <ion-col no-margin no-padding class="center-center">\n            <div class="paddingContent">\n              <div class="textLevel">LEVEL {{yourLevel.levelNo}}</div>\n              <div class="textTitle ">{{yourLevel.levelName}}</div>\n              <div *ngFor="let rec of yourLevel.lsInfo">\n                <div class="textDescription">{{rec.info}}</div>\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </div>\n    <div class="textNextLevel">Next level</div>\n  </div>\n\n</ion-header>\n\n<ion-content class="mainContent">\n\n  <div class="cardContentPadding">\n    <ion-card *ngFor="let record of nextLevel;let i= index">\n      <ion-row no-margin no-padding class="rowHeight">\n        <ion-col col-5 no-margin no-padding class="center-center">\n          <img src="{{record.levelImage}}" class="imgLevel">\n        </ion-col>\n        <ion-col no-margin no-padding class="center-center">\n          <div class="areaFullWidth">\n            <div class="textLevel">LEVEL {{record.levelNo}}</div>\n            <div class="textTitle ">{{record.levelName}}</div>\n\n            <!-- <ion-row no-margin no-padding class="marginTextPoint">\n              <ion-col col-2 class="center-center" no-margin no-padding>\n                <img src="assets/imgs/icons/icon_gold_coin.png" class="imgGoldCoin">\n              </ion-col>\n              <ion-col no-margin no-padding class="center-left" class="textSmilePoint">\n                <div class="textPoint">+ {{record.smile_point}} Smile points</div>\n              </ion-col>\n            </ion-row> -->\n            <div *ngFor="let rec of record.lsInfo">\n            <div class="textDescriptionNext">{{rec.info}}</div>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/weekly-achivement/weekly-achivement.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_weekly_achievement_weekly_achievement__["a" /* WeeklyAchievementProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */]
            // private settings: SettingsProvider
        ])
    ], WeeklyAchivementPage);
    return WeeklyAchivementPage;
}());

//# sourceMappingURL=weekly-achivement.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppleProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_health_kit__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppleProvider = /** @class */ (function () {
    function AppleProvider(http, platform, healthKit, config) {
        this.http = http;
        this.platform = platform;
        this.healthKit = healthKit;
        this.config = config;
        console.log('Hello AppleProvider Provider');
    }
    AppleProvider.prototype.getSyncDate = function (brand, mode) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('brand', brand);
        params.set('mode', mode);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getSyncDate', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    AppleProvider.prototype.syncAppleRawData = function (lsStep, lstWorkoOut) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('lsStep', JSON.stringify(lsStep));
        params.set('lsWorkOut', JSON.stringify(lstWorkoOut));
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.post(_this.config.serverAPI + 'syncAppleRawData', params)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    AppleProvider.prototype.checkPermission = function (func) {
        var me = this;
        if (me.platform.is('cordova')) {
            me.platform.ready().then(function () {
                me.healthKit.available().then(function (available) {
                    if (available) {
                        var options = {
                            readTypes: [
                                'HKQuantityTypeIdentifierStepCount',
                                'HKWorkoutTypeIdentifier',
                                'HKQuantityTypeIdentifierActiveEnergyBurned',
                                'HKQuantityTypeIdentifierDistanceCycling',
                                'HKQuantityTypeIdentifierHeartRate',
                                'HKQuantityTypeIdentifierDistanceWalkingRunning'
                            ]
                        };
                        me.healthKit.requestAuthorization(options).then(function (_) {
                            console.log('allow permisstion');
                            func("allow");
                        });
                    }
                    else {
                        console.log('ios can not access health function');
                        func("denied");
                    }
                });
            });
        }
    };
    AppleProvider.prototype.getStep = function (objDate, func) {
        console.log('step');
        var stepOptions = {
            'startDate': objDate.startDate,
            'endDate': objDate.endDate,
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierStepCount',
            'unit': 'count'
        };
        this.healthKit.querySampleTypeAggregated(stepOptions).then(function (data) {
            func(data);
        }, function (err) {
            console.log('No steps: ', err);
            func(err);
        });
    };
    AppleProvider.prototype.getActivity = function (func) {
        var _this = this;
        var me = this;
        me.healthKit.available().then(function (available) {
            if (available) {
                var options = {
                    readTypes: [
                        'HKQuantityTypeIdentifierStepCount',
                        'HKWorkoutTypeIdentifier',
                        'HKQuantityTypeIdentifierActiveEnergyBurned',
                        'HKQuantityTypeIdentifierDistanceCycling',
                        'HKQuantityTypeIdentifierHeartRate',
                        'HKQuantityTypeIdentifierDistanceWalkingRunning'
                    ]
                };
                me.healthKit.requestAuthorization(options).then(function (_) {
                    console.log('allow permisstion');
                    _this.healthKit.findWorkouts().then(function (x) {
                        console.log('workout ==> ', x, ' !!!!');
                        func(x);
                    }, function (err) {
                        console.log(err);
                        func(err);
                    });
                });
            }
            else {
                console.log('ios can not access health function');
            }
        });
    };
    AppleProvider.prototype.getHeartRate = function (start, end, func) {
        var options = {
            startDate: start,
            endDate: end,
            sampleType: 'HKQuantityTypeIdentifierHeartRate',
            unit: 'count/min',
            limit: 3000
        };
        this.healthKit.querySampleType(options).then(function (data) {
            console.log(data);
            console.log(data.length);
            func(data);
        }, function (err) {
            console.log('No Heartrate Data: ', err);
            func(err);
        });
    };
    AppleProvider.prototype.mapActivityHR = function (objActivity, lsWorkout, func) {
        var me = this;
        if (objActivity.length > 0) {
            var obj = objActivity.shift();
            me.getHeartRate(new Date(obj.startDate), new Date(obj.endDate), function (objHR) {
                console.log('obj HR ', objHR);
                obj["lsHR"] = objHR;
                lsWorkout.push(obj);
                me.mapActivityHR(objActivity, lsWorkout, func);
            });
        }
        else {
            func(lsWorkout);
        }
    };
    AppleProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_health_kit__["a" /* HealthKit */],
            __WEBPACK_IMPORTED_MODULE_5__config_config__["a" /* ConfigProvider */]])
    ], AppleProvider);
    return AppleProvider;
}());

//# sourceMappingURL=apple.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverSettingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopoverSettingComponent = /** @class */ (function () {
    function PopoverSettingComponent(navCtrl, viewCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
    }
    PopoverSettingComponent.prototype.btnEditProfile = function () {
        this.viewCtrl.dismiss("editProfile");
    };
    PopoverSettingComponent.prototype.btnDeviceSetting = function () {
        this.viewCtrl.dismiss("deviceSync");
    };
    PopoverSettingComponent.prototype.btnTermsAndCondition = function () {
        this.viewCtrl.dismiss("termAndCondition");
    };
    PopoverSettingComponent.prototype.btnLogout = function () {
        this.viewCtrl.dismiss("logout");
    };
    PopoverSettingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "popover-setting",template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/components/popover-setting/popover-setting.html"*/'<ion-list no-lines>\n  <!-- <ion-item (click)="btnEditProfile()">\n    <div class="text-right">Profile Setting</div>\n  </ion-item> -->\n  <div class="line-seperate"></div>\n  <ion-item (click)="btnDeviceSetting()">\n    <div class="text-right">Device Setting</div>\n  </ion-item>\n  <!-- <div class="line-seperate"></div>\n  <ion-item (click)="btnTermsAndCondition()">\n    <div class="text-right">Terms & Condition</div>\n  </ion-item> -->\n  <div class="line-seperate"></div>\n  <ion-item (click)="btnLogout()">\n    <div class="text-right">Log out</div>\n  </ion-item>\n</ion-list>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/components/popover-setting/popover-setting.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], PopoverSettingComponent);
    return PopoverSettingComponent;
}());

//# sourceMappingURL=popover-setting.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThisWeekProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ThisWeekProvider = /** @class */ (function () {
    function ThisWeekProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello ThisWeekProvider Provider');
    }
    ThisWeekProvider.prototype.getDataInitial = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getDataInitial', { search: null })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ThisWeekProvider.prototype.getDateRemain = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getDateRemain', { search: null })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ThisWeekProvider.prototype.checkTokenExpiry = function (brand) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('brand', brand);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'checkToken', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ThisWeekProvider.prototype.getSyncData = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getSyncData', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ThisWeekProvider.prototype.syncData = function (brand) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('brand', brand);
        params.set('version', this.config.appVersion);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'SyncData', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ThisWeekProvider.prototype.getLastSync = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getLastSync', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ThisWeekProvider.prototype.updateToken = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('tokenDevice', this.config.tokenDevice);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.post(_this.config.serverAPI + 'updateToken', params)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ThisWeekProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], ThisWeekProvider);
    return ThisWeekProvider;
}());

//# sourceMappingURL=this-week.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { ConfigProvider } from '../config/config';
var HealthProvider = /** @class */ (function () {
    function HealthProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello HealthProvider');
    }
    HealthProvider.prototype.getHealthDateScore = function (userId) {
        var _this = this;
        var postParams = { 'user_id': userId };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            apikey: 'l77b2059164ab14c539a214d56647b7822'
        });
        this.URLAPI = this.config.serverGolang + 'healthScore';
        //this.URLAPI = "http://localhost:33100/api/healthscore";
        return new Promise(function (resolve) {
            _this.http.post(_this.URLAPI, JSON.stringify(postParams), {
                headers: headers
            })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('getHealthDate error ', err);
                resolve(null);
            });
        });
    };
    HealthProvider.prototype.getHealthDate = function (userId) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            apikey: 'l77b2059164ab14c539a214d56647b7822'
        });
        var postParams = { 'user_id': userId };
        this.URLAPI = this.config.serverGolang + 'gethealthData';
        //this.URLAPI = "http://10.31.11.126:33100/api/getHealthData";
        //this.URLAPI = "http://localhost:33100/api/getHealthData";
        return new Promise(function (resolve) {
            _this.http.post(_this.URLAPI, JSON.stringify(postParams), {
                headers: headers
            })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('getHealthDate error ', err);
                resolve(null);
            });
        });
    };
    HealthProvider.prototype.getHealthIndex = function (userId) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            apikey: 'l77b2059164ab14c539a214d56647b7822'
        });
        var postParams = { 'user_id': userId };
        this.URLAPI = this.config.serverGolang + 'getHealthIndex';
        //this.URLAPI = "http://10.31.11.126:33100/api/getHealthIndex";
        //this.URLAPI = "http://localhost:33100/api/getHealthIndex";
        return new Promise(function (resolve) {
            _this.http.post(_this.URLAPI, JSON.stringify(postParams), { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('getHealthDate error ', err);
                resolve(null);
            });
        });
    };
    HealthProvider.prototype.insertHealthData = function (userId, height, weight, bloodPressure1, bloodPressure2, cholesterol, HDL, bloodSugar) {
        var _this = this;
        var data = { 'user_id': userId,
            'health_data_list': [
                //   {
                //     "type_name": "Steps",
                //     "value1": "500"
                // },
                // {
                //     "type_name": "Exercises",
                //     "value1": "30"
                // },
                {
                    "type_name": "Blood Pressure",
                    "value1": bloodPressure1,
                    "value2": bloodPressure2
                },
                {
                    "type_name": "BMI",
                    "value1": "20"
                },
                {
                    "type_name": "Cholesterol",
                    "value1": cholesterol
                },
                {
                    "type_name": "Blood Sugar",
                    "value1": bloodSugar
                },
                {
                    "type_name": "Height",
                    "value1": height
                },
                {
                    "type_name": "Weight",
                    "value1": weight
                },
                {
                    "type_name": "HDL",
                    "value1": HDL
                }
            ] };
        //obj.push('type_name':)
        //let postParams = {'user_id':userId};
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            apikey: 'l77b2059164ab14c539a214d56647b7822'
        });
        this.URLAPI = this.config.serverGolang + 'insert/healthData';
        //this.URLAPI = "http://10.31.11.126:33100/api/insert/healthData";
        //this.URLAPI = "http://localhost:33100/api/insert/healthData";
        return new Promise(function (resolve) {
            _this.http.post(_this.URLAPI, JSON.stringify(data), { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('getHealthDate error ', err);
                resolve(null);
            });
        });
    };
    HealthProvider.prototype.insertHealthUserProfile = function (userId, mydate) {
        var _this = this;
        var data = {
            "user_id": userId,
            "date_of_birth": mydate
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            apikey: 'l77b2059164ab14c539a214d56647b7822'
        });
        //obj.push('type_name':)
        //let postParams = {'user_id':userId};
        this.URLAPI = this.config.serverGolang + 'insert/healthUserprofile';
        //this.URLAPI = "http://10.31.11.126:33100/api/insert/healthUserProfile";
        //this.URLAPI = "http://localhost:33100/api/insert/healthUserProfile";
        return new Promise(function (resolve) {
            _this.http.post(_this.URLAPI, JSON.stringify(data), { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('getHealthDate error ', err);
                resolve(null);
            });
        });
    };
    HealthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], HealthProvider);
    return HealthProvider;
}());

//# sourceMappingURL=health.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ForgotPasswordPage = /** @class */ (function () {
    function ForgotPasswordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ForgotPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotPasswordPage');
    };
    ForgotPasswordPage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__login_login__["a" /* LoginPage */], {}, { animate: true, direction: 'back' });
    };
    ForgotPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-forgot-password',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/forgot-password/forgot-password.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Forgot Password</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  Wating for design ui/ux\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/forgot-password/forgot-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */]])
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());

//# sourceMappingURL=forgot-password.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginProvider = /** @class */ (function () {
    function LoginProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello LoginProvider Provider');
    }
    LoginProvider.prototype.login = function (userNo, password) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', userNo);
        params.set('password', password);
        params.set('tokenDevice', this.config.tokenDevice);
        return new Promise(function (resolve) {
            _this.http.post(_this.config.serverAPI + "loginApp", params)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    LoginProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], LoginProvider);
    return LoginProvider;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 184:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 184;

/***/ }),

/***/ 229:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 229;

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__this_week_this_week__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__history_history__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__leader_board_leader_board__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__weekly_achivement_weekly_achivement__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular_navigation_nav_params__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_popover_setting_popover_setting__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__device_sync_device_sync__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__challenge_dashboard_challenge_dashboard__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__device_connected_device_connected__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__health_health__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__notification_notification__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__profile_setting_profile_setting__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_tabs_tabs__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_friends_friends__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




















var TabsPage = /** @class */ (function () {
    function TabsPage(friend, tabsProvider, config, navCtrl, navParams, nav, settings, popoverCtrl, alertCtrl, events, zone) {
        var _this = this;
        this.friend = friend;
        this.tabsProvider = tabsProvider;
        this.config = config;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nav = nav;
        this.settings = settings;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.zone = zone;
        this.rootThisWeekPage = __WEBPACK_IMPORTED_MODULE_2__this_week_this_week__["a" /* ThisWeekPage */];
        this.rooteHistoryPage = __WEBPACK_IMPORTED_MODULE_3__history_history__["a" /* HistoryPage */];
        this.rootLeaderBoardPage = __WEBPACK_IMPORTED_MODULE_4__leader_board_leader_board__["a" /* LeaderBoardPage */];
        this.rootChallengeDashboardPage = __WEBPACK_IMPORTED_MODULE_11__challenge_dashboard_challenge_dashboard__["a" /* ChallengeDashboardPage */];
        this.rootHealthPage = __WEBPACK_IMPORTED_MODULE_13__health_health__["a" /* HealthPage */];
        this.tabIndex = 1;
        this.userImg = '';
        this.showNoti = false;
        this.notificationMessages = 0;
        // this.settings.tabIndex=3;//For Dev
        var me = this;
        //TODO: subscribe event from app.component, assume as push from FCM events
        events.subscribe("notification:changes", function () {
            // alert('subscribe received.');
            // if(this.showNoti){
            //   this.showNoti = false;
            //   this.notificationMessages=0;
            // }else{
            //   this.showNoti = true;
            //   this.notificationMessages=9;
            // }
            // this.zone.run(() => {});
            _this.zone.run(function () {
                console.log('notification total ' + me.config.totalNotification);
                if (me.config.totalNotification > 0) {
                    me.showNoti = true;
                }
                else {
                    me.showNoti = false;
                }
                me.notificationMessages = me.config.totalNotification;
            });
        });
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        var me = this;
        console.log('tabs page');
        me.friend.getFriendRequestMe().then(function (data) {
            console.log('tab friend request ', data.length);
            me.config.objFriendReques = data;
            if (data.length > 0) {
                me.showNoti = true;
            }
            else {
                me.showNoti = false;
            }
            me.notificationMessages = data.length;
        });
        console.log('chk  xxxx', this.settings.tabIndex);
        this.mainTabs.select(this.settings.tabIndex);
        this.tabsProvider.getProfile().then(function (data) {
            console.log('@@@@@@@ ', data);
            me.userImg = data[0].userImg;
        });
    };
    TabsPage.prototype.goWeeklyAchievement = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__weekly_achivement_weekly_achivement__["a" /* WeeklyAchivementPage */], {}, { animate: true, direction: 'forward' });
    };
    TabsPage.prototype.goNotificationView = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_15__notification_notification__["a" /* NotificationPage */], {}, { animate: true, direction: 'forward' });
    };
    TabsPage.prototype.goProfileSettingView = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_16__profile_setting_profile_setting__["a" /* ProfileSettingPage */], {}, { animate: true, direction: 'forward' });
    };
    TabsPage.prototype.onTabSelect = function (index) {
        console.log('index ', index);
        this.settings.tabIndex = index;
    };
    TabsPage.prototype.presentPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_8__components_popover_setting_popover_setting__["a" /* PopoverSettingComponent */]);
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (data) {
            console.log(data, '  config ', _this.config.objProfile);
            if (data == 'deviceSync') {
                //check device sync? goto deviceConnected : DeviceSync
                if (_this.config.deviceConnected == true) {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__device_connected_device_connected__["a" /* DeviceConnectedPage */], {}, { animate: true, direction: 'forward' });
                }
                else {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__device_sync_device_sync__["a" /* DeviceSyncPage */], {}, { animate: true, direction: 'forward' });
                }
            }
            else if (data == 'logout') {
                var confirm_1 = _this.alertCtrl.create({
                    title: 'Logout System',
                    buttons: [
                        {
                            text: 'No'
                        },
                        {
                            text: 'Yes',
                            handler: function () {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__login_login__["a" /* LoginPage */], {}, { animate: true, direction: 'forward' });
                            }
                        }
                    ]
                });
                confirm_1.present();
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('mainTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Tabs */])
    ], TabsPage.prototype, "mainTabs", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/tabs/tabs.html"*/'<ion-header>\n\n  <ion-navbar color="pink">\n\n    <ion-title>\n      <ion-row class="center-left" no-margin no-padding class="iconMarginTop">\n        <ion-col no-margin no-padding>\n          <div class="titleAppName">MTL Health\n          </div>\n        </ion-col>\n\n        <ion-col col-2 class="center-right" col-4 no-margin no-padding>\n          <div class="imageProfile" (click)="goNotificationView()">\n            <div class="positionNumber" *ngIf="showNoti">\n              <div class="amountPushMessage">{{notificationMessages}}</div>\n            </div>\n            <div class="redDotPushStatus" *ngIf="showNoti"></div>\n            <img src="assets/imgs/icons/notification.png" class="buttonReward">\n          </div>\n\n          <!-- <img src="assets/imgs/icons/icon_setting.png" class="buttonSetting" (click)="presentPopover($event)"> -->\n\n          <div class="center-center">\n              <div class="avatarWidth" (click)="goProfileSettingView()">\n                  <img src={{userImg}} />\n              </div>\n            </div>\n              \n          <!-- <img src="assets/imgs/icons/icon_setting.png" class="buttonSetting" (click)="goProfileSettingView()"> -->\n        </ion-col>\n\n      </ion-row>\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n<ion-content>\n  <ion-tabs tabbarPlacement="top" #mainTabs>\n    <ion-tab [root]="rootThisWeekPage" tabTitle="This Week" (ionSelect)="onTabSelect(0)"></ion-tab>\n    <ion-tab [root]="rooteHistoryPage" tabTitle="History" (ionSelect)="onTabSelect(1)"></ion-tab>\n    <ion-tab [root]="rootLeaderBoardPage" tabTitle="Leader" (ionSelect)="onTabSelect(2)"></ion-tab>\n    <ion-tab [root]="rootChallengeDashboardPage" tabTitle="Challenge" (ionSelect)="onTabSelect(3)"></ion-tab>\n    <ion-tab [root]="rootHealthPage" tabTitle="Health" (ionSelect)="onTabSelect(4)"></ion-tab>\n  </ion-tabs>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_18__providers_friends_friends__["a" /* FriendsProvider */],
            __WEBPACK_IMPORTED_MODULE_17__providers_tabs_tabs__["a" /* TabsProvider */],
            __WEBPACK_IMPORTED_MODULE_14__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_angular_navigation_nav_params__["a" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_settings_settings__["a" /* SettingsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThisWeekPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__congratulation_congratulation__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__congratulation_achievment_congratulation_achievment__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_device_sync_device_sync__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_this_week_this_week__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_device_sync_device_sync__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_apple_apple__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__excercise_detail_excercise_detail__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__how_to_challenges_work_how_to_challenges_work__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__weekly_achivement_weekly_achivement__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var ThisWeekPage = /** @class */ (function () {
    // objDebug: any;
    function ThisWeekPage(apple, SQLite, deviceSyncProvider, iab, loadingCtrl, zone, thisWeek, config, navCtrl, navParams, modalCtrl, nav, platform) {
        this.apple = apple;
        this.SQLite = SQLite;
        this.deviceSyncProvider = deviceSyncProvider;
        this.iab = iab;
        this.loadingCtrl = loadingCtrl;
        this.zone = zone;
        this.thisWeek = thisWeek;
        this.config = config;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.nav = nav;
        this.platform = platform;
        this.progress = 0;
        this.timeRemain = '--';
        this.stepDesc = '';
        this.stepGoal = '75,000';
        this.stepComplete = '0';
        this.stepLeft = '0';
        // currentRank: number = 0;
        this.totalRank = 0;
        this.exerciseDesc = '';
        this.exerciseGoal = '3 days';
        this.exerciseComplete = '';
        this.exerciseRemain = '';
        this.currentIndex = 0;
        this.iconSyncStatus = 'iconSync';
        this.version = this.config.appVersion;
        var me = this;
        console.log('Width: ' + platform.width());
        console.log('Height: ' + platform.height());
        this.cycle1 = 'c100 p0 color1';
        this.cycle2 = 'c100 p0 color1';
        this.cycle3 = 'c100 p0 color1';
        this.cycle1Value = '0%';
        this.cycle2Value = '0%';
        this.cycle3Value = '0%';
        this.cycle1Status = '';
        this.cycle2Status = '';
        this.cycle3Status = '';
        this.dateName1 = 'Mon';
        this.dateName2 = 'Tue';
        this.dateName3 = 'Wen';
        // if (platform.height() <= 700) {
        //   console.log('iphone5');
        //   this.fabStatus = false;
        // } else {
        //   console.log('iphone X');
        this.fabStatus = true;
        // }
        if (platform.width() <= 320) {
            this.positionTopRightCorner = 'top-right-corner-small';
        }
        else {
            this.positionTopRightCorner = 'top-right-corner-normal';
        }
        // me.thisWeek.getDateRemain().then((data: any) => {
        //   console.log(data);
        //   me.timeRemain = data == 0 ? 'last ' : data;
        // })
        var loader = me.loadingCtrl.create({
            spinner: 'crescent'
        });
        loader.present();
        me.thisWeek.getDataInitial().then(function (data) {
            loader.dismiss();
            console.log('@@@@@ ', data);
            me.timeRemain = data.dateRemain; //== 1 ? 'last ' : data.dateRemain;
            // me.exerciseDesc = data.exerciseDesc;
            // me.stepDesc = data.stepDesc;
        });
    }
    // testData: any = new Array(20);
    ThisWeekPage.prototype.ionViewDidLoad = function () {
        // this.cycle4 = 'c100 p0 color1';
        // this.cycle5 = 'c100 p0 color1';
        // this.cycle6 = 'c100 p0 color1';
        // this.cycle7 = 'c100 p0 color1';
        // this.cycle4Value = '30m';
        // this.cycle5Value = '30m';
        // this.cycle6Value = '30m';
        // this.cycle7Value = '30m';
        console.log('ionViewDidLoad ThisWeekPage');
        var me = this;
        if (me.config.tokenDevice != '') {
            me.thisWeek.updateToken().then(function (data) {
                console.log('update token', data);
            });
        }
        var sql = "SELECT * FROM tb_config";
        me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result) {
            if (result != null && result.rows.length > 0) {
                console.log('get userNo from db ', result.rows.item(0).user_no);
                me.config.userNo = result.rows.item(0).user_no;
                console.log('this.config.objThisWeek ', me.config.objThisWeek != undefined, '=>', me.config.objThisWeek);
                //clear graph
                me.progress = 0;
                me.updateStepProgress();
                // me.updateExerciseProgress(0, 0, 0);
                if (me.config.objThisWeek != undefined) {
                    me.bindData(me.config.objThisWeek);
                }
                else {
                    me.exerciseGoal = '3 days';
                    me.exerciseComplete = '0 day';
                    me.exerciseRemain = '0 day';
                    me.zone.run(function () { });
                }
                me.config.deviceConnected = me.checkDeviceConnected();
                console.log('device status ', me.config.deviceConnected);
                if (me.config.deviceConnected == true && me.config.objThisWeek == undefined) {
                    //data last sync
                    console.log('load last sync', me.config.objThisWeek);
                    var loader_1 = me.loadingCtrl.create({
                        spinner: 'crescent'
                    });
                    loader_1.present();
                    me.thisWeek.getSyncData().then(function (data) {
                        loader_1.dismiss();
                        console.log('### ', data);
                        me.config.objThisWeek = data;
                        var _day = data.dateNo - 1;
                        me.currentIndex = _day > 4 ? 4 : _day;
                        console.log('current Index ', me.currentIndex, ' ', _day);
                        me.bindData(data);
                    });
                    // me.thisWeek.getLastSync().then((data: any) => {
                    //   loader.dismiss();
                    //   console.log('last sync data ', data)
                    //   if (data.stepGoal > 0) {
                    //     me.bindData(data);
                    //   } else {
                    //     me.progress = 0;
                    //     me.updateStepProgress();
                    //     // me.updateExerciseProgress(0, 0, 0);
                    //     me.exerciseGoal = '3 day';
                    //     me.exerciseComplete = '0 day';
                    //     me.exerciseRemain = '0 day';
                    //     me.zone.run(() => { });
                    //   }
                    // })
                }
            }
        });
    };
    ThisWeekPage.prototype.updateStepProgress = function () {
        if (this.progress > 0) {
            this.progressCss = 'progressInner';
        }
        else {
            this.progressCss = 'progressInnerZero';
        }
    };
    ThisWeekPage.prototype.updateExerciseProgress = function (exerciseComplete, exerciseMinute, exerciseProgress) {
        var _this = this;
        console.log('xxx ', exerciseComplete);
        if (exerciseComplete > 0) {
            this.zone.run(function () {
                for (var i = 1; i <= exerciseComplete; i++) {
                    console.log('i ', i);
                    if (i == 1) {
                        _this.cycle1 = 'c100 p100 color1';
                        _this.cycle1Value = '30m';
                        _this.cycle1Status = 'assets/imgs/icons/checkbox_oval.png';
                        if (exerciseMinute > 0 && exerciseMinute < 30) {
                            _this.cycle2 = 'c100 p' + exerciseProgress + ' color1';
                            _this.cycle2Value = exerciseMinute + 'm';
                        }
                    }
                    else if (i == 2) {
                        _this.cycle1 = 'c100 p100 color1';
                        _this.cycle1Value = '30m';
                        _this.cycle1Status = 'assets/imgs/icons/checkbox_oval.png';
                        _this.cycle2 = 'c100 p100 color1';
                        _this.cycle2Value = '30m';
                        _this.cycle2Status = 'assets/imgs/icons/checkbox_oval.png';
                        if (exerciseMinute > 0 && exerciseMinute < 30) {
                            _this.cycle3 = 'c100 p' + exerciseProgress + ' color1';
                            _this.cycle3Value = exerciseMinute + 'm';
                        }
                    }
                    else {
                        _this.cycle1 = 'c100 p100 color1';
                        _this.cycle1Value = '30m';
                        _this.cycle1Status = 'assets/imgs/icons/checkbox_oval.png';
                        _this.cycle2 = 'c100 p100 color1';
                        _this.cycle2Value = '30m';
                        _this.cycle2Status = 'assets/imgs/icons/checkbox_oval.png';
                        _this.cycle3 = 'c100 p100 color1';
                        _this.cycle3Value = '30m';
                        _this.cycle3Status = 'assets/imgs/icons/checkbox_oval.png';
                    }
                }
            });
        }
        else {
            if (exerciseMinute > 0) {
                this.cycle1 = 'c100 p' + exerciseProgress + ' color1';
                this.cycle1Value = exerciseMinute + 'm';
            }
            else {
                this.cycle1 = 'c100 p0 color1';
                this.cycle2 = 'c100 p0 color1';
                this.cycle3 = 'c100 p0 color1';
                this.cycle1Value = '0m';
                this.cycle2Value = '0m';
                this.cycle3Value = '0m';
                this.cycle1Status = '';
                this.cycle2Status = '';
                this.cycle3Status = '';
            }
        }
    };
    ThisWeekPage.prototype.swipe = function (e) {
        console.log(e.direction);
        if (e.direction == 2) {
            this.nav.parent.select(1);
        }
    };
    ThisWeekPage.prototype.checkDeviceConnected = function () {
        console.log('check device connected ', this.config.objProfile);
        var status = false;
        if (this.config.objProfile == undefined) {
            console.log('not connect');
            status = false;
        }
        else if (this.config.objProfile.rows.item(0).brand == null || this.config.objProfile.rows.item(0).brand == '') {
            console.log('not connect');
            status = false;
        }
        else {
            console.log('connected');
            status = true;
        }
        return status;
    };
    ThisWeekPage.prototype.bindData = function (data) {
        console.log('bind data');
        this.lastSync = data.lastSync;
        this.progress = data.stepProgress;
        this.updateStepProgress();
        this.exerciseGoal = data.exerciseGoal + ' days';
        // this.exerciseDesc = data.exerciseDesc;
        this.exerciseComplete = parseInt(data.exerciseComplete) > 1 ? data.exerciseComplete + ' days' : data.exerciseComplete + ' day';
        this.exerciseRemain = parseInt(data.exerciseRemain) > 1 ? data.exerciseRemain + ' days' : data.exerciseRemain + ' day';
        // this.updateExerciseProgress(data.exerciseComplete, data.exerciseMinute, data.exerciseProgress);
        this.updateExerciseProgressPercent(data.lsExercise);
        this.stepGoal = data.stepGoalDisplay;
        // this.stepDesc = data.stepDesc;
        this.stepComplete = data.stepCompleteDisplay;
        this.stepLeft = data.stepRemainDisplay;
        // this.currentRank = data.currentRank;
        this.totalRank = data.totalRank;
        this.config.currentLevel = data.levelNo;
        this.zone.run(function () { });
    };
    ThisWeekPage.prototype.checkTokenExpiry = function (brand, func) {
        var me = this;
        me.thisWeek.checkTokenExpiry(brand).then(function (status) {
            console.log('token status ', status == '' ? 'good' : status);
            if (status != '') {
                var browser_1 = me.iab.create(status, "_blank", "location=no");
                browser_1.on('loadstop').subscribe(function (event) {
                    console.log('event ', event.url);
                    var currentURL = new URL(event.url);
                    console.log('url ', currentURL);
                    var code = currentURL.searchParams.getAll('code');
                    console.log('code ', code, ' ', code.length);
                    if (code.length > 0) {
                        me.deviceSyncProvider.registerUser(brand, code).then(function (result) {
                            console.log('register result ', result);
                            browser_1.close();
                            func();
                        });
                    }
                });
            }
            else {
                func();
            }
        });
    };
    ThisWeekPage.prototype.chkSyncSDK = function (brand, func) {
        var me = this;
        if (brand == 'Apple') {
            var lsWorkout_1 = [];
            me.apple.getSyncDate('Apple', 'step').then(function (objSyncDate) {
                console.log('start date ', objSyncDate.startDate, ' end date ', objSyncDate.endDate);
                var start = new Date(objSyncDate.startDate);
                start.setHours(0, 0, 0, 0);
                console.log('check start date ', start);
                var end = new Date(objSyncDate.endDate);
                end.setHours(23, 59, 59, 0);
                console.log('check end date ', end);
                var objDate = {
                    startDate: start,
                    endDate: end
                };
                console.log('obj date ', objDate);
                me.apple.getStep(objDate, function (objStep) {
                    console.log('lsStep ', objStep);
                    // me.objDebug = 'step ' + JSON.stringify(objStep);
                    me.apple.getActivity(function (obj) {
                        if (obj === void 0) { obj = []; }
                        var objWorkOut = obj.filter(function (data) { return new Date(data.startDate) >= start; });
                        console.log('filter workout ', objWorkOut);
                        // me.objDebug = me.objDebug + ' workout ' + JSON.stringify(objWorkOut);
                        me.apple.mapActivityHR(objWorkOut, lsWorkout_1, function (objWorkout) {
                            console.log('workout & HR ---------> ', objWorkout);
                            me.apple.syncAppleRawData(objStep, objWorkout).then(function (result) {
                                console.log('xxxxx ', result);
                                // me.objDebug = me.objDebug + ' sync raw data ' + result;
                                func(1);
                            });
                        });
                    });
                });
            });
        }
        else {
            func(0);
        }
    };
    ThisWeekPage.prototype.syncData = function (type, fab) {
        console.log('sync ', type);
        console.log(type);
        console.log(fab);
        var me = this;
        if (type == 'sync') {
            console.log('sync ..... ');
            fab != undefined ? fab.close() : '';
            console.log('config in this week page ', me.config.objProfile);
            //check device connected?
            if (me.config.deviceConnected == true) {
                console.log('get value from db');
                var userNo = me.config.objProfile.rows.item(0).user_no;
                var brand_1 = me.config.objProfile.rows.item(0).brand;
                console.log(userNo, '  ', brand_1);
                //if brand == Garmin check have token register ? go to call sync webservice call device connect
                //check token expiry *** Garmin token never expiry
                // me.checkTokenExpiry(brand, function () {
                //call webservice sync
                var loader_2 = me.loadingCtrl.create({
                    spinner: 'dots',
                    content: 'Syncing your data...'
                });
                // Start animation sync icon
                me.iconSyncStatus = 'iconSyncing';
                loader_2.present();
                //check brand
                me.chkSyncSDK(brand_1, function (x) {
                    me.thisWeek.syncData(brand_1).then(function (data) {
                        loader_2.dismiss();
                        // Stop animation sync icon
                        me.iconSyncStatus = 'iconSync';
                        console.log('@@@ ', data);
                        // let data={stepAchivement:'50000',exerciseAchievement:'1',levelAchievement:'0',
                        // levelNo:'0',levelName:'',levelImage:'http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/level/monkey.png',levelInfo:'[{"info":"- 3 days exercise"},{"info":"- 50k steps and 2 days exercise"},{"info":"- 75k steps and 1 day exercise"}]',smilePoint:''};
                        if (data != null) {
                            me.config.refreshHistory = true;
                            me.config.refreshLeaderboard = true;
                            me.config.refreshChallengeDashboard = true;
                            me.config.objThisWeek = data;
                            var _day = data.dateNo - 1;
                            me.currentIndex = _day > 4 ? 4 : _day;
                            me.bindData(me.config.objThisWeek);
                            //open dialog step,activity,level                   
                            if ((data.stepAchivement != "0" && data.stepAchivement != null) || (data.exerciseAchievement != "0" && data.exerciseAchievement != null)) {
                                var stepModal = me.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__congratulation_congratulation__["a" /* CongratulationPage */], { achievedType: 'step', stepAchivement: data.stepAchivement });
                                var exerciseModal_1 = me.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__congratulation_congratulation__["a" /* CongratulationPage */], { achievedType: 'workout' });
                                var levelModal_1 = me.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__congratulation_achievment_congratulation_achievment__["a" /* CongratulationAchievmentPage */], { levelNo: data.levelNo, levelName: data.levelName, levelImage: data.levelImage, levelInfo: data.levelInfo, smilePoint: data.smilePoint });
                                if (data.stepAchivement != "0") {
                                    console.log('case 1');
                                    stepModal.present();
                                }
                                if (data.stepAchivement == "0" && data.exerciseAchievement != "0") {
                                    console.log('case 2');
                                    exerciseModal_1.present();
                                }
                                stepModal.onDidDismiss(function (typePopup) {
                                    console.log('type popup ', typePopup);
                                    if (data.exerciseAchievement != "0") {
                                        console.log('step case 1');
                                        exerciseModal_1.present();
                                    }
                                    else if (data.levelAchievement != '-1') {
                                        console.log('step case 2');
                                        levelModal_1.present();
                                    }
                                });
                                exerciseModal_1.onDidDismiss(function (typePopup) {
                                    console.log('type popup ', typePopup);
                                    if (data.levelAchievement != '-1') {
                                        console.log('workout case 1');
                                        levelModal_1.present();
                                    }
                                });
                            }
                        }
                    });
                });
                // });
            }
            else {
                console.log('new open app');
                var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_device_sync_device_sync__["a" /* DeviceSyncPage */], { achievedType: type });
                profileModal.present();
            }
        }
    };
    ThisWeekPage.prototype.updateExerciseProgressPercent = function (data) {
        console.log('==> ', data, ' #### ', this.currentIndex);
        var me = this;
        this.zone.run(function () {
            me.cycle1 = 'c100 p' + data[me.currentIndex].exerciseProgress + ' color1';
            me.cycle1Value = data[me.currentIndex].exerciseTotalPercent + '%';
            me.dateName1 = data[me.currentIndex].dateName;
            if (data[me.currentIndex].exerciseProgress == 100) {
                me.cycle1Status = 'assets/imgs/icons/checkbox_oval.png';
            }
            else {
                me.cycle1Status = '';
            }
            me.cycle2 = 'c100 p' + data[me.currentIndex + 1].exerciseProgress + ' color1';
            me.cycle2Value = data[me.currentIndex + 1].exerciseTotalPercent + '%';
            me.dateName2 = data[me.currentIndex + 1].dateName;
            if (data[me.currentIndex + 1].exerciseProgress == 100) {
                me.cycle2Status = 'assets/imgs/icons/checkbox_oval.png';
            }
            else {
                me.cycle2Status = '';
            }
            me.cycle3 = 'c100 p' + data[me.currentIndex + 2].exerciseProgress + ' color1';
            me.cycle3Value = data[me.currentIndex + 2].exerciseTotalPercent + '%';
            me.dateName3 = data[me.currentIndex + 2].dateName;
            if (data[me.currentIndex + 2].exerciseProgress == 100) {
                me.cycle3Status = 'assets/imgs/icons/checkbox_oval.png';
            }
            else {
                me.cycle3Status = '';
            }
        });
    };
    ThisWeekPage.prototype.getSyncData = function () {
        console.log('xxxx');
        var me = this;
        this.thisWeek.getSyncData().then(function (data) {
            console.log('data ', data);
            me.config.objThisWeek = data;
            me.updateExerciseProgressPercent(me.config.objThisWeek.lsExercise);
        });
    };
    ThisWeekPage.prototype.backward = function () {
        if (this.config.objThisWeek != undefined) {
            console.log('left ', this.currentIndex);
            if (this.currentIndex > 0)
                this.currentIndex = this.currentIndex - 1;
            console.log('@@ ', this.currentIndex);
            this.updateExerciseProgressPercent(this.config.objThisWeek.lsExercise);
        }
    };
    ThisWeekPage.prototype.forward = function () {
        if (this.config.objThisWeek != undefined) {
            console.log('right ', this.currentIndex);
            if (this.currentIndex < 4)
                this.currentIndex = this.currentIndex + 1;
            console.log('## ', this.currentIndex, ' ', this.config.objThisWeek);
            this.updateExerciseProgressPercent(this.config.objThisWeek.lsExercise);
        }
    };
    ThisWeekPage.prototype.getDetail = function (el) {
        if (this.config.objThisWeek != undefined) {
            console.log(el, this.currentIndex);
            console.log('detail ', this.config.objThisWeek.lsExercise[this.currentIndex + el]);
            var stepModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_11__excercise_detail_excercise_detail__["a" /* ExcerciseDetailPage */], { objData: this.config.objThisWeek.lsExercise[this.currentIndex + el] });
            stepModal.present();
        }
    };
    ThisWeekPage.prototype.goHowToChallengesWorkPageView = function () {
        // console.log('go goHowToChallengesWorkPageView');
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_12__how_to_challenges_work_how_to_challenges_work__["a" /* HowToChallengesWorkPage */], {});
        profileModal.present();
    };
    ThisWeekPage.prototype.getAchievement = function () {
        console.log(' achievement');
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_13__weekly_achivement_weekly_achivement__["a" /* WeeklyAchivementPage */], { sourcePage: 'thisWeek' });
        profileModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('pageTop'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], ThisWeekPage.prototype, "pageTop", void 0);
    ThisWeekPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-this-week',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/this-week/this-week.html"*/'<ion-toolbar class="topCurve">\n  <div class="center-center">\n    <ion-grid class="gridWidth" no-padding>\n      <ion-row>\n        <ion-col>\n          <div class="center-center">\n            <ion-grid no-margin no-padding>\n              <ion-row no-margin no-padding>\n                <ion-col col-3 no-padding no-margin class="center-center">\n                  <img src="assets/imgs/icons/stopwatch.png" class="iconWidth" />\n                </ion-col>\n                <ion-col no-padding no-margin>\n                  <ion-row no-margin no-padding>\n                    <ion-col no-margin no-padding>\n                      <div class="textZone">\n                        <div class="textCountdownTime">{{timeRemain}}</div>\n                        <div class="textTimeRemain">Time remaining</div>\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n        </ion-col>\n        <ion-col>\n          <div class="center-center" (click)="getAchievement()">\n            <ion-grid no-margin no-padding>\n              <ion-row no-margin no-padding>\n                <ion-col col-3 no-padding no-margin class="center-center">\n                  <img src="assets/imgs/icons/trophy.png" class="iconWidth" />\n                </ion-col>\n                <ion-col no-padding no-margin>\n                  <ion-row no-margin no-padding>\n                    <ion-col no-margin no-padding>\n                      <div class="textZone">\n                        <div class="textCountdownTime">\n                          Level {{this.config.currentLevel}}\n                        </div>\n                        <div class="textTimeRemain">Out of 5</div>\n                        <!-- <div class="textTimeRemain">Out of {{totalRank}}</div> -->\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-toolbar>\n<ion-content #pageTop class="mainContent" scrollable="true">\n  <div class="cardPadding">\n    <ion-card class="cardSpaceTop">\n      <ion-row no-padding no-margin>\n        <ion-col no-padding no-margin>\n          <div class="textWalkTitle">Steps</div>\n          <div class="textWalkDetail">Goal : 75k steps a week</div>\n        </ion-col>\n        <ion-col col-2 no-padding no-margin>\n          <div class="positionTopRightCorner">\n            <img src="assets/imgs/card-bg-blue.png" />\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row no-padding no-margin>\n        <ion-col no-padding no-margin>\n          <div class="barWidth">\n            <div class="progressOuter">\n              <div class="{{progressCss}}" [style.width]="progress + \'%\'"></div>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class="rowMarginBottom">\n        <ion-col>\n          <div class="textWalkTitle_Col1">\n            {{stepComplete}}\n          </div>\n          <div class="textWalkDetail_Col1">COMPLETED</div>\n        </ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col2">\n            {{stepLeft}}\n          </div>\n          <div class="textWalkDetail_Col2">LEFT</div>\n        </ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col3">\n            {{stepGoal}}\n          </div>\n          <div class="textWalkDetail_Col3">GOAL</div>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n\n    <ion-card class="cardMarginTop">\n      <ion-row no-padding no-margin>\n        <ion-col no-padding no-margin>\n          <div class="textWalkTitle">Exercise</div>\n          <div class="textWalkDetail">\n            Goal : Achieve 3 rings a week\n          </div>\n        </ion-col>\n        <ion-col col-2 no-padding no-margin>\n          <div class="positionTopRightCorner">\n            <img src="assets/imgs/card-bg-green.png" />\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row no-padding no-margin class="marginCircleChart">\n        <ion-col no-padding no-margin class="arrowIcon center-center" (click)="backward()">\n          <img src="assets/imgs/Left.png" class="iconSize" />\n        </ion-col>\n        <ion-col no-padding no-margin class="center-center" (click)="getDetail(0)">\n          <div class="{{cycle1}}">\n            <span>{{cycle1Value}}</span>\n            <div class="slice">\n              <div class="bar"></div>\n              <div class="fill"></div>\n            </div>\n          </div>\n        </ion-col>\n\n        <ion-col no-padding no-margin class="center-center" (click)="getDetail(1)">\n          <div class="{{cycle2}}">\n            <span>{{cycle2Value}}</span>\n            <div class="slice">\n              <div class="bar"></div>\n              <div class="fill"></div>\n            </div>\n          </div>\n        </ion-col>\n\n        <ion-col no-padding no-margin class="center-center" (click)="getDetail(2)">\n          <div class="{{cycle3}}">\n            <span>{{cycle3Value}}</span>\n            <div class="slice">\n              <div class="bar"></div>\n              <div class="fill"></div>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col no-padding no-margin class="arrowIcon center-center" (click)="forward()">\n          <img src="assets/imgs/Right.png" class="iconSize" />\n        </ion-col>\n      </ion-row>\n\n      <ion-row no-padding no-margin>\n        <ion-col style="max-width: 2em;"></ion-col>\n        <ion-col no-padding no-margin>\n          <div class="textDayStatus">{{cycle1Detail}}</div>\n        </ion-col>\n        <ion-col no-padding no-margin>\n          <div class="textDayStatus">{{cycle2Detail}}</div>\n        </ion-col>\n        <ion-col no-padding no-margin>\n          <div class="textDayStatus">{{cycle2Detail}}</div>\n        </ion-col>\n        <ion-col style="max-width: 2em;"></ion-col>\n      </ion-row>\n\n      <ion-row no-padding no-margin>\n        <ion-col style="max-width: 2em;"></ion-col>\n        <ion-col no-padding no-margin class="center-center">\n          <div class="checkedStatus">\n            <ion-img [src]="cycle1Status"></ion-img>\n          </div>\n        </ion-col>\n        <ion-col no-padding no-margin class="center-center">\n          <div class="checkedStatus">\n            <ion-img [src]="cycle2Status"></ion-img>\n          </div>\n        </ion-col>\n        <ion-col no-padding no-margin class="center-center">\n          <div class="checkedStatus">\n            <ion-img [src]="cycle3Status"></ion-img>\n          </div>\n        </ion-col>\n        <ion-col style="max-width: 2em;"></ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col style="max-width: 2em;"></ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col1">\n            {{dateName1}}\n          </div>\n        </ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col2">\n            {{dateName2}}\n          </div>\n        </ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col3">\n            {{dateName3}}\n          </div>\n        </ion-col>\n        <ion-col style="max-width: 2em;"></ion-col>\n      </ion-row>\n\n      <ion-row class="rowMarginBottom">\n        <ion-col style="max-width: 2em;"></ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col1">\n            {{exerciseComplete}}\n          </div>\n          <div class="textWalkDetail_Col1">COMPLETED</div>\n        </ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col2">\n            {{exerciseRemain}}\n          </div>\n          <div class="textWalkDetail_Col2">LEFT</div>\n        </ion-col>\n        <ion-col>\n          <div class="textWalkTitle_Col3">\n            {{exerciseGoal}}\n          </div>\n          <div class="textWalkDetail_Col3">GOAL</div>\n        </ion-col>\n        <ion-col style="max-width: 2em;"></ion-col>\n      </ion-row>\n\n      <!-- <ion-row no-padding no-margin class="marginCircleChart">\n\n                <ion-col no-padding no-margin class="center-center">\n\n                    <div class="{{cycle1}}">\n                        <span>{{cycle1Value}}</span>\n                        <div class="slice">\n                            <div class="bar"></div>\n                            <div class="fill"></div>\n                        </div>\n                    </div>\n\n                </ion-col>\n\n                <ion-col no-padding no-margin class="center-center">\n\n                    <div class="{{cycle2}}">\n                        <span>{{cycle2Value}}</span>\n                        <div class="slice">\n                            <div class="bar"></div>\n                            <div class="fill"></div>\n                        </div>\n                    </div>\n\n                </ion-col>\n\n                <ion-col no-padding no-margin class="center-center">\n\n                    <div class="{{cycle3}}">\n                        <span>{{cycle3Value}}</span>\n                        <div class="slice">\n                            <div class="bar"></div>\n                            <div class="fill"></div>\n                        </div>\n                    </div>\n\n                </ion-col>\n            </ion-row>\n\n\n            <ion-row no-padding no-margin>\n                <ion-col no-padding no-margin>\n                    <div class="textDayStatus">DAY 1</div>\n                </ion-col>\n                <ion-col no-padding no-margin>\n                    <div class="textDayStatus">DAY 2</div>\n                </ion-col>\n                <ion-col no-padding no-margin>\n                    <div class="textDayStatus">DAY 3</div>\n                </ion-col>\n            </ion-row>\n\n            <ion-row no-padding no-margin>\n                <ion-col no-padding no-margin class="center-center">\n                    <div class="checkedStatus">\n                        <img src="{{cycle1Status}}">\n                    </div>\n                </ion-col>\n                <ion-col no-padding no-margin class="center-center">\n                    <div class="checkedStatus">\n                        <img src="{{cycle2Status}}">\n                    </div>\n                </ion-col>\n                <ion-col no-padding no-margin class="center-center">\n                    <div class="checkedStatus">\n                        <img src="{{cycle3Status}}">\n                    </div>\n                </ion-col>\n            </ion-row>\n\n            <ion-row class="rowMarginBottom">\n                <ion-col>\n                    <div class="textWalkTitle_Col1">\n                        {{exerciseComplete}}\n                    </div>\n                    <div class="textWalkDetail_Col1">COMPLETED</div>\n                </ion-col>\n                <ion-col>\n                    <div class="textWalkTitle_Col2">\n                        {{exerciseRemain}}\n                    </div>\n                    <div class="textWalkDetail_Col2">LEFT</div>\n                </ion-col>\n                <ion-col>\n                    <div class="textWalkTitle_Col3">\n                        {{exerciseGoal}}\n                    </div>\n                    <div class="textWalkDetail_Col3">GOAL</div>\n                </ion-col>\n            </ion-row> -->\n    </ion-card>\n\n    <div class="" (click)="goHowToChallengesWorkPageView()">\n      <div class="viewDetails">View details</div>\n    </div>\n  </div>\n  <ion-fab right bottom *ngIf="fabStatus==false" #fab>\n    <button ion-fab color="pink" mini (click)="syncData(\'sync\',fab)">\n      <ion-icon name="sync"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-row no-padding no-margin class="syncZoneFooter">\n    <ion-col class="center-center" no-padding no-margin col-8>\n      <!-- <div style="text-align: center;">Version {{version}} Last Sync {{lastSync}}</div> -->\n      <div class="lastSynced">Last synced: {{lastSync}}</div>\n    </ion-col>\n    <ion-col class="center-center" no-padding no-margin col-4>\n      <div\n        class="syncButton center-center"\n        *ngIf="fabStatus==true"\n        (click)="syncData(\'sync\',fab)"\n      >\n        <img src="assets/imgs/icons/icon_sync.png" [ngClass]="iconSyncStatus" /> Sync\n      </div>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n      <ion-col class="center-center" no-padding no-margin>\n      <div class="lastSynced">Version {{version}}</div>\n    </ion-col>\n  </ion-row>\n\n  <!-- <div style="text-align: center;"></div> -->\n  <!-- <div>Debug : {{objDebug}}</div> -->\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/this-week/this-week.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__providers_apple_apple__["a" /* AppleProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_device_sync_device_sync__["a" /* DeviceSyncProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_6__providers_this_week_this_week__["a" /* ThisWeekProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */]])
    ], ThisWeekPage);
    return ThisWeekPage;
}());

//# sourceMappingURL=this-week.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CongratulationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CongratulationPage = /** @class */ (function () {
    function CongratulationPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.achievedType = this.navParams.get('achievedType');
        if (this.achievedType == 'step') {
            this.iconImage = 'icon_step.png';
            this.textAchieved = 'You achieved walking';
            this.textResult = 'over ' + this.navParams.get('stepAchivement') + ' steps.';
        }
        else {
            this.iconImage = 'icon_workout.png';
            this.textAchieved = 'You achieved working out';
            this.textResult = 'over 30 minutes per day.';
        }
    }
    CongratulationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CongratulationPage');
    };
    CongratulationPage.prototype.btnClose = function () {
        this.viewCtrl.dismiss(this.achievedType);
    };
    CongratulationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-congratulation',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/congratulation/congratulation.html"*/'<ion-content no-bounce (click)="btnClose()">\n    <div>\n        <div class="content-center">\n            <div class="logoImagePosition center-center">\n                <img src="assets/imgs/icons/{{iconImage}}" class="logoSize">\n            </div>\n            <div class="headerFont center-center">Congratulations!</div>\n            <div class="contentFont center-center">\n                {{textAchieved}}\n            </div>\n            <div class="contentFont center-center">\n                {{textResult}}\n            </div>\n        </div>\n        <div class="glow">\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/congratulation/congratulation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], CongratulationPage);
    return CongratulationPage;
}());

//# sourceMappingURL=congratulation.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CongratulationAchievmentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CongratulationAchievmentPage = /** @class */ (function () {
    function CongratulationAchievmentPage(zone, navCtrl, navParams, viewCtrl) {
        this.zone = zone;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        console.log(this.navParams.get('levelInfo'));
        console.log('json ', JSON.parse(this.navParams.get('levelInfo')));
        this.levelNo = this.navParams.get('levelNo');
        this.levelName = this.navParams.get('levelName');
        this.levelImage = this.navParams.get('levelImage');
        this.levelInfo = JSON.parse(this.navParams.get('levelInfo'));
        this.smilePoint = this.navParams.get('smilePoint');
        // this.zone.run(()=>{});
    }
    CongratulationAchievmentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CongratulationAchievmentPage');
    };
    CongratulationAchievmentPage.prototype.btnClose = function () {
        this.viewCtrl.dismiss();
    };
    CongratulationAchievmentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-congratulation-achievment',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/congratulation-achievment/congratulation-achievment.html"*/'<ion-content no-bounce (click)="btnClose()">\n\n  <div class="complexView">\n    <div class="vbox">\n      <div class="flex1 center-center">\n        <ion-card>\n          <ion-card-content>\n            <div class="center-center">\n              <img src="{{levelImage}}" class="imgAchievment">\n            </div>\n            <div class="center-center textAchievment">{{levelName}}</div>\n            <div class="center-center textContent">LEVEL {{levelNo}}</div>\n\n            <div class="modalBottom">\n              <div class="lineSeparateCurve"></div>\n              <div *ngFor="let rec of levelInfo">\n                <div class="center-center textContent">{{rec.info}}</div>\n              </div>\n              <div class="center-center textContent">data in a week</div>\n              <div class="center-center textPoint">\n                <img src="assets/imgs/icons/icon_gold_coin.png" class="imgGoldCoin">\n                + {{smilePoint}} Smile points\n              </div>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/congratulation-achievment/congratulation-achievment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], CongratulationAchievmentPage);
    return CongratulationAchievmentPage;
}());

//# sourceMappingURL=congratulation-achievment.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceConfirmConnectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_device_confirm_connect_device_confirm_connect__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apple_apple__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeviceConfirmConnectPage = /** @class */ (function () {
    function DeviceConfirmConnectPage(apple, dCCProvider, navCtrl, navParams, viewCtrl, loadingCtrl) {
        this.apple = apple;
        this.dCCProvider = dCCProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        var record = this.navParams.get('record');
        this.brandId = record.deviceNo;
        this.brandName = record.deviceBrand;
        this.brandDevicePhoto = record.deviceImage;
        this.barndDescription = record.deviceInfo;
    }
    DeviceConfirmConnectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DeviceConfirmConnectPage');
    };
    DeviceConfirmConnectPage.prototype.btnClose = function () {
        this.viewCtrl.dismiss('');
    };
    DeviceConfirmConnectPage.prototype.btnConnect = function (brandName) {
        console.log('connect:' + brandName);
        var me = this;
        if (brandName == 'Apple') {
            //get permission
            me.apple.checkPermission(function (status) {
                console.log('status permission ', status);
                me.viewCtrl.dismiss(status);
            });
        }
        else {
            me.dCCProvider.getLink(brandName).then(function (data) {
                console.log('xxx %%%%%', data);
                me.viewCtrl.dismiss(data);
            });
        }
    };
    DeviceConfirmConnectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-device-confirm-connect',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-confirm-connect/device-confirm-connect.html"*/'<ion-content no-bounce>\n\n  <div class="complexView">\n    <div class="vbox">\n      <div class="flex1 center-center">\n        <ion-card>\n          <ion-card-content>\n            <ion-row no-margin no-padding>\n              <ion-col no-margin no-padding>\n                <div (click)="btnClose()">\n                  <img src="assets/imgs/close-btn.png" class="closeButton">\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <div class="center-center">\n              <img src="{{brandDevicePhoto}}" class="imgAchievment">\n            </div>\n\n            <div class="center-center textAchievment">Getting started with {{brandName}}</div>\n\n            <div class="modalBottom">\n              <div class="lineSeparateCurve"></div>\n              <div class="center-center textContent">\n                {{barndDescription}}\n              </div>\n              <div class="center-center textPoint">\n                <div class="connectDeviceButton center-center" (click)="btnConnect(brandName)">\n                  <div>Connect with {{brandName}}</div>\n                </div>\n              </div>\n            </div>\n\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-confirm-connect/device-confirm-connect.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_apple_apple__["a" /* AppleProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_device_confirm_connect_device_confirm_connect__["a" /* DeviceConfirmConnectProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], DeviceConfirmConnectPage);
    return DeviceConfirmConnectPage;
}());

//# sourceMappingURL=device-confirm-connect.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceConfirmConnectProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeviceConfirmConnectProvider = /** @class */ (function () {
    function DeviceConfirmConnectProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello DeviceConfirmConnectProvider Provider');
    }
    DeviceConfirmConnectProvider.prototype.getLink = function (brand) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('brand', brand);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getLink', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log('link ', data);
                resolve(data);
            });
        });
    };
    DeviceConfirmConnectProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], DeviceConfirmConnectProvider);
    return DeviceConfirmConnectProvider;
}());

//# sourceMappingURL=device-confirm-connect.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceConfirmUnlinkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DeviceConfirmUnlinkPage = /** @class */ (function () {
    function DeviceConfirmUnlinkPage(navCtrl, navParams, viewCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        var record = this.navParams.get('record');
        this.brandId = record.deviceNo;
        this.brandName = record.deviceBrand;
        this.brandDevicePhoto = record.deviceImage;
        this.barndDescription = record.deviceInfo;
    }
    DeviceConfirmUnlinkPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DeviceConfirmUnlinkPage');
    };
    DeviceConfirmUnlinkPage.prototype.btnUnlink = function (data) {
        console.log('##### ', data);
        this.viewCtrl.dismiss(data);
        // this.viewCtrl.dismiss('unlink');
    };
    DeviceConfirmUnlinkPage.prototype.btnClose = function () {
        this.viewCtrl.dismiss('');
    };
    DeviceConfirmUnlinkPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-device-confirm-unlink',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-confirm-unlink/device-confirm-unlink.html"*/'<ion-content no-bounce>\n\n  <div class="complexView">\n    <div class="vbox">\n      <div class="flex1 center-center">\n        <ion-card>\n          <ion-card-content>\n            <ion-row no-margin no-padding>\n              <ion-col no-margin no-padding>\n                <div (click)="btnClose()">\n                  <img src="assets/imgs/close-btn.png" class="closeButton">\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <div class="center-center">\n              <img src="{{brandDevicePhoto}}" class="imgAchievment">\n            </div>\n\n            <div class="center-center textAchievment">Warning! for Unlink {{brandName}}</div>\n\n            <div class="modalBottom">\n              <div class="lineSeparateCurve"></div>\n              <div class="center-center textContent">\n                {{barndDescription}}\n              </div>\n              <div class="center-center textPoint">\n\n                <div class="connectDeviceButton center-center" (click)="btnUnlink(brandId)">\n                  <div>&nbsp;&nbsp;Unlink {{brandName}} Device</div>\n                </div>\n\n              </div>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-confirm-unlink/device-confirm-unlink.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], DeviceConfirmUnlinkPage);
    return DeviceConfirmUnlinkPage;
}());

//# sourceMappingURL=device-confirm-unlink.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PendingRequestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__friends_friends__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_friends_friends__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PendingRequestPage = /** @class */ (function () {
    function PendingRequestPage(friendProvider, navCtrl, navParams) {
        this.friendProvider = friendProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        console.log(this.navParams.data.data);
        if (this.navParams.data.data.length > 0)
            this.showCardData = this.navParams.data.data;
        // this.showCardData = [
        //   {
        //     rankNo: 1,
        //     total: 569,
        //     totalTxt: "569 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/89106_156.jpg",
        //     userName: "Elise",
        //     userNo: "89106",
        //   },
        //   {
        //     rankNo: 2,
        //     total: 570,
        //     totalTxt: "570 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/730394_63.jpg",
        //     userName: "Lisa",
        //     userNo: "730394",
        //   },
        //   {
        //     rankNo: 3,
        //     total: 571,
        //     totalTxt: "571 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/84742_734.jpg",
        //     userName: "Stephanie",
        //     userNo: "84742",
        //   },
        //   {
        //     rankNo: 4,
        //     total: 572,
        //     totalTxt: "572 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/82815_482.jpg",
        //     userName: "Stephanie",
        //     userNo: "82815",
        //   }
        // ]
    }
    PendingRequestPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PendingRequestPage');
    };
    PendingRequestPage.prototype.btnBack = function () {
        console.log('go btnBack');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__friends_friends__["a" /* FriendsPage */], {}, { animate: true, direction: 'back' });
    };
    PendingRequestPage.prototype.onAccept = function (el) {
        console.log(el, el.userNo);
        var me = this;
        this.friendProvider.addFriend(el.friendNo).then(function (data) {
            console.log('---- ', data);
            me.friendProvider.getFriendRequestMe().then(function (data) {
                me.showCardData = data;
            });
        });
    };
    PendingRequestPage.prototype.onReject = function (el) {
        console.log(el);
        var me = this;
        this.friendProvider.rejectFriendRequest(el.friendNo).then(function (data) {
            console.log('### ', data);
            me.friendProvider.getFriendRequestMe().then(function (data) {
                me.showCardData = data;
            });
        });
    };
    PendingRequestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pending-request',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/pending-request/pending-request.html"*/'<ion-header>\n    <ion-toolbar>\n      <ion-title>Pending Request</ion-title>\n      <ion-buttons left>\n        <button ion-button icon-only (click)="btnBack()">\n          <ion-icon name="arrow-back"></ion-icon>\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content class="mainContent" no-pading>\n      <ion-list *ngFor="let record of showCardData;let i= index">\n        <ion-item>\n          <ion-row no-padding no-margin>\n    \n            <ion-col no-padding no-margin col-3>\n              <div class="center-center">\n                <div class="avatarWidth">\n                  <img src="{{record.userImg}}">\n                </div>\n              </div>\n            </ion-col>\n    \n            <ion-col no-padding no-margin class="rightColumn">\n              <div class="yourFriendInvite">{{record.userName}}</div>\n              <div class="">sent you a friend request</div>\n              <div>\n                <span class="minutesAgo">{{record.requestTime}}</span>\n              </div>\n    \n              <ion-row>\n                <ion-col class="center-left">\n                  <div class="acceptButton center-center" (click)="onAccept(record)">Accept</div>\n                  <div class="rejectButton" (click)="onReject(record)">Reject</div>\n                </ion-col>\n              </ion-row>\n    \n              <!-- {{record.userName}} {{record.totalTxt}} {{record.user_status}} -->\n            </ion-col>\n          </ion-row>\n    \n          <div class="lineSeperate"></div>\n        </ion-item>\n      </ion-list>\n    </ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/pending-request/pending-request.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_friends_friends__["a" /* FriendsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], PendingRequestPage);
    return PendingRequestPage;
}());

//# sourceMappingURL=pending-request.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__friends_friends__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_friend_profile_friend_profile__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_friends_friends__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FriendProfilePage = /** @class */ (function () {
    function FriendProfilePage(friendProvider, friendProfileProvider, config, navCtrl, navParams, loadingCtrl, actionSheetController, alertCtrl) {
        // this.userImg = 'assets/imgs/avatar/avatar_04.png';
        this.friendProvider = friendProvider;
        this.friendProfileProvider = friendProfileProvider;
        this.config = config;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetController = actionSheetController;
        this.alertCtrl = alertCtrl;
        this.segmentSwitch1 = 'segment_button_active_history';
        this.segmentSwitch2 = 'segment_button_inactive_history';
        // this's the mockup status get it from back-end services
        //this.friendStatus = 'addFriend';
        console.log('type ', this.navParams.data.profileType);
        this.profileType = this.navParams.data.profileType;
        if (this.profileType == 'F') {
            this.friendStatus = 'unFriend';
        }
        else if (this.profileType == 'S') {
            this.friendStatus = 'requestFriend';
        }
        else {
            this.friendStatus = 'addFriend';
        }
        console.log('==> ', this.navParams.data.friendNo);
        this.frinedNo = this.navParams.data.friendNo;
        var me = this;
        this.friendProfileProvider.getFriendProfile(this.frinedNo).then(function (data) {
            console.log('profile ', data);
            console.log('img ', data.userImg);
            me.userImg = data.userImg;
            me.totalSmilePoint = data.totalSmilePoint;
        });
        //check params == friend? call profile 
        //button text un friend : add friend
    }
    FriendProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FriendProfilePage');
    };
    FriendProfilePage.prototype.btnBack = function () {
        console.log('go btnBack');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__friends_friends__["a" /* FriendsPage */], {}, { animate: true, direction: 'back' });
    };
    FriendProfilePage.prototype.goFriendsView = function (status) {
        var _this = this;
        if (status === 'addFriend') {
            var confirm_1 = this.alertCtrl.create({
                title: 'Confirm',
                message: "Un Friend",
                buttons: [
                    {
                        text: 'No'
                    },
                    {
                        text: 'Yes',
                        handler: function () {
                            var friendStatus = 'B';
                            var me = _this;
                            _this.friendProvider.updateFriendStatus(_this.frinedNo, friendStatus).then(function (data) {
                                me.friendStatus = status;
                                console.log(data);
                            });
                        }
                    }
                ]
            });
            confirm_1.present();
        }
        else {
            var confirm_2 = this.alertCtrl.create({
                title: 'Confirm',
                message: this.profileType == 'S' ? "Request Friend" : 'Add Friend',
                buttons: [
                    {
                        text: 'No'
                    },
                    {
                        text: 'Yes',
                        handler: function () {
                            var me = _this;
                            if (me.profileType == 'S') {
                                //request friend
                                console.log('request friend');
                                me.friendProvider.requestFriend(_this.frinedNo).then(function (data) {
                                    me.friendStatus = 'requestFriend';
                                });
                            }
                            else {
                                //add friend
                                console.log('add friend');
                                _this.friendProvider.addFriend(_this.frinedNo).then(function (data) {
                                    console.log(data);
                                    me.friendStatus = status;
                                });
                            }
                        }
                    }
                ]
            });
            confirm_2.present();
        }
    };
    FriendProfilePage.prototype.btnSegment = function (index) {
        console.log(index);
        if (index == 1) {
            this.segmentSwitch1 = 'segment_button_active_history';
            this.segmentSwitch2 = 'segment_button_inactive_history';
        }
        else if (index == 2) {
            this.segmentSwitch1 = 'segment_button_inactive_history';
            this.segmentSwitch2 = 'segment_button_active_history';
        }
    };
    FriendProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-friend-profile',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/friend-profile/friend-profile.html"*/'<ion-header>\n  <ion-toolbar>\n\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-header>\n\n<div class="fixedHeader topHeader">\n\n  <div class="imageHeader">\n    <div class="headerPadding">\n\n      <ion-row no-margin no-padding class="marginRow">\n\n        <ion-col col no-margin no-padding class="center-center">\n          <div class="imageProfile center-center">\n            <img src="{{userImg}}" class="avatarProfileWidth">\n          </div>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n        <ion-col no-margin no-padding class="center-center">\n\n          <div class="">\n\n            <div class="textTitle">\n              <span class="textPoint">Friend Profile View</span>\n            </div>\n\n            <div class="textPoint">\n              <span class="textPoint">{{totalSmilePoint}}</span>\n            </div>\n\n            <div class="textDetail">\n              <span class="textPoint">Total Smile Points</span>\n            </div>\n\n          </div>\n\n        </ion-col>\n      </ion-row>\n\n    </div>\n\n  </div>\n\n  <div class="topCurve">\n    <div class="center-center">\n      <div class="buttonAddFriends center-center" (click)="goFriendsView(\'unFriend\')" *ngIf="friendStatus == \'requestFriend\'">REQUEST BUDDY</div>\n      <div class="buttonAddFriends center-center" (click)="goFriendsView(\'addFriend\')" *ngIf="friendStatus == \'unFriend\'">UN BUDDY</div>\n      <div class="buttonAddFriends center-center" (click)="goFriendsView(\'unFriend\')" *ngIf="friendStatus == \'addFriend\'">ADD BUDDY</div>\n    </div>\n  </div>\n\n</div>\n\n<ion-content class="mainContent">\n\n  <div class="segmentCenter">\n    <div class="segmentBg">\n      <ion-grid no-padding no-margin>\n\n        <ion-row no-padding no-margin>\n          <ion-col no-padding no-margin (click)="btnSegment(1)">\n            <div [ngClass]="segmentSwitch1" class="center-center">\n              <div class="marginSegmentTopHistory">Challenges</div>\n            </div>\n          </ion-col>\n          <ion-col no-padding no-margin (click)="btnSegment(2)">\n            <div [ngClass]="segmentSwitch2" class="center-center">\n              <div class="marginSegmentTopHistory">Achievements</div>\n            </div>\n          </ion-col>\n        </ion-row>\n\n      </ion-grid>\n    </div>\n  </div>\n\n  <!-- <div class="cardContentPadding">\n\n    <div class="cardSeparate">\n      <div class="textCardTitle">Current Challenges</div>\n      <ion-card class="cardPaddingChallenge">\n\n        <ion-row class="rowMarin">\n          <ion-col col-4 col no-margin no-padding class="center-center">\n            <div class="imageChallenge center-center">\n              <img src="assets/imgs/profile/profile_photo_01.png" class="">\n            </div>\n          </ion-col>\n  \n          <ion-col col-8 no-margin no-padding class="center-left">\n            <div class="marginLeft">\n              <div class="titleHeaderInCard">MTL Marathon 2019</div>\n              <div class="titleContentInCard">July 2, 2019</div>\n              <div class="titleContentInCard">5,000 participants</div>\n              <button ion-button class="buttonJoin">join</button>\n            </div>\n          </ion-col>\n        </ion-row>\n\n      </ion-card>\n    </div>\n\n    <div class="cardSeparate">\n      <div class="textCardTitle">Completed Challenges</div>\n      <ion-card class="cardPaddingChallenge">\n\n          <ion-row class="rowMarin">\n              <ion-col col-4 col no-margin no-padding class="center-center">\n                <div class="imageChallenge center-center">\n                  <img src="assets/imgs/profile/profile_photo_02.png" class="">\n                </div>\n              </ion-col>\n      \n              <ion-col col-8 no-margin no-padding class="center-left">\n                <div class="marginLeft">\n                  <div class="titleHeaderInCard">Weekly Challenge</div>\n                  <div class="titleContentInCard">Jun 3 - Jun 9, 2019</div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n      </ion-card>\n\n      <ion-card class="cardPaddingChallenge">\n\n          <ion-row class="rowMarin">\n              <ion-col col-4 col no-margin no-padding class="center-center">\n                <div class="imageChallenge center-center">\n                  <img src="assets/imgs/profile/profile_photo_03.png" class="">\n                </div>\n              </ion-col>\n      \n              <ion-col col-8 no-margin no-padding class="center-left">\n                <div class="marginLeft">\n                  <div class="titleHeaderInCard">Weekly Challenge</div>\n                  <div class="titleContentInCard">Jun 3 - Jun 9, 2019</div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n      </ion-card>\n\n      <ion-card class="cardPaddingChallenge">\n\n        <ion-row class="rowMarin">\n            <ion-col col-4 col no-margin no-padding class="center-center">\n              <div class="imageChallenge center-center">\n                <img src="assets/imgs/profile/profile_photo_01.png" class="">\n              </div>\n            </ion-col>\n    \n            <ion-col col-8 no-margin no-padding class="center-left">\n              <div class="marginLeft">\n                <div class="titleHeaderInCard">Weekly Challenge</div>\n                <div class="titleContentInCard">Jun 3 - Jun 9, 2019</div>\n                <div class="titleContentInCard">5,000 participants</div>\n              </div>\n            </ion-col>\n          </ion-row>\n\n    </ion-card> \n\n    </div>\n  </div>-->\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/friend-profile/friend-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_friends_friends__["a" /* FriendsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_friend_profile_friend_profile__["a" /* FriendProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FriendProfilePage);
    return FriendProfilePage;
}());

//# sourceMappingURL=friend-profile.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendProfileProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FriendProfileProvider = /** @class */ (function () {
    function FriendProfileProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello FriendProfileProvider Provider');
    }
    FriendProfileProvider.prototype.getFriendProfile = function (friendNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', friendNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getProfileSetting', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendProfileProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], FriendProfileProvider);
    return FriendProfileProvider;
}());

//# sourceMappingURL=friend-profile.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermAndConditionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TermAndConditionPage = /** @class */ (function () {
    function TermAndConditionPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    TermAndConditionPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad TermAndConditionPage");
    };
    TermAndConditionPage.prototype.btnClose = function () {
        this.viewCtrl.dismiss();
    };
    TermAndConditionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-term-and-condition",template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/term-and-condition/term-and-condition.html"*/'<ion-content no-bounce (click)="btnClose()">\n\n  <div class="complexView">\n    <div class="vbox">\n      <div class="flex1 center-center">\n        <ion-card>\n          <ion-card-content>\n\n            <div class="center-center">\n              <img src="assets/imgs/termAndCondition.png" class="imgTermAndCondition">\n            </div>\n\n            <div class="modalBottom">\n              <div class="lineSeparateCurve"></div>\n\n              <div class="center-center textTitle">\n                Our Terms and Conditions\n              </div>\n\n              <div class="wordingPadding">\n                <ion-row no-margin no-padding>\n                  <ion-col col-2 class="top-center" no-margin no-padding>\n                    <img src="assets/imgs/icons/icon_rule_1.png" class="imgIcon">\n                  </ion-col>\n                  <ion-col no-margin no-padding>\n                    <div class="textInvite">What to include in a Terms and Conditions</div>\n                    <div class="textDetail">This template is free to download and use for your website or mobile app.</div>\n                  </ion-col>\n                </ion-row>\n\n                <ion-row no-margin no-padding>\n                  <ion-col col-2 class="top-center" no-margin no-padding>\n                    <img src="assets/imgs/icons/icon_rule_1.png" class="imgIcon">\n                  </ion-col>\n                  <ion-col no-margin no-padding>\n                    <div class="textInvite">What are Terms and Conditions</div>\n                    <div class="textDetail">agreement that includes the terms, the rules and the guidelines of acceptable behavior and other useful sections to which users must agree in order to use or access your website and mobile app.</div>\n                    <div class="textDetail">It’s up to you to set the rules and guidelines that the user must agree to. You can think of your Terms and Conditions agreement as the legal agreement where you maintain your rights to exclude users from your app in the event that they abuse your app, where you maintain your legal rights against potential app abusers, and so on.</div>\n                  </ion-col>\n                </ion-row>\n              </div>\n\n              <div class="center-center textInvite">\n                Oct 30, 2019\n              </div>\n\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/term-and-condition/term-and-condition.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], TermAndConditionPage);
    return TermAndConditionPage;
}());

//# sourceMappingURL=term-and-condition.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RewardedSmailePointsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rewarded_smile_point_rewarded_smile_point__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__reward_condition_reward_condition__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__profile_setting_profile_setting__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RewardedSmailePointsPage = /** @class */ (function () {
    function RewardedSmailePointsPage(modalCtrl, reward, navCtrl, navParams, settings) {
        this.modalCtrl = modalCtrl;
        this.reward = reward;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.settings = settings;
        this.totalSmilePoint = '0.0';
        this.totalPremium = '0';
        this.annualPremium = '0';
        var me = this;
        this.reward.getRewardSmilePoint().then(function (data) {
            console.log('!!!', data);
            me.showCardData = data;
        });
        this.reward.getSumSmilePoint().then(function (data) {
            console.log('total smile point ==>', data);
            me.totalSmilePoint = data.smilePoint;
            me.totalPremium = data.premium;
            me.annualPremium = data.annualPremium;
        });
        // this.showCardData = [
        //   { level: '1', date: '1 JAN - 7 JAN 2019', smile_point: '1', photo: 'flamingo.png' },
        //   { level: '5', date: '25 dec - 31 dec 2018', smile_point: '2', photo: 'sheetah.png' },
        //   { level: '4', date: '18 JAN - 24 jan 2018', smile_point: '3', photo: 'zebra.png' },
        //   { level: '3', date: '11 JAN - 17 JAN 2018', smile_point: '1', photo: 'monkey.png' },
        //   { level: '2', date: '4 JAN - 10 JAN 2018', smile_point: '5', photo: 'elephant.png' },
        // ];
    }
    RewardedSmailePointsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RewardedSmailePointsPage');
    };
    RewardedSmailePointsPage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__profile_setting_profile_setting__["a" /* ProfileSettingPage */], {}, { animate: true, direction: 'back' });
    };
    RewardedSmailePointsPage.prototype.getInfo = function () {
        console.log('info');
        var stepModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__reward_condition_reward_condition__["a" /* RewardConditionPage */], { annualPremium: this.annualPremium });
        stepModal.present();
    };
    RewardedSmailePointsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-rewarded-smaile-points',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/rewarded-smaile-points/rewarded-smaile-points.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>Rewarded Smile Points</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n\n  <div class="headerTop">\n\n    <div class="topCurve">\n      <div class="textTitleEarned">My total points</div>\n    </div>\n    <div class="cardFloat">\n      <ion-card class="cardPaddingHead">\n\n        <div class="centerEarnedPoint">\n\n          <ion-row no-margin no-padding>\n              <ion-col col-7>\n            <!-- <ion-col  no-margin no-padding class="center-center"> -->\n              <!-- <img src="assets/imgs/icons/icon_gold_coin_big.png" class="imgGoldCoin"> -->\n              <!-- </ion-col>\n            <ion-col no-margin no-padding class="center-left"> -->\n                <div class="textEarnedPoint">{{totalSmilePoint}} </div>\n              <!-- <div class="textEarnedPoint">{{totalSmilePoint}} <span>Pts</span></div> -->\n            </ion-col>\n            <ion-col col-3>\n                <img src="assets/imgs/icons/point.png" class="imgPoint">\n            </ion-col>\n          </ion-row>\n          <div>\n          </div>\n          <!-- <div class="textMyPoint">Annual Premium: 50,000 baht</div> -->\n          <div class="textMyPoint">Weekly Premium: {{totalPremium}} THB.</div>\n        </div>\n\n      </ion-card>\n    </div>\n    <ion-row (click)="getInfo()">\n      <div class="textRewardHistory" style="width: max-content;">Achievement Reward history</div>\n      <div  style="padding-left: 10px;">\n        <ion-icon name="ios-information-circle-outline" style="font-size: 2em;"></ion-icon>\n      </div>\n    </ion-row>\n    <!-- <div class="textRewardHistory">Achievement Reward history</div> -->\n\n  </div>\n\n</ion-header>\n\n\n<ion-content class="mainContent">\n\n  <div class="cardContentPadding">\n    <ion-card class="cardPadding">\n      <ion-list no-margin no-padding *ngFor="let record of showCardData;let i= index">\n        <ion-item>\n          <ion-row no-padding no-margin class="center-center">\n\n            <ion-col no-padding no-margin col-3>\n              <div class="center-center">\n                <img src="{{record.levelImage}}" class="avatarWidth">\n              </div>\n            </ion-col>\n\n            <ion-col no-padding no-margin>\n              <div class="vbox">\n\n                <div class="flex1">\n                  <div class="textName bottom-left">Level {{record.levelNo}}</div>\n                </div>\n                <div class="flex1">\n                  <div class="textData">{{record.period}}</div>\n                </div>\n\n              </div>\n            </ion-col>\n\n            <ion-col no-padding no-margin col-2>\n\n\n              <!-- <div class="textRankBg center-center">\n                <span class="textRank">+ {{record.smilePoint}}</span>\n              </div> -->\n              <div class="flex1">\n                <span class="textRank" style="font-size:0.8em">{{record.smilePoint}} pts</span>\n              </div>\n              <div class="flex1">\n                <span class="textRank" style="font-size:0.8em"></span>\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <div class="lineSeperate" *ngIf="i != (showCardData.length)-1"></div>\n          <div class="lineSeperateNone" *ngIf="i == (showCardData.length)-1"></div>\n        </ion-item>\n      </ion-list>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/rewarded-smaile-points/rewarded-smaile-points.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rewarded_smile_point_rewarded_smile_point__["a" /* RewardedSmilePointProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */]])
    ], RewardedSmailePointsPage);
    return RewardedSmailePointsPage;
}());

//# sourceMappingURL=rewarded-smaile-points.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebSqlProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
var WebSqlProvider = /** @class */ (function () {
    function WebSqlProvider(
        // private sqliteNative: SQLite,
        platform, config) {
        this.platform = platform;
        this.config = config;
        console.log('WebSql Running....');
    }
    WebSqlProvider.prototype.setDBConnection = function (data) {
        this.dbConnection = data;
    };
    WebSqlProvider.prototype.getDBConnection = function () {
        return this.dbConnection;
    };
    WebSqlProvider.prototype.executeQuery = function (dbConn, sql, params, func) {
        var me = this;
        params != null ? params : [];
        console.log('xxx ', me.platform.is('ios'));
        if (me.platform.is('ios')) {
            console.log('@@@@@ ', me.config.dbNative, sql, params);
            me.config.dbNative.executeSql(sql, params)
                .then(function (result) {
                console.log('$$$$$ ', result);
                func(result);
            })
                .catch(function (e) {
                'ERROR ' + JSON.stringify(e);
                func(null);
            });
        }
        else {
            dbConn.transaction(function (tx) {
                tx.executeSql(sql, params, function (tx, results) {
                    func(results);
                }, function (tx, err) {
                    func(null);
                });
            });
        }
    };
    WebSqlProvider.prototype.executeNonQuery = function (dbConn, sql, params, func) {
        console.log('!!!!!!!!');
        var me = this;
        params != null ? params : [];
        if (this.platform.is('ios')) {
            me.config.dbNative.executeSql(sql, params)
                .then(function (result) {
                console.log('##### ', result);
                func(result);
            })
                .catch(function (e) {
                "Error: " + JSON.stringify(e);
                func(null);
            });
        }
        else {
            dbConn.transaction(function (tx) {
                tx.executeSql(sql, params, function (tx, results) {
                    func(true);
                }, function (tx, err) {
                    func(false);
                });
            });
        }
    };
    WebSqlProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* ConfigProvider */]])
    ], WebSqlProvider);
    return WebSqlProvider;
}());

//# sourceMappingURL=web-sql.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RewardedSmilePointProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RewardedSmilePointProvider = /** @class */ (function () {
    function RewardedSmilePointProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello RewardedSmilePointProvider Provider');
    }
    RewardedSmilePointProvider.prototype.getRewardSmilePoint = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getRewardSmilePoint', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    RewardedSmilePointProvider.prototype.getSumSmilePoint = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getSumSmilePoint', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    RewardedSmilePointProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], RewardedSmilePointProvider);
    return RewardedSmilePointProvider;
}());

//# sourceMappingURL=rewarded-smile-point.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RewardConditionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RewardConditionPage = /** @class */ (function () {
    function RewardConditionPage(viewCtrl, navCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.annualPremium = '0';
    }
    RewardConditionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RewardConditionPage', this.navParams.get('annualPremium'));
        this.annualPremium = this.navParams.get('annualPremium');
    };
    RewardConditionPage.prototype.btnClose = function () {
        this.viewCtrl.dismiss();
    };
    RewardConditionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reward-condition',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/reward-condition/reward-condition.html"*/'<ion-content no-bounce (click)="btnClose()">\n  <div class="content-center">\n    <ion-card>\n      <ion-card-header>\n        Points are calculated from :\n      </ion-card-header>\n      <ion-list>\n        <ion-item class="info">\n          <ion-row style="white-space: initial;">\n            - Weekly premium is Annual premium/52\n          </ion-row>\n          <ion-row style="white-space: initial;">\n            - Annual premium equals to a maximum of 50,000 and a sum of 1,000 and Health & CI rider.\n          </ion-row>\n          <ion-row style="white-space: initial;">\n            - Your current Health & CI Rider is {{annualPremium}} THB.\n          </ion-row>\n        </ion-item>\n\n      </ion-list>\n\n    </ion-card>\n\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/reward-condition/reward-condition.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], RewardConditionPage);
    return RewardConditionPage;
}());

//# sourceMappingURL=reward-condition.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeeklyAchievementProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WeeklyAchievementProvider = /** @class */ (function () {
    function WeeklyAchievementProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello WeeklyAchievementProvider Provider');
    }
    WeeklyAchievementProvider.prototype.getWeeklyAchievement = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getWeeklyAchievement', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log('in ', data);
                resolve(data);
            });
        });
    };
    WeeklyAchievementProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], WeeklyAchievementProvider);
    return WeeklyAchievementProvider;
}());

//# sourceMappingURL=weekly-achievement.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileSettingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileSettingProvider = /** @class */ (function () {
    function ProfileSettingProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello ProfileSettingProvider Provider');
    }
    ProfileSettingProvider.prototype.getProfileSetting = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getProfileSetting', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ProfileSettingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* ConfigProvider */]])
    ], ProfileSettingProvider);
    return ProfileSettingProvider;
}());

//# sourceMappingURL=profile-setting.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExcerciseDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExcerciseDetailPage = /** @class */ (function () {
    function ExcerciseDetailPage(viewCtrl, navCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ExcerciseDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ExcerciseDetailPage', this.navParams.get('objData'));
        var obj = this.navParams.get('objData');
        console.log('xxx ', obj);
        this.activityDate = obj.activityDate;
        this.exerciseSwimDistance = obj.exerciseSwimDistance;
        this.exerciseTotalMinute = obj.exerciseTotalMinute;
        this.exerciseTotalPercent = obj.exerciseTotalPercent;
        this.exerciseZone2Minute = obj.exerciseZone2Minute;
        this.exerciseZone2Percent = obj.exerciseZone2Percent;
        this.exerciseZone3Minute = obj.exerciseZone3Minute;
        this.exerciseZone3Percent = obj.exerciseZone3Percent;
        this.exereciseSwimPercent = obj.exereciseSwimPercent;
        this.exerciseTotalCalories = obj.exerciseTotalCalories;
    };
    ExcerciseDetailPage.prototype.btnClose = function () {
        this.viewCtrl.dismiss();
    };
    ExcerciseDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-excercise-detail',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/excercise-detail/excercise-detail.html"*/'<ion-content no-bounce (click)="btnClose()">\n  <div class="content-center">\n    <ion-card>\n      <ion-card-header no-padding no-margin class="cardHeader">\n        <ion-row class="center-center" no-padding no-margin>\n          <ion-col col-4  no-padding no-margin class="center-right"\n            ><img\n              src="assets/imgs/icons_exercise/icon-barbell.png"\n              class="iconSize"\n          /></ion-col>\n          <ion-col col-8 no-padding no-margin>\n            <span class="cardTitle"> EXERCISE DETAILS</span>\n            </ion-col>\n        </ion-row>\n      </ion-card-header>\n      <ion-list no-padding no-margin>\n        <ion-item no-padding no-margin>\n\n          <ion-row  no-padding no-margin class="itemMargin">\n            <ion-col col-3 class="center-center iconLeftPosition"  no-padding no-margin\n              ><img\n                src="assets/imgs/icons_exercise/icon-heart.png"\n                class="iconInItems"\n            /></ion-col>\n            <ion-col  no-padding no-margin>\n              <div class="lineMarginBottom">\n                <span class="textTitle">ZONE 2</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Time:</span>\n                <span class="textDescBold">{{exerciseZone2Minute}}  minutes</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Minutes/60:</span>\n                <span class="textDescBold">{{exerciseZone2Percent}}%</span>\n              </div>\n            </ion-col>\n          </ion-row>\n          <div class="separateLine"></div>\n\n          <ion-row  no-padding no-margin class="itemMargin">\n            <ion-col col-3 class="center-center iconLeftPosition"  no-padding no-margin\n              ><img\n                src="assets/imgs/icons_exercise/icon-heart.png"\n                class="iconInItems"\n            /></ion-col>\n            <ion-col  no-padding no-margin>\n              <div class="lineMarginBottom">\n                <span class="textTitle">ZONE 3</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Time:</span>\n                <span class="textDescBold">{{exerciseZone3Minute}}  minutes</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Minutes/30:</span>\n                <span class="textDescBold">{{exerciseZone3Percent}}%</span>\n              </div>\n            </ion-col>\n          </ion-row>\n          <div class="separateLine"></div>\n\n          <ion-row  no-padding no-margin class="itemMargin">\n            <ion-col col-3 class="center-center iconLeftPosition"  no-padding no-margin\n              ><img\n                src="assets/imgs/icons_exercise/icon-distance.png"\n                class="iconInItems"\n            /></ion-col>\n            <ion-col  no-padding no-margin>\n              <div class="lineMarginBottom">\n                <span class="textTitle">SPORT TYPE</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Distance:</span>\n                <span class="textDescBold">{{exerciseSwimDistance}}  m.</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Percent (distance/750):</span>\n                <span class="textDescBold">{{exereciseSwimPercent}}%</span>\n              </div>\n            </ion-col>\n          </ion-row>\n          <div class="separateLine"></div>\n\n          <ion-row  no-padding no-margin class="itemMargin">\n            <ion-col col-3 class="center-center iconLeftPosition"  no-padding no-margin\n              ><img\n                src="assets/imgs/icons_exercise/icon-zoom.png"\n                class="iconInItems"\n            /></ion-col>\n            <ion-col  no-padding no-margin>\n              <div class="lineMarginBottom">\n                <span class="textTitle">TOTAL</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Percent:</span>\n                <span class="textDescBold">{{exerciseTotalPercent}}  %</span>\n              </div>\n              <div class="lineMarginBottom">\n                <span class="textDesc">Calories:</span>\n                <span class="textDescBold">{{exerciseTotalCalories}} cal.</span>\n              </div>\n            </ion-col>\n          </ion-row>\n\n          <div class="center-right cardFooter">\n            Updated: {{activityDate}}\n          </div>\n          \n        </ion-item>\n\n      </ion-list>\n    </ion-card>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/excercise-detail/excercise-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ExcerciseDetailPage);
    return ExcerciseDetailPage;
}());

//# sourceMappingURL=excercise-detail.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HowToChallengesWorkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HowToChallengesWorkPage = /** @class */ (function () {
    function HowToChallengesWorkPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HowToChallengesWorkPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HowToChallengesWorkPage');
    };
    HowToChallengesWorkPage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
    };
    HowToChallengesWorkPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-how-to-challenges-work',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/how-to-challenges-work/how-to-challenges-work.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>How To Challenges Work</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<img src="assets/imgs/challenge/how-to-challenges-work.jpg" class="longImageHowToChallengesWork">\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/how-to-challenges-work/how-to-challenges-work.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], HowToChallengesWorkPage);
    return HowToChallengesWorkPage;
}());

//# sourceMappingURL=how-to-challenges-work.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_history_history__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var HistoryPage = /** @class */ (function () {
    function HistoryPage(config, 
        // private zone: NgZone,
        historyProvider, navCtrl, navParams, nav, loadingCtrl, actionSheetController) {
        this.config = config;
        this.historyProvider = historyProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nav = nav;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetController = actionSheetController;
        this.periodTime = 'w';
        this.activityType = 's';
        this.currentIndex = 1;
        this.titlePage = 'Steps history';
        this.unit = 'STEPS';
        this.iconStepScale = 'image-scale-up';
        this.iconWorkoutScale = 'image-scale-normal';
        this.showIndexOf = -1;
        this.showDetailStatus = false;
        this.dateWeekTextDetail = 'dateWeekTextDetail colorStep';
        this.dataTextDetail = 'dataTextDetail colorStep';
        this.unitTextDetail = 'unitTextDetail colorStep';
        this.segmentSwitch1 = 'segment_button_active_history';
        this.segmentSwitch2 = 'segment_button_inactive_history';
        this.segmentSwitch3 = 'segment_button_inactive_history';
        this.actionSheetSelected = 'Exercise (Overall)';
        this.chartTxt1 = 'MON';
        this.chartTxt2 = 'TUE';
        this.chartTxt3 = 'WED';
        this.chartTxt4 = 'THU';
        this.chartTxt5 = 'FRI';
        this.chartTxt6 = 'SAT';
        this.chartTxt7 = 'SUN';
        this.indicator1 = '0';
        this.indicator2 = '2,500'; //'15';
        this.indicator3 = '5,000';
        this.indicator4 = '7,500';
        this.indicator5 = '10,000';
        this.actionSheetStatus = false;
        this.actionSheetSelected = "Exercise (Overall)";
        this.lines = [
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' }
        ];
    }
    HistoryPage.prototype.updateActionSheetSelected = function (value) {
        if (value === 1) {
            this.actionSheetSelected = 'Exercise (Overall)'; //'Exercise (Overall)';
            this.activityType = 'a';
        }
        else if (value === 2) {
            this.actionSheetSelected = 'Running'; //'Running';
            this.activityType = 'r';
        }
        else if (value === 3) {
            this.actionSheetSelected = 'Cycling'; //'Cycling';
            this.activityType = 'c';
        }
        else if (value === 4) {
            this.actionSheetSelected = 'Swimming'; //'Swimming';   
            this.activityType = 'sw';
        }
        else if (value === 5) {
            this.actionSheetSelected = 'Others'; //'Other';
            this.activityType = 'o';
        }
        console.log('activity type ', this.actionSheetSelected);
        this.bindDataByCondition();
    };
    HistoryPage.prototype.presentActionSheet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            title: 'Select catergories',
                            buttons: [
                                {
                                    text: 'Exercise (Overall)',
                                    icon: 'exercise',
                                    handler: function () {
                                        _this.updateActionSheetSelected(1);
                                    }
                                },
                                {
                                    text: 'Running',
                                    icon: 'running',
                                    handler: function () {
                                        _this.updateActionSheetSelected(2);
                                    }
                                },
                                {
                                    text: 'Cycling',
                                    icon: 'bike',
                                    handler: function () {
                                        _this.updateActionSheetSelected(3);
                                    }
                                },
                                {
                                    text: 'Swimming',
                                    icon: 'swimming',
                                    handler: function () {
                                        _this.updateActionSheetSelected(4);
                                    }
                                },
                                {
                                    text: 'Others',
                                    icon: 'other',
                                    handler: function () {
                                        _this.updateActionSheetSelected(5);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HistoryPage.prototype.changeData = function (rec, i) {
        console.log('===>', rec, ' ', i);
        this.clearGraph();
        this.periodInfo = rec.showPeriod;
        // this.showIndexOf = i;//กำหนด rec ที่จะแสดง detail
        var me = this;
        if (me.periodTime == 'w') {
            me.historyProvider.getHistoryDetail('d', me.activityType, rec.aggPeriod).then(function (data) {
                console.log('#### ', data);
                if (me.showIndexOf == i && me.showDetailStatus == true) {
                    me.showIndexOf == -1;
                    me.showDetailStatus = false;
                }
                else {
                    me.showIndexOf = i;
                    me.showDetailStatus = true;
                    me.objDetail = data;
                }
                me.paintIndicatorGraph();
                me.paintPeriodGraph(data);
                me.paintGraph(data);
            });
        }
        else {
            //render by shift data from first active
            this.chartTxt1 = '';
            this.chartTxt2 = '';
            this.chartTxt3 = '';
            this.chartTxt4 = '';
            this.chartTxt5 = '';
            this.chartTxt6 = '';
            this.chartTxt7 = '';
            if (this.periodTime == 'w') {
                this.chartTxt1 = 'MON';
                this.chartTxt2 = 'TUE';
                this.chartTxt3 = 'WED';
                this.chartTxt4 = 'THU';
                this.chartTxt5 = 'FRI';
                this.chartTxt6 = 'SAT';
                this.chartTxt7 = 'SUN';
            }
            else if (this.periodTime == 'm' || this.periodTime == 'y') {
                //shift data
                console.log('month ', this.objHistory);
                var j = i;
                var n = 1;
                var txt = '';
                var obj = [];
                for (j; j < this.objHistory.length; j++) {
                    txt = this.objHistory[j].showPeriod;
                    console.log('%%% ', txt);
                    switch (n) {
                        case 1: {
                            this.chartTxt1 = txt;
                            obj.push(this.objHistory[j]);
                            break;
                        }
                        case 2: {
                            this.chartTxt2 = txt;
                            obj.push(this.objHistory[j]);
                            break;
                        }
                        case 3: {
                            this.chartTxt3 = txt;
                            obj.push(this.objHistory[j]);
                            break;
                        }
                        case 4: {
                            this.chartTxt4 = txt;
                            obj.push(this.objHistory[j]);
                            break;
                        }
                        case 5: {
                            this.chartTxt5 = txt;
                            obj.push(this.objHistory[j]);
                            break;
                        }
                        case 6: {
                            this.chartTxt6 = txt;
                            obj.push(this.objHistory[j]);
                            break;
                        }
                        case 7: {
                            this.chartTxt7 = txt;
                            obj.push(this.objHistory[j]);
                            break;
                        }
                    }
                    n++;
                }
                me.paintGraph(obj);
            }
        }
    };
    HistoryPage.prototype.ionViewWillEnter = function () {
        console.log('will');
        this.pageTop.scrollTo(0, 0, 0);
    };
    HistoryPage.prototype.ionViewDidEnter = function () {
        console.log('enter');
        this.pageTop.scrollTo(0, 0, 0);
        if (this.config.refreshHistory == true) {
            this.config.refreshHistory = false;
            //   console.log('call api')
            this.refreshHistory();
        }
    };
    HistoryPage.prototype.ionViewDidLeave = function () {
        console.log('leave');
        this.pageTop.scrollTo(0, 0, 100);
    };
    HistoryPage.prototype.refreshHistory = function () {
        console.log('refreshHistory');
        var me = this;
        var loader = this.loadingCtrl.create({
            spinner: 'crescent'
        });
        loader.present();
        this.historyProvider.getHistory().then(function (data) {
            console.log('history ', data);
            me.objTmp = data;
            me.periodInfo = data.lsStepWeek[0].showPeriod;
            me.objHistory = data.lsStepWeek;
            var runNo = data.lsStepWeek[0].aggPeriod;
            me.historyProvider.getHistoryDetail('d', me.activityType, runNo).then(function (objInfo) {
                // console.log('@@@@ ', objInfo)
                loader.dismiss();
                me.paintGraph(objInfo);
            });
        });
    };
    HistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HistoryPage');
        console.log('current period ', this.periodTime, ' ==> ', this.objHistory);
        this.refreshHistory();
    };
    HistoryPage.prototype.getStep = function () {
        this.showIndexOf == -1;
        // this.showDetailStatus = false;
        this.iconStepScale = 'image-scale-up';
        this.iconWorkoutScale = 'image-scale-normal';
        this.titlePage = 'Steps history';
        this.unit = 'STEPS';
        this.activityType = 's';
        this.bindDataByCondition();
        // this.renderGraph(this.objHistory[0]);
        this.actionSheetStatus = false;
    };
    HistoryPage.prototype.getWorkout = function () {
        this.showIndexOf == -1;
        // this.showDetailStatus = false;
        this.iconStepScale = 'image-scale-normal';
        this.iconWorkoutScale = 'image-scale-up';
        this.titlePage = 'Exercise history';
        this.unit = 'Minutes';
        this.activityType = 'a';
        this.updateActionSheetSelected(1);
        this.bindDataByCondition();
        this.actionSheetStatus = true;
    };
    HistoryPage.prototype.btnSegment = function (index) {
        if (index == 1) {
            this.periodTime = 'w';
            this.segmentSwitch1 = 'segment_button_active_history';
            this.segmentSwitch2 = 'segment_button_inactive_history';
            this.segmentSwitch3 = 'segment_button_inactive_history';
        }
        else if (index == 2) {
            this.periodTime = 'm';
            this.segmentSwitch1 = 'segment_button_inactive_history';
            this.segmentSwitch2 = 'segment_button_active_history';
            this.segmentSwitch3 = 'segment_button_inactive_history';
        }
        else {
            this.periodTime = 'y';
            this.segmentSwitch1 = 'segment_button_inactive_history';
            this.segmentSwitch2 = 'segment_button_inactive_history';
            this.segmentSwitch3 = 'segment_button_active_history';
        }
        console.log('period ', this.periodTime);
        //call webservie rendered data
        this.bindDataByCondition();
    };
    HistoryPage.prototype.bindDataByCondition = function () {
        this.showDetailStatus = false;
        this.clearGraph();
        console.log('period ', this.periodTime, ' type ', this.activityType);
        var me = this;
        switch (me.periodTime) {
            case 'w': {
                switch (me.activityType) {
                    case 'a': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsOverAllWeek;
                        break;
                    }
                    case 'r': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsRunningWeek;
                        break;
                    }
                    case 'c': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsCyclingWeek;
                        break;
                    }
                    case 'sw': {
                        me.unit = 'KM';
                        me.objHistory = me.objTmp.lsSwimmingWeek;
                        break;
                    }
                    case 'o': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsOtherWeek;
                        break;
                    }
                    case 's': {
                        me.unit = 'STEPS';
                        me.objHistory = me.objTmp.lsStepWeek;
                        break;
                    }
                }
                break;
            }
            case 'm': {
                switch (me.activityType) {
                    case 'a': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsOverAllMonth;
                        break;
                    }
                    case 'r': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsRunningMonth;
                        break;
                    }
                    case 'c': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsCyclingMonth;
                        break;
                    }
                    case 'sw': {
                        me.unit = 'KM';
                        me.objHistory = me.objTmp.lsSwimmingMonth;
                        break;
                    }
                    case 'o': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsOtherMonth;
                        break;
                    }
                    case 's': {
                        me.unit = 'STEPS';
                        me.objHistory = me.objTmp.lsStepMonth;
                        break;
                    }
                }
                break;
            }
            case 'y': {
                switch (me.activityType) {
                    case 'a': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsOverAllYear;
                        break;
                    }
                    case 'r': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsRunningYear;
                        break;
                    }
                    case 'c': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsCyclingYear;
                        break;
                    }
                    case 'sw': {
                        me.unit = 'KM';
                        me.objHistory = me.objTmp.lsSwimmingYear;
                        break;
                    }
                    case 'o': {
                        me.unit = 'HR';
                        me.objHistory = me.objTmp.lsOtherYear;
                        break;
                    }
                    case 's': {
                        me.unit = 'STEPS';
                        me.objHistory = me.objTmp.lsStepYear;
                        break;
                    }
                }
                break;
            }
        }
        me.paintPeriodGraph(me.objHistory);
        me.paintIndicatorGraph();
        console.log('xxx ', this.objHistory);
        if (me.objHistory.length > 0) {
            me.periodInfo = me.objHistory[0].showPeriod;
            if (me.periodTime == 'w') {
                var runNo = me.objHistory[0].aggPeriod;
                me.historyProvider.getHistoryDetail('d', me.activityType, runNo).then(function (objInfo) {
                    console.log('@@@@ ', objInfo);
                    me.paintGraph(objInfo);
                });
            }
            else {
                me.paintGraph(me.objHistory);
            }
        }
        else {
            me.periodInfo = '';
        }
    };
    HistoryPage.prototype.paintPeriodGraph = function (data) {
        if (this.periodTime != 'w') {
            this.chartTxt1 = '';
            this.chartTxt2 = '';
            this.chartTxt3 = '';
            this.chartTxt4 = '';
            this.chartTxt5 = '';
            this.chartTxt6 = '';
            this.chartTxt7 = '';
            for (var i = 0; i < data.length; i++) {
                if (i < 7) {
                    switch (i) {
                        case 0: {
                            this.chartTxt1 = data[i].showPeriod;
                            break;
                        }
                        case 1: {
                            this.chartTxt2 = data[i].showPeriod;
                            break;
                        }
                        case 2: {
                            this.chartTxt3 = data[i].showPeriod;
                            break;
                        }
                        case 3: {
                            this.chartTxt4 = data[i].showPeriod;
                            break;
                        }
                        case 4: {
                            this.chartTxt5 = data[i].showPeriod;
                            break;
                        }
                        case 5: {
                            this.chartTxt6 = data[i].showPeriod;
                            break;
                        }
                        case 6: {
                            this.chartTxt7 = data[i].showPeriod;
                            break;
                        }
                    }
                }
            }
        }
        else {
            this.chartTxt1 = 'MON';
            this.chartTxt2 = 'TUE';
            this.chartTxt3 = 'WED';
            this.chartTxt4 = 'THU';
            this.chartTxt5 = 'FRI';
            this.chartTxt6 = 'SAT';
            this.chartTxt7 = 'SUN';
        }
    };
    HistoryPage.prototype.paintIndicatorGraph = function () {
        //change info of graph
        if (this.unit == 'STEPS') {
            switch (this.periodTime) {
                case 'w': {
                    this.indicator1 = '0';
                    this.indicator2 = '2,500';
                    this.indicator3 = '5,000';
                    this.indicator4 = '7,500';
                    this.indicator5 = '10,000';
                    break;
                }
                case 'm': {
                    this.indicator1 = '0';
                    this.indicator2 = '75,000';
                    this.indicator3 = '150,000';
                    this.indicator4 = '225,000';
                    this.indicator5 = '300,000';
                    break;
                }
                case 'y': {
                    this.indicator1 = '0';
                    this.indicator2 = '900,000';
                    this.indicator3 = '1,800,000';
                    this.indicator4 = '2,700,000';
                    this.indicator5 = '3,600,000';
                    break;
                }
            }
        }
        else if (this.unit == 'HR') {
            switch (this.periodTime) {
                case 'w': {
                    this.indicator1 = '0';
                    this.indicator2 = '15';
                    this.indicator3 = '30';
                    this.indicator4 = '45';
                    this.indicator5 = '60';
                    break;
                }
                case 'm': {
                    this.indicator1 = '0';
                    this.indicator2 = '450';
                    this.indicator3 = '900';
                    this.indicator4 = '1,350';
                    this.indicator5 = '1,800';
                    break;
                }
                case 'y': {
                    this.indicator1 = '0';
                    this.indicator2 = '5,400';
                    this.indicator3 = '10,800';
                    this.indicator4 = '16,200';
                    this.indicator5 = '21,600';
                    break;
                }
            }
        }
        else {
            switch (this.periodTime) {
                case 'w': {
                    this.indicator1 = '0';
                    this.indicator2 = '0.25';
                    this.indicator3 = '0.5';
                    this.indicator4 = '0.75';
                    this.indicator5 = '1';
                    break;
                }
                case 'm': {
                    this.indicator1 = '0';
                    this.indicator2 = '7.5';
                    this.indicator3 = '15';
                    this.indicator4 = '22.5';
                    this.indicator5 = '30';
                    break;
                }
                case 'y': {
                    this.indicator1 = '0';
                    this.indicator2 = '90';
                    this.indicator3 = '180';
                    this.indicator4 = '270';
                    this.indicator5 = '360';
                    break;
                }
            }
        }
    };
    HistoryPage.prototype.paintGraph = function (data) {
        for (var i = 0; i < data.length; i++) {
            console.log(data[i]);
            if (i < 7) {
                if (this.activityType == 's') {
                    this.lines[i].value = 'chart-color1-h' + data[i].percentScale;
                }
                else {
                    this.lines[i].value = 'chart-color2-h' + data[i].percentScale;
                }
            }
        }
    };
    HistoryPage.prototype.clearGraph = function () {
        this.lines = [
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' },
            { value: 'chart-color1-h0' }
        ];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('pageTop'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], HistoryPage.prototype, "pageTop", void 0);
    HistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-history',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/history/history.html"*/'<ion-toolbar class="topCurve">\n</ion-toolbar>\n<div class="fixedHeader headerTop">\n\n  <ion-grid no-padding no-margin class="gridLineChart">\n\n    <ion-row class="positionTextMonth">\n      <ion-col col-1>\n      </ion-col>\n      <!-- <ion-col text-center><span class="textWeek">{{periodInfo}}</span></ion-col> -->\n    </ion-row>\n\n    <ion-row no-padding no-margin>\n\n      <ion-col>\n\n        <ion-row no-padding no-margin>\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col>\n                <div class="indicatorBar">\n                  <div class="complexView">\n                    <div class="vbox">\n                      <div class="flex1 bottom-center">\n                        <div class="indicatorText indicatorTextTop">{{indicator5}}</div>\n                      </div>\n                      <div class="flex1 bottom-center">\n                        <span class="indicatorText" style="padding-bottom: 0.5em;">{{indicator4}}</span>\n                      </div>\n                      <div class="flex1 bottom-center">\n                        <span class="indicatorText" style="padding-bottom: 0.5em;">{{indicator3}}</span>\n                      </div>\n                      <div class="flex1 bottom-center">\n                        <span class="indicatorText" style="padding-bottom: 0.3em;">{{indicator2}}</span>\n                      </div>\n                      <div class="flex1 bottom-center">\n                        <span class="indicatorText">{{indicator1}}</span>\n                      </div>\n                    </div>\n\n                    <!-- <div class="vbox" *ngIf="unit ===\'STEPS\'">\n                      <div class="flex1 bottom-center">\n                        <div class="indicatorText indicatorTextTop">10,000</div>\n                      </div>\n                      <div class="flex1 bottom-center">\n                        <span class="indicatorText" style="padding-bottom:0.7em;">6,000</span>\n                      </div>\n                      <div class="flex1 bottom-center">\n                        <span class="indicatorText" style="padding-bottom:0.5em;">3,000</span>\n                      </div>\n                      <div class="flex1 bottom-center">\n                        <span class="indicatorText">0</span>\n                      </div>\n                    </div> -->\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n          </ion-col>\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col text-center>\n                <div class="singleBar">\n                  <div class="bar">\n                    <div [ngClass]="lines[0].value"></div>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col>\n                <div class="chartDateText">{{chartTxt1}}</div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col>\n                <div class="singleBar">\n                  <div class="bar">\n                    <div [ngClass]="lines[1].value"></div>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col>\n                <div class="chartDateText">{{chartTxt2}}</div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col>\n                <div class="singleBar">\n                  <div class="bar">\n                    <div [ngClass]="lines[2].value"></div>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col>\n                <div class="chartDateText">{{chartTxt3}}</div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col>\n                <div class="singleBar">\n                  <div class="bar">\n                    <div [ngClass]="lines[3].value"></div>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col>\n                <div class="chartDateText">{{chartTxt4}}</div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col>\n                <div class="singleBar">\n                  <div class="bar">\n                    <div [ngClass]="lines[4].value"></div>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col>\n                <div class="chartDateText">{{chartTxt5}}</div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col>\n                <div class="singleBar">\n                  <div class="bar">\n                    <div [ngClass]="lines[5].value"></div>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col>\n                <div class="chartDateText">{{chartTxt6}}</div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n\n\n          <ion-col no-padding no-margin>\n            <ion-row>\n              <ion-col>\n                <div class="singleBar">\n                  <div class="bar">\n                    <div [ngClass]="lines[6].value"></div>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row>\n              <ion-col>\n                <div class="chartDateText">{{chartTxt7}}</div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n\n        </ion-row>\n\n\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4></ion-col>\n      <ion-col class="iconRight">\n        <img src="assets/imgs/icons/icon_step.png" (click)="getStep()" [ngClass]="iconStepScale">\n        <!-- <img  src="assets/imgs/icons/icon_step.png" (click)="getStep()" class="iconResize" [ngClass]="iconStepScale"> -->\n      </ion-col>\n      <ion-col class="iconLeft">\n        <img src="assets/imgs/icons/icon_workout.png" (click)="getWorkout()" [ngClass]="iconWorkoutScale">\n        <!-- <img src="assets/imgs/icons/icon_workout.png" (click)="getWorkout()" class="iconResize" [ngClass]="iconWorkoutScale"> -->\n      </ion-col>\n      <ion-col col-4></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div class="segmentCenterHistory">\n          <div class="segmentBgHistory">\n            <ion-grid no-padding no-margin>\n              <ion-row no-padding no-margin>\n                <ion-col no-padding no-margin (click)="btnSegment(1)">\n                  <div [ngClass]="segmentSwitch1" class="center-center">\n                    <div class="marginSegmentTopHistory">Weekly</div>\n                  </div>\n                </ion-col>\n                <ion-col no-padding no-margin (click)="btnSegment(2)">\n                  <div [ngClass]="segmentSwitch2" class="center-center">\n                    <div class="marginSegmentTopHistory">Monthly</div>\n                  </div>\n                </ion-col>\n                <ion-col no-padding no-margin (click)="btnSegment(3)">\n                  <div [ngClass]="segmentSwitch3" class="center-center">\n                    <div class="marginSegmentTopHistory">Yearly</div>\n                  </div>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</div>\n<ion-content fullscreen #mycontent class="mainContent" #pageTop>\n\n\n\n  <ion-row *ngIf="actionSheetStatus === true">\n    <ion-col class="center-center">\n      <div class="customActionSheet" (click)="presentActionSheet()">\n        <ion-row>\n          <ion-col col-1></ion-col>\n          <ion-col col-8 class="center-center"><span class="displayTextInside">{{actionSheetSelected}}</span></ion-col>\n          <ion-col col-2 class="center-center"><img src="assets/imgs/icons/icon_arrow-down.png" class="iconArrowDown">\n          </ion-col>\n        </ion-row>\n      </div>\n    </ion-col>\n  </ion-row>\n\n\n  <div class="cardMainPadding">\n    <ion-card>\n      <ion-card-header>\n        {{titlePage}}\n      </ion-card-header>\n      <!-- <div class="scrollPositionList"> -->\n        <!-- <ion-scroll scrollY="true"> -->\n          <ion-list *ngFor="let record of objHistory;let i= index">\n            <ion-item (click)="changeData(record,i)">\n              <ion-row no-padding no-margin>\n                <ion-col no-padding no-margin col-8>\n                  <span class="dateWeekText">{{record.showPeriod}}</span>\n                </ion-col>\n                <ion-col no-padding no-margin>\n                  <ion-row no-padding no-margin>\n                    <ion-col no-padding no-margin text-right>\n                      <span class="dataText">{{record.result}}</span>\n                      <!-- &nbsp; -->\n                      <!-- <span class="unitText">{{unit}}</span> -->\n                    </ion-col>\n\n                    <!-- <ion-col no-padding no-margin text-right *ngIf="unit ===\'STEPS\'">\n                      <span class="dataText">{{record.totalStep}}</span>\n                      &nbsp;\n                      <span class="unitText">{{unit}}</span>\n                    </ion-col> -->\n                  </ion-row>\n                </ion-col>\n              </ion-row>    \n              \n              <div no-padding no-margin *ngIf="showIndexOf==i && showDetailStatus==true">\n                <ion-item style="margin-bottom: -11px;" *ngFor="let detail of objDetail">\n                  <ion-row>\n                    <ion-col no-padding no-margin col-8>\n                      <span [ngClass]="dateWeekTextDetail">{{detail.showPeriod}}</span>\n                    </ion-col>\n                    <ion-col no-padding no-margin text-right>\n                      <span [ngClass]="dataTextDetail">{{detail.result}}</span>       \n                    </ion-col>\n                  </ion-row>\n                </ion-item>\n              </div>\n\n\n              <!-- <div no-padding no-margin *ngIf="showIndexOf==i && showDetailStatus==true && unit===\'STEPS\'">\n                <ion-item style="margin-bottom: -11px;" *ngFor="let detail of record.lsStepPerWeek">\n                  <ion-row>\n                    <ion-col no-padding no-margin col-8>\n                      <span [ngClass]="dateWeekTextDetail">{{detail.activityDate}}</span>\n                    </ion-col>\n                    <ion-col no-padding no-margin text-right>\n                      <span [ngClass]="dataTextDetail">{{detail.stepTotalDisplay}}</span>\n                      &nbsp;\n                      <span [ngClass]="unitTextDetail">{{unit}}</span>\n                    </ion-col>\n                  </ion-row>\n                </ion-item>\n              </div>\n\n              <div no-padding no-margin *ngIf="showIndexOf==i && showDetailStatus==true && unit!==\'STEPS\'">\n                <ion-item style="margin-bottom: -11px;" *ngFor="let detail of record.lsExercise">\n                  <ion-row>\n                    <ion-col no-padding no-margin col-8>\n                      <span [ngClass]="dateWeekTextDetail">{{detail.activityDate}}</span>\n                    </ion-col>\n                    <ion-col no-padding no-margin text-right>\n                      <span [ngClass]="dataTextDetail">{{detail.exerciseTotal}}</span>\n                      &nbsp;\n                      <span [ngClass]="unitTextDetail">{{unit}}</span>\n                    </ion-col>\n                  </ion-row>\n                </ion-item>\n              </div> -->\n\n              <div class="lineSeperate" *ngIf="i != (objHistory.length)-1"></div>\n            </ion-item>\n          </ion-list>\n        <!-- </ion-scroll> -->\n      <!-- </div> -->\n      <!-- <div class="scrollPositionList">\n        <ion-scroll scrollY="true">\n          <ion-list *ngFor="let record of mockupData;let i= index" (swipe)="swipe($event)">\n            <ion-item>\n              <ion-row no-padding no-margin>\n                <ion-col no-padding no-margin col-8>\n                  <span class="dateWeekText">{{record.title}}</span>\n                </ion-col>\n                <ion-col no-padding no-margin>\n                  <ion-row no-padding no-margin>\n                    <ion-col no-padding no-margin text-right>\n                      <span class="dataText">{{record.data}}</span>\n                      &nbsp;\n                      <span class="unitText">{{unit}}</span>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n              <div class="lineSeperate" *ngIf="i != (mockupData.length)-1"></div>\n            </ion-item>\n          </ion-list>\n        </ion-scroll>\n      </div> -->\n    </ion-card>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/history/history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_history_history__["a" /* HistoryProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]])
    ], HistoryPage);
    return HistoryPage;
}());

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HistoryProvider = /** @class */ (function () {
    function HistoryProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello HistoryProvider Provider');
    }
    HistoryProvider.prototype.getScale = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getScale', { search: null })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    HistoryProvider.prototype.getDetailPerWeek = function (startDate, endDate) {
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
    };
    HistoryProvider.prototype.getHistory = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        // params.set('period',period);
        // params.set('activityType',activityType);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getHistory', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    HistoryProvider.prototype.getHistoryDetail = function (period, activityType, runNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('period', period);
        params.set('activityType', activityType);
        params.set('runNo', runNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getHistory', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    HistoryProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], HistoryProvider);
    return HistoryProvider;
}());

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaderBoardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_leader_board_leader_board__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var LeaderBoardPage = /** @class */ (function () {
    function LeaderBoardPage(config, alertCtrl, camera, transfer, file, leaderboard, navCtrl, navParams, zone, loadingCtrl, nav, settings, actionSheetController) {
        this.config = config;
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.leaderboard = leaderboard;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.zone = zone;
        this.loadingCtrl = loadingCtrl;
        this.nav = nav;
        this.settings = settings;
        this.actionSheetController = actionSheetController;
        this.section = 'two';
        this.somethings = new Array(10);
        this.footerStatus = true;
        this.segmentSwitch1 = 'segment_button_inactive';
        this.segmentSwitch2 = 'segment_button_active';
        this.showCardRadius = 'cardRadiusTrue';
        this.startIndex = 1;
        this.endIndex = 1;
        this.mode = 'current';
        var me = this;
        this.showCardPadding = 'cardPaddingTrue';
        this.magicFooter = 'magicFooterShow';
        this.scrollStatus = false;
        this.userImg = 'assets/imgs/avatar/Default.jpg';
        this.actionSheetGroup = 'Public'; //"Friends";
        this.actionSheetSelected = "Steps";
        this.actionSheetSportIcon = 'steps.svg';
    }
    LeaderBoardPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter LeaderBoardPage');
        //check WeeklyAchivementPag มันจะเข้้าหน้า ionViewDidLoad อีกครั้ง ทุกหน้าที่ เป็น popup
        // if (this.config.refreshLeaderboard == true && this.objRankOfWeek != undefined) {
        //   console.log('call service get data')
        //   this.config.refreshLeaderboard = false;
        //   this.bindLeaderboardData();
        // }
    };
    LeaderBoardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LeaderBoardPage ');
        this.bindLeaderboardData();
    };
    LeaderBoardPage.prototype.updateActionSheetSelected = function (value) {
        this.pageTop.scrollToTop(100);
        if (value === 1) {
            this.actionSheetSelected = 'Steps';
            this.actionSheetSportIcon = 'steps.svg';
        }
        else if (value === 2) {
            this.actionSheetSelected = 'Exercise (Overall)';
            this.actionSheetSportIcon = 'exercise.svg';
        }
        else if (value === 3) {
            this.actionSheetSelected = 'Cycling';
            this.actionSheetSportIcon = 'bike.svg';
        }
        else if (value === 4) {
            this.actionSheetSelected = 'Running';
            this.actionSheetSportIcon = 'running.svg';
        }
        else if (value === 5) {
            this.actionSheetSelected = 'Swimming';
            this.actionSheetSportIcon = 'swimming.svg';
        }
        else if (value === 6) {
            this.actionSheetSelected = 'Others';
            this.actionSheetSportIcon = 'other.svg';
        }
        this.bindLeaderboardData();
    };
    LeaderBoardPage.prototype.updateActionSheetGroup = function (value) {
        if (value === 1) {
            this.actionSheetGroup = 'Friends';
        }
        else if (value === 2) {
            this.actionSheetGroup = 'Public';
        }
        this.bindLeaderboardData();
    };
    LeaderBoardPage.prototype.presentActionSheetGroup = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            title: 'Select Group',
                            buttons: [
                                {
                                    text: 'Friends',
                                    role: 'destructive',
                                    icon: 'friends',
                                    handler: function () {
                                        _this.updateActionSheetGroup(1);
                                    }
                                },
                                {
                                    text: 'Public',
                                    icon: 'group',
                                    handler: function () {
                                        _this.updateActionSheetGroup(2);
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LeaderBoardPage.prototype.presentActionSheetSports = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            title: 'Select catagories',
                            buttons: [{
                                    text: 'Steps',
                                    role: 'destructive',
                                    icon: 'steps',
                                    handler: function () {
                                        _this.updateActionSheetSelected(1);
                                    }
                                },
                                {
                                    text: 'Exercise (Overall)',
                                    icon: 'exercise',
                                    handler: function () {
                                        _this.updateActionSheetSelected(2);
                                    }
                                },
                                {
                                    text: 'Running',
                                    icon: 'running',
                                    handler: function () {
                                        _this.updateActionSheetSelected(4);
                                    }
                                },
                                {
                                    text: 'Cycling',
                                    icon: 'bike',
                                    handler: function () {
                                        _this.updateActionSheetSelected(3);
                                    }
                                },
                                {
                                    text: 'Swimming',
                                    icon: 'swimming',
                                    handler: function () {
                                        _this.updateActionSheetSelected(5);
                                    }
                                },
                                {
                                    text: 'Others',
                                    icon: 'other',
                                    handler: function () {
                                        _this.updateActionSheetSelected(6);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LeaderBoardPage.prototype.bindLeaderboardData = function () {
        var _this = this;
        var me = this;
        var loader = this.loadingCtrl.create({
            content: 'Loading...',
            spinner: 'crescent'
        });
        loader.present();
        this.leaderboard.getLeaderboard().then(function (data) {
            loader.dismiss();
            console.log('leaderboard ', data, 'mode ', me.mode, ' type ', me.actionSheetSelected);
            me.userImg = data.profile[0].userImg; //'assets/imgs/avatar/Default.jpg';
            me.userName = data.profile[0].userName;
            me.highLight = data.profile[0].userNo;
            me.rankNo = '-';
            me.smilePoint = data.smilePointInfo.smilePoint;
            // me.segmentSwitch1 = 'segment_button_inactive';
            // me.segmentSwitch2 = 'segment_button_active';
            // me.showCardData = data.rankOfWeek;
            if (me.mode == 'current') {
                if (_this.actionSheetGroup == 'Friends') {
                    switch (me.actionSheetSelected) {
                        case 'Steps': {
                            me.showCardData = data.lsStepFriendThisWeek;
                            me.rankNo = data.rankFriendThisWeek.rankOfStep;
                            break;
                        }
                        case 'Exercise (Overall)': {
                            me.showCardData = data.lsOverAllFriendThisWeek;
                            me.rankNo = data.rankFriendThisWeek.rankOfOverAll;
                            break;
                        }
                        case 'Cycling': {
                            me.showCardData = data.lsCyclingFriendThisWeek;
                            me.rankNo = data.rankFriendThisWeek.rankOfCycling;
                            break;
                        }
                        case 'Running': {
                            me.showCardData = data.lsRunningFriendThisWeek;
                            me.rankNo = data.rankFriendThisWeek.rankOfRunning;
                            break;
                        }
                        case 'Swimming': {
                            me.showCardData = data.lsSwimmingFriendThisWeek;
                            me.rankNo = data.rankFriendThisWeek.rankOfSwimming;
                            break;
                        }
                        case 'Others': {
                            me.showCardData = data.lsOtherFriendThisWeek;
                            me.rankNo = data.rankFriendThisWeek.rankOfOther;
                            break;
                        }
                    }
                }
                else {
                    switch (me.actionSheetSelected) {
                        case 'Steps': {
                            me.showCardData = data.lsStepThisWeek;
                            me.rankNo = data.rankThisWeek.rankOfStep;
                            break;
                        }
                        case 'Exercise (Overall)': {
                            me.showCardData = data.lsOverAllThisWeek;
                            me.rankNo = data.rankThisWeek.rankOfOverAll;
                            break;
                        }
                        case 'Cycling': {
                            me.showCardData = data.lsCyclingThisWeek;
                            me.rankNo = data.rankThisWeek.rankOfCycling;
                            break;
                        }
                        case 'Running': {
                            me.showCardData = data.lsRunningThisWeek;
                            me.rankNo = data.rankThisWeek.rankOfRunning;
                            break;
                        }
                        case 'Swimming': {
                            me.showCardData = data.lsSwimmingThisWeek;
                            me.rankNo = data.rankThisWeek.rankOfSwimming;
                            break;
                        }
                        case 'Others': {
                            me.showCardData = data.lsOtherThisWeek;
                            me.rankNo = data.rankThisWeek.rankOfOther;
                            break;
                        }
                    }
                }
            }
            else {
                if (_this.actionSheetGroup == 'Friends') {
                    switch (me.actionSheetSelected) {
                        case 'Steps': {
                            me.showCardData = data.lsStepFriendLastWeek;
                            me.rankNo = data.rankFriendLastWeek.rankOfStep;
                            break;
                        }
                        case 'Exercise (Overall)': {
                            me.showCardData = data.lsOverAllFriendLastWeek;
                            me.rankNo = data.rankFriendLastWeek.rankOfOverAll;
                            break;
                        }
                        case 'Cycling': {
                            me.showCardData = data.lsCyclingFriendLastWeek;
                            me.rankNo = data.rankFriendLastWeek.rankOfCycling;
                            break;
                        }
                        case 'Running': {
                            me.showCardData = data.lsRunningFriendLastWeek;
                            me.rankNo = data.rankFriendLastWeek.rankOfRunning;
                            break;
                        }
                        case 'Swimming': {
                            me.showCardData = data.lsSwimmingFriendLastWeek;
                            me.rankNo = data.rankFriendLastWeek.rankOfSwimming;
                            break;
                        }
                        case 'Others': {
                            me.showCardData = data.lsOtherFriendLastWeek;
                            me.rankNo = data.rankFriendLastWeek.rankOfOther;
                            break;
                        }
                    }
                }
                else {
                    switch (me.actionSheetSelected) {
                        case 'Steps': {
                            me.showCardData = data.lsStepLastWeek;
                            me.rankNo = data.rankLastWeek.rankOfStep;
                            break;
                        }
                        case 'Exercise (Overall)': {
                            me.showCardData = data.lsOverAllLastWeek;
                            me.rankNo = data.rankLastWeek.rankOfOverAll;
                            break;
                        }
                        case 'Cycling': {
                            me.showCardData = data.lsCyclingLastWeek;
                            me.rankNo = data.rankLastWeek.rankOfCycling;
                            break;
                        }
                        case 'Running': {
                            me.showCardData = data.lsRunningLastWeek;
                            me.rankNo = data.rankLastWeek.rankOfRunning;
                            break;
                        }
                        case 'Swimming': {
                            me.showCardData = data.lsSwimmingLastWeek;
                            me.rankNo = data.rankLastWeek.rankOfSwimming;
                            break;
                        }
                        case 'Others': {
                            me.showCardData = data.lsOtherLastWeek;
                            me.rankNo = data.rankLastWeek.rankOfOther;
                            break;
                        }
                    }
                }
            }
        });
    };
    LeaderBoardPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.content.ionScrollEnd.subscribe(function (e) {
            _this.scrollStatus = false;
        });
        this.content.ionScroll.subscribe(function (data) {
            _this.scrollStatus = true;
        });
    };
    LeaderBoardPage.prototype.paddingCard = function (data) {
        var _this = this;
        if (data.scrollTop == 0) {
            this.zone.run(function () {
                _this.showCardPadding = 'cardPaddingTrue';
                _this.showCardRadius = 'cardRadiusTrue';
            });
        }
        else {
            this.zone.run(function () {
                _this.showCardPadding = 'cardPaddingFalse';
                _this.showCardRadius = 'cardRadiusFalse';
            });
        }
    };
    LeaderBoardPage.prototype.pageToTop = function () {
        this.content.scrollToTop(30);
    };
    LeaderBoardPage.prototype.pageToBottom = function () {
        this.pageTop.scrollTo(0, 223, 30);
    };
    // swipe(e) {
    //   if (e.direction == 4) {
    //     console.log(e.direction);
    //     if (this.scrollStatus == false) {
    //       this.nav.parent.select(1);
    //     }
    //   } else if (e.direction == 2) {
    //     console.log(e.direction);
    //     if (this.scrollStatus == false) {
    //       this.nav.parent.select(3);
    //     }
    //   }
    // }
    LeaderBoardPage.prototype.btnSegment = function (index) {
        var me = this;
        if (this.scrollStatus == false) {
            // const loader = this.loadingCtrl.create({
            //   content: 'Loading...',
            //   spinner: 'crescent'
            // });
            // loader.present();
            if (index == 1) {
                console.log('last');
                this.mode = 'last';
                this.segmentSwitch1 = 'segment_button_active';
                this.segmentSwitch2 = 'segment_button_inactive';
                // this.showCardData = this.objRankOfAll;
                // console.log('show ', this.showCardData)
                // if (this.objCurrentRankOfAll.length > 0) {
                //   this.rankNo = this.objCurrentRankOfAll[0].rankNo;
                //   this.highLight = this.objCurrentRankOfAll[0].userNo;
                // } else {
                //   this.rankNo = '-';
                // }
                // console.log('current rank ', this.objCurrentRankOfAll)
                this.pageTop.scrollToTop(100);
            }
            else {
                console.log('current');
                this.mode = 'current';
                this.segmentSwitch1 = 'segment_button_inactive';
                this.segmentSwitch2 = 'segment_button_active';
                // this.showCardData = this.objRankOfWeek;
                // console.log('show ', this.showCardData)
                // if (this.objCurrentRankOfWeek.length > 0) {
                //   this.rankNo = this.objCurrentRankOfWeek[0].rankNo;
                //   this.highLight = this.objCurrentRankOfWeek[0].userNo;
                // } else {
                //   this.rankNo = '-';
                // }
                // console.log('rank ', this.rankNo, ' ==> ', this.highLight)
                // console.log('chk week ', this.objCurrentRankOfWeek)
                this.pageTop.scrollToTop(100);
            }
            this.zone.run(function () {
                me.bindLeaderboardData();
            });
            this.startIndex = 1;
            this.endIndex = 1;
        }
    };
    LeaderBoardPage.prototype.onLoadRank = function (infiniteScroll) {
        console.log('e ', infiniteScroll);
        // let me = this;
        // this.startIndex = this.endIndex + 1;
        // infiniteScroll.complete();
        // if (this.mode == 'week') {
        //   this.leaderboard.getRankOfWeek(this.startIndex).then((data: any) => {
        //     console.log('week ', data)
        //     me.objRankOfWeek = data;
        //     me.showCardData = me.objRankOfWeek;
        //     me.endIndex = data.length;
        //     console.log('end index ', me.endIndex)
        //   })
        // } else {
        //   this.leaderboard.getRankOfAll(this.startIndex).then((data: any) => {
        //     console.log('all ', data);
        //     me.objRankOfAll = data;
        //     me.showCardData = me.objRankOfAll;
        //     me.endIndex = data.length;
        //   })
        // }
    };
    LeaderBoardPage.prototype.btnSelectPhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var me, actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        me = this;
                        return [4 /*yield*/, this.actionSheetController.create({
                                title: 'Select catergories',
                                buttons: [
                                    {
                                        text: 'Take Photo',
                                        handler: function () {
                                            console.log('Take Photo na ja');
                                            _this.openCamera();
                                        }
                                    }, {
                                        text: 'Select from Gallery',
                                        handler: function () {
                                            console.log('Select from Gallery');
                                            _this.openPhotoGallery();
                                        }
                                    }, {
                                        text: 'Cancel',
                                        handler: function () {
                                            console.log('Cancel');
                                        }
                                    }, {
                                        role: 'cancel',
                                        handler: function () {
                                            console.log('Cancel clicked');
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LeaderBoardPage.prototype.openCamera = function () {
        var me = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 150,
            targetHeight: 150,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            var filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            var path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            me.file.readAsDataURL(path, filename).then(function (data) {
                console.log('xx ', data);
                me.userImg = data;
            });
            console.log('src image ', imageData);
            me.uploadImageProfile(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    LeaderBoardPage.prototype.openPhotoGallery = function () {
        var me = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 150,
            targetHeight: 150,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            console.log(Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* normalizeURL */])(imageData));
            var path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            var filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            var index = filename.indexOf('?');
            if (index > -1) {
                filename = filename.substring(0, index);
            }
            me.file.readAsDataURL(path, filename).then(function (data) {
                console.log('### ', data);
                me.userImg = data;
            });
            me.uploadImageProfile(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    LeaderBoardPage.prototype.uploadImageProfile = function (imageData) {
        var _this = this;
        var me = this;
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: this.config.userNo,
            fileName: 'profile',
            httpMethod: 'POST',
            headers: {
                'Content-Type': undefined
            }
        };
        var loader = this.loadingCtrl.create({
            content: "Uploading...",
        });
        loader.present();
        fileTransfer.upload(imageData, encodeURI(this.config.serverUploadAPI), options)
            .then(function (data) {
            loader.dismiss();
        }, function (err) {
            loader.dismiss();
            var _alert = _this.alertCtrl.create({
                title: 'Fail Upload',
                buttons: ['Dismiss']
            });
            _alert.present();
            console.log(err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('pageTop'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], LeaderBoardPage.prototype, "pageTop", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], LeaderBoardPage.prototype, "content", void 0);
    LeaderBoardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-leader-board',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/leader-board/leader-board.html"*/'<div class="fixedHeader topHeader">\n  <div class="imageHeader">\n    <div class="headerPadding">\n      <ion-row no-margin no-padding class="marginRow">\n        <ion-col col-4 col no-margin no-padding class="center-left">\n          <div class="imageChallenge center-center">\n            <!-- <img\n              src="assets/imgs/icons/icon_camera.png"\n              class="positionIcon"\n              (click)="btnSelectPhoto()"\n            /> -->\n\n            <div class="avatarProfileWidth">\n              <img src="{{userImg}}" />\n            </div>\n          </div>\n        </ion-col>\n\n        <ion-col col-8 no-margin no-padding class="center-left">\n          <div class="marginLeft">\n            <div class="textDetailChallenge">\n              Username : <span class="textPoint">{{userName}}</span>\n            </div>\n\n            <div class="textDetailChallenge">\n              Rank : <span class="textPoint">{{rankNo}}</span>\n            </div>\n\n            <div class="textDetailChallenge">\n              Total smile point : <span class="textPoint">{{smilePoint}}</span>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n  <div class="topCurve">\n    <div class="segmentCenter">\n      <div class="segmentBg">\n        <ion-grid no-padding no-margin>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin (click)="btnSegment(1)">\n              <div [ngClass]="segmentSwitch1">last week</div>\n            </ion-col>\n            <ion-col no-padding no-margin (click)="btnSegment(2)">\n              <div [ngClass]="segmentSwitch2">this week</div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n\n    <ion-row style="margin-left:1.6em;margin-right:1.6em">\n      <ion-col col-6 class="center-left">\n        <div class="customActionSheetGroup" (click)="presentActionSheetGroup()">\n          <ion-row>\n            <ion-col class="center-center"\n              ><span class="displayTextInside"\n                >{{actionSheetGroup}}</span\n              ></ion-col\n            >\n            <!-- <ion-col col-1></ion-col>\n            <ion-col col-2 class="center-center">\n              <img src="assets/imgs/icons/icon_arrow-down.png" class="iconArrowDown" />\n            </ion-col>\n            <ion-col col-8 class="center-center"><span class="displayTextInside">{{actionSheetGroup}}</span></ion-col> -->\n          </ion-row>\n        </div>\n      </ion-col>\n\n      <ion-col col-6 class="center-right">\n        <div class="customActionSheetIcons" (click)="presentActionSheetSports()">\n          <ion-row>\n\n            <ion-col col-1></ion-col>\n            <ion-col col-3 class="center-center">\n              <img src="assets/imgs/icons/icon_arrow-down.png" class="iconArrowDown" />\n            </ion-col>\n            <ion-col col-7 class="center-center">\n              <img\n              src="assets/imgs/icon_svg/{{actionSheetSportIcon}}"\n              class="iconSportTypes"\n            />\n            </ion-col>\n\n          </ion-row>\n        </div>\n      </ion-col>\n\n    </ion-row>\n  </div>\n</div>\n\n<ion-content fullscreen #mycontent class="mainContent" #pageTop>\n  <div [ngClass]="showCardPadding">\n    <ion-card [ngClass]="showCardRadius">\n      <ion-list *ngFor="let record of showCardData;let i= index">\n        <ion-item [ngClass]="record.userNo == highLight?\'active-item-color\':\'\'">\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin col-2>\n              <div class="textRankBg center-center">\n                <span\n                  class="textRank"\n                  [ngClass]="record.userNo == highLight?\'textRankWhite\':\'textRank\'"\n                  >{{record.rankNo}}</span\n                >\n              </div>\n            </ion-col>\n\n            <ion-col no-padding no-margin col-2>\n              <div class="center-center">\n                <div class="avatarWidth">\n                  <img src="{{record.userImg}}" />\n                </div>\n              </div>\n            </ion-col>\n\n            <ion-col no-padding no-margin col-3 style="align-self: center;">\n              <div\n                class="center-center"\n                style="text-align: -webkit-left;\n                position: absolute;\n                margin-left: 1em;\n                margin-top: -0.8em;"\n                [ngClass]="record.userNo == highLight?\'textNameWhite\':\'textName\'"\n              >\n                {{record.userName}}\n              </div>\n            </ion-col>\n            <ion-col no-padding no-margin col-5 style="align-self: center;">\n              <div\n                style="text-align: -webkit-right;\n                margin-right: 1em;"\n                [ngClass]="record.userNo == highLight?\'textDataWhite\':\'textData\'"\n              >\n                {{record.total}}\n                <!-- {{record.user_status}} -->\n              </div>\n            </ion-col>\n          </ion-row>\n          <div class="lineSeperate" *ngIf="i != (showCardData.length)-1"></div>\n          <div\n            class="lineSeperateNone"\n            *ngIf="i == (showCardData.length)-1"\n          ></div>\n        </ion-item>\n      </ion-list>\n      <!-- <ion-infinite-scroll (ionInfinite)="onLoadRank($event)">\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n      </ion-infinite-scroll>  -->\n    </ion-card>\n  </div>\n</ion-content>\n\n<!-- \n<ion-content fullscreen #mycontent class="mainContent" #pageTop>\n  <div [ngClass]="showCardPadding" (swipe)="swipe($event)">\n    <ion-card [ngClass]="showCardRadius">\n      <ion-list *ngFor="let record of showCardData;let i= index" (swipe)="swipe($event)">\n        <ion-item [ngClass]="record.userNo == highLight?\'active-item-color\':\'\'">\n          <ion-row no-padding no-margin>\n\n            <ion-col no-padding no-margin col-2>\n              <div class="center-center">\n                <img src="{{record.userImg}}" class="avatarWidth">\n              </div>\n            </ion-col>\n\n            <ion-col no-padding no-margin col-8>\n              <div class="vbox">\n                <div class="flex1">\n                  <div class="textName bottom-left" [ngClass]="record.userNo == highLight?\'textNameWhite\':\'textName\'">\n                    {{record.userName}}</div>\n                </div>\n                <div class="flex1">\n                  <div class="textData" [ngClass]="record.userNo == highLight?\'textDataWhite\':\'textData\'">\n                    {{record.totalTxt}}{{record.user_status}}</div>\n                </div>\n              </div>\n            </ion-col>\n\n            <ion-col no-padding no-margin col-2>\n              <div class="textRankBg center-center">\n                <span class="textRank"\n                  [ngClass]="record.userNo == highLight?\'textRankWhite\':\'textRank\'">{{record.rankNo}}</span>\n              </div>\n            </ion-col>\n\n          </ion-row>\n          <div class="lineSeperate" *ngIf="i != (showCardData.length)-1"></div>\n          <div class="lineSeperateNone" *ngIf="i == (showCardData.length)-1"></div>\n\n        </ion-item>\n      </ion-list>\n       <ion-infinite-scroll (ionInfinite)="onLoadRank($event)">\n          <ion-infinite-scroll-content></ion-infinite-scroll-content>\n        </ion-infinite-scroll> \n    </ion-card>\n  </div>\n</ion-content>\n-->\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/leader-board/leader-board.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_3__providers_leader_board_leader_board__["a" /* LeaderBoardProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]])
    ], LeaderBoardPage);
    return LeaderBoardPage;
}());

//# sourceMappingURL=leader-board.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaderBoardProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LeaderBoardProvider = /** @class */ (function () {
    function LeaderBoardProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello LeaderBoardProvider Provider');
    }
    LeaderBoardProvider.prototype.getProfile = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getProfile', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    LeaderBoardProvider.prototype.getLeaderboard = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getLeaderType', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    LeaderBoardProvider.prototype.getCurrentRankOfWeek = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getCurrentRankOfWeek', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    LeaderBoardProvider.prototype.getRankOfWeek = function (currentIndex) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('startIndex', currentIndex);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getRankOfWeek', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    LeaderBoardProvider.prototype.getRankOfAll = function (currentIndex) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('startIndex', currentIndex);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getRankOfAll', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    LeaderBoardProvider.prototype.getCurrentRankOfAll = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getCurrentRankOfAll', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    LeaderBoardProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], LeaderBoardProvider);
    return LeaderBoardProvider;
}());

//# sourceMappingURL=leader-board.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeDashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__challenge_create_challenge_create__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__challenge_join_challenge_join__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_challenge_dashboard_challenge_dashboard__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_barcode_scanner__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ChallengeDashboardPage = /** @class */ (function () {
    function ChallengeDashboardPage(config, loadingCtrl, zone, barcodeScanner, navCtrl, navParams, app, setting, alertCtrl, challengeDashboard) {
        this.config = config;
        this.loadingCtrl = loadingCtrl;
        this.zone = zone;
        this.barcodeScanner = barcodeScanner;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.setting = setting;
        this.alertCtrl = alertCtrl;
        this.challengeDashboard = challengeDashboard;
        this.segmentSwitch1 = "segment_button_active";
        this.segmentSwitch2 = "segment_button_inactive";
        this.segmentSwitch3 = "segment_button_inactive";
        this.modeType = '';
        var me = this;
        var loader = me.loadingCtrl.create({
            spinner: "crescent"
        });
        loader.present();
        this.challengeDashboard.getChallengeDashboard().then(function (data) {
            loader.dismiss();
            console.log('upcoming ', data.objUpComing);
            console.log('onGoing ', data.objOnGoing);
            console.log('History ', data.objHistory);
            // me.objChallenge = data.objChallenge;
            // me.objCompetition = data.objCompetition;
            me.objUpComing = data.objUpComing;
            me.objOnGoing = data.objOnGoing;
            me.objHistory = data.objHistory;
            me.bindChallengeDashboardData();
        });
    }
    ChallengeDashboardPage.prototype.bindChallengeDashboardData = function () {
        var me = this;
        if (this.setting.activeSegment == undefined) {
            if (me.objUpComing.length > 0) {
                console.log("case 1 ", me.objUpComing.length);
                // me.setting.activeSegment = 2;
                me.btnSegment(3);
                me.modeType = 'UpComing';
                // me.showCardData=me.objChallenge;
            }
            else if (me.objOnGoing.length > 0) {
                console.log("case 2");
                // me.setting.activeSegment = 1;
                me.btnSegment(2);
                me.modeType = 'OnGoing';
            }
            else {
                console.log("case 3");
                me.setting.activeSegment = 1;
                me.modeType = 'History';
            }
        }
        else {
            me.btnSegment(me.setting.activeSegment);
        }
    };
    ChallengeDashboardPage.prototype.ionViewDidEnter = function () {
        console.log("enter");
        var me = this;
        if (this.config.refreshChallengeDashboard == true) {
            var loader_1 = me.loadingCtrl.create({
                spinner: "crescent"
            });
            loader_1.present();
            this.challengeDashboard.getChallengeDashboard().then(function (data) {
                me.config.refreshChallengeDashboard = false;
                loader_1.dismiss();
                console.log(data.objUpComing);
                console.log(data.objOnGoing);
                // me.objChallenge = data.objChallenge;
                // me.objCompetition = data.objCompetition;
                me.objUpComing = data.objUpComing;
                me.objOnGoing = data.objOnGoing;
                me.bindChallengeDashboardData();
            });
        }
    };
    ChallengeDashboardPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ChallengeDashboardPage");
        // this.segmentSwitch1 = 'segment_button_active';
        // this.segmentSwitch2 = 'segment_button_inactive';
        // this.btnSegment(this.setting.activeSegment);
        // console.log(this.setting.activeSegment);
        // if (this.setting.activeSegment == 1 || this.setting.activeSegment == undefined) {
        //   this.showCardData = this.objCompetition;
        // } else {
        //   this.showCardData = this.objChallenge;
        // }
    };
    // swipe(e) {
    //   if (e.direction == 4) {
    //     console.log(e.direction);
    //     // if (this.scrollStatus == false) {
    //     this.navCtrl.parent.select(2);
    //     // }
    //   } else if (e.direction == 2) {
    //     this.navCtrl.parent.select(0);
    //   }
    // }
    ChallengeDashboardPage.prototype.btnSegment = function (index) {
        console.log(index);
        if (index == 1) {
            // console.log('xxx 1')
            this.segmentSwitch1 = "segment_button_active";
            this.segmentSwitch2 = "segment_button_inactive";
            this.segmentSwitch3 = "segment_button_inactive";
            this.showCardData = this.objHistory;
            this.zone.run(function () { });
            this.setting.activeSegment = index;
        }
        else if (index == 2) {
            // console.log('xxx 2')
            this.segmentSwitch1 = "segment_button_inactive";
            this.segmentSwitch2 = "segment_button_active";
            this.segmentSwitch3 = "segment_button_inactive";
            this.showCardData = this.objOnGoing;
            // console.log('==> ', this.showCardData)
            this.zone.run(function () { });
            // this.showCardData = this.mockupData_challenge;
            this.setting.activeSegment = index;
        }
        else {
            this.segmentSwitch1 = "segment_button_inactive";
            this.segmentSwitch2 = "segment_button_inactive";
            this.segmentSwitch3 = "segment_button_active";
            this.showCardData = this.objUpComing;
            this.zone.run(function () { });
        }
    };
    ChallengeDashboardPage.prototype.btnCreateChallenge = function () {
        console.log("create challenge");
        this.app
            .getRootNav()
            .setRoot(__WEBPACK_IMPORTED_MODULE_3__challenge_create_challenge_create__["a" /* ChallengeCreatePage */], {}, { animate: true, direction: "forward" });
    };
    ChallengeDashboardPage.prototype.btnReject = function (record) {
        console.log("btnReject");
        var confirm = this.alertCtrl.create({
            title: "Reject this group",
            message: record.challenge_name,
            buttons: [
                {
                    text: "No"
                },
                {
                    text: "Yes",
                    handler: function () {
                        console.log("btnReject");
                    }
                }
            ]
        });
        confirm.present();
    };
    ChallengeDashboardPage.prototype.btnJoin = function (record) {
        console.log("btnJoin", record);
        var me = this;
        var confirm = this.alertCtrl.create({
            title: "Join this group",
            message: record.challenge_name,
            buttons: [
                {
                    text: "No"
                },
                {
                    text: "Yes",
                    handler: function () {
                        console.log("btnJoin");
                        me.challengeDashboard
                            .joinChallenge(record.challengeNo)
                            .then(function (data) {
                            console.log(data);
                            if (data == 1) {
                                me.app
                                    .getRootNav()
                                    .setRoot(__WEBPACK_IMPORTED_MODULE_4__challenge_join_challenge_join__["a" /* ChallengeJoinPage */], { statusJoin: true, passRecord: record }, { animate: true, direction: "forward" });
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    ChallengeDashboardPage.prototype.btnShowDetail = function (record) {
        console.log("btnJoinDetail", record);
        var status = record.joinStatus != "pending" ? true : false;
        console.log("status ", status);
        this.app
            .getRootNav()
            .setRoot(__WEBPACK_IMPORTED_MODULE_4__challenge_join_challenge_join__["a" /* ChallengeJoinPage */], { statusJoin: status, showType: this.modeType, passRecord: record }, { animate: true, direction: "forward" });
    };
    ChallengeDashboardPage.prototype.btnJoinDetail = function (record) {
        console.log("btnJoinDetail", record);
        var status = record.joinStatus != "pending" ? true : false;
        console.log("status ", status);
        this.app
            .getRootNav()
            .setRoot(__WEBPACK_IMPORTED_MODULE_4__challenge_join_challenge_join__["a" /* ChallengeJoinPage */], { statusJoin: status, showType: "challenge", passRecord: record }, { animate: true, direction: "forward" });
    };
    ChallengeDashboardPage.prototype.btnCompetitionDetail = function (record) {
        console.log("btnCompetitionDetail", record);
        this.app
            .getRootNav()
            .setRoot(__WEBPACK_IMPORTED_MODULE_4__challenge_join_challenge_join__["a" /* ChallengeJoinPage */], { statusJoin: true, showType: "competition", passRecord: record }, { animate: true, direction: "forward" });
    };
    ChallengeDashboardPage.prototype.btnJoinQrcode = function () {
        var _this = this;
        var me = this;
        console.log("====================================");
        console.log("join qr code");
        console.log("====================================");
        this.barcodeScanner
            .scan()
            .then(function (barcodeData) {
            console.log("Barcode data", barcodeData);
            console.log("barcode data ", barcodeData.text);
            me.challengeDashboard
                .addChallengeMember(barcodeData.text)
                .then(function (status) {
                console.log("status ", status);
                if (status == 1) {
                    me.challengeDashboard.getChallengeList().then(function (data) {
                        console.log("challenge ", data);
                        _this.segmentSwitch1 = "segment_button_inactive";
                        _this.segmentSwitch2 = "segment_button_active";
                        me.setting.activeSegment = 2;
                        me.objChallenge = data;
                        me.showCardData = data;
                    });
                }
            });
        })
            .catch(function (err) {
            console.log("Error", err);
        });
    };
    ChallengeDashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: "page-challenge-dashboard",template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-dashboard/challenge-dashboard.html"*/'<div class="fixedHeader topHeader">\n  <div class="topCurveChallenge">\n    <div class="segmentCenter">\n      <div class="segmentBg">\n        <ion-grid no-padding no-margin>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin (click)="btnSegment(1)">\n              <div [ngClass]="segmentSwitch1">History</div>\n            </ion-col>\n            <ion-col no-padding no-margin (click)="btnSegment(2)">\n              <div [ngClass]="segmentSwitch2">Ongoing</div>\n            </ion-col>\n            <ion-col no-padding no-margin (click)="btnSegment(3)">\n              <div [ngClass]="segmentSwitch3">Upcoming</div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n  </div>\n</div>\n\n<ion-content class="mainContent">\n  <div class="cardContentPadding">\n    <div class="titleChallengeSegment">Your Challenges</div>\n\n    <div class="imageRankMain" *ngFor="let record of showCardData;let i= index">\n      <div\n        class="triangle-topright"\n        *ngIf="segmentSwitch1 == \'segment_button_active\'"\n      ></div>\n      <div\n        class="positionRank"\n        *ngIf="segmentSwitch1 == \'segment_button_active\'"\n      >\n        <span\n          class="textRank center-center"\n          *ngIf="segmentSwitch1 == \'segment_button_active\'"\n        >\n          {{ record.rank === null ? \'324\' : record.rank }}\n        </span>\n      </div>\n\n      <ion-card class="cardPaddingChallenge" (click)="btnShowDetail(record)">\n        <ion-row no-margin no-padding>\n          <ion-col col no-margin no-padding col-3>\n            <img src="{{record.challengeImage}}" class="imageChallengeWidth" />\n          </ion-col>\n          <ion-col col no-margin no-padding col-9>\n            <ion-row no-margin no-padding>\n              <ion-col col no-margin no-padding>\n                <div class="textHeaderChallenge">\n                  {{record.challengeName}}\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row no-margin no-padding class="marginRow">\n              <ion-col col no-margin no-padding>\n                <span class="textDetailChallenge">Hosted by</span>\n                <span class="textDetailCreateBy">{{record.createBy}}</span>\n\n                <div class="textDetailChallenge">\n                  {{record.allMember}} members\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n\n        <div class="lineSeparate"></div>\n\n        <ion-row no-margin no-padding>\n          <ion-col col no-margin no-padding col-5>\n            <div class="textDetailChallenge">\n              REWARD\n            </div>\n            <div class="textDetailOwner">\n              {{record.rewardTxt}}\n            </div>\n          </ion-col>\n          <ion-col col-7 col no-margin no-padding>\n            <div class="textDetailChallenge">\n              PERIOD\n            </div>\n            <div class="textDetailOwner">\n              {{record.startDate}} - {{record.endDate}}\n            </div>\n          </ion-col>\n        </ion-row>\n        <div\n          class="marginRow"\n          *ngIf="segmentSwitch2 == \'segment_button_active\'"\n        ></div>\n        <ion-row\n          no-margin\n          no-padding\n          *ngIf="segmentSwitch2 == \'segment_button_active\' && record.ownerStatus==\'N\' && record.joinStatus==\'N\'"\n        >\n          <ion-col col no-margin no-padding>\n            <div class="buttonReject center-center" (click)="btnReject(record)">\n              <div>Reject</div>\n            </div>\n          </ion-col>\n          <ion-col col no-margin no-padding class="">\n            <div class="buttonJoin center-center" (click)="btnJoin(record)">\n              <div>Join</div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </div>\n  </div>\n  <!-- <ion-fab right center #fab>\n    <button ion-fab color="pink" mini (click)="btnCreateChallenge()">\n      <ion-icon name="md-add"></ion-icon>\n    </button>\n  </ion-fab> -->\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-dashboard/challenge-dashboard.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_0__providers_settings_settings__["a" /* SettingsProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_challenge_dashboard_challenge_dashboard__["a" /* ChallengeDashboardProvider */]])
    ], ChallengeDashboardPage);
    return ChallengeDashboardPage;
}());

//# sourceMappingURL=challenge-dashboard.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_challenge_create_challenge_create__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var ChallengeCreatePage = /** @class */ (function () {
    function ChallengeCreatePage(config, loadingCtrl, camera, transfer, file, challengeCreate, navCtrl, navParams, actionSheetController, alertCtrl) {
        this.config = config;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.challengeCreate = challengeCreate;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetController = actionSheetController;
        this.alertCtrl = alertCtrl;
        this.txtChallengeName = '';
        // txtConditionStep: string = '';
        // txtConditionHeartRate: string = '';
        this.txtCondition = '';
        this.txtReward = '';
        this.imgChallenge = 'assets/imgs/icons/group_challenge.png';
        this.objCondition = '';
    }
    ChallengeCreatePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChallengeCreatePage');
        this.actionSheetSelected = "Steps";
        // today
        this.startDate = new Date().toISOString();
        console.log(this.startDate);
        // tomorrow
        this.endDate = this.startDate;
    };
    ChallengeCreatePage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
    };
    ChallengeCreatePage.prototype.btnSaveChallenge = function () {
        console.log(this.startDate);
        var me = this;
        //this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
        if (this.txtChallengeName != '' && this.startDate != undefined && this.endDate != undefined
            && this.objCondition != '' && this.txtCondition != '') {
            var conditionStep = '0';
            var conditionHR = '0';
            var conditionDistance = '0';
            switch (me.objCondition) {
                case 'HR': {
                    conditionHR = this.txtCondition;
                    break;
                }
                case 'Distance': {
                    conditionDistance = this.txtCondition;
                }
                case 'Steps': {
                    conditionStep = this.txtCondition;
                }
            }
            this.txtReward = this.txtReward == '' ? '-' : this.txtReward;
            this.challengeCreate.create(this.txtChallengeName, conditionStep, conditionHR, conditionDistance, this.txtReward, this.startDate, this.endDate).then(function (data) {
                console.log('result create challenge ', data);
                //check change image ? call upload : nothing
                //call upload image
                if (data != '' && me.objImageData != undefined)
                    me.uploadImageChallenge(data);
                if (data != '')
                    me.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
            });
        }
        else {
            console.log('empty');
            var alert_1 = this.alertCtrl.create({
                title: 'Create Challenge',
                message: 'please insert information',
                buttons: [
                    {
                        text: 'YES'
                    }
                ]
            });
            alert_1.present();
        }
    };
    ChallengeCreatePage.prototype.updateActionSheetSelected = function (value) {
        if (value === 1) {
            this.actionSheetSelected = 'Steps';
        }
        else if (value === 2) {
            this.actionSheetSelected = 'Exercise (Overall)';
        }
        else if (value === 3) {
            this.actionSheetSelected = 'Biking';
        }
        else if (value === 4) {
            this.actionSheetSelected = 'Running';
        }
        else if (value === 5) {
            this.actionSheetSelected = 'Swimming';
        }
        else if (value === 6) {
            this.actionSheetSelected = 'Other';
        }
    };
    ChallengeCreatePage.prototype.presentActionSheet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            title: 'Select catergories',
                            buttons: [{
                                    text: 'Steps',
                                    role: 'destructive',
                                    icon: 'steps',
                                    // cssClass: "share actionSheet_withIcomoon",
                                    handler: function () {
                                        _this.updateActionSheetSelected(1);
                                    }
                                }, {
                                    text: 'Exercise (Overall)',
                                    icon: 'exercise',
                                    handler: function () {
                                        _this.updateActionSheetSelected(2);
                                    }
                                }, {
                                    text: 'Biking',
                                    icon: 'bike',
                                    handler: function () {
                                        _this.updateActionSheetSelected(3);
                                    }
                                }, {
                                    text: 'Running',
                                    icon: 'running',
                                    handler: function () {
                                        _this.updateActionSheetSelected(4);
                                    }
                                }, {
                                    text: 'Swimming',
                                    icon: 'swimming',
                                    handler: function () {
                                        _this.updateActionSheetSelected(5);
                                    }
                                }, {
                                    text: 'Other',
                                    icon: 'other',
                                    handler: function () {
                                        _this.updateActionSheetSelected(6);
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ChallengeCreatePage.prototype.btnSelectPhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            title: 'Select method take photo',
                            buttons: [{
                                    text: 'Take Photo',
                                    role: 'destructive',
                                    handler: function () {
                                        console.log('Take Photo xxx');
                                        _this.openCamera();
                                    }
                                }, {
                                    text: 'Select from Gallery',
                                    handler: function () {
                                        console.log('Select from Gallery');
                                        _this.openPhotoGallery();
                                    }
                                }, {
                                    text: 'Cancel',
                                    handler: function () {
                                        console.log('Cancel');
                                    }
                                }, {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ChallengeCreatePage.prototype.openCamera = function () {
        var me = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 150,
            targetHeight: 150,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            me.objImageData = imageData;
            var filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            var path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            me.file.readAsDataURL(path, filename).then(function (data) {
                console.log('xx ', data);
                me.imgChallenge = data;
            });
            // console.log('src image ', imageData)
            // me.uploadImageProfile(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    ChallengeCreatePage.prototype.openPhotoGallery = function () {
        var me = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 150,
            targetHeight: 150,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            me.objImageData = imageData;
            var path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
            var filename = imageData.substring(imageData.lastIndexOf('/') + 1);
            var index = filename.indexOf('?');
            if (index > -1) {
                filename = filename.substring(0, index);
            }
            me.file.readAsDataURL(path, filename).then(function (data) {
                console.log('### ', data);
                me.imgChallenge = data;
            });
            // me.uploadImageProfile(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    ChallengeCreatePage.prototype.uploadImageChallenge = function (challengeNo) {
        var _this = this;
        var me = this;
        console.log('upload challenge ', me.objImageData);
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: challengeNo,
            fileName: 'challenge',
            httpMethod: 'POST',
            headers: {
                'Content-Type': undefined
            }
        };
        var loader = this.loadingCtrl.create({
            content: "Uploading...",
        });
        loader.present();
        fileTransfer.upload(me.objImageData, encodeURI(this.config.serverUploadAPI), options)
            .then(function (data) {
            loader.dismiss();
        }, function (err) {
            loader.dismiss();
            var _alert = _this.alertCtrl.create({
                title: 'Fail Upload',
                buttons: ['Dismiss']
            });
            _alert.present();
            console.log(err);
        });
    };
    ChallengeCreatePage.prototype.onConditionChange = function (e) {
        console.log('c', e);
        this.objCondition = e;
    };
    ChallengeCreatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-challenge-create',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-create/challenge-create.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>Group challenge details</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n\n  <div class="imageHeader center-center">\n    <div>\n      <div class="imageChallenge center-center" (click)="btnSelectPhoto()">\n        <img src="assets/imgs/icons/icon_camera.png" class="positionIcon">\n        <img src="{{imgChallenge}}" class="avatarProfileWidth">\n      </div>\n    </div>\n  </div>\n\n</ion-header>\n\n\n<ion-content class="mainContent">\n\n\n\n  <div class="bodyPadding">\n    <div class="inputStyle">\n      <div class="lableForm datePadding">Group name</div>\n      <ion-item>\n        <ion-input type="text" [(ngModel)]="txtChallengeName" maxlength="20" placeholder="Please enter" clearInput>\n        </ion-input>\n      </ion-item>\n    </div>\n\n    <div class="inputStyle">\n      <div class="lableForm datePadding">Challenge mode</div>\n      <ion-item>\n        <ion-row no-padding no-margin>\n          <ion-col class="center-center" no-padding no-margin>\n            <div class="customActionSheetChallenge" (click)="presentActionSheet()">\n              <ion-row no-padding no-margin>\n                <ion-col col-9 class="center-left marginColumn">{{actionSheetSelected}}</ion-col>\n                <ion-col col-2 class="center-right" no-padding no-margin><img\n                    src="assets/imgs/icons/icon_arrow-down.png" class="iconArrowDown">\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </div>\n\n    <div class="inputStyle">\n      <div class="lableForm datePadding">Challenge start</div>\n      <ion-item>\n        <ion-datetime [(ngModel)]="startDate" displayFormat="DDDD DD MMM YYYY" pickerFormat="DDDD MMM YYYY">\n        </ion-datetime>\n      </ion-item>\n    </div>\n\n\n    <div class="inputStyle">\n      <div class="lableForm datePadding">Challenge ends</div>\n      <ion-item>\n        <ion-datetime [(ngModel)]="endDate" displayFormat="DDDD DD MMM YYYY" pickerFormat="DDDD MMM YYYY">\n        </ion-datetime>\n      </ion-item>\n    </div>\n\n    <div class="inputStyle">\n      <div class="lableForm datePadding">Members</div>\n      <ion-item>\n        <ion-input type="text" [(ngModel)]="txtChallengeName" maxlength="20" placeholder="3 people"\n          clearInput>\n        </ion-input>\n      </ion-item>\n    </div>\n\n    <div class="inputStyle">\n        <div class="lableForm datePadding">Invite by link or QR code</div>\n        <ion-item>\n          <ion-input type="text" [(ngModel)]="txtChallengeName" maxlength="20" placeholder="Create Link & QR code"\n            clearInput>\n          </ion-input>\n        </ion-item>\n      </div>\n\n      <div class="inputStyle">\n          <div class="lableForm datePadding">Short description</div>\n          <ion-item>\n            <ion-textarea rows="5" [(ngModel)]="txtChallengeName" maxlength="20" placeholder="Please add your friends"\n              clearInput>\n            </ion-textarea>\n          </ion-item>\n        </div>\n\n    <div class="footerPadding">\n      <div class="saveButton center-center" (click)="btnSaveChallenge()">\n        <div class="textSave">Finish</div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-create/challenge-create.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_3__providers_challenge_create_challenge_create__["a" /* ChallengeCreateProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ChallengeCreatePage);
    return ChallengeCreatePage;
}());

//# sourceMappingURL=challenge-create.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeCreateProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChallengeCreateProvider = /** @class */ (function () {
    function ChallengeCreateProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello ChallengeCreateProvider Provider');
    }
    ChallengeCreateProvider.prototype.create = function (name, step, HR, distance, reward, startDate, endDate) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('challengeType', "88");
        params.set('challengeName', name);
        params.set('conditionStep', step);
        params.set('conditionHeartRate', HR);
        params.set('conditionDistance', distance);
        params.set('rewardTxt', reward);
        params.set('startDate', startDate);
        params.set('endDate', endDate);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'createChallenge', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeCreateProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], ChallengeCreateProvider);
    return ChallengeCreateProvider;
}());

//# sourceMappingURL=challenge-create.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeJoinPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__qr_code_qr_code__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_challenge_join_challenge_join__ = __webpack_require__(397);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChallengeJoinPage = /** @class */ (function () {
    function ChallengeJoinPage(challengeJoin, navCtrl, navParams, popoverCtrl, modalCtrl, alertCtrl) {
        this.challengeJoin = challengeJoin;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.segmentSwitch1 = 'segment_button_active';
        this.segmentSwitch2 = 'segment_button_inactive';
        var me = this;
        this.showType = this.navParams.get('showType');
        console.log('show type ', this.showType);
        // this for test button joined
        // this.joinStatus = true;
        // this.joinStatus = false;
        this.joinStatus = this.navParams.get('statusJoin');
        this.record = this.navParams.get('passRecord');
        console.log('status ', this.joinStatus);
        console.log('record ', this.record);
        if (this.showType == 'UpComimg') {
            this.txtDate = 'Beginning in';
            this.challengeJoin.getChallengeDetail(this.record.challengeNo).then(function (data) {
                console.log('xx ', data);
                // debugger;
                me.objChallengeDetail = data;
            });
        }
        else {
            this.txtDate = 'Remaining';
            this.challengeJoin.getCompetitionDetail(this.record.challengeNo).then(function (data) {
                console.log('xxx ', data);
                me.objChallengeDetail = data;
            });
        }
    }
    ChallengeJoinPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChallengeJoinPage');
    };
    ChallengeJoinPage.prototype.btnBack = function () {
        console.log('b');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
    };
    ChallengeJoinPage.prototype.btnReject = function () {
        var _this = this;
        console.log('btnReject');
        var confirm = this.alertCtrl.create({
            title: 'Reject this challenge',
            message: this.record.challengeName,
            buttons: [
                {
                    text: 'No'
                },
                {
                    text: 'Yes',
                    handler: function () {
                        console.log('btnReject');
                        _this.challengeJoin.rejectChallenge(_this.record.challengeNo).then(function (data) {
                            console.log(data);
                            if (data == 1) {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    ChallengeJoinPage.prototype.btnJoin = function () {
        var _this = this;
        console.log('btnJoin', this.record);
        var me = this;
        var confirm = this.alertCtrl.create({
            title: 'Join this challenge',
            message: this.record.challengeName,
            buttons: [
                {
                    text: 'No'
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.challengeJoin.joinChallenge(_this.record.challengeNo).then(function (data) {
                            console.log(data);
                            if (data == 1) {
                                _this.joinStatus = true;
                                _this.challengeJoin.getChallengeDetail(_this.record.challengeNo).then(function (data) {
                                    console.log('xx ', data);
                                    me.objChallengeDetail = data;
                                });
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    ChallengeJoinPage.prototype.btnCancelJoin = function () {
        // this.joinStatus = false;
    };
    // btnShare(myEvent) {
    //   let popover = this.popoverCtrl.create(PopoverShareComponent);
    //   popover.present({
    //     ev: myEvent
    //   });
    //   let _urlQRCode = this.record.qrCode;
    //   popover.onDidDismiss(data => {
    //     console.log(data);
    //     if (data == 'qr') {
    //       console.log('QR ', _urlQRCode);
    //       const profileModal = this.modalCtrl.create(QrCodePage, { achievedType: 'generate_qr', urlQRCode: _urlQRCode });
    //       profileModal.present();
    //     } else if (data == 'email') {
    //       console.log('Email');
    //     }
    //   });
    // }
    ChallengeJoinPage.prototype.btnShare = function (myEvent) {
        var _urlQRCode = this.record.qrCode;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__qr_code_qr_code__["a" /* QrCodePage */], { achievedType: 'generate_qr', urlQRCode: _urlQRCode });
        profileModal.present();
    };
    ChallengeJoinPage.prototype.btnSegment = function (index) {
        if (index == 1) {
            this.segmentSwitch1 = 'segment_button_active';
            this.segmentSwitch2 = 'segment_button_inactive';
        }
        else {
            this.segmentSwitch1 = 'segment_button_inactive';
            this.segmentSwitch2 = 'segment_button_active';
        }
    };
    ChallengeJoinPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-challenge-join',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-join/challenge-join.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>{{record.challengeName}}</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <!-- <ion-buttons right>\n      <button ion-button icon-only (click)="btnShare($event)">\n        <ion-icon name="share"></ion-icon>\n      </button>\n\n      <button ion-button icon-only (click)="btnShare($event)">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-toolbar>\n\n  <div class="topHeader">\n    <div class="topCurveChallenge"></div>\n    <div class="cardFloat">\n      <ion-card class="cardPaddingChallenge">\n        <ion-row no-margin no-padding>\n          <ion-col col no-margin no-padding col-3>\n            <img src="{{record.challengeImage}}" class="imageChallengeWidth" />\n          </ion-col>\n          <ion-col col no-margin no-padding col-9>\n            <ion-row no-margin no-padding>\n              <ion-col col no-margin no-padding>\n                <div class="textHeaderChallenge">\n                  {{record.challengeName}}\n                </div>\n              </ion-col>\n            </ion-row>\n\n            <ion-row no-margin no-padding class="marginRow">\n              <ion-col col no-margin no-padding>\n                <span class="textDetailChallenge">Hosted by</span>\n                <span class="textDetailCreateBy">{{record.createBy}}</span>\n\n                <div class="textDetailChallenge">\n                  {{record.allMember}} members\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n\n        <ion-row no-margin no-padding>\n          <ion-col col no-margin no-padding col-5>\n            <div class="textDetailChallenge">\n              BEGINNING IN\n            </div>\n            <div class="textDetailOwner">\n              {{record.startDate}}\n            </div>\n          </ion-col>\n          <ion-col col-3 col no-margin no-padding></ion-col>\n          <ion-col col-4 col no-margin no-padding>\n            <div class="textDetailChallenge">\n              END DATE\n            </div>\n            <div class="textDetailOwner">\n              {{record.endDate}}\n            </div>\n          </ion-col>\n        </ion-row>\n\n        <ion-row\n          no-margin\n          no-padding\n          class="buttonMarginTop"\n          *ngIf="!joinStatus"\n        >\n          <ion-col col no-margin no-padding class="center-center">\n            <div class="buttonReject center-center" (click)="btnReject(record)">\n              <div>Reject</div>\n            </div>\n          </ion-col>\n          <ion-col col no-margin no-padding class="center-center">\n            <div class="buttonJoin center-center" (click)="btnJoin(record)">\n              <div>Join</div>\n            </div>\n          </ion-col>\n        </ion-row>\n\n        <ion-row\n          no-margin\n          no-padding\n          class="buttonMarginTop"\n          *ngIf="joinStatus"\n        >\n          <ion-col col no-margin no-padding class="center-center">\n            <div class="buttonJoined center-center">\n              <div>Joined</div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </div>\n\n    <div class="segmentCenter">\n      <div class="segmentBg">\n        <ion-grid no-padding no-margin>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin (click)="btnSegment(1)">\n              <div [ngClass]="segmentSwitch1">Details</div>\n            </ion-col>\n            <ion-col no-padding no-margin (click)="btnSegment(2)">\n              <div [ngClass]="segmentSwitch2">Leaderboard</div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n  </div>\n</ion-header>\n\n<ion-content class="mainContent">\n  <div *ngIf="segmentSwitch2==\'segment_button_active\'">\n    <ion-list *ngFor="let record of objChallengeDetail;let i= index">\n      <ion-item [ngClass]="record.statusAchieve == \'T\'?\'highLight\':\'\'">\n        <ion-row no-padding no-margin>\n          <ion-col no-padding no-margin col-2 class="rankMarginRight">\n            <div class="textRankBg center-center">\n              <span class="textRank">{{record.rank}}</span>\n            </div>\n          </ion-col>\n          <ion-col no-padding no-margin col-2>\n            <div class="center-center">\n              <img src="{{record.userImage}}" class="avatarWidth" />\n            </div>\n          </ion-col>\n          <ion-col no-padding no-margin col-3>\n            <!-- <ion-row class="textName bottom-left"style="text-align: -webkit-left;\n          margin-left: 1em;\n          margin-top: 0.8em;">\n              {{record.userName}}\n          </ion-row>\n          <ion-row class="textData bottom-left"style="text-align: -webkit-left;\n          margin-left: 1em;">\n            {{record.lastSync}}\n          </ion-row> -->\n            <div\n              class="textName bottom-left"\n              style="text-align: -webkit-left;\n          margin-left: 1em;\n          margin-top: 0.8em;"\n            >\n              {{record.userName}}\n            </div>\n          </ion-col>\n          <ion-col no-padding no-margin col-4>\n            <!-- <div class="textData" style="text-align: -webkit-right;\n          margin-right: 1em;\n          margin-top: 0.8em;">\n            {{record.total}}<span *ngIf="showType == \'challenge\'">{{record.status}}</span>\n          </div> -->\n            <ion-row>\n              <div\n                class="textName"\n                style="text-align: -webkit-right;\n              width: -webkit-fill-available;\n              margin-top: 0.8em;"\n              >\n                {{record.total}}<span *ngIf="showType == \'challenge\'"\n                  >{{record.rank}}</span\n                >\n              </div>\n            </ion-row>\n            <ion-row>\n              <div\n                class="textData"\n                style="text-align: -webkit-right;\n              width: -webkit-fill-available;\n              font-size: 0.8em"\n              >\n                {{record.lastSync}}\n              </div>\n            </ion-row>\n          </ion-col>\n          <!-- <div class="light"></div>\n        <div class="light"></div> -->\n        </ion-row>\n        <div class="lineSeperate"></div>\n\n        <!-- </div> -->\n      </ion-item>\n    </ion-list>\n  </div>\n\n  <div *ngIf="segmentSwitch1==\'segment_button_active\'" class="paddingDetails">\n    <div class="textHeaderDetail">รายละเอียด</div>\n    <div class="textContentDetail">{{record.challengeDetail}}\n    </div>\n    <div class="textHeaderDetail">รางวัล</div>\n    <ion-card>\n      <ion-card-header><span class="textCardHeader">เงื่อนไข</span></ion-card-header>\n      <ion-card-content>\n\n        <ion-row no-margin no-padding class="cardRowMargin">\n          <ion-col col no-margin no-padding col-3>\n            <img src="{{record.rewardImage}}" class="imageCardWidth" />\n          </ion-col>\n          <ion-col col no-margin no-padding col-9>\n            <ion-row no-margin no-padding>\n              <ion-col col no-margin no-padding>\n                <div class="cardConditionHeader">\n                  {{record.rewardLine1}}\n                </div>\n              </ion-col>\n            </ion-row>\n            <ion-row no-margin no-padding class="marginRow">\n              <ion-col col no-margin no-padding>\n                <div class="cardConditionAmount">{{record.rewardLine2}}</div>\n                <ion-row no-margin no-padding>\n                  <ion-col col no-margin no-padding>\n                    <div class="cardConditionAward">\n                      {{record.rewardLine3}}\n                    </div>\n                  </ion-col>\n                  <ion-col col no-margin no-padding col-2 class="center-right">\n                    <span class="cardConditionAward"></span>\n                  </ion-col>\n                  </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n\n        <div class="lineSeparateDot"></div>\n\n\n\n        <!-- <ion-row no-margin no-padding class="cardRowMargin">\n          <ion-col col no-margin no-padding col-3>\n            <img src="{{record.challengeImage}}" class="imageCardWidth" />\n          </ion-col>\n          <ion-col col no-margin no-padding col-9>\n            <ion-row no-margin no-padding>\n              <ion-col col no-margin no-padding>\n                <div class="cardConditionHeader">\n                  ระยะทาง 30,000 กิโลเมตร\n                </div>\n              </ion-col>\n            </ion-row>\n            <ion-row no-margin no-padding class="marginRow">\n              <ion-col col no-margin no-padding>\n                <div class="cardConditionAmount">10 รางวัล</div>\n                <ion-row no-margin no-padding>\n                  <ion-col col no-margin no-padding>\n                    <div class="cardConditionAward">\n                      บัตรวิ่งเมืองไทยมาราธอน\n                    </div>\n                  </ion-col>\n                  <ion-col col no-margin no-padding col-2 class="center-right">\n                    <span class="cardConditionAward">x 1</span>\n                  </ion-col>\n                  </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>         -->\n\n      </ion-card-content>\n    </ion-card>\n\n    <div class="textHeaderDetail">เงื่อนไขโครงการ</div>\n    <div class="textContentDetail">\n      {{record.conditionDetail}}\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-join/challenge-join.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_challenge_join_challenge_join__["a" /* ChallengeJoinProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ChallengeJoinPage);
    return ChallengeJoinPage;
}());

//# sourceMappingURL=challenge-join.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QrCodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var QrCodePage = /** @class */ (function () {
    function QrCodePage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.urlQRCode = this.navParams.get('urlQRCode');
        console.log('url qrcode ', this.urlQRCode);
    }
    QrCodePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad QrCodePage');
    };
    QrCodePage.prototype.btnClose = function () {
        this.viewCtrl.dismiss();
    };
    QrCodePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-qr-code',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/qr-code/qr-code.html"*/'<ion-content no-bounce (click)="btnClose()">\n\n  <div class="complexView">\n    <div class="vbox">\n      <div class="flex1 center-center">\n        <ion-card>\n          <ion-card-content>\n\n            <div class="center-center">\n                <img src="{{urlQRCode}}" class="imgQrCode">\n              <!-- <img src="assets/imgs/generate_qrcode.png" class="imgQrCode"> -->\n            </div>\n\n            <div class="modalBottom">\n              <div class="lineSeparateCurve"></div>\n\n              <div class="center-center textTitle">\n                Scan QR code to join the challenge\n              </div>\n\n              <div class="wordingPadding">\n                <ion-row no-margin no-padding>\n                  <ion-col col-2 class="top-center" no-margin no-padding>\n                    <img src="assets/imgs/icons/icon_install.png" class="imgIcon">\n                  </ion-col>\n                  <ion-col no-margin no-padding>\n                    <div class="textInvite">Log in to Healthy app on phone</div>\n                    <div class="textDetail">Install and open Healthy app</div>\n                  </ion-col>\n                </ion-row>\n\n                <ion-row no-margin no-padding>\n                  <ion-col col-2 class="top-center" no-margin no-padding>\n                    <img src="assets/imgs/icons/icon_scan.png" class="imgIcon">\n                  </ion-col>\n                  <ion-col no-margin no-padding>\n                    <div class="textInvite">Scan QR code</div>\n                    <div class="textDetail">In Leaderboard menu</div>\n                    <div class="textDetail">choose Start a challenge</div>\n                  </ion-col>\n                </ion-row>\n              </div>\n\n              <div class="center-center textInvite">\n                Invite your friends by scan this QR Code\n              </div>\n\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </div>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/qr-code/qr-code.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], QrCodePage);
    return QrCodePage;
}());

//# sourceMappingURL=qr-code.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeJoinProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChallengeJoinProvider = /** @class */ (function () {
    function ChallengeJoinProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello ChallengeJoinProvider Provider');
    }
    ChallengeJoinProvider.prototype.getCompetitionDetail = function (challengeNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('challengeNo', challengeNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getCompetitionDetail', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeJoinProvider.prototype.getChallengeDetail = function (challengeNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('challengeNo', challengeNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getChallengeDetail', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeJoinProvider.prototype.joinChallenge = function (challengeNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('challengeNo', challengeNo);
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'joinChallenge', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeJoinProvider.prototype.rejectChallenge = function (challengeNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('challengeNo', challengeNo);
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'rejectChallenge', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeJoinProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], ChallengeJoinProvider);
    return ChallengeJoinProvider;
}());

//# sourceMappingURL=challenge-join.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeDashboardProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChallengeDashboardProvider = /** @class */ (function () {
    function ChallengeDashboardProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello ChallengeDashboardProvider Provider');
    }
    ChallengeDashboardProvider.prototype.getChallengeDashboard = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getChallengeDashboard', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeDashboardProvider.prototype.getChallengeList = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getChallengeList', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeDashboardProvider.prototype.getCompetitionList = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getCompetitionList', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeDashboardProvider.prototype.joinChallenge = function (challengeNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('challengeNo', challengeNo);
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'joinChallenge', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeDashboardProvider.prototype.addChallengeMember = function (challengeNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('challengeNo', challengeNo);
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'addChallengeMember', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    ChallengeDashboardProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], ChallengeDashboardProvider);
    return ChallengeDashboardProvider;
}());

//# sourceMappingURL=challenge-dashboard.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_health_health__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__health_add_health_add__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__health_welcome_health_welcome__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__health_information_health_information__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








//import { SplashScreen } from '@ionic-native/splash-screen';

var HealthPage = /** @class */ (function () {
    function HealthPage(loadingCtrl, healthProvider, modalController, config, SQLite, storage) {
        this.loadingCtrl = loadingCtrl;
        this.healthProvider = healthProvider;
        this.modalController = modalController;
        this.config = config;
        this.SQLite = SQLite;
        this.storage = storage;
        this.HealthScore = "0";
        this.userId = this.config.userNo;
        // this.userId= "730659"
    }
    HealthPage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, welcomePage;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        console.log('ionViewDidLoad HealthPage ');
                        this.getHealthDateScore();
                        this.getHealthIndex();
                        //this.bindLeaderboardData();
                        _b = (_a = console).log;
                        _c = ['welcomePage storage'];
                        return [4 /*yield*/, this.storage.get('welcomePage')];
                    case 1:
                        //this.bindLeaderboardData();
                        _b.apply(_a, _c.concat([_d.sent()]));
                        welcomePage = this.storage.get('welcomePage');
                        return [4 /*yield*/, welcomePage];
                    case 2:
                        if ((_d.sent()) != true) {
                            this.getWelcomePage();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthPage.prototype.ionViewDidEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = console).log;
                        return [4 /*yield*/, this.storage.get('welcomePage')];
                    case 1:
                        _b.apply(_a, [_c.sent()]);
                        return [2 /*return*/];
                }
            });
        });
    };
    HealthPage.prototype.getHealthDateScore = function () {
        var me = this;
        var loader = me.loadingCtrl.create({
            spinner: 'crescent'
        });
        loader.present();
        me.healthProvider.getHealthDateScore(me.userId).then(function (data) {
            console.log('getHealthDate ', data);
            me.HealthScore = data.data.health_score;
            //console.log('data.health_score ',data.data.health_score);
            loader.dismiss();
        });
    };
    HealthPage.prototype.editModal = function () {
        return __awaiter(this, void 0, void 0, function () {
            var termAndConditionModal;
            return __generator(this, function (_a) {
                termAndConditionModal = this.modalController.create(__WEBPACK_IMPORTED_MODULE_3__health_add_health_add__["a" /* ModalPage */], {}, { cssClass: 'select-modal' });
                termAndConditionModal.present();
                return [2 /*return*/];
            });
        });
    };
    // async welcomeModal() {
    //   const termAndConditionModal = this.modalController.create(
    //     HealthWelcomePage,
    //     {},{cssClass: 'select-modal' }
    //   );
    //   termAndConditionModal.present();
    //   }
    HealthPage.prototype.welcomeModal = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.storage.set('welcomePage', null);
                console.log('Set storage', null);
                return [2 /*return*/];
            });
        });
    };
    HealthPage.prototype.getHealthIndex = function () {
        var me = this;
        var loader = me.loadingCtrl.create({
            spinner: 'crescent'
        });
        loader.present();
        me.healthProvider.getHealthIndex(me.userId).then(function (data) {
            console.log('getHealthIndex ', data);
            me.healthIndexRateArray = data.data.health_index_resp;
            console.log('data.getHealthIndex ', data.data.health_index_resp);
            me.paintPeriodGraph(me.healthIndexRateArray);
            loader.dismiss();
        });
    };
    HealthPage.prototype.paintPeriodGraph = function (healthIndexRateArray) {
        var n = 1;
        for (var i = 0; i < healthIndexRateArray.length; i++) {
            var value1 = this.healthIndexRateArray[i].value1;
            var value2 = this.healthIndexRateArray[i].value2;
            var health_index = this.healthIndexRateArray[i].health_index;
            switch (n) {
                case 1: {
                    this.BMI = value1;
                    this.healthIndexRateArray[i];
                    break;
                }
                case 2: {
                    this.bloodPressure1 = value1;
                    this.bloodPressure2 = value2;
                    this.healthIndexRateArray[i];
                    break;
                }
                case 3: {
                    this.cholesterol = value1;
                    this.healthIndexRateArray[i];
                    break;
                }
                case 4: {
                    this.bloodSugar = value1;
                    this.healthIndexRateArray[i];
                    break;
                }
                case 5: {
                    this.weeklySteps = value1;
                    this.healthIndexRateArray[i];
                    break;
                }
                case 6: {
                    this.weeklyExercises = value1;
                    this.healthIndexRateArray[i];
                    break;
                }
                case 7: {
                    this.HealthScoreIndex = health_index + ".png";
                    this.healthIndexRateArray[i];
                    break;
                }
            }
            n++;
        }
        // BMI: any
        // bloodPressure1: string;
        // bloodPressure2: string;
        // cholesterol: any
        // bloodSugar: any
        // weeklySteps: any
        // weeklyExercises: any
    };
    HealthPage.prototype.informationModal = function () {
        console.log("HealthInformationPage");
        var termAndConditionModal = this.modalController.create(__WEBPACK_IMPORTED_MODULE_5__health_information_health_information__["a" /* HealthInformationPage */], {}, { cssClass: 'select-modal' });
        termAndConditionModal.present();
    };
    // btnJoinQrcode(){
    //   console.log('====================================');
    //   console.log('join qr code');
    //   console.log('====================================');
    // }
    HealthPage.prototype.getWelcomePage = function () {
        var termAndConditionModal = this.modalController.create(__WEBPACK_IMPORTED_MODULE_4__health_welcome_health_welcome__["a" /* HealthWelcomePage */], {}, { cssClass: 'select-modal' });
        termAndConditionModal.present();
    };
    HealthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-health',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health/health.html"*/'<div class="fixedHeader topHeader">\n\n  <div class="imageHeader">\n\n    <div class="headerPadding">\n\n      <!-- <div>\n\n        <ion-icon name="alert-circle-outline"></ion-icon>\n\n      </div> -->\n\n      <ion-col class="center-center col" col="" no-margin="">\n\n        <!-- <div class="imageChallenge center-center"> -->\n\n          <div class="avatarProfileWidth" (click)="informationModal()">\n\n            \n\n            <!-- <img src="assets/imgs/health_information/Animal_class1.png"> -->\n\n            <img src="assets/imgs/health_information/Animal_class{{HealthScoreIndex}}">\n\n\n\n                  <!-- {{HealthScoreIndex}} -->\n\n          </div>\n\n\n\n        <!-- </div> -->\n\n\n\n      </ion-col>\n\n    </div>\n\n    <div class="topCurve">\n\n      <div class="center-center col" col="" no-margin="" no-padding="">\n\n        <ion-card-content class="healthScore">\n\n          Health score : {{HealthScore}}\n\n        </ion-card-content>\n\n        \n\n      </div>\n\n    </div>\n\n  </div>\n\n</div>\n\n<div fullscreen #mycontent class="mainContent" #pageTop>\n\n\n\n      <!-- <ion-infinite-scroll (ionInfinite)="onLoadRank($event)">\n\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n      </ion-infinite-scroll>  -->\n\n\n\n      <ion-row class="row">\n\n        <ion-col class="center-left col" col-6="">\n\n          <div>\n\n            <ion-row class="textHealthData">\n\n              Health data\n\n            </ion-row>\n\n          </div>\n\n        </ion-col>\n\n        \n\n        <ion-col class="center-right" col-6="">\n\n          <ion-col class="center-right col" col-4="">\n\n            <div class="customActionSheetGroup" (click)="editModal()">\n\n              <ion-col class="iconSportTypes">\n\n                <span class="displayTextInside">Edit</span>\n\n              </ion-col>\n\n            </div>\n\n            <!-- <button class="center-right col displayTextInside">Edit</button> -->\n\n          </ion-col>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row class="row">\n\n        <div class="scrolling-wrapper">\n\n          <div class="card box" >\n\n            <div >\n\n              <ion-row class="row">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_10@3x.png">\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                Weekly Steps\n\n              </ion-row>\n\n              <ion-row class="text3">\n\n                {{weeklySteps}}\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                steps\n\n              </ion-row>\n\n            </div>\n\n          </div>\n\n          <div class="card box">\n\n            <div>\n\n              <ion-row class="row">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_9@3x.png">\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                Weekly Exercises\n\n              </ion-row>\n\n              <ion-row class="text3">\n\n                {{weeklyExercises}}\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                mins\n\n              </ion-row>\n\n            </div>\n\n          </div>\n\n          <div class="card box">\n\n            <div>\n\n              <ion-row class="row">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_1@3x.png">\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                BMI\n\n              </ion-row>\n\n              <ion-row class="text3">\n\n                {{BMI}}\n\n              </ion-row>\n\n              <ion-row class="row" style="opacity: -0.6;">\n\n                .\n\n              </ion-row>\n\n            </div>\n\n          </div>\n\n          <div class="card box">\n\n            <div>\n\n              <ion-row class="row">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_8@3x.png">\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                Cholesterol ratio\n\n              </ion-row>\n\n              <ion-row class="text3">\n\n                {{cholesterol}}\n\n              </ion-row>\n\n              <ion-row class="row" style="opacity: -0.6;">\n\n                .\n\n              </ion-row>\n\n            </div>\n\n          </div>\n\n          <div class="card box">\n\n            <div>\n\n              <ion-row class="row">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_6@3x.png">\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                Blood pressure\n\n              </ion-row>\n\n              <ion-row class="text3">\n\n                {{bloodPressure1}}/{{bloodPressure2}}\n\n              </ion-row>\n\n              <ion-row class="row" style="opacity: -0.6;">\n\n                .\n\n              </ion-row>\n\n            </div>\n\n          </div>\n\n          <div class="card box">\n\n            <div>\n\n              <ion-row class="row">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_5@3x.png">\n\n              </ion-row>\n\n              <ion-row class="row">\n\n                Fasting blood sugar\n\n              </ion-row>\n\n              <ion-row class="text3">\n\n                {{bloodSugar}}\n\n              </ion-row>\n\n              <ion-row class="row" style="opacity: -0.6;">\n\n                .\n\n              </ion-row>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </ion-row>\n\n    \n\n    <!-- <ion-content>\n\n        <ion-scroll scrollX="true">\n\n    \n\n            <ion-card>\n\n                <ion-card-content>\n\n                    content\n\n                </ion-card-content>\n\n            </ion-card>\n\n            <ion-card>\n\n                <ion-card-content>\n\n                    content\n\n                </ion-card-content>\n\n            </ion-card>\n\n    \n\n        </ion-scroll>\n\n    </ion-content> -->\n\n\n\n    <!-- <ion-col class="center-right" col-6="">\n\n      <ion-col class="center-right col" col-4="">\n\n        <div class="customActionSheetGroup" (click)="welcomeModal()">\n\n          <ion-col class="iconSportTypes"><span class="displayTextInside">welcome</span></ion-col>\n\n        </div>\n\n      </ion-col>\n\n    </ion-col> -->\n\n\n\n      \n\n\n\n\n\n      \n\n\n\n      \n\n  </div>\n\n\n\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health/health.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_health_health__["a" /* HealthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */]])
    ], HealthPage);
    return HealthPage;
}());

//# sourceMappingURL=health.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_health_health__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ModalPage = /** @class */ (function () {
    function ModalPage(navCtrl, healthProvider, loadingCtrl, config, alertCtrl, app) {
        this.navCtrl = navCtrl;
        this.healthProvider = healthProvider;
        this.loadingCtrl = loadingCtrl;
        this.config = config;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.heartRateArray = [];
        // style inactive,active
        this.segmentSwitch1 = 'segment_height_inactive';
        this.segmentSwitch2 = 'segment_weight_inactive';
        this.segmentSwitch3 = 'segment_bloodPressure_inactive';
        this.segmentSwitch4 = 'segment_bloodPressure_1_inactive';
        this.segmentSwitch5 = 'segment_cholesterol_inactive';
        this.segmentSwitch6 = 'segment_HDL_inactive';
        this.segmentSwitch7 = 'segment_bloodSugar_inactive';
        this.HealthScore = "100";
        this.userId = this.config.userNo;
        // this.userId= "730659"
        this.steps = "0";
        this.exercises = "0";
        this.bloodPressure1 = "0";
        this.bloodPressure2 = "0";
        this.BMI = "0";
        this.cholesterol = "0";
        this.bloodSugar = "0";
        this.height = "0";
        this.weight = "0";
        this.HDL = "0";
        this.Age = "0";
    }
    ModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalPage ');
        this.getApiHealthData();
        //this.bindLeaderboardData();
    };
    // btnBack() {
    //   this.navCtrl.setRoot(
    //     HealthPage,
    //     {},
    //     { animate: true, direction: "back" }
    //   );
    // }
    ModalPage.prototype.btnBack = function () {
        var _this = this;
        console.log('empty');
        var alert = this.alertCtrl.create({
            title: 'Your changes will not be saved',
            message: 'Do you want to continue?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    },
                    cssClass: 'profalert'
                },
                {
                    text: 'YES',
                    handler: function () {
                        console.log('Cancel clicked');
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
                    },
                },
            ]
        });
        alert.present();
    };
    ModalPage.prototype.doSomething = function (date) {
        console.log('date', this.mydate); // 2019-04-22
    };
    ModalPage.prototype.getApiHealthData = function () {
        var me = this;
        var loader = me.loadingCtrl.create({
            spinner: 'crescent'
        });
        loader.present();
        me.healthProvider.getHealthDate(me.userId).then(function (data) {
            console.log('getHealthDate ', data);
            console.log('getHealthDate health_data_list ', data.data.health_data_list);
            me.heartRateArray = data.data.health_data_list;
            me.paintPeriodGraph(me.heartRateArray);
            loader.dismiss();
        });
    };
    ModalPage.prototype.paintPeriodGraph = function (heartRateArray) {
        var n = 1;
        if (heartRateArray.length == 1) {
            for (var i = 0; i < heartRateArray.length; i++) {
                var value1 = this.heartRateArray[i].value1;
                var value2 = this.heartRateArray[i].value2;
                switch (n) {
                    case 1: {
                        this.Age = value1;
                        this.heartRateArray[i];
                        break;
                    }
                }
                n++;
            }
        }
        else {
            for (var i = 0; i < heartRateArray.length; i++) {
                var value1 = this.heartRateArray[i].value1;
                var value2 = this.heartRateArray[i].value2;
                switch (n) {
                    // case 1: { this.steps = value1;  this.heartRateArray[i]; break; }
                    // case 2: { this.exercises = value1;  this.heartRateArray[i]; break; }
                    case 1: {
                        this.bloodPressure1 = value1;
                        this.bloodPressure2 = value2;
                        this.heartRateArray[i];
                        ;
                        break;
                    }
                    case 2: {
                        this.BMI = value1;
                        this.heartRateArray[i];
                        break;
                    }
                    case 3: {
                        this.cholesterol = value1;
                        this.heartRateArray[i];
                        break;
                    }
                    case 4: {
                        this.bloodSugar = value1;
                        this.heartRateArray[i];
                        break;
                    }
                    case 5: {
                        this.height = value1;
                        this.heartRateArray[i];
                        break;
                    }
                    case 6: {
                        this.weight = value1;
                        this.heartRateArray[i];
                        break;
                    }
                    case 7: {
                        this.HDL = value1;
                        this.heartRateArray[i];
                        break;
                    }
                    case 8: {
                        this.Age = value1;
                        this.heartRateArray[i];
                        break;
                    }
                }
                n++;
            }
        }
    };
    ModalPage.prototype.btnSegment = function (even) {
        if (even == 1) {
            this.segmentSwitch1 = 'segment_height_active';
        }
        else if (even == 2) {
            this.segmentSwitch2 = 'segment_weight_active';
        }
        else if (even == 3) {
            this.segmentSwitch3 = 'segment_bloodPressure_active';
        }
        else if (even == 4) {
            this.segmentSwitch4 = 'segment_bloodPressure_1_active';
        }
        else if (even == 5) {
            this.segmentSwitch5 = 'segment_cholesterol_active';
        }
        else if (even == 6) {
            this.segmentSwitch6 = 'segment_HDL_active';
        }
        else if (even == 7) {
            this.segmentSwitch7 = 'segment_bloodSugar_active';
        }
    };
    ModalPage.prototype.btnSaveChallenge = function () {
        var _this = this;
        var me = this;
        console.log('height', this.height, 'weight', this.weight, 'bloodPressure', this.bloodPressure1 + '/' + this.bloodPressure2, 'cholesterol', this.cholesterol, 'HDL', this.HDL, 'bloodSugar', this.bloodSugar);
        //let me = this;
        //this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
        // if(this.steps!=0){
        // } else {
        //   console.log('empty')
        var alert = me.alertCtrl.create({
            title: 'Your changes will be saved',
            message: 'Do you want to continue?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    },
                    cssClass: 'profalert'
                },
                {
                    text: 'Continue',
                    handler: function () {
                        console.log('Continue');
                        var loader = me.loadingCtrl.create({
                            spinner: 'crescent'
                        });
                        loader.present();
                        if (_this.mydate != null) {
                            me.healthProvider.insertHealthUserProfile(me.userId, me.mydate);
                        }
                        me.healthProvider.insertHealthData(me.userId, me.height, me.weight, me.bloodPressure1, me.bloodPressure2, me.cholesterol, me.HDL, me.bloodSugar).then(function (data) {
                            console.log('btnSaveChallenge ', data);
                            console.log('btnSaveChallenge ', data.response.status.code, ':', data.response.status.description);
                            loader.dismiss();
                            //me.btnBack();
                            if (data.response.status.code == 0) {
                                me.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
                                //this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
                            }
                        });
                        // loader.dismiss();
                    }
                }
            ]
        });
        alert.present();
        // }
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'health-add',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health-add/health-add.html"*/'\n\n    <!-- <ion-buttons left>\n\n      <button ion-button icon-only (click)="btnBack()">\n\n        <ion-icon name="arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-header> -->\n\n      <ion-content class="main-view">\n\n        <div class="overlay" (click)="btnBack()"></div>\n\n        <div class="modal_content">\n\n          <h2 style="font-weight: bold;\n\n          line-height: 38px;\n\n          letter-spacing: 0.56px;\n\n          color: #383838;">Edit your health information</h2>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-around">\n\n            <ion-col size="3">\n\n              <div class="iconHealth">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_2@3x.png">\n\n                <ion-col class="textWidth">Date of Birth</ion-col>\n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="3" class="boxInput">\n\n              <div class="boxValue">\n\n                <ion-label class="segment_age_inactive" position="floating" *ngIf="mydate == null">{{Age}}</ion-label >\n\n                <ion-datetime displayFormat="DD/MM/YYYY" min="1900-03-14" max="2070-12-09" [(ngModel)]="mydate" \n\n                (ionChange)="doSomething(this.mydate)" style="width: 124px;"></ion-datetime>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-end">\n\n            <ion-col size="3">\n\n              <div class="iconHealth">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_1@3x.png">\n\n                <ion-col class="textWidth">Height (cm)</ion-col>\n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="3" class="boxInput">\n\n              <div class="boxValue">\n\n                <ion-input [ngClass]="segmentSwitch1" (change)="btnSegment(1)" placeholder="Enter Input" type="tel" maxlength="3" [(ngModel)]="height" text-right max="3" pattern="[0-9]*" value="{{height}}" inputmode="numeric"></ion-input>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-around">\n\n            <ion-col size="3">\n\n              <div class="iconHealth">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_7@3x.png">\n\n                <ion-col class="textWidth">Weight (kg)</ion-col>\n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="3" class="boxInput">\n\n              <div class="boxValue">\n\n                <ion-input [ngClass]="segmentSwitch2" (change)="btnSegment(2)" type="tel" maxlength="3" [(ngModel)]="weight" text-right pattern="[0-9]*" value="{{weight}}" inputmode="numeric"></ion-input>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-around">\n\n            <ion-col size="2">\n\n              <div class="iconHealth">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_6@3x.png">\n\n                <ion-col class="textWidth"> Blood Pressure</ion-col>\n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="1" class="bloodPressure1">\n\n              <div class="boxValue">\n\n                <ion-input [ngClass]="segmentSwitch3" (change)="btnSegment(3)" type="tel" maxlength="3"[(ngModel)]="bloodPressure1" text-right max="3" pattern="[0-9]*" value="{{bloodPressure1}}" inputmode="numeric"></ion-input>\n\n              </div>\n\n            </ion-col>\n\n            <ion-col class="bloodPressure">/</ion-col>\n\n            <ion-col size="1" class="bloodPressure2">\n\n              <div class="boxValue">\n\n                <ion-input [ngClass]="segmentSwitch4" (change)="btnSegment(4)" type="tel" maxlength="3" [(ngModel)]="bloodPressure2" text-right max="3" pattern="[0-9]*" value="{{bloodPressure2}}" inputmode="numeric"></ion-input>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-around">\n\n            <ion-col size="3">\n\n              <div class="iconHealth">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_8@3x.png">\n\n                <ion-col class="textWidth"> Total Cholesterol</ion-col>\n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="3" class="boxInput">\n\n              <div class="boxValue">\n\n                <ion-input [ngClass]="segmentSwitch5" (change)="btnSegment(5)" type="tel" maxlength="3" [(ngModel)]="cholesterol" text-right max="3" pattern="[0-9]*" value="{{cholesterol}}" inputmode="numeric"></ion-input>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-around">\n\n            <ion-col size="3">\n\n              <div class="iconHealth">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_11@3x.png">\n\n                <ion-col class="textWidth"> HDL</ion-col>\n\n                \n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="3" class="boxInput">\n\n              <div class="boxValue">\n\n                <ion-input [ngClass]="segmentSwitch6" (change)="btnSegment(6)" type="tel" maxlength="3" [(ngModel)]="HDL" text-right max="3" pattern="[0-9]*" value="{{HDL}}" inputmode="numeric"></ion-input>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-around">\n\n            <ion-col size="3">\n\n              <div class="iconHealth">\n\n                <img class="iconWidth" src="assets/imgs/health_add/Asset_5@3x.png">\n\n                <ion-col class="textWidth"> Fasting Blood Sugar</ion-col>\n\n                \n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="3" class="boxInput">\n\n              <div class="boxValue">\n\n                <ion-input [ngClass]="segmentSwitch7" (change)="btnSegment(7)" type="tel" maxlength="3" [(ngModel)]="bloodSugar" text-right max="3" pattern="[0-9]*" value="{{bloodSugar}}" inputmode="numeric"></ion-input>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <hr>\n\n          <ion-row class="ion-justify-content-end">\n\n            <ion-col size="3" class="center-center">\n\n              <div class="cancelButton center-center" style="color: #2FA2D6;" (click)="btnBack()">\n\n                <div class="textSave">Cancel</div>\n\n              </div>\n\n            </ion-col>\n\n            <ion-col size="3" style="padding-right: 45px;">\n\n              <div class="saveButton center-center" (click)="btnSaveChallenge()">\n\n                <div class="textSave">Save</div>\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n           \n\n          <!-- <ion-list>\n\n            <ion-item *ngFor="let heart of heartRateArray">\n\n              AAA : {{heart.type_name}} : {{heart.value1}}\n\n              <ion-col *ngIf="heart.type_name == \'Steps\'">\n\n                <ion-icon name="md-flame"></ion-icon>&nbsp;&nbsp;{{heart.value1}}\n\n              </ion-col>\n\n            </ion-item>\n\n          </ion-list> -->\n\n        </div>\n\n        \n\n          \n\n      </ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health-add/health-add.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_health_health__["a" /* HealthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=health-add.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthWelcomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { HealthPage } from '../health/health';

var HealthWelcomePage = /** @class */ (function () {
    function HealthWelcomePage(navCtrl, storage) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.count = 1;
        this.textWelcom = 'Your health score shows how healthy you are based on your data. Its based on scientific research and global health recommendations.';
    }
    HealthWelcomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalPage ');
        //console.log(this.storage)
        //this.bindLeaderboardData();
    };
    HealthWelcomePage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
    };
    HealthWelcomePage.prototype.btnNext = function () {
        console.log('count', this.count++);
        console.log('textWelcom=====', this.textWelcom);
        if (this.count == 2) {
            this.textWelcom = 'Starting by adding your health data  on the next screen';
        }
        else if (this.count == 3) {
            this.storage.set('welcomePage', true);
            this.btnBack();
        }
        console.log('textAAAAA', this.textWelcom);
    };
    HealthWelcomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'health-welcome',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health-welcome/health-welcome.html"*/'\n\n    <!-- <ion-buttons left>\n\n      <button ion-button icon-only (click)="btnBack()">\n\n        <ion-icon name="arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-header> -->\n\n      <ion-content class="main-view">\n\n        <div class="overlay" (click)="btnBack()"></div>\n\n        <div class="modal_content">\n\n          <ion-row class="welcomTextheader">\n\n            Welcome to your  health score\n\n          </ion-row>\n\n          <ion-row class="ion-justify-content-end">\n\n            <ion-col>\n\n              <div class="avatarProfileWidth" style="width: 350px;\n\n              height: 272px;">\n\n                <img src="assets/imgs/health_welcom/welcome_01.png">\n\n                <!-- /Users/apiwat_n/MTLHealthProject_3nd/design2nd/src/assets/imgs/health_welcom/welcome_01.png -->\n\n                <!-- <img src="assets/imgs/icons/Challenge_New.png"> -->\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row class="welcomText">\n\n            <!-- <ion-col slot="fixed" refreshingText="Refreshing..."> -->\n\n              {{textWelcom}}\n\n            <!-- </ion-col> -->\n\n          </ion-row>\n\n            <ion-row>\n\n              <ion-col class="center" style="margin-left: 80px;">\n\n                <div class="welcomButton" (click)="btnNext()">\n\n                  <div class="nextText">Next</div>\n\n                </div>\n\n              </ion-col>\n\n            </ion-row>\n\n          \n\n        </div>\n\n        \n\n          \n\n      </ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health-welcome/health-welcome.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], HealthWelcomePage);
    return HealthWelcomePage;
}());

//# sourceMappingURL=health-welcome.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthInformationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HealthInformationPage = /** @class */ (function () {
    function HealthInformationPage(navCtrl, config) {
        this.navCtrl = navCtrl;
        this.config = config;
        this.count = 1;
    }
    HealthInformationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalPage ');
        //console.log(this.storage)
        //this.bindLeaderboardData();
    };
    HealthInformationPage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
    };
    HealthInformationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'health-information',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health-information/health-information.html"*/'      <ion-content class="main-view">\n\n        <div class="overlay" (click)="btnBack()"></div>\n\n        <div class="modal_content">\n\n          <ion-icon name="close-circle" class="center-right" (click)="btnBack()"></ion-icon>\n\n          <ion-row class="informationheader">\n\n            Health score\n\n          </ion-row>\n\n          <ion-row class="informationText">\n\n            shows how healthy you are based on your data. There are 5 categories.\n\n          </ion-row>\n\n          <ion-row class="marginFormIcon">\n\n            <ion-col col-3>\n\n              <img class="icon_left" src="assets/imgs/health_information/Animal_class1.png">\n\n            </ion-col>\n\n            <ion-col col-9>\n\n              <h3 class="textboldInfor" >Lazy sloth</h3> \n\n              <div class="textInfor">\n\n                Health score < 400 \n\n              You can be hunted easily. Improving your health is critical for survival.\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row class="marginFormIcon">\n\n            <ion-col col-3>\n\n              <img class="icon_left" src="assets/imgs/health_information/Animal_class2.png">\n\n            </ion-col>\n\n            <ion-col col-9>\n\n              <h3 class="textboldInfor" >Sleepy koala</h3> \n\n              <div class="textInfor">\n\n                Health score 401 - 550\n\n                Wake up and move your body a bit more to become healthier.\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row class="marginFormIcon">\n\n            <ion-col col-3>\n\n              <img class="icon_left" src="assets/imgs/health_information/Animal_class3.png">\n\n            </ion-col>\n\n            <ion-col col-9>\n\n              <h3 class="textboldInfor" >Playful wolf</h3> \n\n              <div class="textInfor">\n\n                Health score 551 - 700\n\n                You are part of the pack. Your health is at the average level.\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row class="marginFormIcon">\n\n            <ion-col col-3>\n\n              <img class="icon_left" src="assets/imgs/health_information/Animal_class4.png">\n\n            </ion-col>\n\n            <ion-col col-9>\n\n              <h3 class="textboldInfor" >Active kangaroo</h3> \n\n              <div class="textInfor">\n\n                Health score 701 - 850\n\n                Beating you is not that easy. You have the fighting spirit of a kangaroo.\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row class="marginFormIcon">\n\n            <ion-col col-3>\n\n              <img class="icon_left" src="assets/imgs/health_information/Animal_class5.png">\n\n            </ion-col>\n\n            <ion-col col-9>\n\n              <h3 class="textboldInfor" >Mighty lion</h3> \n\n              <div class="textInfor">\n\n                Health score 850+ \n\n                Only people like you can be as strong as a lion. You health is at its peak!\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>          \n\n          \n\n        </div>\n\n        \n\n          \n\n      </ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/health-information/health-information.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_config_config__["a" /* ConfigProvider */]])
    ], HealthInformationPage);
    return HealthInformationPage;
}());

//# sourceMappingURL=health-information.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_friends_friends__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NotificationPage = /** @class */ (function () {
    function NotificationPage(friend, config, navCtrl, navParams) {
        this.friend = friend;
        this.config = config;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        console.log('friend data ', this.config.objFriendReques);
        this.showCardData = this.config.objFriendReques;
        // this.showCardData = [
        //   {
        //     rankNo: 1,
        //     total: 569,
        //     totalTxt: "569 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/89106_156.jpg",
        //     userName: "Elise",
        //     userNo: "89106",
        //   },
        //   {
        //     rankNo: 2,
        //     total: 570,
        //     totalTxt: "570 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/730394_63.jpg",
        //     userName: "Lisa",
        //     userNo: "730394",
        //   },
        //   {
        //     rankNo: 3,
        //     total: 571,
        //     totalTxt: "571 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/84742_734.jpg",
        //     userName: "Stephanie",
        //     userNo: "84742",
        //   },
        //   {
        //     rankNo: 4,
        //     total: 572,
        //     totalTxt: "572 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/82815_482.jpg",
        //     userName: "Stephanie",
        //     userNo: "82815",
        //   }
        // ]
    }
    NotificationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationPage');
    };
    NotificationPage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
    };
    NotificationPage.prototype.btnAccept = function (el) {
        console.log('btnAccepted');
        console.log(el, el.userNo);
        var me = this;
        this.friend.addFriend(el.friendNo).then(function (data) {
            console.log('---- ', data);
            me.friend.getFriendRequestMe().then(function (data) {
                me.showCardData = data;
            });
        });
    };
    NotificationPage.prototype.btnReject = function (el) {
        console.log('btnRejected');
        console.log(el);
        var me = this;
        this.friend.rejectFriendRequest(el.friendNo).then(function (data) {
            console.log('### ', data);
            me.friend.getFriendRequestMe().then(function (data) {
                me.showCardData = data;
            });
        });
    };
    NotificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notification',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/notification/notification.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>Notification</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class="mainContent" no-pading>\n  <ion-list *ngFor="let record of showCardData;let i= index">\n    <ion-item [ngClass]="record.userNo == 89106?\'active-item-color\':\'\'">\n      <ion-row no-padding no-margin>\n\n        <ion-col no-padding no-margin col-3>\n          <div class="center-center">\n            <div class="avatarWidth">\n              <img src="{{record.userImg}}">\n            </div>\n          </div>\n        </ion-col>\n\n        <ion-col no-padding no-margin class="rightColumn">\n          <div class="yourFriendInvite">{{record.userName}}</div>\n          <div class="">sent you a friend request</div>\n          <div>\n            <span class="minutesAgo">{{record.requestTime}}</span>\n          </div>\n\n          <!-- <ion-row *ngIf="(record.userNo == 89106) || (record.userNo == 730394)"> -->\n          <ion-row>\n            <ion-col class="center-left">\n              <div class="acceptButton center-center" (click)="btnAccept(record)">Accept</div>\n              <div class="rejectButton" (click)="btnReject(record)">Reject</div>\n            </ion-col>\n          </ion-row>\n\n          <!-- {{record.userName}} {{record.totalTxt}} {{record.user_status}} -->\n        </ion-col>\n      </ion-row>\n\n      <div class="lineSeperate"></div>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/notification/notification.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_friends_friends__["a" /* FriendsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], NotificationPage);
    return NotificationPage;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsProvider = /** @class */ (function () {
    function TabsProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello TabsProvider Provider');
    }
    TabsProvider.prototype.getProfile = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        console.log('param ', params, ' ', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getProfile', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    TabsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], TabsProvider);
    return TabsProvider;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConditionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forgot_password_forgot_password__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_login_login__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ConditionPage = /** @class */ (function () {
    function ConditionPage(alertCtrl, loadingCtrl, SQLite, config, navCtrl, loginPvd) {
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.SQLite = SQLite;
        this.config = config;
        this.navCtrl = navCtrl;
        this.loginPvd = loginPvd;
        this.isenabled = false;
    }
    ConditionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
        // this.txtUsername='mtl88866';
        // this.txtPassword='P@ssw0rd';
    };
    ConditionPage.prototype.btnLogin = function () {
        var _this = this;
        var me = this;
        if (this.txtUsername.length > 0 && this.txtPassword.length > 0) {
            var loader_1 = me.loadingCtrl.create({
                spinner: 'crescent'
            });
            loader_1.present();
            this.loginPvd.login(this.txtUsername, this.txtPassword).then(function (data) {
                console.log('login ', data);
                loader_1.dismiss();
                if (data != null && data.errMsg == null) {
                    // me.config.userNo='89107';//for test
                    me.config.userNo = data.userNo;
                    var sql = "INSERT INTO tb_config(user_no)VALUES('" + me.config.userNo + "')";
                    console.log('sql', sql);
                    me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
                        me.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'forward' });
                    });
                }
                else {
                    var alertMsg = _this.alertCtrl.create({
                        title: 'Login Message',
                        message: data.errMsg,
                        buttons: [
                            {
                                text: 'Yes'
                            }
                        ]
                    });
                    alertMsg.present();
                }
            });
        }
    };
    ConditionPage.prototype.createAAA = function () {
        console.log('AAAAA');
    };
    ConditionPage.prototype.createWaste = function () {
        // this.service.save(this.setDump())
        //     .then(() => {
        //       alert('salvo com sucesso')
        //     })
        console.log(this.isenabled);
    };
    ConditionPage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */], {}, { animate: true, direction: 'back' });
    };
    ConditionPage.prototype.btnForgotPassword = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__forgot_password_forgot_password__["a" /* ForgotPasswordPage */], {}, { animate: true, direction: 'forward' });
    };
    ConditionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-Condition',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/condition-Health/Condition.html"*/'<ion-content class="mainContent">\n\n  <div style="margin-top: 20px;margin-right: 20px;">\n\n    <ion-icon name="close-circle" class="center-right iconSize" (click)="btnBack()"></ion-icon>\n\n  </div>\n\n  <div style="width: 310px;\n\n  height: 76px;\n\n\n\n  font-family: KaLaTeXa Text;\n\n  font-style: normal;\n\n  font-weight: bold;\n\n  font-size: 24px;\n\n  line-height: 38px;\n\n  letter-spacing: 0.35px;\n\n  margin-left: 40px;\n\n  \n\n  color: #383838;">\n\n\n\nข้อตกลงและเงื่อนไขการใช้บริการ MTL HEALTH\n\n\n\n  </div>\n\n  <div style="width: 327px;\n\n  height: 1900px;\n\n  margin-left: 40px;\n\n  margin-top: 21px;\n\n  \n\n  font-family: KaLaTeXa Text;\n\n  font-style: normal;\n\n  font-weight: normal;\n\n  font-size: 16px;\n\n  line-height: 26px;\n\n  letter-spacing: 0.35px;">\n\n    HEALTH (“แอปพลิเคชัน”) มีผลบังคับระหว่างบริษัท เมืองไทยประกันชีวิต จำกัด (มหาชน) ซึ่งต่อไปในข้อตกลงนี้จะเรียกว่า “บริษัท” กับ ผู้เข้าร่วมโครงการ MTL HEALTH ซึ่งต่อไปใน ข้อตกลงนี้จะเรียกว่า “ผู้เข้าร่วมโครงการ” โดยมีรายละเอียดดังต่อไปนี้\n\nบททั่วไป\n\nเมื่อผู้เข้าร่วมโครงการ ลงทะเบียนเข้าใช้บริการแอปพลิเคชันโดยการเลือก “ตกลง” เรียบร้อยแล้ว ถือว่าผู้เข้าร่วมโครงการ ได้รับทราบและยอมรับข้อตกลงและเงื่อนไขทั้งหมดนี้แล้ว ทั้งนี้ บริษัทขอสงวนสิทธิ์ในการแก้ไขหรือเปลี่ยนแปลงข้อตกลงและเงื่อนไขการใช้บริการแอปพลิเคชันได้ตลอดเวลาตามดุลยพินิจของบริษัท\n\nผู้เข้าร่วมโครงการตกลงยินยอมให้บริษัทจัดเก็บ รวบรวมและใช้ข้อมูลที่เกิดจากการที่ผู้เข้าร่วมโครงการเข้าใช้งาน แอปพลิเคชัน รวมทั้งการเปิดเผยข้อมูลของผู้เข้าร่วมโครงการต่อบุคคลที่สาม ทั้งนี้ เพื่อวัตถุประสงค์ในการทำการตลาด การติดต่อสื่อสาร การเสนอขายผลิตภัณฑ์ต่างๆ การจัดทำสถิติ การจัดทำแบบสอบถามหรือการสำรวจเกี่ยวกับธุรกิจหรือกิจกรรมหรือเพื่อการอื่นใดตามที่บริษัทเห็นว่าจะเป็นประโยชน์แก่ผู้เข้าร่วมโครงการ\n\nผู้เข้าร่วมโครงการมีสิทธิในการเข้าถึงข้อมูลและใช้บริการแอปพลิเคชันที่แตกต่างกันตามสถานะของผู้เข้าร่วมโครงการ\n\nบริษัทขอสงวนสิทธิ์ไม่รับผิดชอบในความเสียหายใดๆ หรือการไม่สามารถทำงานได้ตามปกติของอุปกรณ์ของผู้เข้าร่วมโครงการที่เกิดขึ้นจากการติดตั้งแอปพลิเคชัน หรือการใช้งานใดๆ บนแอปพลิเคชันนี้\n\nข้อมูลบนแอปพลิเคชั่นไม่ใช่คำแนะนำทางการแพทย์หรือการวินิจฉัยโรค หากท่านมีข้อสงสัยเกี่ยวกับสุขภาพของท่าน ท่านควรปรึกษาแพทย์ผู้เชี่ยวชาญเสมอ\n\nเนื้อหา แนวคิด รูปภาพ สัญลักษณ์ ตัวอักษร กราฟฟิก การออกแบบ ข้อมูลทั้งหมด และทรัพย์สินทางปัญญาทุกชนิดที่บริษัทได้สร้างสรรค์ขึ้นตามที่ปรากฎในแอปพลิเคชันนี้ ถือเป็นกรรมสิทธิ์และลิขสิทธิ์ของบริษัท โดยผู้เข้าร่วมโครงการตกลงไม่ทำซ้ำหรือดัดแปลง เผยแพร่ต่อสาธารณชน ขายหรือแจกจ่ายในลักษณะที่อาจก่อให้เกิดความเสียหายแก่บริษัท และ/หรือกระทำการอื่นใดอันเป็นการละเมิดทรัพย์สินทางปัญญาของบริษัท ไม่ว่าจะส่วนหนึ่งส่วนใด หรือทั้งหมดของแอปพลิเคชันนอกเหนือจากการใช้งานตามปกติส่วนบุคคล\n\nกรณีที่บริษัทมีความจำเป็นต้องเปิดเผยข้อมูล หรือธุรกรรมที่เกี่ยวข้องกับการใช้บริการของผู้เข้าร่วมโครงการให้แก่หน่วยงานของรัฐภายใต้บทบัญญัติของกฎหมาย คำสั่ง หรือกฎระเบียบของรัฐ ผู้เข้าร่วมโครงการตกลงยินยอมให้บริษัทเปิดเผย หรือรายงานข้อมูล และ/หรือจัดทำรายงานเกี่ยวกับผู้เข้าร่วมโครงการมอบให้แก่เจ้าหน้าที่ หรือหน่วยงานของรัฐได้ทุกประการ\n\nบริษัทตระหนักและให้ความสำคัญสูงสุดถึงความปลอดภัยในข้อมูลของผู้เข้าร่วมโครงการและพร้อมยึดถือปฏิบัติตามกรอบข้อบังคับของกฎหมายด้านการดูแลรักษาข้อมูล ทั้งนี้ ผู้เข้าร่วมโครงการมีสิทธิในการเลือกที่จะให้หรือปฏิเสธการให้ข้อมูลส่วนบุคคลตามที่บริษัทร้องขอ อย่างไรก็ตามการปฏิเสธไม่ให้ข้อมูลดังกล่าวอาจมีผลต่อข้อจำกัดในการใช้บริการต่างๆ ของบริษัทเช่นกัน\n\nในกรณีที่ผู้เข้าร่วมโครงการใช้บริการโดยผ่านเว็บไซต์หรือโปรแกรมของบริษัทพันธมิตรหรือบุคคลที่สามผ่านทาง แอปพลิเคชัน ผู้เข้าร่วมโครงการอาจถูกร้องขอให้เปิดเผยข้อมูลส่วนบุคคลหรือปฏิบัติตามข้อตกลงและเงื่อนไขในการใช้งานของบริษัทพันธมิตรหรือบุคคลที่สามดังกล่าว โดยบริษัทไม่มีส่วนเกี่ยวข้องในธุรกรรมระหว่างท่านกับบริษัทพันธมิตรหรือบุคคลที่สามดังกล่าวใดๆ ทั้งสิ้น\n\nบรรดาหนังสือบอกกล่าว หรือการแจ้งคำบอกกล่าวใดๆ ที่บริษัทส่งให้ผู้เข้าร่วมโครงการ ตามที่อยู่ หรือ หมายเลขโทรศัพท์มือถือ หรือ อีเมล ที่ผู้เข้าร่วมโครงการได้แจ้งไว้กับบริษัทนั้น ให้ถือว่าเป็นการส่งโดยชอบและผู้เข้าร่วมโครงการได้รับทราบเรียบร้อยแล้ว ในกรณีที่ผู้เข้าร่วมโครงการ มีการเปลี่ยนแปลงที่อยู่ หรือ หมายเลขโทรศัพท์มือถือ หรือ อีเมล จะต้องแจ้งให้บริษัททราบทันที หากผู้เข้าร่วมโครงการมิได้แจ้งการเปลี่ยนแปลงดังกล่าว ให้ถือว่าการส่งคำบอกกล่าวไปยังที่อยู่ หรือ หมายเลข โทรศัพท์มือถือหรืออีเมลเดิม เป็นการส่งโดยชอบแล้ว\n\nกรณีมีเงื่อนไขข้อใดข้อหนึ่งในข้อตกลงและเงื่อนไขการใช้บริการ MTL HEALTH ขัดต่อกฎหมายหรือไม่มีผลใช้บังคับ ให้ถือว่าเงื่อนไขนั้น ๆ เป็นโมฆะเฉพาะส่วนที่ขัดต่อกฎหมายหรือไม่มีผลใช้บังคับเท่านั้น แต่เงื่อนไขส่วนอื่นยังคงมีผลใช้บังคับอยู่อย่างสมบูรณ์ และผู้เข้าร่วมโครงการยังคงผูกพันภายใต้เงื่อนไขที่เหลือดังกล่าวนี้\n\n  </div>\n\n  <div class="center">\n\n    <div class="welcomButton" (click)="btnBack()">\n\n      <div class="textCenter">Close</div>\n\n    </div>\n\n  </div>\n\n \n\n\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/condition-Health/Condition.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_login_login__["a" /* LoginProvider */]])
    ], ConditionPage);
    return ConditionPage;
}());

//# sourceMappingURL=condition.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_health_kit__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(healthKit, platform, navCtrl) {
        var _this = this;
        this.healthKit = healthKit;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.energy = 0;
        this.walk_distance = 0;
        this.cycling_distance = 0;
        this.currentHeight = 'No Data';
        this.stepcount = 'No Data';
        this.workouts = [];
        var start = new Date();
        start.setDate(start.getDate());
        start.setHours(0, 0, 0, 0);
        var end = new Date();
        end.setHours(23, 59, 59, 0); // next midnight
        this.selectDate = {
            myStartDate: start,
            myEndDate: end
        };
        console.log(this.selectDate);
        if (this.platform.is('cordova')) {
            this.platform.ready().then(function () {
                _this.healthKit.available().then(function (available) {
                    if (available) {
                        // Request all permissions up front if you like to
                        var options = {
                            readTypes: [
                                'HKQuantityTypeIdentifierStepCount',
                                'HKWorkoutTypeIdentifier',
                                'HKQuantityTypeIdentifierActiveEnergyBurned',
                                'HKQuantityTypeIdentifierDistanceCycling',
                                'HKQuantityTypeIdentifierHeartRate',
                                'HKQuantityTypeIdentifierDistanceWalkingRunning'
                            ]
                        };
                        _this.healthKit.requestAuthorization(options).then(function (_) {
                            _this.loadHealthData();
                        });
                    }
                });
            });
        }
    }
    HomePage.prototype.onDaySelect = function (event) {
        var start = new Date(event.year, event.month, event.date, 0, 0, 0, 0);
        var end = new Date(event.year, event.month, event.date, 23, 59, 59, 0);
        this.selectDate = {
            myStartDate: start,
            myEndDate: end
        };
        console.log('start-end date ', this.selectDate);
        this.loadHealthData();
    };
    HomePage.prototype.loadHealthData = function () {
        var _this = this;
        var stepOptions = {
            'startDate': this.selectDate.myStartDate,
            'endDate': this.selectDate.myEndDate,
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierStepCount',
            'unit': 'count'
        };
        this.healthKit.querySampleTypeAggregated(stepOptions).then(function (data) {
            console.log('step ', data);
            _this.stepcount = data[0].quantity;
        }, function (err) {
            console.log('No steps: ', err);
        });
        var heartRateOptions = {
            'startDate': this.selectDate.myStartDate,
            'endDate': this.selectDate.myEndDate,
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierHeartRate',
            'unit': 'count/min',
        };
        this.healthKit.querySampleTypeAggregated(heartRateOptions).then(function (data) {
            console.log('HR ', data);
            _this.heartRate = Math.round(data[0].quantity);
        }, function (err) {
            console.log('No heart rate: ', err);
        });
        var energyOptions = {
            'startDate': this.selectDate.myStartDate,
            'endDate': this.selectDate.myEndDate,
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierActiveEnergyBurned',
            'unit': 'kcal',
        };
        this.healthKit.querySampleTypeAggregated(energyOptions).then(function (data) {
            console.log('Active Energy ', data);
            _this.energy = Math.round(data[0].quantity);
        }, function (err) {
            console.log('No energy: ', err);
        });
        var walkDistanceOptions = {
            'startDate': this.selectDate.myStartDate,
            'endDate': this.selectDate.myEndDate,
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierDistanceWalkingRunning',
            'unit': 'm',
        };
        this.healthKit.querySampleTypeAggregated(walkDistanceOptions).then(function (data) {
            console.log('distance walking & running ', data, ' ####');
            _this.walk_distance = Number(data[0].quantity / 1000).toFixed(1);
            // this.walk_distance = Math.round(data[0].quantity / 100);
        }, function (err) {
            console.log('No energy: ', err);
        });
        var cyclingDistanceOptions = {
            'startDate': this.selectDate.myStartDate,
            'endDate': this.selectDate.myEndDate,
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierDistanceCycling',
            'unit': 'm',
        };
        this.healthKit.querySampleTypeAggregated(cyclingDistanceOptions).then(function (data) {
            console.log('distance cycling ', data);
            _this.cycling_distance = Number(data[0].quantity / 1000).toFixed(1);
        }, function (err) {
            console.log('No energy: ', err);
        });
        // var workoutOptions = {
        //   'startDate': this.selectDate.myStartDate,
        //   'endDate': this.selectDate.myEndDate,
        //   'aggregation': 'day',
        //   'sampleType': 'HKWorkoutTypeIdentifier',
        //   'activity': 'activityTypem',
        // }
        // this.healthKit.querySampleType(workoutOptions).then(data => {
        //   console.log('workout ==> ',data);
        //   console.log(data.length);
        // }, err => {
        //   console.log('No Workout Data: ', err);
        // });
        console.log('work');
        // this.healthKit.findWorkouts().then(data => {
        this.healthKit.findWorkouts().then(function (x) {
            console.log('workout ==> ', x, ' !!!!');
            _this.workouts = x;
        }, function (err) {
            console.log(err);
            // Sometimes the result comes in here, very strange.
            _this.workouts = err;
        });
    };
    HomePage.prototype.showHeartrate = function () {
        console.log('===> ', this.workouts);
        //this.navCtrl.setRoot(HeartratePage, { passDate: this.selectDate }, { animate: true, direction: 'forward' });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/home/home.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Apple Watch\n    </ion-title>\n\n  </ion-toolbar>\n\n\n</ion-header>\n\n<ion-content>\n\n  <div class="favorite-list" (click)="showHeartrate()">\n    <ion-grid no-margin>\n      <ion-row>\n        <ion-col col-4>\n          <ion-icon name="heart"></ion-icon>\n          <span class="data-type">&nbsp;Heart Rate</span>\n        </ion-col>\n        <ion-col col-8 text-right>\n          <span class="number-show">{{heartRate}}</span>\n          <span class="number-unit">bpm</span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class="favorite-list">\n    <ion-grid no-margin>\n      <ion-row>\n        <ion-col col-4>\n          <ion-icon name="md-battery-charging"></ion-icon>\n          <span class="data-type">&nbsp;Energy</span>\n        </ion-col>\n        <ion-col col-8 text-right>\n          <span class="number-show">{{energy}}</span>\n          <span class="number-unit">kcal</span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class="favorite-list">\n    <ion-grid no-margin>\n      <ion-row>\n        <ion-col col-4>\n          <ion-icon name="ios-man"></ion-icon>\n          <span class="data-type">&nbsp;Steps</span>\n        </ion-col>\n        <ion-col col-8 text-right>\n          <span class="number-show">{{stepcount}}</span>\n          <span class="number-unit">steps</span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class="favorite-list">\n    <ion-grid no-margin>\n      <ion-row>\n        <ion-col col-4>\n          <ion-icon name="md-pin"></ion-icon>\n          <span class="data-type">&nbsp;Walk Distance</span>\n        </ion-col>\n        <ion-col col-8 text-right>\n          <span class="number-show">{{walk_distance}}</span>\n          <span class="number-unit">km</span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class="favorite-list">\n    <ion-grid no-margin>\n      <ion-row>\n        <ion-col col-4>\n          <ion-icon name="md-pin"></ion-icon>\n          <span class="data-type">&nbsp;Cycling Distance</span>\n        </ion-col>\n        <ion-col col-8 text-right>\n          <span class="number-show">{{cycling_distance}}</span>\n          <span class="number-unit">km</span>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <ion-list>\n    <ion-card *ngFor="let workout of workouts">\n      <ion-card-header>{{ workout.calories }}</ion-card-header>\n      <ion-card-content>\n        <p>Activity: {{ workout.activityType }}</p>\n        <p>Duration: {{ workout.duration / 100 }} min</p>\n        <p>Date: {{ workout.startDate | date:\'short\' }}</p>\n        <p>Distance: {{ workout.miles }} miles</p>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_health_kit__["a" /* HealthKit */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(412);


// console.log = function () { };
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_health_kit__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_transfer__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_barcode_scanner__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_sqlite__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_fcm__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_storage__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_component__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_popover_setting_popover_setting__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_popover_share_popover_share__ = __webpack_require__(736);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_settings_settings__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_login_login__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_this_week_this_week__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_device_sync_device_sync__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_device_confirm_connect_device_confirm_connect__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_weekly_achievement_weekly_achievement__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_history_history__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_leader_board_leader_board__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_rewarded_smile_point_rewarded_smile_point__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_challenge_create_challenge_create__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_challenge_dashboard_challenge_dashboard__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_challenge_join_challenge_join__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_health_health__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_home_home__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_heartrate_heartrate__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_this_week_this_week__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_history_history__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_weekly_achivement_weekly_achivement__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_rewarded_smaile_points_rewarded_smaile_points__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_congratulation_congratulation__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_congratulation_achievment_congratulation_achievment__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_device_sync_device_sync__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_device_connected_device_connected__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_device_confirm_unlink_device_confirm_unlink__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_device_confirm_connect_device_confirm_connect__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_leader_board_leader_board__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_forgot_password_forgot_password__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_challenge_add_by_user_challenge_add_by_user__ = __webpack_require__(738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_challenge_create_challenge_create__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_challenge_dashboard_challenge_dashboard__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_challenge_invite_challenge_invite__ = __webpack_require__(739);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_challenge_join_challenge_join__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_challenge_your_rank_challenge_your_rank__ = __webpack_require__(740);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_qr_code_qr_code__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_applewatch_applewatch__ = __webpack_require__(741);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_reward_condition_reward_condition__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__providers_apple_apple__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_notification_notification__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__pages_user_profile_user_profile__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_profile_setting_profile_setting__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_friends_friends__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_friend_profile_friend_profile__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__pages_pending_request_pending_request__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__pages_how_to_challenges_work_how_to_challenges_work__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__pages_term_and_condition_term_and_condition__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__pages_excercise_detail_excercise_detail__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__providers_profile_setting_profile_setting__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__providers_tabs_tabs__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__providers_friends_friends__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__providers_friend_profile_friend_profile__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__pages_health_health__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__pages_health_add_health_add__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__pages_health_welcome_health_welcome__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__pages_health_information_health_information__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__pages_condition_Health_condition__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// Plugin











// Component



// Provider















// Views












































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_33__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_heartrate_heartrate__["a" /* HeartratePage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_this_week_this_week__["a" /* ThisWeekPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_leader_board_leader_board__["a" /* LeaderBoardPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_weekly_achivement_weekly_achivement__["a" /* WeeklyAchivementPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_rewarded_smaile_points_rewarded_smaile_points__["a" /* RewardedSmailePointsPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_congratulation_congratulation__["a" /* CongratulationPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_congratulation_achievment_congratulation_achievment__["a" /* CongratulationAchievmentPage */],
                __WEBPACK_IMPORTED_MODULE_16__components_popover_setting_popover_setting__["a" /* PopoverSettingComponent */],
                __WEBPACK_IMPORTED_MODULE_43__pages_device_sync_device_sync__["a" /* DeviceSyncPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_device_connected_device_connected__["a" /* DeviceConnectedPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_device_confirm_connect_device_confirm_connect__["a" /* DeviceConfirmConnectPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_device_confirm_unlink_device_confirm_unlink__["a" /* DeviceConfirmUnlinkPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_challenge_add_by_user_challenge_add_by_user__["a" /* ChallengeAddByUserPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_challenge_create_challenge_create__["a" /* ChallengeCreatePage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_challenge_dashboard_challenge_dashboard__["a" /* ChallengeDashboardPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_challenge_invite_challenge_invite__["a" /* ChallengeInvitePage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_challenge_join_challenge_join__["a" /* ChallengeJoinPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_challenge_your_rank_challenge_your_rank__["a" /* ChallengeYourRankPage */],
                __WEBPACK_IMPORTED_MODULE_17__components_popover_share_popover_share__["a" /* PopoverShareComponent */],
                __WEBPACK_IMPORTED_MODULE_55__pages_qr_code_qr_code__["a" /* QrCodePage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_applewatch_applewatch__["a" /* ApplewatchPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_reward_condition_reward_condition__["a" /* RewardConditionPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_user_profile_user_profile__["a" /* UserProfilePage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_profile_setting_profile_setting__["a" /* ProfileSettingPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_friends_friends__["a" /* FriendsPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_friend_profile_friend_profile__["a" /* FriendProfilePage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_pending_request_pending_request__["a" /* PendingRequestPage */],
                __WEBPACK_IMPORTED_MODULE_65__pages_how_to_challenges_work_how_to_challenges_work__["a" /* HowToChallengesWorkPage */],
                __WEBPACK_IMPORTED_MODULE_66__pages_term_and_condition_term_and_condition__["a" /* TermAndConditionPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_excercise_detail_excercise_detail__["a" /* ExcerciseDetailPage */],
                __WEBPACK_IMPORTED_MODULE_72__pages_health_health__["a" /* HealthPage */],
                __WEBPACK_IMPORTED_MODULE_73__pages_health_add_health_add__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_74__pages_health_welcome_health_welcome__["a" /* HealthWelcomePage */],
                __WEBPACK_IMPORTED_MODULE_75__pages_health_information_health_information__["a" /* HealthInformationPage */],
                __WEBPACK_IMPORTED_MODULE_76__pages_condition_Health_condition__["a" /* ConditionPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */], {
                    mode: 'md',
                    tabsPlacement: 'top'
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_14__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_33__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_heartrate_heartrate__["a" /* HeartratePage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_this_week_this_week__["a" /* ThisWeekPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_leader_board_leader_board__["a" /* LeaderBoardPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_weekly_achivement_weekly_achivement__["a" /* WeeklyAchivementPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_rewarded_smaile_points_rewarded_smaile_points__["a" /* RewardedSmailePointsPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_congratulation_congratulation__["a" /* CongratulationPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_congratulation_achievment_congratulation_achievment__["a" /* CongratulationAchievmentPage */],
                __WEBPACK_IMPORTED_MODULE_16__components_popover_setting_popover_setting__["a" /* PopoverSettingComponent */],
                __WEBPACK_IMPORTED_MODULE_43__pages_device_sync_device_sync__["a" /* DeviceSyncPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_device_connected_device_connected__["a" /* DeviceConnectedPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_device_confirm_connect_device_confirm_connect__["a" /* DeviceConfirmConnectPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_device_confirm_unlink_device_confirm_unlink__["a" /* DeviceConfirmUnlinkPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_challenge_add_by_user_challenge_add_by_user__["a" /* ChallengeAddByUserPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_challenge_create_challenge_create__["a" /* ChallengeCreatePage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_challenge_dashboard_challenge_dashboard__["a" /* ChallengeDashboardPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_challenge_invite_challenge_invite__["a" /* ChallengeInvitePage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_challenge_join_challenge_join__["a" /* ChallengeJoinPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_challenge_your_rank_challenge_your_rank__["a" /* ChallengeYourRankPage */],
                __WEBPACK_IMPORTED_MODULE_17__components_popover_share_popover_share__["a" /* PopoverShareComponent */],
                __WEBPACK_IMPORTED_MODULE_55__pages_qr_code_qr_code__["a" /* QrCodePage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_applewatch_applewatch__["a" /* ApplewatchPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_reward_condition_reward_condition__["a" /* RewardConditionPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_user_profile_user_profile__["a" /* UserProfilePage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_profile_setting_profile_setting__["a" /* ProfileSettingPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_friends_friends__["a" /* FriendsPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_friend_profile_friend_profile__["a" /* FriendProfilePage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_pending_request_pending_request__["a" /* PendingRequestPage */],
                __WEBPACK_IMPORTED_MODULE_65__pages_how_to_challenges_work_how_to_challenges_work__["a" /* HowToChallengesWorkPage */],
                __WEBPACK_IMPORTED_MODULE_66__pages_term_and_condition_term_and_condition__["a" /* TermAndConditionPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_excercise_detail_excercise_detail__["a" /* ExcerciseDetailPage */],
                __WEBPACK_IMPORTED_MODULE_72__pages_health_health__["a" /* HealthPage */],
                __WEBPACK_IMPORTED_MODULE_73__pages_health_add_health_add__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_74__pages_health_welcome_health_welcome__["a" /* HealthWelcomePage */],
                __WEBPACK_IMPORTED_MODULE_75__pages_health_information_health_information__["a" /* HealthInformationPage */],
                __WEBPACK_IMPORTED_MODULE_76__pages_condition_Health_condition__["a" /* ConditionPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_health_kit__["a" /* HealthKit */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_fcm__["a" /* FCM */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_18__providers_settings_settings__["a" /* SettingsProvider */],
                __WEBPACK_IMPORTED_MODULE_19__providers_login_login__["a" /* LoginProvider */],
                __WEBPACK_IMPORTED_MODULE_20__providers_config_config__["a" /* ConfigProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_this_week_this_week__["a" /* ThisWeekProvider */],
                __WEBPACK_IMPORTED_MODULE_22__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
                __WEBPACK_IMPORTED_MODULE_23__providers_device_sync_device_sync__["a" /* DeviceSyncProvider */],
                __WEBPACK_IMPORTED_MODULE_24__providers_device_confirm_connect_device_confirm_connect__["a" /* DeviceConfirmConnectProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_weekly_achievement_weekly_achievement__["a" /* WeeklyAchievementProvider */],
                __WEBPACK_IMPORTED_MODULE_26__providers_history_history__["a" /* HistoryProvider */],
                __WEBPACK_IMPORTED_MODULE_27__providers_leader_board_leader_board__["a" /* LeaderBoardProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_rewarded_smile_point_rewarded_smile_point__["a" /* RewardedSmilePointProvider */],
                __WEBPACK_IMPORTED_MODULE_29__providers_challenge_create_challenge_create__["a" /* ChallengeCreateProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_challenge_dashboard_challenge_dashboard__["a" /* ChallengeDashboardProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_challenge_join_challenge_join__["a" /* ChallengeJoinProvider */],
                __WEBPACK_IMPORTED_MODULE_58__providers_apple_apple__["a" /* AppleProvider */],
                __WEBPACK_IMPORTED_MODULE_58__providers_apple_apple__["a" /* AppleProvider */],
                __WEBPACK_IMPORTED_MODULE_58__providers_apple_apple__["a" /* AppleProvider */],
                __WEBPACK_IMPORTED_MODULE_68__providers_profile_setting_profile_setting__["a" /* ProfileSettingProvider */],
                __WEBPACK_IMPORTED_MODULE_69__providers_tabs_tabs__["a" /* TabsProvider */],
                __WEBPACK_IMPORTED_MODULE_70__providers_friends_friends__["a" /* FriendsProvider */],
                __WEBPACK_IMPORTED_MODULE_71__providers_friend_profile_friend_profile__["a" /* FriendProfileProvider */],
                __WEBPACK_IMPORTED_MODULE_32__providers_health_health__["a" /* HealthProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_fcm__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_sqlite__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_friends_friends__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { HomePage } from '../pages/home/home';
// import { ApplewatchPage } from '../pages/applewatch/applewatch';


// import { NotificationPage } from '../pages/notification/notification';
// import { FriendsPage } from '../pages/friends/friends';
// import { ProfileSettingPage } from '../pages/profile-setting/profile-setting';




var MyApp = /** @class */ (function () {
    function MyApp(friend, fcm, sqliteNative, objSQLite, zone, config, SQLite, platform, statusBar, splashScreen, events) {
        var _this = this;
        this.friend = friend;
        this.fcm = fcm;
        this.sqliteNative = sqliteNative;
        this.objSQLite = objSQLite;
        this.zone = zone;
        this.config = config;
        this.SQLite = SQLite;
        this.platform = platform;
        this.events = events;
        //DB
        this.win = window;
        //  rootPage: any ;//for test
        this.selectedTheme = "light-theme";
        // this.config.userNo='88970';//for test
        var me = this;
        platform.ready().then(function () {
            console.log("platform ready");
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            me.initializeDB(function () {
                console.log("open db success");
                me.createTableConfig(function () {
                    console.log("create table success");
                });
                // me.zone.run(()=>{
                //   me.rootPage=ApplewatchPage;
                // });
            });
            statusBar.overlaysWebView(false);
            statusBar.backgroundColorByHexString("#d30876");
            splashScreen.hide();
            if (platform.is("cordova")) {
                _this.initApp();
            }
            platform.registerBackButtonAction(function () { });
        });
    }
    MyApp.prototype.openDB = function () {
        var me = this;
        return this.sqliteNative
            .create({
            name: "mtl_healthy.db",
            location: "default"
        })
            .then(function (db) {
            //storage object to property
            me.config.dbNative = db;
        });
    };
    MyApp.prototype.initApp = function () {
        var _this = this;
        var me = this;
        console.log('init app');
        this.fcm.subscribeToTopic("senchabox_broadcast");
        this.fcm.getToken().then(function (token) {
            console.log('token device ==> ' + token);
            //add insert,update token
            me.config.tokenDevice = token;
        });
        this.fcm.onNotification().subscribe(function (data) {
            if (data.wasTapped) {
                console.log("Received in background");
                //call webservice  for update icon notification
                me.friend.getFriendRequestMe().then(function (data) {
                    console.log('friend request ', data.length);
                    me.config.objFriendReques = data;
                    me.config.totalNotification = data.length;
                });
            }
            else {
                _this.events.publish('notification:changes');
                console.log("Received in foreground");
                //call webservice for update icon notification
                me.friend.getFriendRequestMe().then(function (data) {
                    console.log('friend request ', data.length);
                    me.config.objFriendReques = data;
                    me.config.totalNotification = data.length;
                });
            }
        });
        this.fcm.onTokenRefresh().subscribe(function (token) {
            console.log('token ==> ' + token);
            //add insert update token
            me.config.tokenDevice = token;
        });
    };
    MyApp.prototype.initializeDB = function (func) {
        var me = this;
        if (me.platform.is("ios")) {
            me.openDB().then(function () {
                func();
            });
        }
        else {
            me.config.dbMaster = me.win.openDatabase("mtl_healthy", "1.0", "", 5 * 1024 * 1024);
            func();
        }
    };
    MyApp.prototype.createTableConfig = function (func) {
        var me = this;
        console.log("db ", me.config.dbMaster, " db native ", me.config.dbNative);
        var sql = "SELECT * FROM tb_config";
        me.objSQLite.executeQuery(me.config.dbMaster, sql, [], function (result) {
            console.log("db table ", result);
            if (result == null) {
                console.log("create table");
                sql =
                    "CREATE table IF NOT EXISTS tb_config " +
                        "(user_no VARCHAR(50) PRIMARY KEY ," +
                        "brand VARCHAR(50) ," +
                        "date_connect DATETIME);";
                console.log("create sql ", sql);
                me.objSQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
                    console.log(status);
                    me.zone.run(function () {
                        me.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
                        func();
                    });
                });
            }
            else {
                me.zone.run(function () {
                    if (result.rows.length == 0) {
                        me.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
                    }
                    else {
                        me.config.objProfile = result;
                        me.config.userNo = result.rows.item(0).user_no;
                        me.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */];
                    }
                    func();
                });
            }
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/app/app.html"*/'<ion-nav [root]="rootPage" [class]="selectedTheme"></ion-nav>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__providers_friends_friends__["a" /* FriendsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_fcm__["a" /* FCM */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_7__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FriendsProvider = /** @class */ (function () {
    function FriendsProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello FriendsProvider Provider');
    }
    FriendsProvider.prototype.searchFriendList = function (str) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('strSearch', str);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'searchFriendList', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider.prototype.searchSuggestFriend = function (str) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('strSearch', str);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'searchSuggestFriend', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider.prototype.getFriendList = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getFriendData', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider.prototype.getMutualFriendList = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getMutualFriendList', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    //#############//
    FriendsProvider.prototype.addFriend = function (friendNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('friendNo', friendNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'addFriend', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider.prototype.rejectFriendRequest = function (friendNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('friendNo', friendNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'rejectFriendRequest', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider.prototype.getFriendRequestMe = function () {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getFriendRequestMe', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider.prototype.updateFriendStatus = function (friendNo, friendStatus) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('friendNo', friendNo);
        params.set('friendStatus', friendStatus);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'updateFriendStatus', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider.prototype.requestFriend = function (friendNo) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('friendNo', friendNo);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'requestFriend', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    FriendsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], FriendsProvider);
    return FriendsProvider;
}());

//# sourceMappingURL=friends.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forgot_password_forgot_password__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_login_login__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__condition_Health_condition__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var LoginPage = /** @class */ (function () {
    function LoginPage(alertCtrl, loadingCtrl, SQLite, config, modalController, navCtrl, loginPvd) {
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.SQLite = SQLite;
        this.config = config;
        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.loginPvd = loginPvd;
        this.isenabled = false;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage ' + this.config.tokenDevice);
        // this.txtUsername='mtl88866';
        // this.txtPassword='P@ssw0rd';
    };
    LoginPage.prototype.btnLogin = function () {
        var _this = this;
        var me = this;
        if (this.txtUsername.length > 0 && this.txtPassword.length > 0) {
            var loader_1 = me.loadingCtrl.create({
                spinner: 'crescent'
            });
            loader_1.present();
            this.loginPvd.login(this.txtUsername, this.txtPassword).then(function (data) {
                console.log('login ', data);
                loader_1.dismiss();
                if (data != null && data.errMsg == null) {
                    // me.config.userNo='89107';//for test
                    me.config.userNo = data.userNo;
                    var sql = "INSERT INTO tb_config(user_no)VALUES('" + me.config.userNo + "')";
                    console.log('sql', sql);
                    me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
                        me.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'forward' });
                    });
                }
                else {
                    var alertMsg = _this.alertCtrl.create({
                        title: 'Login Message',
                        message: data.errMsg,
                        buttons: [
                            {
                                text: 'Yes'
                            }
                        ]
                    });
                    alertMsg.present();
                }
            });
        }
    };
    LoginPage.prototype.btnForgotPassword = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__forgot_password_forgot_password__["a" /* ForgotPasswordPage */], {}, { animate: true, direction: 'forward' });
    };
    LoginPage.prototype.createWaste = function () {
        // this.service.save(this.setDump())
        //     .then(() => {
        //       alert('salvo com sucesso')
        //     })
        console.log(this.isenabled);
    };
    LoginPage.prototype.condutionModal = function () {
        return __awaiter(this, void 0, void 0, function () {
            var termAndConditionModal;
            return __generator(this, function (_a) {
                termAndConditionModal = this.modalController.create(__WEBPACK_IMPORTED_MODULE_7__condition_Health_condition__["a" /* ConditionPage */], {}, { cssClass: 'select-modal' });
                termAndConditionModal.present();
                return [2 /*return*/];
            });
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/login/login.html"*/'<ion-content class="mainContent">\n\n  <div class="flex-center">\n    <div class="formLgoin">\n\n      <div class="logo">\n        <img src="assets/imgs/mtl-colour-rgb.png">\n      </div>\n\n      <div class="loginPadding">\n        <div class="textSignin">Sign in</div>\n\n        <div class="inputStyle">\n          <div class="lableForm">USERNAME</div>\n          <ion-item>\n            <ion-input type="text" [(ngModel)]="txtUsername" maxlength="20" placeholder="Please enter" clearInput>\n            </ion-input>\n          </ion-item>\n        </div>\n\n        <div class="inputStyle inputMarginTop">\n          <div class="lableForm">PASSWORD</div>\n          <ion-item>\n            <ion-input type="password" [(ngModel)]="txtPassword" maxlength="20" placeholder="Please enter" clearInput\n              (keyup.enter)="btnLogin()">\n            </ion-input>\n          </ion-item>\n        </div>\n\n        <!-- <div text-right class="textForgetPassword" (click)="btnForgotPassword()">\n          Forgot password\n        </div> -->\n\n        <ion-item text-wrap>\n          <ion-row>\n             <ion-col col-2 no-padding no-margin>\n               <ion-item no-padding no-margin no-lines>\n                 <ion-checkbox [(ngModel)]="isenabled" (click)="createWaste()"></ion-checkbox>\n               </ion-item>\n             </ion-col>\n             <ion-col col-10 no-padding no-margin>\n               <ion-item no-padding no-margin no-lines>\n                I agree to the <a target="_blank" (click)="condutionModal()">Terms and Conditions</a>\n               </ion-item>\n             </ion-col>\n           </ion-row>\n         </ion-item>\n\n        <div class="loginButton center-center" *ngIf="isenabled" (click)="btnLogin()">\n          <div>Login</div>\n        </div>\n        <div class="loginButtonEnabled center-center" *ngIf="!isenabled">\n          <div>Login</div>\n        </div>\n        <ion-row>\n          <ion-col col-6 style="margin-top: 20px; opacity: 0.4;">\n            In case of error, please\n          </ion-col>\n          <ion-col col-6 style="margin-top: 20px; color: #0070ffb3;" class="item" href="#" onclick="window.open(\'https://mtladss.muangthai.co.th/showLogin.cc\'); return false;">\n            reset your password\n          </ion-col>\n          <!-- <a style="margin-top: 23px;" class="item" href="#" onclick="window.open(\'https://mtladss.muangthai.co.th/showLogin.cc\'); return false;">\n            reset your password\n          </a> -->\n        </ion-row>\n        <div style="margin-top: 20px; opacity: 0.4;">\n          This is a beta version. Your data will be used to help us develop a better app in the future\n        </div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_login_login__["a" /* LoginProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileSettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__friends_friends__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_popover_setting_popover_setting__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__device_connected_device_connected__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__device_sync_device_sync__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__term_and_condition_term_and_condition__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__rewarded_smaile_points_rewarded_smaile_points__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__weekly_achivement_weekly_achivement__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_settings_settings__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_profile_setting_profile_setting__ = __webpack_require__(385);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

















var ProfileSettingPage = /** @class */ (function () {
    function ProfileSettingPage(profileSettingProvider, config, navCtrl, navParams, alertCtrl, camera, transfer, file, loadingCtrl, actionSheetController, popoverCtrl, modalCtrl, settings) {
        this.profileSettingProvider = profileSettingProvider;
        this.config = config;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetController = actionSheetController;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        this.settings = settings;
        this.segmentSwitch1 = "segment_button_active_history";
        this.segmentSwitch2 = "segment_button_inactive_history";
        this.userName = "-";
        this.totalSmilePoint = "0";
        this.currentPoint = "0";
        this.totalFriends = "0";
        this.userStatus = "-";
        this.userBadges = "0";
        this.userEvents = "0";
        var me = this;
        //this.userImg = "assets/imgs/avatar/Default.jpg";
        this.profileSettingProvider.getProfileSetting().then(function (data) {
            console.log(data);
            me.userName = data.userName;
            me.userImg = data.userImg;
            me.userStatus = data.userStatus;
            me.userBadges = data.currentLevel;
            me.totalFriends = data.totalFriend;
            me.totalSmilePoint = data.totalSmilePoint;
            me.userEvents = data.totalEvent;
            me.currentPoint = data.currentPoint;
        });
    }
    ProfileSettingPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ProfileSettingPage");
    };
    ProfileSettingPage.prototype.btnBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: "back" });
    };
    ProfileSettingPage.prototype.goWeeklyAchivement = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_14__weekly_achivement_weekly_achivement__["a" /* WeeklyAchivementPage */], {}, { animate: true, direction: "forward" });
    };
    ProfileSettingPage.prototype.goRewardSmilePoints = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_13__rewarded_smaile_points_rewarded_smaile_points__["a" /* RewardedSmailePointsPage */], {}, { animate: true, direction: "forward" });
    };
    ProfileSettingPage.prototype.goTabChallengeView = function () {
        this.settings.tabIndex = 3;
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], { currentTab: "challenge_view" }, { animate: true, direction: "back" });
    };
    ProfileSettingPage.prototype.goFriendsView = function () {
        console.log("go NotificationView");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__friends_friends__["a" /* FriendsPage */], {}, { animate: true, direction: "forward" });
    };
    ProfileSettingPage.prototype.btnSelectPhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            title: "Select method take photo",
                            buttons: [
                                {
                                    text: "Take Photo",
                                    role: "destructive",
                                    handler: function () {
                                        console.log("Take Photo xxx");
                                        _this.openCamera();
                                    }
                                },
                                {
                                    text: "Select from Gallery",
                                    handler: function () {
                                        console.log("Select from Gallery");
                                        _this.openPhotoGallery();
                                    }
                                },
                                {
                                    text: "Cancel",
                                    handler: function () {
                                        console.log("Cancel");
                                    }
                                },
                                {
                                    text: "Cancel",
                                    role: "cancel",
                                    handler: function () {
                                        console.log("Cancel clicked");
                                    }
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfileSettingPage.prototype.openCamera = function () {
        var me = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 150,
            targetHeight: 150,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            var filename = imageData.substring(imageData.lastIndexOf("/") + 1);
            var path = imageData.substring(0, imageData.lastIndexOf("/") + 1);
            me.file.readAsDataURL(path, filename).then(function (data) {
                console.log("xx ", data);
                me.userImg = data;
            });
            console.log("src image ", imageData);
            me.uploadImageProfile(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    ProfileSettingPage.prototype.openPhotoGallery = function () {
        var me = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 150,
            targetHeight: 150,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            console.log(Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* normalizeURL */])(imageData));
            var path = imageData.substring(0, imageData.lastIndexOf("/") + 1);
            var filename = imageData.substring(imageData.lastIndexOf("/") + 1);
            var index = filename.indexOf("?");
            if (index > -1) {
                filename = filename.substring(0, index);
            }
            me.file.readAsDataURL(path, filename).then(function (data) {
                console.log("### ", data);
                me.userImg = data;
            });
            me.uploadImageProfile(imageData);
        }, function (err) {
            console.log(err);
        });
    };
    ProfileSettingPage.prototype.uploadImageProfile = function (imageData) {
        var _this = this;
        var me = this;
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: this.config.userNo,
            fileName: "profile",
            httpMethod: "POST",
            headers: {
                "Content-Type": undefined
            }
        };
        var loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        fileTransfer
            .upload(imageData, encodeURI(this.config.serverUploadAPI), options)
            .then(function (data) {
            loader.dismiss();
        }, function (err) {
            loader.dismiss();
            var _alert = _this.alertCtrl.create({
                title: "Fail Upload",
                buttons: ["Dismiss"]
            });
            _alert.present();
            console.log(err);
        });
    };
    ProfileSettingPage.prototype.btnSegment = function (index) {
        if (index == 1) {
            this.segmentSwitch1 = "segment_button_active_history";
            this.segmentSwitch2 = "segment_button_inactive_history";
        }
        else if (index == 2) {
            this.segmentSwitch1 = "segment_button_inactive_history";
            this.segmentSwitch2 = "segment_button_active_history";
        }
    };
    ProfileSettingPage.prototype.presentPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_8__components_popover_setting_popover_setting__["a" /* PopoverSettingComponent */]);
        popover.present({
            ev: myEvent
        });
        popover.onDidDismiss(function (data) {
            console.log(data, "  config ", _this.config.objProfile);
            if (data === "deviceSync") {
                //check device sync? goto deviceConnected : DeviceSync
                if (_this.config.deviceConnected == true) {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__device_connected_device_connected__["a" /* DeviceConnectedPage */], {}, { animate: true, direction: "forward" });
                }
                else {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__device_sync_device_sync__["a" /* DeviceSyncPage */], {}, { animate: true, direction: "forward" });
                }
            }
            else if (data === "editProfile") {
                console.log("editProfile");
            }
            else if (data === "termAndCondition") {
                var termAndConditionModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_12__term_and_condition_term_and_condition__["a" /* TermAndConditionPage */], {});
                termAndConditionModal.present();
            }
            else if (data === "logout") {
                var confirm_1 = _this.alertCtrl.create({
                    title: "Logout System",
                    buttons: [
                        {
                            text: "No"
                        },
                        {
                            text: "Yes",
                            handler: function () {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__login_login__["a" /* LoginPage */], {}, { animate: true, direction: "forward" });
                            }
                        }
                    ]
                });
                confirm_1.present();
            }
        });
    };
    ProfileSettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-profile-setting",template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/profile-setting/profile-setting.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-buttons right>\n      <button ion-button icon-only (click)="presentPopover($event)">\n        <ion-icon name="settings"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<div class="fixedHeader topHeader">\n  <div class="imageHeader">\n    <div class="headerPadding">\n      <ion-row no-margin no-padding class="marginRow">\n        <ion-col col no-margin no-padding class="center-center">\n          <div class="imageProfile center-center">\n            <img\n              src="assets/imgs/icons/icon_camera.png"\n              class="positionIcon"\n              (click)="btnSelectPhoto()"\n            />\n\n            <div class="avatarProfileWidth">\n              <img src="{{userImg}}" />\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col no-margin no-padding class="center-center">\n          <div class="textInProfile">\n\n            <div class="textTitle">\n              {{userName}}\n            </div>\n\n            <div class="textPoint">\n              {{totalSmilePoint}}\n            </div>\n\n            <div class="textDetail">\n              {{userStatus}}\n            </div>\n            \n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n\n  <div class="topCurve">\n    <div class="topCurveWidth">\n      <ion-row no-padding no-margin>\n        <ion-col no-padding no-margin (click)="goWeeklyAchivement()">\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center">\n              <img\n                src="assets/imgs/icon_friend_profile/icon_award.png"\n                class="iconWidth"\n              />\n            </ion-col>\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextTitle">Level</span></ion-col\n            >\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextNumber">{{userBadges}}</span></ion-col\n            >\n          </ion-row>\n        </ion-col>\n\n        <ion-col no-padding no-margin (click)="goRewardSmilePoints()">\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center">\n              <img\n                src="assets/imgs/icon_friend_profile/icon_point.png"\n                class="iconWidth"\n              />\n            </ion-col>\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextTitle">Weekly Points</span></ion-col\n            >\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextNumber">{{currentPoint}}</span></ion-col\n            >\n          </ion-row>\n        </ion-col>\n\n        <ion-col no-padding no-margin (click)="goFriendsView()">\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center">\n              <img\n                src="assets/imgs/icon_friend_profile/icon_friend.png"\n                class="iconWidth"\n              />\n            </ion-col>\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextTitle">Friends</span></ion-col\n            >\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextNumber">{{totalFriends}}</span></ion-col\n            >\n          </ion-row>\n        </ion-col>\n\n        <ion-col no-padding no-margin (click)="goTabChallengeView()">\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center">\n              <img\n                src="assets/imgs/icon_friend_profile/icon_event.png"\n                class="iconWidth"\n              />\n            </ion-col>\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextTitle">Events</span></ion-col\n            >\n          </ion-row>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin class="center-center"\n              ><span class="topCurveTextNumber">{{userEvents}}</span></ion-col\n            >\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n</div>\n\n<ion-content class="mainContent">\n  <!-- <div class="segmentCenter">\n    <div class="segmentBg">\n      <ion-grid no-padding no-margin>\n        <ion-row no-padding no-margin>\n          <ion-col no-padding no-margin (click)="btnSegment(1)">\n            <div [ngClass]="segmentSwitch1" class="center-center">\n              <div class="marginSegmentTopHistory">Challenges</div>\n            </div>\n          </ion-col>\n          <ion-col no-padding no-margin (click)="btnSegment(2)">\n            <div [ngClass]="segmentSwitch2" class="center-center">\n              <div class="marginSegmentTopHistory">Achievements</div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n  </div> -->\n\n  <div class="cardContentPadding">\n    <!-- <div class="cardSeparate">\n      <div class="textCardTitle">Current Challenges</div>\n      <ion-card class="cardPaddingChallenge">\n        <ion-row class="rowMarin">\n          <ion-col col-4 col no-margin no-padding class="center-center">\n            <div class="imageChallenge center-center">\n              <img src="assets/imgs/profile/profile_photo_01.png" class="" />\n            </div>\n          </ion-col>\n\n          <ion-col col-8 no-margin no-padding class="center-left">\n            <div class="marginLeft">\n              <div class="titleHeaderInCard">MTL Marathon 2019</div>\n              <div class="titleContentInCard">July 2, 2019</div>\n              <div class="titleContentInCard">5,000 participants</div>\n              <button ion-button class="buttonJoin">join</button>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </div>\n\n    <div class="cardSeparate">\n      <div class="textCardTitle">Completed Challenges</div>\n      <ion-card class="cardPaddingChallenge">\n        <ion-row class="rowMarin">\n          <ion-col col-4 col no-margin no-padding class="center-center">\n            <div class="imageChallenge center-center">\n              <img src="assets/imgs/profile/profile_photo_02.png" class="" />\n            </div>\n          </ion-col>\n\n          <ion-col col-8 no-margin no-padding class="center-left">\n            <div class="marginLeft">\n              <div class="titleHeaderInCard">Weekly Challenge</div>\n              <div class="titleContentInCard">Jun 3 - Jun 9, 2019</div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n\n      <ion-card class="cardPaddingChallenge">\n        <ion-row class="rowMarin">\n          <ion-col col-4 col no-margin no-padding class="center-center">\n            <div class="imageChallenge center-center">\n              <img src="assets/imgs/profile/profile_photo_03.png" class="" />\n            </div>\n          </ion-col>\n\n          <ion-col col-8 no-margin no-padding class="center-left">\n            <div class="marginLeft">\n              <div class="titleHeaderInCard">Weekly Challenge</div>\n              <div class="titleContentInCard">Jun 3 - Jun 9, 2019</div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n\n      <ion-card class="cardPaddingChallenge">\n        <ion-row class="rowMarin">\n          <ion-col col-4 col no-margin no-padding class="center-center">\n            <div class="imageChallenge center-center">\n              <img src="assets/imgs/profile/profile_photo_01.png" class="" />\n            </div>\n          </ion-col>\n\n          <ion-col col-8 no-margin no-padding class="center-left">\n            <div class="marginLeft">\n              <div class="titleHeaderInCard">Weekly Challenge</div>\n              <div class="titleContentInCard">Jun 3 - Jun 9, 2019</div>\n              <div class="titleContentInCard">5,000 participants</div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </div> -->\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/profile-setting/profile-setting.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_16__providers_profile_setting_profile_setting__["a" /* ProfileSettingProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_15__providers_settings_settings__["a" /* SettingsProvider */]])
    ], ProfileSettingPage);
    return ProfileSettingPage;
}());

//# sourceMappingURL=profile-setting.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingsProvider = /** @class */ (function () {
    function SettingsProvider() {
        this.theme = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["BehaviorSubject"]('dark-theme');
    }
    SettingsProvider.prototype.setActiveTheme = function (val) {
        this.theme.next(val);
    };
    SettingsProvider.prototype.getActiveTheme = function () {
        return this.theme.asObservable();
    };
    SettingsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], SettingsProvider);
    return SettingsProvider;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 6:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConfigProvider = /** @class */ (function () {
    function ConfigProvider() {
        this.deviceConnected = false;
        this.currentLevel = "0";
        this.allLevel = "5";
        this.tokenDevice = "";
        this.totalNotification = 0;
        this.refreshHistory = false;
        this.refreshLeaderboard = false;
        this.refreshChallengeDashboard = false;
        console.log('Hello ConfigProvider Provider');
        // this.serverAPI="http://localhost:58124/";
        // this.serverAPI='http://10.115.103.128/MTLHealthyAPI/';
        this.serverAPI = 'https://apixdev.muangthai.co.th/MTLHealthyAPI2/'; //https only
        this.serverUploadAPI = 'http://apixdev.muangthai.co.th/MTLHealthyAPI/UploadFile';
        // this.serverUploadAPI='http://10.31.53.133/MTLHealthyAPI/UploadFile';
        this.appVersion = '0.35';
        this.serverGolang = 'https://gateway-uat.muangthai.co.th/api/mtlhealth/';
    }
    ConfigProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ConfigProvider);
    return ConfigProvider;
}());

//# sourceMappingURL=config.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSyncProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeviceSyncProvider = /** @class */ (function () {
    function DeviceSyncProvider(http, config) {
        this.http = http;
        this.config = config;
        console.log('Hello DeviceSyncProvider Provider');
    }
    DeviceSyncProvider.prototype.getDeviceConfig = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'Config/getDeviceConfig', {})
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
                resolve(null);
            });
        });
    };
    DeviceSyncProvider.prototype.getLink = function (brand) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('brand', brand);
        return new Promise(function (resolve) {
            // https://apixdev.muangthai.co.th/MTLHealthyAPI/FitbitOAuth/Authorize
            _this.http.get(_this.config.serverAPI + 'getLink', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log('link ', data);
                resolve(data);
            });
        });
    };
    DeviceSyncProvider.prototype.registerUser = function (brand, token) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('brand', brand);
        params.set('token', token);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'registerUser', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    //########## APPLE ##########
    DeviceSyncProvider.prototype.getSyncDate = function (brand, mode) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('brand', brand);
        params.set('mode', mode);
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.get(_this.config.serverAPI + 'getSyncDate', { search: params })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    DeviceSyncProvider.prototype.syncAppleRawData = function (lsStep, lstWorkoOut) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */]();
        params.set('userNo', this.config.userNo);
        params.set('lsStep', JSON.stringify(lsStep));
        params.set('lsWorkOut', JSON.stringify(lstWorkoOut));
        console.log('param ', params);
        return new Promise(function (resolve) {
            _this.http.post(_this.config.serverAPI + 'syncAppleRawData', params)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log('error ', err);
                resolve(null);
            });
        });
    };
    DeviceSyncProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* ConfigProvider */]])
    ], DeviceSyncProvider);
    return DeviceSyncProvider;
}());

//# sourceMappingURL=device-sync.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverShareComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopoverShareComponent = /** @class */ (function () {
    function PopoverShareComponent(navCtrl, viewCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
    }
    PopoverShareComponent.prototype.btnQR = function () {
        this.viewCtrl.dismiss('qr');
    };
    PopoverShareComponent.prototype.btnEmail = function () {
        this.viewCtrl.dismiss('email');
    };
    PopoverShareComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'popover-share',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/components/popover-share/popover-share.html"*/'<ion-list no-lines>\n  <ion-item (click)="btnQR()">\n    <div class="text-right">QR Code</div>\n  </ion-item>\n  <!-- <div class="line-seperate"></div>\n  <ion-item (click)="btnEmail()">\n    <div class="text-right">Email</div>\n  </ion-item> -->\n</ion-list>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/components/popover-share/popover-share.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], PopoverShareComponent);
    return PopoverShareComponent;
}());

//# sourceMappingURL=popover-share.js.map

/***/ }),

/***/ 737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeartratePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_home__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_health_kit__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeartratePage = /** @class */ (function () {
    function HeartratePage(navCtrl, navParams, healthKit) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.healthKit = healthKit;
        this.heartRateArray = [];
        this.selectDate = this.navParams.get('passDate');
        console.log(this.selectDate);
        this.showDateTitle = this.selectDate.myStartDate.toLocaleDateString("en-US");
    }
    HeartratePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HeartratePage');
        this.showHeartrate();
    };
    HeartratePage.prototype.showHeartrate = function () {
        var _this = this;
        var options = {
            startDate: this.selectDate.myStartDate,
            endDate: this.selectDate.myEndDate,
            sampleType: 'HKQuantityTypeIdentifierHeartRate',
            unit: 'count/min',
            limit: 1000
        };
        this.healthKit.querySampleType(options).then(function (data) {
            console.log(data);
            console.log(data.length);
            // console.log(data[0]);
            // console.log(data[0].quantity);
            _this.heartRateArray = data;
        }, function (err) {
            console.log('No Heartrate Data: ', err);
        });
    };
    HeartratePage.prototype.back = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__home_home__["a" /* HomePage */], {}, { animate: true, direction: 'back' });
    };
    HeartratePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-heartrate',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/heartrate/heartrate.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Heart Rate : {{showDateTitle}}</ion-title>\n        <ion-buttons left>\n          <button ion-button icon-only (click)="back()">\n            <ion-icon name="arrow-back"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<div class="favorite-list" (click)="showHeartrate()" *ngIf="heartRateArray.length == 0">\n  <ion-grid no-margin>\n    <ion-row>\n      <ion-col text-center>\n      <ion-icon name="heart"></ion-icon>\n      <span class="data-type">&nbsp;Please Update Heart Rate First! </span>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</div>\n\n    <ion-list>\n    <ion-item *ngFor="let heartrate of heartRateArray">\n      \n      <ion-row>       \n        <ion-col *ngIf="heartrate.metadata.HKMetadataKeyHeartRateMotionContext == 2">\n          <ion-icon name="md-flame"></ion-icon>&nbsp;&nbsp;{{ heartrate.quantity }}\n        </ion-col>\n        \n        <ion-col *ngIf="heartrate.metadata.HKMetadataKeyHeartRateMotionContext == 0 || heartrate.metadata.HKMetadataKeyHeartRateMotionContext == 1">\n          <ion-icon name="heart"></ion-icon>&nbsp;&nbsp;{{ heartrate.quantity }}\n        </ion-col>\n\n        <ion-col text-right>\n          {{ heartrate.startDate | date: \'H:mm:ss\'  }}\n        </ion-col>\n      </ion-row>\n\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/heartrate/heartrate.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_health_kit__["a" /* HealthKit */]])
    ], HeartratePage);
    return HeartratePage;
}());

//# sourceMappingURL=heartrate.js.map

/***/ }),

/***/ 738:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeAddByUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChallengeAddByUserPage = /** @class */ (function () {
    function ChallengeAddByUserPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChallengeAddByUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChallengeAddByUserPage');
    };
    ChallengeAddByUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-challenge-add-by-user',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-add-by-user/challenge-add-by-user.html"*/'<!--\n  Generated template for the ChallengeAddByUserPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>ChallengeAddByUser</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-add-by-user/challenge-add-by-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ChallengeAddByUserPage);
    return ChallengeAddByUserPage;
}());

//# sourceMappingURL=challenge-add-by-user.js.map

/***/ }),

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeInvitePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChallengeInvitePage = /** @class */ (function () {
    function ChallengeInvitePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChallengeInvitePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChallengeInvitePage');
    };
    ChallengeInvitePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-challenge-invite',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-invite/challenge-invite.html"*/'<!--\n  Generated template for the ChallengeInvitePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>ChallengeInvite</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-invite/challenge-invite.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ChallengeInvitePage);
    return ChallengeInvitePage;
}());

//# sourceMappingURL=challenge-invite.js.map

/***/ }),

/***/ 740:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeYourRankPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChallengeYourRankPage = /** @class */ (function () {
    function ChallengeYourRankPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChallengeYourRankPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChallengeYourRankPage');
    };
    ChallengeYourRankPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-challenge-your-rank',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-your-rank/challenge-your-rank.html"*/'<!--\n  Generated template for the ChallengeYourRankPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>ChallengeYourRank</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/challenge-your-rank/challenge-your-rank.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ChallengeYourRankPage);
    return ChallengeYourRankPage;
}());

//# sourceMappingURL=challenge-your-rank.js.map

/***/ }),

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplewatchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_health_kit__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_device_sync_device_sync__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_this_week_this_week__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ApplewatchPage = /** @class */ (function () {
    function ApplewatchPage(thisWeek, deviceSync, config, SQLite, healthKit, platform, navCtrl) {
        // var start = new Date();
        // start.setDate(start.getDate() - 7)
        // start.setHours(0, 0, 0, 0);
        // console.log('check start date ', start)
        this.thisWeek = thisWeek;
        this.deviceSync = deviceSync;
        this.config = config;
        this.SQLite = SQLite;
        this.healthKit = healthKit;
        this.platform = platform;
        this.navCtrl = navCtrl;
        // var end = new Date();
        // //end.setDate(end.getDate());
        // end.setHours(23, 59, 59, 0); // next midnight
        //  this.objDate = {
        //   startDate: start,
        //   endDate: end
        // }
    }
    ApplewatchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApplewatchPage');
    };
    ApplewatchPage.prototype.checkPermission = function (func) {
        var me = this;
        this.platform.ready().then(function () {
            me.healthKit.available().then(function (available) {
                if (available) {
                    // Request all permissions up front if you like to
                    var options = {
                        readTypes: [
                            'HKQuantityTypeIdentifierStepCount',
                            'HKWorkoutTypeIdentifier',
                            'HKQuantityTypeIdentifierActiveEnergyBurned',
                            'HKQuantityTypeIdentifierDistanceCycling',
                            'HKQuantityTypeIdentifierHeartRate',
                            'HKQuantityTypeIdentifierDistanceWalkingRunning'
                        ]
                    };
                    me.healthKit.requestAuthorization(options).then(function (_) {
                        console.log('allow permisstion');
                        func("allow");
                    });
                }
                else {
                    console.log('ios can not access health function');
                    func("denied");
                }
            });
        });
    };
    ApplewatchPage.prototype.requestPermission = function () {
        this.checkPermission(function (result) {
            console.log('check permission ', result);
        });
    };
    ApplewatchPage.prototype.checkDeviceConnect = function (func) {
        var me = this;
        var sql = "SELECT * FROM tb_config";
        var status = '';
        console.log('db ', me.config.dbMaster);
        me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result) {
            console.log('get userNo from db ', result.rows);
            if (result.rows.length > 0)
                status = result.rows.item(0).brand;
            func(status);
        });
    };
    ApplewatchPage.prototype.deviceConnect = function () {
        console.log('connect');
        var me = this;
        this.checkDeviceConnect(function (statusConnect) {
            console.log('status Connect ', statusConnect);
            if (statusConnect == undefined || statusConnect == '') {
                console.log('disconnect');
                // let statusPermission = me.checkPermission();
                me.checkPermission(function (statusPermission) {
                    console.log('status permission ', statusPermission);
                    me.config.userNo = '81276';
                    if (statusPermission == 'allow') {
                        console.log('change status device connected');
                        var sql = "INSERT OR REPLACE INTO tb_config(user_no,brand,date_connect)VALUES('" + me.config.userNo + "','Apple',DATE())";
                        console.log('sql', sql);
                        me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
                            console.log('query status ', status);
                        });
                    }
                });
            }
            else {
                console.log('connect ', statusConnect);
            }
        });
    };
    ApplewatchPage.prototype.deviceDisconnect = function () {
        console.log('disconnect');
        var me = this;
        var sql = "UPDATE tb_config SET brand='' WHERE user_no='" + me.config.userNo + "'";
        console.log('sql update ', sql);
        me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
            console.log('status update ', status);
        });
    };
    ApplewatchPage.prototype.syncData = function () {
        //call webservice get startDate-endDate
        console.log('objDate ', this.objDate);
        // this.getStep(this.objDate);
        // this.getWorkOut();
    };
    ApplewatchPage.prototype.getStep = function (objDate, func) {
        console.log('step');
        var stepOptions = {
            'startDate': objDate.startDate,
            'endDate': objDate.endDate,
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierStepCount',
            'unit': 'count'
        };
        this.healthKit.querySampleTypeAggregated(stepOptions).then(function (data) {
            // console.log('objStep ', data, ' step ', data[0].quantity);
            func(data);
        }, function (err) {
            console.log('No steps: ', err);
            func(err);
        });
    };
    ApplewatchPage.prototype.getActivity = function (func) {
        var _this = this;
        var me = this;
        if (this.platform.is('cordova')) {
            this.platform.ready().then(function () {
                me.healthKit.available().then(function (available) {
                    if (available) {
                        // Request all permissions up front if you like to
                        var options = {
                            readTypes: [
                                'HKQuantityTypeIdentifierStepCount',
                                'HKWorkoutTypeIdentifier',
                                'HKQuantityTypeIdentifierActiveEnergyBurned',
                                'HKQuantityTypeIdentifierDistanceCycling',
                                'HKQuantityTypeIdentifierHeartRate',
                                'HKQuantityTypeIdentifierDistanceWalkingRunning'
                            ]
                        };
                        me.healthKit.requestAuthorization(options).then(function (_) {
                            console.log('allow permisstion');
                            _this.healthKit.findWorkouts().then(function (x) {
                                console.log('workout ==> ', x, ' !!!!');
                                func(x);
                                //this.workoutData = x;
                            }, function (err) {
                                console.log(err);
                                // Sometimes the result comes in here, very strange.
                                func(err);
                                // this.workoutData = err;
                            });
                        });
                    }
                    else {
                        console.log('ios can not access health function');
                    }
                });
            });
        }
    };
    ApplewatchPage.prototype.mapActivityHR = function (objActivity, lsWorkout, func) {
        var me = this;
        if (objActivity.length > 0) {
            var obj = objActivity.shift();
            this.getHeartRate(new Date(obj.startDate), new Date(obj.endDate), function (objHR) {
                console.log('obj HR ', objHR);
                obj["lsHR"] = objHR;
                lsWorkout.push(obj);
                me.mapActivityHR(objActivity, lsWorkout, func);
            });
        }
        else {
            func(lsWorkout);
        }
    };
    ApplewatchPage.prototype.getWorkOutx = function () {
        var me = this;
        // let objAppleWatch={};
        var lsWorkout = [];
        me.config.userNo = '81276';
        console.log('ls ', lsWorkout);
        me.deviceSync.getSyncDate('Apple', 'step').then(function (syncDate) {
            console.log('sync Date ', syncDate);
            var start = new Date(syncDate);
            // start.setDate(start.getDate() - 7)
            start.setHours(0, 0, 0, 0);
            console.log('check start date ', start);
            var end = new Date();
            end.setHours(23, 59, 59, 0);
            console.log('check end date ', end);
            me.objDate = {
                startDate: start,
                endDate: end
            };
            console.log('obj date ', me.objDate);
            me.getStep(me.objDate, function (objStep) {
                console.log('lsStep ', objStep);
                // console.log('objStep.lsStep ',objStep.lsStep);
                // objAppleWatch['lsStep']=objStep;
                // console.log('chk ',objAppleWatch)
                // lsWorkout.push(objStep);
                // console.log('chk ls ',lsWorkout);
                me.getActivity(function (obj) {
                    if (obj === void 0) { obj = []; }
                    me.workoutData = JSON.parse(JSON.stringify(obj)); //clone
                    console.log('lsWorkout ', obj);
                    var newWorkOut = obj.filter(function (data) { return new Date(data.startDate) >= start; });
                    console.log('filter workout ', newWorkOut);
                    me.mapActivityHR(newWorkOut, lsWorkout, function (objWorkout) {
                        console.log('workout & HR ---------> ', objWorkout);
                        me.deviceSync.syncAppleRawData(objStep, objWorkout).then(function (result) {
                            console.log('xxxxx ', result);
                            if (result == 1) {
                                console.log('sync');
                                me.thisWeek.syncData('Apple').then(function (objSync) {
                                    console.log('====> ', objSync);
                                });
                            }
                        });
                    });
                });
            });
        });
        // console.log('workout ==> ', data, ' ===>',data.length);
        // for(var i=0;i<data.length;i++){
        //   console.log(data[i]);
        // }
        // for (var d = new Date(2012, 0, 1); d <= now; d.setDate(d.getDate() + 1)) {
        // }
    };
    ApplewatchPage.prototype.getHeartRate = function (start, end, func) {
        console.log('HR ');
        // var start = new Date();
        //   start.setDate(start.getDate() - 2)
        //   start.setHours(0, 0, 0, 0);
        //   console.log('check start date ', start)
        //   var end = new Date();
        //   end.setDate(end.getDate()-2);
        //   end.setHours(23, 59, 59, 0); // next midnight
        console.log('start ', start, ' end ', end);
        var options = {
            startDate: start,
            endDate: end,
            sampleType: 'HKQuantityTypeIdentifierHeartRate',
            unit: 'count/min',
            limit: 3000
        };
        this.healthKit.querySampleType(options).then(function (data) {
            console.log(data);
            console.log(data.length);
            func(data);
        }, function (err) {
            console.log('No Heartrate Data: ', err);
            func(err);
        });
    };
    ApplewatchPage.prototype.getQuery = function () {
        console.log('query');
        var stepOptions = {
            startDate: this.objDate.startDate,
            endDate: this.objDate.endDate,
            sampleType: 'HKQuantityTypeIdentifierStepCount',
            unit: 'count'
        };
        this.healthKit.querySampleType(stepOptions).then(function (data) {
            console.log('data ', data);
        });
    };
    ApplewatchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-applewatch',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/applewatch/applewatch.html"*/'<!--\n  Generated template for the ApplewatchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>applewatch</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <button (click)=\'requestPermission()\'>Permission</button>\n  <button (click)=\'deviceConnect()\'>Connect</button>\n  <button (click)=\'deviceDisconnect()\'>Disconnect</button>\n  <button (click)=\'syncData()\'>Sync</button>\n  <button (click)=\'getWorkOutx()\'>workout</button>\n  <button (click)=\'getQuery()\'>Query</button>\n  <button (click)=\'getHeartRate()\'>Heart Rate</button>\n\n  <ion-list>\n    <ion-card *ngFor="let workout of workoutData">\n      <ion-card-header>{{ workout.calories }}</ion-card-header>\n      <ion-card-content>\n        <p>Activity: {{ workout.activityType }}</p>\n        <p>Duration: {{ workout.duration / 100 }} min</p>\n        <p>Date: {{ workout.startDate | date:\'short\' }}</p>\n        <p>Distance: {{ workout.miles }} miles</p>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/applewatch/applewatch.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_this_week_this_week__["a" /* ThisWeekProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_device_sync_device_sync__["a" /* DeviceSyncProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_health_kit__["a" /* HealthKit */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
    ], ApplewatchPage);
    return ApplewatchPage;
}());

//# sourceMappingURL=applewatch.js.map

/***/ }),

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UserProfilePage = /** @class */ (function () {
    function UserProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    UserProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UserProfilePage');
    };
    UserProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-user-profile',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/user-profile/user-profile.html"*/'<!--\n  Generated template for the UserProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>UserProfile</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/user-profile/user-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], UserProfilePage);
    return UserProfilePage;
}());

//# sourceMappingURL=user-profile.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSyncPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_confirm_connect_device_confirm_connect__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__device_connected_device_connected__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_device_sync_device_sync__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_web_sql_web_sql__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__profile_setting_profile_setting__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// declare var cordova: any;









var DeviceSyncPage = /** @class */ (function () {
    function DeviceSyncPage(platform, loadingCtrl, iab, 
        // private zone: NgZone,
        config, SQLite, deviceSyncProvider, navCtrl, navParams, modalCtrl) {
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.iab = iab;
        this.config = config;
        this.SQLite = SQLite;
        this.deviceSyncProvider = deviceSyncProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        var me = this;
        me.deviceSyncProvider.getDeviceConfig().then(function (data) {
            console.log('getDeviceConfig ', data);
            me.deviceData = data;
            me.config.deviceData = data;
            // console.log('find apple ',data.findIndex(x=>x.deviceBrand=='Apple'))
            // if(me.platform.is('android')) {
            //   let indexApple =data.findIndex(x=>x.deviceBrand=='Apple');
            //   console.log('android')
            //   data.removeAt(0)
            //   console.log('chk ',data)
            // }else{
            //   console.log('ios')
            // }
            console.log('deviceData ', me.config.deviceData);
        });
    }
    DeviceSyncPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DeviceSyncPage');
    };
    DeviceSyncPage.prototype.btnSkip = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__profile_setting_profile_setting__["a" /* ProfileSettingPage */], {}, { animate: true, direction: 'back' });
    };
    DeviceSyncPage.prototype.btnConnectDevice = function (data) {
        var _this = this;
        var me = this;
        console.log('btnConnectDevice', data);
        var deviceConfirmModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__device_confirm_connect_device_confirm_connect__["a" /* DeviceConfirmConnectPage */], { record: data });
        deviceConfirmModal.present();
        deviceConfirmModal.onDidDismiss(function (url) {
            console.log('url ####', url, 'chk ', url == '');
            if (data.deviceBrand != 'Apple' && url != '') {
                console.log('url ', url);
                var browser_1 = me.iab.create(url, "_blank", "location=no");
                browser_1.on('exit').subscribe(function (event) {
                    console.log('browser closed', event);
                }, function (err) {
                    console.error(err);
                });
                browser_1.on('loadstop').subscribe(function (event) {
                    console.log('event ', event.url);
                    var currentURL = new URL(event.url);
                    console.log('url ', currentURL);
                    var code = currentURL.searchParams.getAll('code');
                    console.log('code ', code, ' ', code.length);
                    if (code.length > 0) {
                        browser_1.close();
                        me.deviceSyncProvider.registerUser(data.deviceBrand, code).then(function (result) {
                            console.log('register authen', result);
                            console.log(data.deviceBrand);
                            data.status = 'Y';
                            //save in brand , dateConnect in db
                            var sql = "INSERT OR REPLACE INTO tb_config(user_no,brand,date_connect)VALUES('" + me.config.userNo + "','" + data.deviceBrand + "',DATE())";
                            console.log('sql', sql);
                            _this.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
                                console.log(status);
                                sql = "SELECT * FROM tb_config WHERE user_no='" + me.config.userNo + "'";
                                console.log('### ', sql);
                                me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result) {
                                    console.log(result);
                                    me.config.objProfile = result;
                                    data.dateConnect = result.rows.item(0).date_connect; //[0]["date_connect"];
                                    me.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__device_connected_device_connected__["a" /* DeviceConnectedPage */], {}, { animate: true, direction: 'forward' });
                                });
                            });
                        });
                    }
                });
            }
            else {
                if (data.deviceBrand == 'Apple') {
                    if (url == 'allow')
                        data.status = 'Y';
                    //save in brand , dateConnect in db
                    var sql_1 = "INSERT OR REPLACE INTO tb_config(user_no,brand,date_connect)VALUES('" + me.config.userNo + "','" + data.deviceBrand + "',DATE())";
                    console.log('sql', sql_1);
                    _this.SQLite.executeNonQuery(me.config.dbMaster, sql_1, [], function (status) {
                        console.log(status);
                        sql_1 = "SELECT * FROM tb_config WHERE user_no='" + me.config.userNo + "'";
                        console.log('### ', sql_1);
                        me.SQLite.executeQuery(me.config.dbMaster, sql_1, [], function (result) {
                            console.log(result);
                            me.config.objProfile = result;
                            data.dateConnect = result.rows.item(0).date_connect;
                            me.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__device_connected_device_connected__["a" /* DeviceConnectedPage */], {}, { animate: true, direction: 'forward' });
                        });
                    });
                }
            }
        });
    };
    DeviceSyncPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-device-sync',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-sync/device-sync.html"*/'<ion-header>\n\n  <ion-toolbar>\n    <ion-buttons right>\n      <button ion-button icon-only (click)="btnSkip()">\n        Skip\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n\n  <div class="headerTop">\n    <div class="topCurve">\n      <div class="textFloatArea">\n        <div class="textTitle">\n          Welcome, MTL Health App\n        </div>\n        <div class="textDescription">\n          Choose the device\n          and sync your activity\n        </div>\n      </div>\n    </div>\n  </div>\n\n</ion-header>\n\n\n<ion-content class="mainContent">\n\n    <div class="cardContentPadding">\n        <ion-card class="cardPadding" *ngFor="let record of deviceData;let i=index">\n          <ion-row no-margin no-padding (click)="btnConnectDevice(record)">\n            <ion-col col-4 no-margin no-padding class="center-center">\n              <img src="{{record.deviceImage}}" class="imgDevice">\n            </ion-col>\n            <ion-col no-margin no-padding class="center-left">\n              <img src="{{record.deviceBrandImage}}" class="imgLogo">\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </div>\n\n  <!-- <div class="cardContentPadding">\n    <ion-card class="cardPadding" *ngFor="let record of mockupData;let i=index">\n      <ion-row no-margin no-padding (click)="btnConnectDevice(record)">\n        <ion-col col-4 no-margin no-padding class="center-center">\n          <img src="assets/imgs/devices/{{record.device_photo}}" class="imgDevice">\n        </ion-col>\n        <ion-col no-margin no-padding class="center-left">\n          <img src="assets/imgs/devices/logo/{{record.logo_photo}}" class="imgLogo">\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div> -->\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-sync/device-sync.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_6__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_device_sync_device_sync__["a" /* DeviceSyncProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], DeviceSyncPage);
    return DeviceSyncPage;
}());

//# sourceMappingURL=device-sync.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceConnectedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__device_confirm_unlink_device_confirm_unlink__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_config_config__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_device_sync_device_sync__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DeviceConnectedPage = /** @class */ (function () {
    function DeviceConnectedPage(
        // private zone:NgZone,
        SQLite, deviceSyncProvider, config, navCtrl, navParams, modalCtrl) {
        this.SQLite = SQLite;
        this.deviceSyncProvider = deviceSyncProvider;
        this.config = config;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        var me = this;
        //this.deviceData = this.navParams.get('obj');
        console.log('xxx =>', this.config.deviceData);
        if (this.config.deviceData == undefined) {
            this.deviceSyncProvider.getDeviceConfig().then(function (data) {
                console.log('!!!!! ', data, ' me.config.objProfile.rows ', me.config.objProfile.rows);
                var objConnected = data.find(function (x) { return x.deviceBrand === me.config.objProfile.rows.item(0).brand; }); //[0]['brand']);
                console.log('obj connect ', objConnected);
                me.objDeviceBrand = objConnected.deviceBrand;
                data[objConnected.deviceNo - 1].status = 'Y';
                console.log('obj profile ', me.config.objProfile);
                data[objConnected.deviceNo - 1].dateConnect = me.config.objProfile.rows.item(0).date_connect; //[0]['date_connect'];
                me.deviceData = data;
                me.config.deviceData = data;
            });
        }
        else {
            me.deviceData = me.config.deviceData;
        }
    }
    DeviceConnectedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DeviceConnectedPage');
    };
    DeviceConnectedPage.prototype.btnDashboard = function () {
        console.log('btnDashboard');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
    };
    DeviceConnectedPage.prototype.btnUnlink = function (data) {
        console.log('unlink ', data);
        var me = this;
        if (data.status == 'Y') {
            var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__device_confirm_unlink_device_confirm_unlink__["a" /* DeviceConfirmUnlinkPage */], { record: data });
            profileModal.present();
            profileModal.onDidDismiss(function (data) {
                console.log('====>', data);
                // if (data == 'unlink') {
                if (data > 0) {
                    //update status deviceData from index-1 
                    var objDisConnect = me.config.deviceData.find(function (x) { return x.deviceNo === data; });
                    console.log('device dis connect ', objDisConnect);
                    me.config.deviceData[objDisConnect.deviceNo - 1].status = 'N';
                    console.log('end ', me.config.deviceData);
                    // delete record in db
                    var sql_1 = "UPDATE tb_config SET brand='' WHERE user_no='" + me.config.userNo + "'";
                    console.log('sql delete ', sql_1);
                    me.SQLite.executeNonQuery(me.config.dbMaster, sql_1, [], function (status) {
                        console.log('status delete ', status);
                        sql_1 = "SELECT * FROM tb_config WHERE user_no='" + me.config.userNo + "'";
                        console.log('sql select', sql_1);
                        me.SQLite.executeQuery(me.config.dbMaster, sql_1, [], function (result) {
                            console.log(result);
                            me.config.objProfile = result;
                            // me.config.objProfile = undefined;
                            me.config.deviceData = undefined;
                            me.config.deviceConnected = false;
                            me.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {}, { animate: true, direction: 'back' });
                        });
                    });
                }
            });
        }
    };
    DeviceConnectedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-device-connected',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-connected/device-connected.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Success!\n    </ion-title>\n  </ion-toolbar>\n\n  <div class="headerTop">\n    <div class="topCurve">\n      <div class="textFloatArea">\n        <div class="textDescription">\n          Your {{objDeviceBrand}} Account\n          is linked to MTL HealthApp.\n        </div>\n        <div class="textTitle">\n          Please allow 3-5 minutes for synced\n          activities to appear on MTL HealthApp\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-header>\n\n\n<ion-content class="mainContent">\n\n  <div class="cardContentPadding">\n    <ion-card *ngFor="let record of deviceData;let i= index" [ngClass]="record.status == \'N\'?\'active-item-color\':\'\'">\n\n      <ion-row no-margin no-padding>\n        <ion-col col-4 no-margin no-padding class="center-center">\n          <div>\n            <img src="{{record.deviceImage}}" class="imgDevice">\n          </div>\n        </ion-col>\n        <ion-col no-margin no-padding class="center-center">\n\n          <div>\n            <img src="{{record.deviceBrandImage}}" class="imgLogo">\n            <div class="textTitle "></div>\n\n            <ion-row no-margin no-padding class="marginTextPoint">\n              <ion-col no-margin no-padding class="center-right">\n                <div *ngIf="record.status==\'Y\'" class="textLastSync">\n                  Last sync:{{record.dateConnect}}\n                </div>\n              </ion-col>\n            </ion-row>\n            <ion-row no-margin no-padding class="marginTextPoint">\n              <ion-col no-margin no-padding class="center-right">\n                <div *ngIf="record.status==\'Y\'" class="textUnlink" (click)="btnUnlink(record)">\n                  Unlink Device\n                </div>\n              </ion-col>\n            </ion-row>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div>\n\n</ion-content>\n\n\n<ion-footer>\n\n  <div class="footerPadding">\n    <div class="connectDeviceButton center-center" (click)="btnDashboard()">\n      <div class="textActivities">See your activities</div>\n    </div>\n  </div>\n\n</ion-footer>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/device-connected/device-connected.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_web_sql_web_sql__["a" /* WebSqlProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_device_sync_device_sync__["a" /* DeviceSyncProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_config_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], DeviceConnectedPage);
    return DeviceConnectedPage;
}());

//# sourceMappingURL=device-connected.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pending_request_pending_request__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_setting_profile_setting__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__friend_profile_friend_profile__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_friends_friends__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FriendsPage = /** @class */ (function () {
    function FriendsPage(friendProvider, navCtrl, navParams) {
        this.friendProvider = friendProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.segmentSwitch1 = 'segment_button_active';
        this.segmentSwitch2 = 'segment_button_inactive';
        this.searchType = '1';
        this.friendRequestMe = 0;
        // check friend list >0 search Type =1 : 0 and segmentSwitch2 = active
        var me = this;
        me.friendProvider.getFriendList().then(function (data) {
            console.log('friend list ', data);
            me.friendData = data;
            me.showCardData = data.friendList;
            me.friendRequestMe = data.friendRequestMe.length;
        });
        // this.showCardData = [
        //   {
        //     rankNo: 1,
        //     total: 569,
        //     totalTxt: "569 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/89106_156.jpg",
        //     userName: "Elise",
        //     userNo: "89106",
        //   },
        //   {
        //     rankNo: 2,
        //     total: 570,
        //     totalTxt: "570 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/730394_63.jpg",
        //     userName: "Lisa",
        //     userNo: "730394",
        //   },
        //   {
        //     rankNo: 3,
        //     total: 571,
        //     totalTxt: "571 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/84742_734.jpg",
        //     userName: "Stephanie",
        //     userNo: "84742",
        //   },
        //   {
        //     rankNo: 4,
        //     total: 572,
        //     totalTxt: "572 mins",
        //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/82815_482.jpg",
        //     userName: "Stephanie",
        //     userNo: "82815",
        //   }
        // ]
    }
    FriendsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FriendsPage');
    };
    FriendsPage.prototype.goPendingRequestView = function () {
        console.log('go goPendingRequestView ', this.friendData.friendRequestMe);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pending_request_pending_request__["a" /* PendingRequestPage */], { data: this.friendData.friendRequestMe }, { animate: true, direction: 'forward' });
    };
    FriendsPage.prototype.goFriendProfileView = function (el) {
        console.log('go goFriendProfileView', el.userNo);
        var _profileType = this.searchType == '1' ? 'F' : 'S';
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__friend_profile_friend_profile__["a" /* FriendProfilePage */], { friendNo: el.userNo, profileType: _profileType }, { animate: true, direction: 'forward' });
    };
    FriendsPage.prototype.btnBack = function () {
        console.log('go btnBack');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__profile_setting_profile_setting__["a" /* ProfileSettingPage */], {}, { animate: true, direction: 'back' });
    };
    FriendsPage.prototype.btnSegment = function (index) {
        if (index == 1) {
            this.searchType = '1';
            this.segmentSwitch1 = 'segment_button_active';
            this.segmentSwitch2 = 'segment_button_inactive';
            this.showCardData = this.friendData.friendList;
        }
        else {
            this.searchType = '2';
            this.segmentSwitch1 = 'segment_button_inactive';
            this.segmentSwitch2 = 'segment_button_active';
            this.showCardData = this.friendData.suggestFriendList;
        }
    };
    FriendsPage.prototype.searchFriend = function (e) {
        console.log(e.value);
        var me = this;
        //check segment == yourfriend or suggested
        if (me.searchType == '1') {
            me.friendProvider.searchFriendList(e.value).then(function (data) {
                console.log(data);
                me.showCardData = data;
            });
        }
        else {
            me.friendProvider.searchSuggestFriend(e.value).then(function (data) {
                console.log(data);
                me.showCardData = data;
            });
        }
    };
    FriendsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-friends',template:/*ion-inline-start:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/friends/friends.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-searchbar placeholder="Search by username or email" (ionChange)="searchFriend($event)"></ion-searchbar>\n    <ion-buttons left>\n      <button ion-button icon-only (click)="btnBack()">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<div class="fixedHeader topHeader">\n  <div class="topCurve">\n    <div class="segmentCenter">\n      <div class="segmentBg">\n        <ion-grid no-padding no-margin>\n          <ion-row no-padding no-margin>\n            <ion-col no-padding no-margin (click)="btnSegment(1)">\n              <div [ngClass]="segmentSwitch1">Your Friends</div>\n            </ion-col>\n            <ion-col no-padding no-margin (click)="btnSegment(2)">\n              <div [ngClass]="segmentSwitch2">Suggested</div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n  </div>\n\n</div>\n\n\n\n<ion-content class="mainContent">\n\n  <ion-list *ngIf="segmentSwitch2 === \'segment_button_active\'" class="peddingArea" no-padding no-margin>\n    <ion-item (click)="goPendingRequestView()" no-padding no-margin>\n      <ion-row>\n        <ion-col>\n            <span class="textPendingRequest">Pending requests</span>\n        </ion-col>\n        <ion-col class="center-right">\n            <span class="yourFriendInvite">{{friendRequestMe}}</span> \n            <img src="assets/imgs/icons/icon_right_arrow.png" class="rightArrowIcon">\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n\n  <!-- <ion-row *ngIf="segmentSwitch2 === \'segment_button_active\'">\n    <ion-col>\n      <button ion-button full (click)="goPendingRequestView()">goto Pending Request</button>\n    </ion-col>\n  </ion-row> -->\n\n  <ion-list *ngFor="let record of showCardData;let i= index">\n    <ion-item (click)="goFriendProfileView(record)">\n      <ion-row no-padding no-margin>\n\n        <ion-col no-padding no-margin col-3 class="center-center">\n          <div class="center-center">\n            <div class="avatarWidth">\n              <img src="{{record.userImg}}">\n            </div>\n          </div>\n        </ion-col>\n\n        <ion-col no-padding no-margin style="align-self: center;">\n          <div>\n            <span class="yourFriendInvite">{{record.userName}}</span>\n          </div>\n          <div>\n            <span class="minutesAgo">{{record.totalTxt}}</span>\n          </div>\n\n          <!-- {{record.userName}} {{record.totalTxt}} {{record.user_status}} -->\n        </ion-col>\n\n        <ion-col no-padding no-margin col-5 *ngIf="segmentSwitch2 === \'segment_button_active\'" class="center-right">\n            <div style="text-align: -webkit-right;\n            margin-right: 1em;">\n              <img src="assets/imgs/icons/circle_plus_icon.png" class="plusIcon">\n            </div>\n        </ion-col>\n\n\n      </ion-row>\n\n      <div class="lineSeperate"></div>\n    </ion-item>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/apiwat_n/health/Final-MTLHealthProject/src/pages/friends/friends.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_friends_friends__["a" /* FriendsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], FriendsPage);
    return FriendsPage;
}());

//# sourceMappingURL=friends.js.map

/***/ })

},[407]);
//# sourceMappingURL=main.js.map