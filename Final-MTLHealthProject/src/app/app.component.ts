import { Component, NgZone } from "@angular/core";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { LoginPage } from "./../pages/login/login";
import { TabsPage } from "../pages/tabs/tabs";
// import { HomePage } from '../pages/home/home';
// import { ApplewatchPage } from '../pages/applewatch/applewatch';
import { WebSqlProvider } from "../providers/web-sql/web-sql";
import { ConfigProvider } from "../providers/config/config";
// import { NotificationPage } from '../pages/notification/notification';
// import { FriendsPage } from '../pages/friends/friends';
// import { ProfileSettingPage } from '../pages/profile-setting/profile-setting';
import { FCM } from "@ionic-native/fcm";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { Events } from "ionic-angular";
import {FriendsProvider} from '../providers/friends/friends';

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  //DB
  win: any = window;

  rootPage: any; //= TabsPage;//LoginPage;
  //  rootPage: any ;//for test
  selectedTheme: any = "light-theme";

  constructor(
    private friend:FriendsProvider,
    private fcm: FCM,
    private sqliteNative: SQLite,
    private objSQLite: WebSqlProvider,
    private zone: NgZone,
    private config: ConfigProvider,
    private SQLite: WebSqlProvider,
    private platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public events: Events
  ) {

    // this.config.userNo='88970';//for test
    let me = this;

   

    platform.ready().then(() => {
      console.log("platform ready");
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      me.initializeDB(function() {
        console.log("open db success");
        me.createTableConfig(function() {
          console.log("create table success");          
        });
        // me.zone.run(()=>{
        //   me.rootPage=ApplewatchPage;
        // });
      });

      statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString("#d30876");
      splashScreen.hide();

      if (platform.is("cordova")) {
        this.initApp();
      }

      platform.registerBackButtonAction(() => {});
    });
  }
  public openDB(): Promise<void> {
    let me = this;
    return this.sqliteNative
      .create({
        name: "mtl_healthy.db",
        location: "default"
      })
      .then((db: SQLiteObject) => {
        //storage object to property
        me.config.dbNative = db;
      });
  }

  initApp() {
    let me=this;
    console.log('init app')
    this.fcm.subscribeToTopic("senchabox_broadcast");

    this.fcm.getToken().then(token => {
      console.log('token device ==> '+token);
      //add insert,update token
      me.config.tokenDevice=token;
    });

    this.fcm.onNotification().subscribe(data => {    
      
      if (data.wasTapped) {
        console.log("Received in background");
        //call webservice  for update icon notification
        me.friend.getFriendRequestMe().then((data:any)=>{
          console.log('friend request ',data.length)
          me.config.objFriendReques=data;
          me.config.totalNotification=data.length;
        });
      } else {
        this.events.publish('notification:changes');
        console.log("Received in foreground");
        //call webservice for update icon notification
        me.friend.getFriendRequestMe().then((data:any)=>{
          console.log('friend request ',data.length)
          me.config.objFriendReques=data;
          me.config.totalNotification=data.length;
        });
      }
    });

    this.fcm.onTokenRefresh().subscribe(token => {
      console.log('token ==> '+token);
      //add insert update token
      me.config.tokenDevice=token;
    });
  }

  initializeDB(func) {
    let me = this;
    if (me.platform.is("ios")) {
      me.openDB().then(() => {
        func();
      });
    } else {
      me.config.dbMaster = me.win.openDatabase(
        "mtl_healthy",
        "1.0",
        "",
        5 * 1024 * 1024
      );
      func();
    }
  }

  createTableConfig(func) {
    let me = this;
    console.log("db ", me.config.dbMaster, " db native ", me.config.dbNative);
    let sql = "SELECT * FROM tb_config";
    me.objSQLite.executeQuery(me.config.dbMaster, sql, [], function(result) {
      console.log("db table ", result);
      if (result == null) {
        console.log("create table");
        sql =
          "CREATE table IF NOT EXISTS tb_config " +
          "(user_no VARCHAR(50) PRIMARY KEY ," +
          "brand VARCHAR(50) ," +
          "date_connect DATETIME);";
        console.log("create sql ", sql);
        me.objSQLite.executeNonQuery(me.config.dbMaster, sql, [], function(
          status
        ) {
          console.log(status);
          me.zone.run(() => {
            me.rootPage = LoginPage;
            func();
          });
        });
      } else {
        me.zone.run(() => {
          if (result.rows.length == 0) {
            me.rootPage = LoginPage;
          } else {
            me.config.objProfile = result;
            me.config.userNo = result.rows.item(0).user_no;
            me.rootPage = TabsPage;
          }
          func();
        });
      }
    });
  }
}
