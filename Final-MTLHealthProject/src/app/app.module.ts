import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

// Plugin
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HealthKit } from '@ionic-native/health-kit';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { FCM } from '@ionic-native/fcm';
import { IonicStorageModule } from '@ionic/storage';
// Component
import { MyApp } from './app.component';
import { PopoverSettingComponent } from '../components/popover-setting/popover-setting';
import { PopoverShareComponent } from '../components/popover-share/popover-share';

// Provider
import { SettingsProvider } from '../providers/settings/settings';
import { LoginProvider } from '../providers/login/login';
import { ConfigProvider } from '../providers/config/config';
import { ThisWeekProvider } from '../providers/this-week/this-week';
import { WebSqlProvider } from '../providers/web-sql/web-sql';
import { DeviceSyncProvider } from '../providers/device-sync/device-sync';
import { DeviceConfirmConnectProvider } from '../providers/device-confirm-connect/device-confirm-connect';
import { WeeklyAchievementProvider } from '../providers/weekly-achievement/weekly-achievement';
import { HistoryProvider } from '../providers/history/history';
import { LeaderBoardProvider } from '../providers/leader-board/leader-board';
import { RewardedSmilePointProvider } from '../providers/rewarded-smile-point/rewarded-smile-point';
import { ChallengeCreateProvider } from '../providers/challenge-create/challenge-create';
import { ChallengeDashboardProvider } from '../providers/challenge-dashboard/challenge-dashboard';
import { ChallengeJoinProvider } from '../providers/challenge-join/challenge-join';
import { HealthProvider } from '../providers/health/health';

// Views
import { HomePage } from '../pages/home/home';
import { HeartratePage } from '../pages/heartrate/heartrate';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { ThisWeekPage } from '../pages/this-week/this-week';
import { HistoryPage } from '../pages/history/history';
import { WeeklyAchivementPage } from '../pages/weekly-achivement/weekly-achivement';
import { RewardedSmailePointsPage } from '../pages/rewarded-smaile-points/rewarded-smaile-points';
import { CongratulationPage } from '../pages/congratulation/congratulation';
import { CongratulationAchievmentPage } from '../pages/congratulation-achievment/congratulation-achievment';
import { DeviceSyncPage } from '../pages/device-sync/device-sync';
import { DeviceConnectedPage } from '../pages/device-connected/device-connected';
import { DeviceConfirmUnlinkPage } from './../pages/device-confirm-unlink/device-confirm-unlink';
import { DeviceConfirmConnectPage } from './../pages/device-confirm-connect/device-confirm-connect';
import { LeaderBoardPage } from './../pages/leader-board/leader-board';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { ChallengeAddByUserPage } from '../pages/challenge-add-by-user/challenge-add-by-user';
import { ChallengeCreatePage } from '../pages/challenge-create/challenge-create';
import { ChallengeDashboardPage } from '../pages/challenge-dashboard/challenge-dashboard';
import { ChallengeInvitePage } from '../pages/challenge-invite/challenge-invite';
import { ChallengeJoinPage } from '../pages/challenge-join/challenge-join';
import { ChallengeYourRankPage } from '../pages/challenge-your-rank/challenge-your-rank';
import { QrCodePage } from '../pages/qr-code/qr-code';

import {ApplewatchPage} from '../pages/applewatch/applewatch';
import {RewardConditionPage} from '../pages/reward-condition/reward-condition';
import { AppleProvider } from '../providers/apple/apple';
import { NotificationPage } from '../pages/notification/notification';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { ProfileSettingPage } from '../pages/profile-setting/profile-setting';
import { FriendsPage } from '../pages/friends/friends';
import { FriendProfilePage } from '../pages/friend-profile/friend-profile';
import { PendingRequestPage } from '../pages/pending-request/pending-request';
import { HowToChallengesWorkPage } from '../pages/how-to-challenges-work/how-to-challenges-work';
import { TermAndConditionPage } from '../pages/term-and-condition/term-and-condition';
import {ExcerciseDetailPage} from '../pages/excercise-detail/excercise-detail';
import { ProfileSettingProvider } from '../providers/profile-setting/profile-setting';
import { TabsProvider } from '../providers/tabs/tabs';
import { FriendsProvider } from '../providers/friends/friends';
import { FriendProfileProvider } from '../providers/friend-profile/friend-profile';

import { HealthPage } from './../pages/health/health';
import { ModalPage } from '../pages/health-add/health-add';
import { HealthWelcomePage } from '../pages/health-welcome/health-welcome';
import { HealthInformationPage } from '../pages/health-information/health-information';
import { ConditionPage } from '../pages/condition-Health/condition';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HeartratePage,
    LoginPage,
    TabsPage,
    ThisWeekPage,
    HistoryPage,
    LeaderBoardPage,
    WeeklyAchivementPage,
    RewardedSmailePointsPage,
    CongratulationPage,
    CongratulationAchievmentPage,
    PopoverSettingComponent,
    DeviceSyncPage,
    DeviceConnectedPage,
    DeviceConfirmConnectPage,
    DeviceConfirmUnlinkPage,
    ForgotPasswordPage,
    ChallengeAddByUserPage,
    ChallengeCreatePage,
    ChallengeDashboardPage,
    ChallengeInvitePage,
    ChallengeJoinPage,
    ChallengeYourRankPage,
    PopoverShareComponent,
    QrCodePage,
    ApplewatchPage,
    RewardConditionPage,
    NotificationPage,
    UserProfilePage,
    ProfileSettingPage,
    FriendsPage,
    FriendProfilePage,
    PendingRequestPage,
    HowToChallengesWorkPage,
    TermAndConditionPage,
    ExcerciseDetailPage,
    HealthPage,
    ModalPage,
    HealthWelcomePage,
    HealthInformationPage,
    ConditionPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md',
      tabsPlacement: 'top'
    }),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HeartratePage,
    LoginPage,
    TabsPage,
    ThisWeekPage,
    HistoryPage,
    LeaderBoardPage,
    WeeklyAchivementPage,
    RewardedSmailePointsPage,
    CongratulationPage,
    CongratulationAchievmentPage,
    PopoverSettingComponent,
    DeviceSyncPage,
    DeviceConnectedPage,
    DeviceConfirmConnectPage,
    DeviceConfirmUnlinkPage,
    ForgotPasswordPage,
    ChallengeAddByUserPage,
    ChallengeCreatePage,
    ChallengeDashboardPage,
    ChallengeInvitePage,
    ChallengeJoinPage,
    ChallengeYourRankPage,
    PopoverShareComponent,
    QrCodePage,
    ApplewatchPage,
    RewardConditionPage,
    NotificationPage,
    UserProfilePage,
    ProfileSettingPage,
    FriendsPage,
    FriendProfilePage,
    PendingRequestPage,
    HowToChallengesWorkPage,
    TermAndConditionPage,
    ExcerciseDetailPage,
    HealthPage,
    ModalPage,
    HealthWelcomePage,
    HealthInformationPage,
    ConditionPage
  ],
  providers: [
    SQLite,
    BarcodeScanner,
    Camera,
    File,
    FileTransfer,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    HealthKit,
    FCM,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SettingsProvider,
    LoginProvider,
    ConfigProvider,
    ThisWeekProvider,
    WebSqlProvider,
    DeviceSyncProvider,
    DeviceConfirmConnectProvider,
    WeeklyAchievementProvider,
    HistoryProvider,
    LeaderBoardProvider,
    RewardedSmilePointProvider,
    ChallengeCreateProvider,
    ChallengeDashboardProvider,
    ChallengeJoinProvider,
    AppleProvider,
    AppleProvider,
    AppleProvider,
    ProfileSettingProvider,
    TabsProvider,
    FriendsProvider,
    FriendProfileProvider,
    HealthProvider
  ]
})
export class AppModule { }
