import { Injectable } from '@angular/core';

import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfigProvider } from '../config/config';

@Injectable()
export class LoginProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello LoginProvider Provider');
  }

  login(userNo:string,password:string){
    var params=new URLSearchParams();
    params.set('userNo',userNo);
    params.set('password',password);
    params.set('tokenDevice',this.config.tokenDevice)

    return new Promise(resolve=>{
      this.http.post(this.config.serverAPI+"loginApp",params)
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    })
  }

}
