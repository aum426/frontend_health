import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfigProvider } from '../config/config';

@Injectable()
export class DeviceConfirmConnectProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello DeviceConfirmConnectProvider Provider');
  }

  getLink(brand:string){

    var params=new URLSearchParams();
    params.set('brand',brand);
    return new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getLink',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        console.log('link ',data)
        resolve(data);
      })
    })

  }
}
