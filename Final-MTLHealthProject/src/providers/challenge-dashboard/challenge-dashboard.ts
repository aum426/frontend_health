import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';
@Injectable()
export class ChallengeDashboardProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello ChallengeDashboardProvider Provider');
  }

  getChallengeDashboard(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getChallengeDashboard',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  getChallengeList(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getChallengeList',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  getCompetitionList(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getCompetitionList',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
  

  joinChallenge(challengeNo){
    var params=new URLSearchParams();
    params.set('challengeNo',challengeNo);
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'joinChallenge',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  addChallengeMember(challengeNo){
    var params=new URLSearchParams();
    params.set('challengeNo',challengeNo);
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'addChallengeMember',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
}
