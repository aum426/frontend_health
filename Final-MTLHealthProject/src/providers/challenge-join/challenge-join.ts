import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';
@Injectable()
export class ChallengeJoinProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello ChallengeJoinProvider Provider');
  }

  getCompetitionDetail(challengeNo){
    var params=new URLSearchParams();
    params.set('challengeNo',challengeNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getCompetitionDetail',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  getChallengeDetail(challengeNo){
    var params=new URLSearchParams();
    params.set('challengeNo',challengeNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getChallengeDetail',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
  
  joinChallenge(challengeNo){
    var params=new URLSearchParams();
    params.set('challengeNo',challengeNo);
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'joinChallenge',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  rejectChallenge(challengeNo){
    var params=new URLSearchParams();
    params.set('challengeNo',challengeNo);
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'rejectChallenge',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
 
}
