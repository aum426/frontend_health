import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';

@Injectable()
export class TabsProvider {

  constructor(public http: Http, private config: ConfigProvider) {
    console.log('Hello TabsProvider Provider');
  }

  getProfile(){
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);
    console.log('param ', params,' ',this.config.userNo)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getProfile', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

}
