import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import { ConfigProvider } from '../config/config';

@Injectable()
export class ProfileSettingProvider {

  constructor(public http: Http,private config:ConfigProvider) {
    console.log('Hello ProfileSettingProvider Provider');
  }

  getProfileSetting(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getProfileSetting',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

}
