import { Injectable } from '@angular/core';

import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfigProvider } from '../config/config';

@Injectable()
export class DeviceSyncProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello DeviceSyncProvider Provider');
  }

  getDeviceConfig(){
    return new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'Config/getDeviceConfig',{})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log(err);
        resolve(null);
      })
    })
  }

  getLink(brand:string){
    var params=new URLSearchParams();
    params.set('brand',brand);
    return new Promise(resolve=>{
      // https://apixdev.muangthai.co.th/MTLHealthyAPI/FitbitOAuth/Authorize
      this.http.get(this.config.serverAPI+'getLink',{search:params})
      // this.http.get(this.config.serverAPI+'FitbitOAuth/Authorize',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        console.log('link ',data)
        resolve(data);
      })
    })
  }

  registerUser(brand:string,token){
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);
    params.set('brand', brand);
    params.set('token',token);
    console.log('param ', params)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'registerUser', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })

    });
  }
  //########## APPLE ##########
  getSyncDate(brand,mode){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('brand',brand);
    params.set('mode',mode)

    console.log('param ', params)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getSyncDate', {search:params})
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }
  syncAppleRawData(lsStep,lstWorkoOut){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo)
    params.set('lsStep',JSON.stringify(lsStep));
    params.set('lsWorkOut',JSON.stringify(lstWorkoOut));

    console.log('param ', params)
    return new Promise(resolve => {
      this.http.post(this.config.serverAPI + 'syncAppleRawData', params)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }
}
