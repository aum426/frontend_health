import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { HealthKit, HealthKitOptions } from '@ionic-native/health-kit';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfigProvider } from '../config/config';

@Injectable()
export class AppleProvider {

  constructor(private http: Http,
    private platform: Platform,
     private healthKit: HealthKit, 
     private config:ConfigProvider) {
    console.log('Hello AppleProvider Provider');
  }
  getSyncDate(brand,mode){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('brand',brand);
    params.set('mode',mode)

    console.log('param ', params)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getSyncDate', {search:params})
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }
  syncAppleRawData(lsStep,lstWorkoOut){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('lsStep', JSON.stringify(lsStep));
    params.set('lsWorkOut',JSON.stringify(lstWorkoOut));

    console.log('param ', params)
    return new Promise(resolve => {
      this.http.post(this.config.serverAPI + 'syncAppleRawData', params)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  checkPermission(func): any {
    let me = this;
    if (me.platform.is('cordova')) {
      me.platform.ready().then(() => {
        me.healthKit.available().then(available => {
          if (available) {
            var options: HealthKitOptions = {
              readTypes: [
                'HKQuantityTypeIdentifierStepCount',
                'HKWorkoutTypeIdentifier',
                'HKQuantityTypeIdentifierActiveEnergyBurned',
                'HKQuantityTypeIdentifierDistanceCycling',
                'HKQuantityTypeIdentifierHeartRate',
                'HKQuantityTypeIdentifierDistanceWalkingRunning']
            }
            me.healthKit.requestAuthorization(options).then(_ => {
              console.log('allow permisstion')
              func("allow");
            })
          } else {
            console.log('ios can not access health function')
            func("denied");
          }
        });
      });
    }
  }


  getStep(objDate: any, func: any) {
    console.log('step')
    var stepOptions = {
      'startDate': objDate.startDate,
      'endDate': objDate.endDate,
      'aggregation': 'day',
      'sampleType': 'HKQuantityTypeIdentifierStepCount',
      'unit': 'count'
    }

    this.healthKit.querySampleTypeAggregated(stepOptions).then(data => {
      func(data);
    }, err => {
      console.log('No steps: ', err);
      func(err);
    });
  }

  getActivity(func) {
    let me = this;

    me.healthKit.available().then(available => {
      if (available) {
        var options: HealthKitOptions = {
          readTypes: [
            'HKQuantityTypeIdentifierStepCount',
            'HKWorkoutTypeIdentifier',
            'HKQuantityTypeIdentifierActiveEnergyBurned',
            'HKQuantityTypeIdentifierDistanceCycling',
            'HKQuantityTypeIdentifierHeartRate',
            'HKQuantityTypeIdentifierDistanceWalkingRunning']
        }
        me.healthKit.requestAuthorization(options).then(_ => {
          console.log('allow permisstion')
          this.healthKit.findWorkouts().then(x => {
            console.log('workout ==> ', x, ' !!!!');
            func(x);
          }, err => {
            console.log(err);
            func(err);
          });
        })
      } else {
        console.log('ios can not access health function')
      }
    });
  }
  getHeartRate(start: Date, end: Date, func: any) {
    var options = {
      startDate: start,
      endDate: end,
      sampleType: 'HKQuantityTypeIdentifierHeartRate',
      unit: 'count/min',
      limit: 3000
    }

    this.healthKit.querySampleType(options).then(data => {
      console.log(data);
      console.log(data.length);
      func(data);

    }, err => {
      console.log('No Heartrate Data: ', err);
      func(err);
    });
  }

  mapActivityHR(objActivity: Array<any>, lsWorkout: Array<object>, func: any) {
    let me = this;
    if (objActivity.length > 0) {
      var obj: any = objActivity.shift();
      me.getHeartRate(new Date(obj.startDate), new Date(obj.endDate), function (objHR) {
        console.log('obj HR ', objHR);
        obj["lsHR"] = objHR;
        lsWorkout.push(obj);
        me.mapActivityHR(objActivity, lsWorkout, func);
      })
    } else {
      func(lsWorkout);
    }
  }
}
