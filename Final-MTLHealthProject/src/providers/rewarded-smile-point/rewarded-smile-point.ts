import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';
@Injectable()
export class RewardedSmilePointProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello RewardedSmilePointProvider Provider');
  }

  getRewardSmilePoint(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getRewardSmilePoint',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  getSumSmilePoint(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getSumSmilePoint',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
}
