import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';

@Injectable()
export class WeeklyAchievementProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello WeeklyAchievementProvider Provider');
  }

  getWeeklyAchievement(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);

    return new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getWeeklyAchievement',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        console.log('in ',data);
        resolve(data);
      })
    })
  }
}
