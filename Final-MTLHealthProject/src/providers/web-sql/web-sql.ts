import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { ConfigProvider } from '../config/config';
// import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()

export class WebSqlProvider {

  dbConnection: any;

  constructor(
    // private sqliteNative: SQLite,
    private platform: Platform,
    private config: ConfigProvider) {
    console.log('WebSql Running....');
  }

  setDBConnection(data) {
    this.dbConnection = data;
  }

  getDBConnection() {
    return this.dbConnection;
  }


  executeQuery(dbConn, sql, params, func) {
    let me = this;
    params != null ? params : [];
    console.log('xxx ', me.platform.is('ios'))
    if (me.platform.is('ios')) {
      console.log('@@@@@ ', me.config.dbNative, sql, params)
      me.config.dbNative.executeSql(sql, params)
        .then((result) => {
          console.log('$$$$$ ', result)
          func(result);
        })
        .catch(e => {
          'ERROR ' + JSON.stringify(e)
          func(null);
        });

    } else {
      dbConn.transaction(function (tx) {
        tx.executeSql(sql, params,
          function (tx, results) {
            func(results);
          },
          function (tx, err) {
            func(null)
          }
        );
      });
    }
  }

  executeNonQuery(dbConn, sql, params, func) {
    console.log('!!!!!!!!')
    let me = this;
    params != null ? params : [];
    if (this.platform.is('ios')) {
      me.config.dbNative.executeSql(sql, params)
        .then((result) => {
          console.log('##### ', result)
          func(result)
        })
        .catch(e => {
          "Error: " + JSON.stringify(e);
          func(null);
        });
    } else {
      dbConn.transaction(function (tx) {
        tx.executeSql(sql, params,
          function (tx, results) {
            func(true);
          }, function (tx, err) {
            func(false);
          }
        );
      });
    }
  }

}
