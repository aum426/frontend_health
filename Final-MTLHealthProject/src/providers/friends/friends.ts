import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config'; 
@Injectable()
export class FriendsProvider {

  constructor(public http: Http,private config:ConfigProvider) {
    console.log('Hello FriendsProvider Provider');
  }

  searchFriendList(str){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('strSearch',str);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'searchFriendList',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  searchSuggestFriend(str){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('strSearch',str);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'searchSuggestFriend',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  getFriendList(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getFriendData',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
  
  getMutualFriendList(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getMutualFriendList',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  //#############//
  addFriend(friendNo){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('friendNo',friendNo);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'addFriend',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  rejectFriendRequest(friendNo){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('friendNo',friendNo);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'rejectFriendRequest',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  getFriendRequestMe(){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getFriendRequestMe',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  updateFriendStatus(friendNo:string,friendStatus:string){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('friendNo',friendNo);
    params.set('friendStatus',friendStatus);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'updateFriendStatus',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  requestFriend(friendNo:string){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('friendNo',friendNo);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'requestFriend',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
}
