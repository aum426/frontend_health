import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';


@Injectable()
export class ThisWeekProvider {

  constructor(private http: Http, private config: ConfigProvider) {
    console.log('Hello ThisWeekProvider Provider');
  }

  getDataInitial(){
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getDataInitial', { search: null })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        })
    })
  }
  getDateRemain() {
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getDateRemain', { search: null })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        })
    })
  }

  checkTokenExpiry(brand){
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);
    params.set('brand', brand);
    console.log('param ', params)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'checkToken', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }
  getSyncData() {
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);
    console.log('param ', params)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getSyncData', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  syncData(brand: string) {
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);
    params.set('brand', brand);
    params.set('version',this.config.appVersion);
    console.log('param ', params)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'SyncData', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  getLastSync(){
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);
    console.log('param ', params)
    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getLastSync', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  updateToken(){
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);
    params.set('tokenDevice',this.config.tokenDevice);
    console.log('param ', params)
    return new Promise(resolve => {
      this.http.post(this.config.serverAPI + 'updateToken', params)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }
}
