import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';


@Injectable()
export class LeaderBoardProvider {

  constructor(private http: Http, private config: ConfigProvider) {
    console.log('Hello LeaderBoardProvider Provider');
  }

  getProfile(){
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);

    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getProfile', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  getLeaderboard(){
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);

    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getLeaderType', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  getCurrentRankOfWeek() {
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);

    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getCurrentRankOfWeek', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  getRankOfWeek(currentIndex) {
    var params = new URLSearchParams();
    params.set('startIndex', currentIndex);

    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getRankOfWeek', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  getRankOfAll(currentIndex) {
    var params = new URLSearchParams();
    params.set('startIndex', currentIndex);

    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getRankOfAll', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }

  getCurrentRankOfAll() {
    var params = new URLSearchParams();
    params.set('userNo', this.config.userNo);

    return new Promise(resolve => {
      this.http.get(this.config.serverAPI + 'getCurrentRankOfAll', { search: params })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log('error ', err);
          resolve(null);
        })
    });
  }


}
