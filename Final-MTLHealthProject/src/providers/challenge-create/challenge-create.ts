import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';
@Injectable()
export class ChallengeCreateProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello ChallengeCreateProvider Provider');
  }

  create(name,step,HR,distance,reward,startDate,endDate){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('challengeType',"88");
    params.set('challengeName',name);    
    params.set('conditionStep',step);
    params.set('conditionHeartRate',HR);
    params.set('conditionDistance',distance);
    params.set('rewardTxt',reward);
    params.set('startDate',startDate);
    params.set('endDate',endDate);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'createChallenge',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }


}
