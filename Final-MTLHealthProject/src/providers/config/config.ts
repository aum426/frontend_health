
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class ConfigProvider {

  dbNative:SQLiteObject;
  dbMaster:any;
  userNo:string;
  serverAPI: string;
  serverUploadAPI:string;
  appVersion:string;
  deviceConnected:boolean=false;
  deviceData:any;
  currentLevel:string="0";
  allLevel:string="5";
  objThisWeek:any;
  objProfile:any;//data for device db
  tokenDevice:string="";
  totalNotification:number=0;
  objFriendReques:any;

  refreshHistory:boolean=false;
  refreshLeaderboard:boolean=false;
  refreshChallengeDashboard:boolean=false;

  serverGolang:string;

  constructor() {
    console.log('Hello ConfigProvider Provider');
    // this.serverAPI="http://localhost:58124/";
    // this.serverAPI='http://10.115.103.128/MTLHealthyAPI/';
    this.serverAPI='https://apixdev.muangthai.co.th/MTLHealthyAPI2/'; //https only
    this.serverUploadAPI='http://apixdev.muangthai.co.th/MTLHealthyAPI/UploadFile';
    // this.serverUploadAPI='http://10.31.53.133/MTLHealthyAPI/UploadFile';
    this.appVersion='0.35';
    this.serverGolang = 'https://gateway-uat.muangthai.co.th/api/mtlhealth/';
  }

}
