import { Injectable } from '@angular/core';
import {
  Http, Headers, RequestOptions, RequestOptionsArgs 
} from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfigProvider } from '../config/config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
//import { ConfigProvider } from '../config/config';


@Injectable()
export class HealthProvider {
  URLAPI: string;

  constructor(
    private http: Http ,
    private config:ConfigProvider
    ) {
    console.log('Hello HealthProvider');
  }

  getHealthDateScore(userId:string){

    let postParams = {'user_id':userId};

    let headers = new Headers(
      {
        'Content-Type' : 'application/json',
          apikey : 'l77b2059164ab14c539a214d56647b7822'
      });

    this.URLAPI = this.config.serverGolang+'healthScore';
    //this.URLAPI = "http://localhost:33100/api/healthscore";
    return new Promise(resolve=>{
      this.http.post(this.URLAPI,JSON.stringify(postParams),
      {
        headers: headers
      })
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('getHealthDate error ',err);
        resolve(null);
      })
    })
  }

  getHealthDate(userId: String){
    
    let headers = new Headers(
      {
        'Content-Type' : 'application/json',
          apikey : 'l77b2059164ab14c539a214d56647b7822'
      });

    let postParams = {'user_id':userId};
    this.URLAPI = this.config.serverGolang+'gethealthData';
    //this.URLAPI = "http://10.31.11.126:33100/api/getHealthData";
    //this.URLAPI = "http://localhost:33100/api/getHealthData";
    return new Promise(resolve=>{
      this.http.post(this.URLAPI,JSON.stringify(postParams)
      , 
      {
        headers: headers
      })
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('getHealthDate error ',err);
        resolve(null);
      })
    })
  }
  getHealthIndex(userId:string){
    let headers = new Headers(
      {
        'Content-Type' : 'application/json',
          apikey : 'l77b2059164ab14c539a214d56647b7822'
      });

    let postParams = {'user_id':userId};
    this.URLAPI = this.config.serverGolang+'getHealthIndex';
    //this.URLAPI = "http://10.31.11.126:33100/api/getHealthIndex";
    //this.URLAPI = "http://localhost:33100/api/getHealthIndex";
    return new Promise(resolve=>{
      this.http.post(this.URLAPI,JSON.stringify(postParams),{ headers: headers })
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('getHealthDate error ',err);
        resolve(null);
      })
    })
  }
  insertHealthData(userId,height,weight,bloodPressure1,bloodPressure2,cholesterol,HDL,bloodSugar){
    let data = {'user_id':userId,
                    'health_data_list':[
                    //   {
                    //     "type_name": "Steps",
                    //     "value1": "500"
                    // },
                    // {
                    //     "type_name": "Exercises",
                    //     "value1": "30"
                    // },
                    {
                        "type_name": "Blood Pressure",
                        "value1": bloodPressure1,
                        "value2" : bloodPressure2
                    },
                    {
                        "type_name": "BMI",
                        "value1": "20"
            
                    },
                    {
                        "type_name": "Cholesterol",
                        "value1": cholesterol
                    },
                    {
                        "type_name": "Blood Sugar",
                        "value1": bloodSugar
                    },
                    {
                        "type_name": "Height",
                        "value1": height
                    },
                    {
                        "type_name": "Weight",
                        "value1": weight
                    },
                    {
                        "type_name": "HDL",
                        "value1": HDL
                    }
                    ]};
    //obj.push('type_name':)
    //let postParams = {'user_id':userId};
    let headers = new Headers(
      {
        'Content-Type' : 'application/json',
          apikey : 'l77b2059164ab14c539a214d56647b7822'
      });

    this.URLAPI = this.config.serverGolang+'insert/healthData';
    //this.URLAPI = "http://10.31.11.126:33100/api/insert/healthData";
    //this.URLAPI = "http://localhost:33100/api/insert/healthData";
    return new Promise(resolve=>{
      this.http.post(this.URLAPI,JSON.stringify(data),{ headers: headers })
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('getHealthDate error ',err);
        resolve(null);
      })
    })
  }

  insertHealthUserProfile(userId,mydate){
    let data = {
      "user_id": userId,
      "date_of_birth" : mydate
     }
     let headers = new Headers(
      {
        'Content-Type' : 'application/json',
          apikey : 'l77b2059164ab14c539a214d56647b7822'
      });
    //obj.push('type_name':)
    //let postParams = {'user_id':userId};
    this.URLAPI = this.config.serverGolang+'insert/healthUserprofile';
    //this.URLAPI = "http://10.31.11.126:33100/api/insert/healthUserProfile";
    //this.URLAPI = "http://localhost:33100/api/insert/healthUserProfile";
    return new Promise(resolve=>{
      this.http.post(this.URLAPI,JSON.stringify(data),{ headers: headers })
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('getHealthDate error ',err);
        resolve(null);
      })
    })
  }


}
