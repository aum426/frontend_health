import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config';
@Injectable()
export class HistoryProvider {

  constructor(private http: Http,private config:ConfigProvider) {
    console.log('Hello HistoryProvider Provider');
  }

  getScale(){
    return new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getScale',{search:null})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    })
  }

  getDetailPerWeek(startDate,endDate){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo)
  }

  getHistory(){//(period,activityType){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    // params.set('period',period);
    // params.set('activityType',activityType);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getHistory',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }

  getHistoryDetail(period,activityType,runNo){
    var params=new URLSearchParams();
    params.set('userNo',this.config.userNo);
    params.set('period',period);
    params.set('activityType',activityType);
    params.set('runNo',runNo);
    
    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getHistory',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }


  // getHistory(currentIndex){
  //   var params=new URLSearchParams();
  //   params.set('userNo',this.config.userNo);
  //   params.set('currentIndex',currentIndex);
  //   return  new Promise(resolve=>{
  //     this.http.get(this.config.serverAPI+'getHistory',{search:params})
  //     .map(res=>res.json())
  //     .subscribe(data=>{
  //       resolve(data);
  //     },err=>{
  //       console.log('error ',err);
  //       resolve(null);
  //     })

  //   });

  // }

}
