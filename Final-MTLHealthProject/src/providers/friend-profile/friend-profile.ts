import { Injectable } from '@angular/core';
import {
  Http,
  URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigProvider } from '../config/config'; 
@Injectable()
export class FriendProfileProvider {

  constructor(public http: Http,private config:ConfigProvider) {
    console.log('Hello FriendProfileProvider Provider');
  }

  getFriendProfile(friendNo){
    var params=new URLSearchParams();
    params.set('userNo',friendNo);

    return  new Promise(resolve=>{
      this.http.get(this.config.serverAPI+'getProfileSetting',{search:params})
      .map(res=>res.json())
      .subscribe(data=>{
        resolve(data);
      },err=>{
        console.log('error ',err);
        resolve(null);
      })
    });
  }
}
