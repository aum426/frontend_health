import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, NavParams, Content, LoadingController, ActionSheetController } from 'ionic-angular';

import { HistoryProvider } from '../../providers/history/history';
import { ConfigProvider } from '../../providers/config/config';

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  @ViewChild('pageTop') pageTop: Content;
  lines: any;
  periodInfo: string;
  periodTime: string = 'w';
  activityType: string = 's';
  currentIndex: number = 1;
  objTmp: any;
  objHistory: any;
  objDetail: any;

  titlePage: string = 'Steps history';
  unit: string = 'STEPS';
  iconStepScale: string = 'image-scale-up';
  iconWorkoutScale: string = 'image-scale-normal';

  showIndexOf: number = -1;
  showDetailStatus: boolean = false;
  dateWeekTextDetail: any = 'dateWeekTextDetail colorStep';
  dataTextDetail: any = 'dataTextDetail colorStep';
  unitTextDetail: any = 'unitTextDetail colorStep';

  segmentSwitch1: any = 'segment_button_active_history';
  segmentSwitch2: any = 'segment_button_inactive_history';
  segmentSwitch3: any = 'segment_button_inactive_history';

  actionSheetSelected: string = 'Exercise (Overall)';
  actionSheetStatus: boolean;

  chartTxt1: string = 'MON';
  chartTxt2: string = 'TUE';
  chartTxt3: string = 'WED';
  chartTxt4: string = 'THU';
  chartTxt5: string = 'FRI';
  chartTxt6: string = 'SAT';
  chartTxt7: string = 'SUN';


  indicator1: string = '0';
  indicator2: string = '2,500';//'15';
  indicator3: string = '5,000';
  indicator4: string = '7,500';
  indicator5: string = '10,000';

  constructor(
    private config: ConfigProvider,
    // private zone: NgZone,
    private historyProvider: HistoryProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public actionSheetController: ActionSheetController
  ) {

    this.actionSheetStatus = false;
    this.actionSheetSelected = "Exercise (Overall)";

    this.lines = [
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' }
    ];

  }

  updateActionSheetSelected(value) {
    if (value === 1) {
      this.actionSheetSelected = 'Exercise (Overall)';//'Exercise (Overall)';
      this.activityType = 'a';
    } else if (value === 2) {
      this.actionSheetSelected = 'Running';//'Running';
      this.activityType = 'r';
    } else if (value === 3) {
      this.actionSheetSelected = 'Cycling';//'Cycling';
      this.activityType = 'c';
    } else if (value === 4) {
      this.actionSheetSelected = 'Swimming';//'Swimming';   
      this.activityType = 'sw';
    } else if (value === 5) {
      this.actionSheetSelected = 'Others';//'Other';
      this.activityType = 'o';
    }
    console.log('activity type ', this.actionSheetSelected)
    this.bindDataByCondition();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      title: 'Select catergories',
      buttons: [
        {
          text: 'Exercise (Overall)',
          icon: 'exercise',
          handler: () => {
            this.updateActionSheetSelected(1);
          }
        },
        {
          text: 'Running',
          icon: 'running',
          handler: () => {
            this.updateActionSheetSelected(2);
          }
        },
        {
          text: 'Cycling',
          icon: 'bike',
          handler: () => {
            this.updateActionSheetSelected(3);
          }
        },
        {
          text: 'Swimming',
          icon: 'swimming',
          handler: () => {
            this.updateActionSheetSelected(4);
          }
        },
        {
          text: 'Others',
          icon: 'other',
          handler: () => {
            this.updateActionSheetSelected(5);
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  changeData(rec: any, i: number) {
    console.log('===>', rec, ' ', i)
    this.clearGraph();
    this.periodInfo = rec.showPeriod;
    // this.showIndexOf = i;//กำหนด rec ที่จะแสดง detail
    let me = this;
    if (me.periodTime == 'w') {
      me.historyProvider.getHistoryDetail('d', me.activityType, rec.aggPeriod).then((data: any) => {
        console.log('#### ', data);
        if (me.showIndexOf == i && me.showDetailStatus == true) {
          me.showIndexOf == -1
          me.showDetailStatus = false;
        } else {
          me.showIndexOf = i;
          me.showDetailStatus = true;
          me.objDetail = data;
        }
        me.paintIndicatorGraph();
        me.paintPeriodGraph(data);
        me.paintGraph(data);
      })
    } else {
      //render by shift data from first active
      this.chartTxt1 = '';
      this.chartTxt2 = '';
      this.chartTxt3 = '';
      this.chartTxt4 = '';
      this.chartTxt5 = '';
      this.chartTxt6 = '';
      this.chartTxt7 = '';
      if (this.periodTime == 'w') {
        this.chartTxt1 = 'MON';
        this.chartTxt2 = 'TUE';
        this.chartTxt3 = 'WED';
        this.chartTxt4 = 'THU';
        this.chartTxt5 = 'FRI';
        this.chartTxt6 = 'SAT';
        this.chartTxt7 = 'SUN';
      } else if (this.periodTime == 'm' || this.periodTime == 'y') {
        //shift data
        console.log('month ', this.objHistory)
        let j = i;
        let n = 1;
        let txt = '';
        let obj: Array<object> = [];
        for (j; j < this.objHistory.length; j++) {
          txt = this.objHistory[j].showPeriod;
          console.log('%%% ', txt)
          switch (n) {
            case 1: { this.chartTxt1 = txt;  obj.push(this.objHistory[j]); break; }
            case 2: { this.chartTxt2 = txt;  obj.push(this.objHistory[j]); break; }
            case 3: { this.chartTxt3 = txt; obj.push(this.objHistory[j]); break; }
            case 4: { this.chartTxt4 = txt;  obj.push(this.objHistory[j]); break; }
            case 5: { this.chartTxt5 = txt;  obj.push(this.objHistory[j]); break; }
            case 6: { this.chartTxt6 = txt;  obj.push(this.objHistory[j]); break; }
            case 7: { this.chartTxt7 = txt;  obj.push(this.objHistory[j]); break; }
          }
          n++;
        }
        me.paintGraph(obj);
      }

    }
  }

  ionViewWillEnter() {
    console.log('will');
    this.pageTop.scrollTo(0, 0, 0);
  }

  ionViewDidEnter() {
    console.log('enter');
    this.pageTop.scrollTo(0, 0, 0);

    if (this.config.refreshHistory == true) {
      this.config.refreshHistory = false;
      //   console.log('call api')
      this.refreshHistory();
    }
  }

  ionViewDidLeave() {
    console.log('leave');
    this.pageTop.scrollTo(0, 0, 100);
  }

  refreshHistory() {
    console.log('refreshHistory')
    let me = this;
    const loader = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loader.present();
    this.historyProvider.getHistory().then((data: any) => {
      console.log('history ', data);
      me.objTmp = data;
      me.periodInfo = data.lsStepWeek[0].showPeriod;
      me.objHistory = data.lsStepWeek;
      let runNo = data.lsStepWeek[0].aggPeriod;
      me.historyProvider.getHistoryDetail('d', me.activityType, runNo).then((objInfo: any) => {
        // console.log('@@@@ ', objInfo)
        loader.dismiss();
        me.paintGraph(objInfo);
      })
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
    console.log('current period ', this.periodTime, ' ==> ', this.objHistory)
    this.refreshHistory();
  }


  getStep() {
    this.showIndexOf == -1
    // this.showDetailStatus = false;
    this.iconStepScale = 'image-scale-up';
    this.iconWorkoutScale = 'image-scale-normal';
    this.titlePage = 'Steps history';
    this.unit = 'STEPS';
    this.activityType = 's';
    this.bindDataByCondition();
    // this.renderGraph(this.objHistory[0]);

    this.actionSheetStatus = false;
  }

  getWorkout() {
    this.showIndexOf == -1
    // this.showDetailStatus = false;
    this.iconStepScale = 'image-scale-normal';
    this.iconWorkoutScale = 'image-scale-up';
    this.titlePage = 'Exercise history';
    this.unit = 'Minutes';
    this.activityType = 'a';
    this.updateActionSheetSelected(1);
    this.bindDataByCondition();
    this.actionSheetStatus = true;
  }

  btnSegment(index) {
    if (index == 1) {
      this.periodTime = 'w';
      this.segmentSwitch1 = 'segment_button_active_history';
      this.segmentSwitch2 = 'segment_button_inactive_history';
      this.segmentSwitch3 = 'segment_button_inactive_history';
    } else if (index == 2) {
      this.periodTime = 'm';
      this.segmentSwitch1 = 'segment_button_inactive_history';
      this.segmentSwitch2 = 'segment_button_active_history';
      this.segmentSwitch3 = 'segment_button_inactive_history';
    } else {
      this.periodTime = 'y';
      this.segmentSwitch1 = 'segment_button_inactive_history';
      this.segmentSwitch2 = 'segment_button_inactive_history';
      this.segmentSwitch3 = 'segment_button_active_history';
    }
    console.log('period ', this.periodTime)
    //call webservie rendered data
    this.bindDataByCondition();

  }



  bindDataByCondition() {
    this.showDetailStatus = false;
    this.clearGraph();
    console.log('period ', this.periodTime, ' type ', this.activityType)
    let me = this;
    switch (me.periodTime) {
      case 'w': {
        switch (me.activityType) {
          case 'a': { me.unit = 'HR'; me.objHistory = me.objTmp.lsOverAllWeek; break; }
          case 'r': { me.unit = 'HR'; me.objHistory = me.objTmp.lsRunningWeek; break; }
          case 'c': { me.unit = 'HR'; me.objHistory = me.objTmp.lsCyclingWeek; break; }
          case 'sw': { me.unit = 'KM'; me.objHistory = me.objTmp.lsSwimmingWeek; break; }
          case 'o': { me.unit = 'HR'; me.objHistory = me.objTmp.lsOtherWeek; break; }
          case 's': { me.unit = 'STEPS'; me.objHistory = me.objTmp.lsStepWeek; break; }
        }
        break;
      }
      case 'm': {
        switch (me.activityType) {
          case 'a': { me.unit = 'HR'; me.objHistory = me.objTmp.lsOverAllMonth; break; }
          case 'r': { me.unit = 'HR'; me.objHistory = me.objTmp.lsRunningMonth; break; }
          case 'c': { me.unit = 'HR'; me.objHistory = me.objTmp.lsCyclingMonth; break; }
          case 'sw': { me.unit = 'KM'; me.objHistory = me.objTmp.lsSwimmingMonth; break; }
          case 'o': { me.unit = 'HR'; me.objHistory = me.objTmp.lsOtherMonth; break; }
          case 's': { me.unit = 'STEPS'; me.objHistory = me.objTmp.lsStepMonth; break; }
        }
        break;
      }
      case 'y': {
        switch (me.activityType) {
          case 'a': { me.unit = 'HR'; me.objHistory = me.objTmp.lsOverAllYear; break; }
          case 'r': { me.unit = 'HR'; me.objHistory = me.objTmp.lsRunningYear; break; }
          case 'c': { me.unit = 'HR'; me.objHistory = me.objTmp.lsCyclingYear; break; }
          case 'sw': { me.unit = 'KM'; me.objHistory = me.objTmp.lsSwimmingYear; break; }
          case 'o': { me.unit = 'HR'; me.objHistory = me.objTmp.lsOtherYear; break; }
          case 's': { me.unit = 'STEPS'; me.objHistory = me.objTmp.lsStepYear; break; }
        }
        break;
      }
    }
    me.paintPeriodGraph(me.objHistory);
    me.paintIndicatorGraph();

    console.log('xxx ', this.objHistory)
    if (me.objHistory.length > 0) {
      me.periodInfo = me.objHistory[0].showPeriod;
      if (me.periodTime == 'w') {
        let runNo = me.objHistory[0].aggPeriod;
        me.historyProvider.getHistoryDetail('d', me.activityType, runNo).then((objInfo: any) => {
          console.log('@@@@ ', objInfo)
          me.paintGraph(objInfo);
        })
      } else {
        me.paintGraph(me.objHistory);
      }
    } else {
      me.periodInfo = '';
    }
  }

  paintPeriodGraph(data: any) {
    if (this.periodTime != 'w') {
      this.chartTxt1 = '';
      this.chartTxt2 = '';
      this.chartTxt3 = '';
      this.chartTxt4 = '';
      this.chartTxt5 = '';
      this.chartTxt6 = '';
      this.chartTxt7 = '';

      for (let i = 0; i < data.length; i++) {
        if (i < 7) {
          switch (i) {
            case 0: { this.chartTxt1 = data[i].showPeriod;  break; }
            case 1: { this.chartTxt2 = data[i].showPeriod;  break; }
            case 2: { this.chartTxt3 = data[i].showPeriod;  break; }
            case 3: { this.chartTxt4 = data[i].showPeriod;  break; }
            case 4: { this.chartTxt5 = data[i].showPeriod;  break; }
            case 5: { this.chartTxt6 = data[i].showPeriod;  break; }
            case 6: { this.chartTxt7 = data[i].showPeriod;  break; }
          }
        }
      }
    } else {
      this.chartTxt1 = 'MON';
      this.chartTxt2 = 'TUE';
      this.chartTxt3 = 'WED';
      this.chartTxt4 = 'THU';
      this.chartTxt5 = 'FRI';
      this.chartTxt6 = 'SAT';
      this.chartTxt7 = 'SUN';
    }
  }

  paintIndicatorGraph() {
    //change info of graph
    if (this.unit == 'STEPS') {
      switch (this.periodTime) {
        case 'w': { this.indicator1 = '0'; this.indicator2 = '2,500'; this.indicator3 = '5,000'; this.indicator4 = '7,500'; this.indicator5 = '10,000'; break; }
        case 'm': { this.indicator1 = '0'; this.indicator2 = '75,000'; this.indicator3 = '150,000'; this.indicator4 = '225,000'; this.indicator5 = '300,000'; break; }
        case 'y': { this.indicator1 = '0'; this.indicator2 = '900,000'; this.indicator3 = '1,800,000'; this.indicator4 = '2,700,000'; this.indicator5 = '3,600,000'; break; }
      }
    } else if (this.unit == 'HR') {
      switch (this.periodTime) {
        case 'w': { this.indicator1 = '0'; this.indicator2 = '15'; this.indicator3 = '30'; this.indicator4 = '45'; this.indicator5 = '60'; break; }
        case 'm': { this.indicator1 = '0'; this.indicator2 = '450'; this.indicator3 = '900'; this.indicator4 = '1,350'; this.indicator5 = '1,800'; break; }
        case 'y': { this.indicator1 = '0'; this.indicator2 = '5,400'; this.indicator3 = '10,800'; this.indicator4 = '16,200'; this.indicator5 = '21,600'; break; }
      }
    } else {//Km
      switch (this.periodTime) {
        case 'w': { this.indicator1 = '0'; this.indicator2 = '0.25'; this.indicator3 = '0.5'; this.indicator4 = '0.75'; this.indicator5 = '1'; break; }
        case 'm': { this.indicator1 = '0'; this.indicator2 = '7.5'; this.indicator3 = '15'; this.indicator4 = '22.5'; this.indicator5 = '30'; break; }
        case 'y': { this.indicator1 = '0'; this.indicator2 = '90'; this.indicator3 = '180'; this.indicator4 = '270'; this.indicator5 = '360'; break; }
      }
    }
  }

  paintGraph(data: any) {
    for (let i = 0; i < data.length; i++) {
      console.log(data[i])
      if (i < 7) {
        if (this.activityType == 's') {
          this.lines[i].value = 'chart-color1-h' + data[i].percentScale;
        } else {
          this.lines[i].value = 'chart-color2-h' + data[i].percentScale;
        }
      }
    }
  }

  clearGraph() {
    this.lines = [
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' },
      { value: 'chart-color1-h0' }
    ];

  }
}
