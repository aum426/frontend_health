import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
// import { SettingsProvider } from '../../providers/settings/settings';
import { WeeklyAchievementProvider } from '../../providers/weekly-achievement/weekly-achievement';
// import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ProfileSettingPage } from '../profile-setting/profile-setting';


@Component({
  selector: 'page-weekly-achivement',
  templateUrl: 'weekly-achivement.html',
})
export class WeeklyAchivementPage {
  rootPage: any;
  yourLevel: any;
  // mockupData: any;
  nextLevel: Array<object> = [];
  lsInfo: any;
  constructor(
    private weeklyAchievement: WeeklyAchievementProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public nav: NavController
    // private settings: SettingsProvider
  ) {

    let me = this;
    this.yourLevel = {
      levelNo: '',
      levelName: '',
      levelImage: '',
      lsInfo: []
    }
    this.weeklyAchievement.getWeeklyAchievement().then((data: any) => {
      console.log('xxx ', data,'--- ',data != undefined)
      if (data.length>0) {
        me.yourLevel = data[0];
        console.log(data[0])
        for (let i = 1; i < data.length; i++) {
          console.log('==>', data[i])
          me.nextLevel.push(data[i]);
        }
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeeklyAchivementPage');
  }

  btnBack() {
    console.log(this.navParams,' ',this.navParams.data.sourcePage);
    if(this.navParams.data.sourcePage=='thisWeek'){
      console.log('this ')
      this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
    }else{
      this.navCtrl.setRoot(ProfileSettingPage, {}, { animate: true, direction: 'back' });
    }
  }

}
