import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import {ConfigProvider} from '../../providers/config/config';
import {FriendsProvider} from '../../providers/friends/friends';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  showCardData: any;
  constructor(
    private friend:FriendsProvider,
    private config:ConfigProvider,
    public navCtrl: NavController,
    public navParams: NavParams) {
    console.log('friend data ',this.config.objFriendReques)
    this.showCardData=this.config.objFriendReques;
    // this.showCardData = [
    //   {
    //     rankNo: 1,
    //     total: 569,
    //     totalTxt: "569 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/89106_156.jpg",
    //     userName: "Elise",
    //     userNo: "89106",
    //   },
    //   {
    //     rankNo: 2,
    //     total: 570,
    //     totalTxt: "570 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/730394_63.jpg",
    //     userName: "Lisa",
    //     userNo: "730394",
    //   },
    //   {
    //     rankNo: 3,
    //     total: 571,
    //     totalTxt: "571 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/84742_734.jpg",
    //     userName: "Stephanie",
    //     userNo: "84742",
    //   },
    //   {
    //     rankNo: 4,
    //     total: 572,
    //     totalTxt: "572 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/82815_482.jpg",
    //     userName: "Stephanie",
    //     userNo: "82815",
    //   }
    // ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

  btnBack() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
  }

  btnAccept(el){
    console.log('btnAccepted');
    console.log(el,el.userNo);
    let me=this;
    this.friend.addFriend(el.friendNo).then((data:any)=>{
      console.log('---- ',data)
        me.friend.getFriendRequestMe().then((data:any)=>{
          me.showCardData=data;
        })
    })
  }

  btnReject(el){
    console.log('btnRejected');
    console.log(el);
    let me=this;
    this.friend.rejectFriendRequest(el.friendNo).then((data:any)=>{
      console.log('### ',data);
      me.friend.getFriendRequestMe().then((data:any)=>{
        me.showCardData=data;
      })
    })
  }

}
