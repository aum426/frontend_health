import { Component } from '@angular/core';
import { NavController, NavParams,ModalController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { SettingsProvider } from '../../providers/settings/settings';

import{RewardedSmilePointProvider} from '../../providers/rewarded-smile-point/rewarded-smile-point';
import { RewardConditionPage } from '../reward-condition/reward-condition';
import { ProfileSettingPage } from '../profile-setting/profile-setting';

@Component({
  selector: 'page-rewarded-smaile-points',
  templateUrl: 'rewarded-smaile-points.html',
})
export class RewardedSmailePointsPage {
  showCardData: any;
  totalSmilePoint:string='0.0';
  totalPremium:string='0';
  annualPremium:string='0';
  constructor(
    public modalCtrl: ModalController,
    private reward:RewardedSmilePointProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private settings: SettingsProvider
  ) {
    let me=this;
    this.reward.getRewardSmilePoint().then((data:any)=>{
      console.log('!!!',data);
      me.showCardData=data;
    })

    this.reward.getSumSmilePoint().then((data:any)=>{
      console.log('total smile point ==>',data);
      me.totalSmilePoint=data.smilePoint;
      me.totalPremium=data.premium;
      me.annualPremium=data.annualPremium;
    })

    // this.showCardData = [
    //   { level: '1', date: '1 JAN - 7 JAN 2019', smile_point: '1', photo: 'flamingo.png' },
    //   { level: '5', date: '25 dec - 31 dec 2018', smile_point: '2', photo: 'sheetah.png' },
    //   { level: '4', date: '18 JAN - 24 jan 2018', smile_point: '3', photo: 'zebra.png' },
    //   { level: '3', date: '11 JAN - 17 JAN 2018', smile_point: '1', photo: 'monkey.png' },
    //   { level: '2', date: '4 JAN - 10 JAN 2018', smile_point: '5', photo: 'elephant.png' },
    // ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardedSmailePointsPage');
  }

  btnBack() {
    this.navCtrl.setRoot(ProfileSettingPage, {}, { animate: true, direction: 'back' });
  }

  getInfo(){
    console.log('info')
    const stepModal = this.modalCtrl.create(RewardConditionPage, {annualPremium:this.annualPremium});
    stepModal.present();
  }

}
