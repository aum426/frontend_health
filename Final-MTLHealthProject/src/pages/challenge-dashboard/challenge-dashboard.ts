import { SettingsProvider } from "./../../providers/settings/settings";
import { Component, NgZone } from "@angular/core";
import {
  NavController,
  NavParams,
  App,
  AlertController,
  LoadingController
} from "ionic-angular";
import { ChallengeCreatePage } from "../challenge-create/challenge-create";
import { ChallengeJoinPage } from "../challenge-join/challenge-join";

import { ChallengeDashboardProvider } from "../../providers/challenge-dashboard/challenge-dashboard";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { ThisWeekPage } from "../this-week/this-week";
import { ConfigProvider } from "../../providers/config/config";

@Component({
  selector: "page-challenge-dashboard",
  templateUrl: "challenge-dashboard.html"
})
export class ChallengeDashboardPage {
  segmentSwitch1: any = "segment_button_active";
  segmentSwitch2: any = "segment_button_inactive";
  segmentSwitch3: any = "segment_button_inactive";

  showCardData: any;
  mockupData_competition: any;
  mockupData_challenge: any;
  objChallenge: any;
  objCompetition: any;

  objUpComing:any;
  objOnGoing:any;
  objHistory:any;

  modeType:string='';

  constructor(
    private config: ConfigProvider,
    private loadingCtrl: LoadingController,
    private zone: NgZone,
    private barcodeScanner: BarcodeScanner,
    public navCtrl: NavController,
    public navParams: NavParams,
    private app: App,
    public setting: SettingsProvider,
    public alertCtrl: AlertController,
    private challengeDashboard: ChallengeDashboardProvider
  ) {
    let me = this;
    const loader = me.loadingCtrl.create({
      spinner: "crescent"
    });

    loader.present();
    this.challengeDashboard.getChallengeDashboard().then((data: any) => {
      loader.dismiss();
      console.log('upcoming ',data.objUpComing);
      console.log('onGoing ',data.objOnGoing);
      console.log('History ',data.objHistory)
      // me.objChallenge = data.objChallenge;
      // me.objCompetition = data.objCompetition;
      me.objUpComing=data.objUpComing;
      me.objOnGoing=data.objOnGoing;
      me.objHistory=data.objHistory;

      me.bindChallengeDashboardData();
    });

  }
  bindChallengeDashboardData() {
    let me = this;
    if (this.setting.activeSegment == undefined) {
      if (me.objUpComing.length > 0) {
        console.log("case 1 ", me.objUpComing.length);
        // me.setting.activeSegment = 2;
        me.btnSegment(3);
        me.modeType='UpComing';

        // me.showCardData=me.objChallenge;
      } else if (me.objOnGoing.length > 0) {
        console.log("case 2");
        // me.setting.activeSegment = 1;
        me.btnSegment(2);
        me.modeType='OnGoing';
      } else {
        console.log("case 3");
        me.setting.activeSegment = 1;
        me.modeType='History';
      }
    } else {
      me.btnSegment(me.setting.activeSegment);
    }
  }

  ionViewDidEnter() {
    console.log("enter");
    let me = this;
    if (this.config.refreshChallengeDashboard == true) {
      const loader = me.loadingCtrl.create({
        spinner: "crescent"
      });
      loader.present();
      this.challengeDashboard.getChallengeDashboard().then((data: any) => {
        me.config.refreshChallengeDashboard = false;
        loader.dismiss();
        console.log(data.objUpComing);
        console.log(data.objOnGoing);
        // me.objChallenge = data.objChallenge;
        // me.objCompetition = data.objCompetition;
        me.objUpComing=data.objUpComing;
        me.objOnGoing=data.objOnGoing;
        
        me.bindChallengeDashboardData();
      });
    }
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad ChallengeDashboardPage");

    // this.segmentSwitch1 = 'segment_button_active';
    // this.segmentSwitch2 = 'segment_button_inactive';

    // this.btnSegment(this.setting.activeSegment);
    // console.log(this.setting.activeSegment);
    // if (this.setting.activeSegment == 1 || this.setting.activeSegment == undefined) {
    //   this.showCardData = this.objCompetition;
    // } else {
    //   this.showCardData = this.objChallenge;
    // }
  }

  // swipe(e) {

  //   if (e.direction == 4) {
  //     console.log(e.direction);
  //     // if (this.scrollStatus == false) {
  //     this.navCtrl.parent.select(2);
  //     // }
  //   } else if (e.direction == 2) {
  //     this.navCtrl.parent.select(0);
  //   }
  // }

  btnSegment(index) {
    console.log(index);
    if (index == 1) {
      // console.log('xxx 1')
      this.segmentSwitch1 = "segment_button_active";
      this.segmentSwitch2 = "segment_button_inactive";
      this.segmentSwitch3 = "segment_button_inactive";
      this.showCardData = this.objHistory;      
      this.zone.run(() => {});    

      this.setting.activeSegment = index;
    } else if (index == 2) {
      // console.log('xxx 2')
      this.segmentSwitch1 = "segment_button_inactive";
      this.segmentSwitch2 = "segment_button_active";
      this.segmentSwitch3 = "segment_button_inactive";
      this.showCardData = this.objOnGoing;
      // console.log('==> ', this.showCardData)
      this.zone.run(() => {});

      // this.showCardData = this.mockupData_challenge;
      this.setting.activeSegment = index;
    } else {
      this.segmentSwitch1 = "segment_button_inactive";
      this.segmentSwitch2 = "segment_button_inactive";
      this.segmentSwitch3 = "segment_button_active";
      this.showCardData=this.objUpComing;
      this.zone.run(() => {});
    }
  }

  btnCreateChallenge() {
    console.log("create challenge");
    this.app
      .getRootNav()
      .setRoot(
        ChallengeCreatePage,
        {},
        { animate: true, direction: "forward" }
      );
  }

  btnReject(record) {
    console.log("btnReject");

    let confirm = this.alertCtrl.create({
      title: "Reject this group",
      message: record.challenge_name,
      buttons: [
        {
          text: "No"
        },
        {
          text: "Yes",
          handler: () => {
            console.log("btnReject");
          }
        }
      ]
    });
    confirm.present();
  }

  btnJoin(record) {
    console.log("btnJoin", record);
    let me = this;
    let confirm = this.alertCtrl.create({
      title: "Join this group",
      message: record.challenge_name,
      buttons: [
        {
          text: "No"
        },
        {
          text: "Yes",
          handler: () => {
            console.log("btnJoin");
            me.challengeDashboard
              .joinChallenge(record.challengeNo)
              .then((data: any) => {
                console.log(data);
                if (data == 1) {
                  me.app
                    .getRootNav()
                    .setRoot(
                      ChallengeJoinPage,
                      { statusJoin: true, passRecord: record },
                      { animate: true, direction: "forward" }
                    );
                }
              });
          }
        }
      ]
    });
    confirm.present();
  }

  btnShowDetail(record){
    console.log("btnJoinDetail", record);
    let status: boolean = record.joinStatus != "pending" ? true : false;
    console.log("status ", status);
    this.app
      .getRootNav()
      .setRoot(
        ChallengeJoinPage,
        { statusJoin: status, showType:this.modeType, passRecord: record },
        { animate: true, direction: "forward" }
      );
  }

  btnJoinDetail(record) {
    console.log("btnJoinDetail", record);
    let status: boolean = record.joinStatus != "pending" ? true : false;
    console.log("status ", status);
    this.app
      .getRootNav()
      .setRoot(
        ChallengeJoinPage,
        { statusJoin: status, showType: "challenge", passRecord: record },
        { animate: true, direction: "forward" }
      );
  }

  btnCompetitionDetail(record) {
    console.log("btnCompetitionDetail", record);
    this.app
      .getRootNav()
      .setRoot(
        ChallengeJoinPage,
        { statusJoin: true, showType: "competition", passRecord: record },
        { animate: true, direction: "forward" }
      );
  }

  btnJoinQrcode() {
    let me = this;
    console.log("====================================");
    console.log("join qr code");
    console.log("====================================");
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        console.log("Barcode data", barcodeData);
        console.log("barcode data ", barcodeData.text);

        me.challengeDashboard
          .addChallengeMember(barcodeData.text)
          .then((status: any) => {
            console.log("status ", status);
            if (status == 1) {
              me.challengeDashboard.getChallengeList().then((data: any) => {
                console.log("challenge ", data);
                this.segmentSwitch1 = "segment_button_inactive";
                this.segmentSwitch2 = "segment_button_active";
                me.setting.activeSegment = 2;
                me.objChallenge = data;
                me.showCardData = data;
              });
            }
          });
      })
      .catch(err => {
        console.log("Error", err);
      });
  }
}
