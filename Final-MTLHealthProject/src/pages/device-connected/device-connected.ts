import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DeviceConfirmUnlinkPage } from '../device-confirm-unlink/device-confirm-unlink';
import { ConfigProvider } from '../../providers/config/config';
import { DeviceSyncProvider } from '../../providers/device-sync/device-sync';
import { WebSqlProvider } from "../../providers/web-sql/web-sql";

@Component({
  selector: 'page-device-connected',
  templateUrl: 'device-connected.html',
})
export class DeviceConnectedPage {
  //mockupData: any;
  deviceData: any;
  objDeviceBrand: string;

  constructor(
    // private zone:NgZone,
    private SQLite: WebSqlProvider,
    private deviceSyncProvider: DeviceSyncProvider,
    private config: ConfigProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
    let me = this;

    //this.deviceData = this.navParams.get('obj');
    console.log('xxx =>', this.config.deviceData)
    if (this.config.deviceData == undefined) {
      this.deviceSyncProvider.getDeviceConfig().then((data: any) => {
        console.log('!!!!! ', data, ' me.config.objProfile.rows ', me.config.objProfile.rows);
        let objConnected = data.find(x => x.deviceBrand === me.config.objProfile.rows.item(0).brand);//[0]['brand']);
        console.log('obj connect ', objConnected)
        me.objDeviceBrand = objConnected.deviceBrand;

        data[objConnected.deviceNo - 1].status = 'Y';
        console.log('obj profile ', me.config.objProfile)
        data[objConnected.deviceNo - 1].dateConnect = me.config.objProfile.rows.item(0).date_connect;//[0]['date_connect'];
        me.deviceData = data;
        me.config.deviceData = data;
      });
    } else {
      me.deviceData = me.config.deviceData;
    }



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeviceConnectedPage');
  }

  btnDashboard() {
    console.log('btnDashboard');
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
  }

  btnUnlink(data) {
    console.log('unlink ', data)
    let me = this;
    if (data.status == 'Y') {
      const profileModal = this.modalCtrl.create(DeviceConfirmUnlinkPage, { record: data });
      profileModal.present();

      profileModal.onDidDismiss(data => {
        console.log('====>', data);
        // if (data == 'unlink') {
        if (data > 0) {
          //update status deviceData from index-1 
          let objDisConnect = me.config.deviceData.find(x => x.deviceNo === data);
          console.log('device dis connect ', objDisConnect)
          me.config.deviceData[objDisConnect.deviceNo - 1].status = 'N';
          console.log('end ', me.config.deviceData)
          // delete record in db
          let sql = "UPDATE tb_config SET brand='' WHERE user_no='" + me.config.userNo + "'";
          console.log('sql delete ', sql)
          me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
            console.log('status delete ', status)
            sql = "SELECT * FROM tb_config WHERE user_no='" + me.config.userNo + "'";
            console.log('sql select', sql);
            me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result: any) {
              console.log(result)
              me.config.objProfile = result;
              // me.config.objProfile = undefined;
              me.config.deviceData = undefined;
              me.config.deviceConnected = false;
              me.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
            });
          })
        }
      });
    }

  }

}
