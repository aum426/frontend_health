import { Component } from '@angular/core';
import { LoadingController, ModalController } from 'ionic-angular';

import { HealthProvider } from '../../providers/health/health';
import { ModalPage } from '../health-add/health-add';
import { HealthWelcomePage } from '../health-welcome/health-welcome';
import { HealthInformationPage } from '../health-information/health-information';
import { ConfigProvider } from '../../providers/config/config';
import { WebSqlProvider } from '../../providers/web-sql/web-sql';
//import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-health',
  templateUrl: 'health.html',
})
export class HealthPage {

  userId: any;
  HealthScore: any;
  actionSheetSelected: any;
  actionSheetGroup: any;
  healthIndexRateArray:any

  BMI: any
  bloodPressure1: string;
  bloodPressure2: string;
  cholesterol: any
  bloodSugar: any
  weeklySteps: any
  weeklyExercises: any
  HealthScoreIndex : any
  
  constructor(
    private loadingCtrl: LoadingController,
    private healthProvider: HealthProvider,
    public modalController: ModalController,
    private config: ConfigProvider,
    private SQLite: WebSqlProvider,
    private storage: Storage,
  ){
    this.HealthScore = "0";
    this.userId=this.config.userNo;
    // this.userId= "730659"
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad HealthPage ');
    this.getHealthDateScore();
    this.getHealthIndex();
    //this.bindLeaderboardData();
    console.log('welcomePage storage',await this.storage.get('welcomePage'));
    const welcomePage = this.storage.get('welcomePage');
    if(await welcomePage != true){
      this.getWelcomePage();
    }  

  }
  async ionViewDidEnter() {
    console.log(await this.storage.get('welcomePage'));
    // this.getWelcomePage();
  }
  getHealthDateScore(){
    let me = this;
      const loader = me.loadingCtrl.create({
        spinner: 'crescent'
      });
    loader.present();
      me.healthProvider.getHealthDateScore(me.userId).then((data: any) => {
        console.log('getHealthDate ',data);
        me.HealthScore = data.data.health_score;
        //console.log('data.health_score ',data.data.health_score);
          loader.dismiss();
       })
    }

    async editModal() {
    const termAndConditionModal = this.modalController.create(
      ModalPage,
      {},{cssClass: 'select-modal' }
    );
    termAndConditionModal.present();
    }

    // async welcomeModal() {
    //   const termAndConditionModal = this.modalController.create(
    //     HealthWelcomePage,
    //     {},{cssClass: 'select-modal' }
    //   );
    //   termAndConditionModal.present();
    //   }
    async welcomeModal() {
      this.storage.set('welcomePage', null);
      console.log('Set storage',null);
      }
      

     getHealthIndex(){
      let me = this;
        const loader = me.loadingCtrl.create({
          spinner: 'crescent'
        });
      loader.present();
        me.healthProvider.getHealthIndex(me.userId).then((data: any) => {
          console.log('getHealthIndex ',data);
          me.healthIndexRateArray = data.data.health_index_resp;
          console.log('data.getHealthIndex ',data.data.health_index_resp);
          me.paintPeriodGraph(me.healthIndexRateArray);
            loader.dismiss();
         })
      }

      paintPeriodGraph(healthIndexRateArray) {
        let n = 1;
    for (let i = 0; i < healthIndexRateArray.length; i++) {
      let value1 = this.healthIndexRateArray[i].value1;
      let value2 = this.healthIndexRateArray[i].value2;
      let health_index = this.healthIndexRateArray[i].health_index;

      switch (n) {
        case 1: { this.BMI = value1;  this.healthIndexRateArray[i]; break; }
        case 2: { this.bloodPressure1 = value1; this.bloodPressure2 = value2; this.healthIndexRateArray[i]; break;}
        case 3: { this.cholesterol = value1;  this.healthIndexRateArray[i]; break; }
        case 4: { this.bloodSugar = value1;  this.healthIndexRateArray[i]; break; }
        case 5: { this.weeklySteps = value1;  this.healthIndexRateArray[i]; break; }
        case 6: { this.weeklyExercises = value1;  this.healthIndexRateArray[i]; break; }
        case 7: { this.HealthScoreIndex = health_index+".png";  this.healthIndexRateArray[i]; break; }
      }
      n++;
    }
    // BMI: any
    // bloodPressure1: string;
    // bloodPressure2: string;
    // cholesterol: any
    // bloodSugar: any
    // weeklySteps: any
    // weeklyExercises: any
  }

  informationModal(){
    console.log("HealthInformationPage")
    const termAndConditionModal = this.modalController.create(
      HealthInformationPage,
      {},{cssClass: 'select-modal' }
    );
    termAndConditionModal.present();
  }
  // btnJoinQrcode(){
  //   console.log('====================================');
  //   console.log('join qr code');
  //   console.log('====================================');
  // }

  
  getWelcomePage(){
    const termAndConditionModal = this.modalController.create(
      HealthWelcomePage,
      {},{cssClass: 'select-modal' }
    );
    termAndConditionModal.present();
  }
}
