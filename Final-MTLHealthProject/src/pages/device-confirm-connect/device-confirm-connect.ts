import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import{DeviceConfirmConnectProvider} from '../../providers/device-confirm-connect/device-confirm-connect';
import {AppleProvider} from '../../providers/apple/apple';


@Component({
  selector: 'page-device-confirm-connect',
  templateUrl: 'device-confirm-connect.html',
})
export class DeviceConfirmConnectPage {

  brandId: any;
  brandName: any;
  barndDescription: any;
  brandDevicePhoto: any;

  constructor(
    private apple:AppleProvider,
    private dCCProvider:DeviceConfirmConnectProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController
  ) {

    let record = this.navParams.get('record');

    this.brandId = record.deviceNo;
    this.brandName = record.deviceBrand;
    this.brandDevicePhoto = record.deviceImage;
    this.barndDescription = record.deviceInfo;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeviceConfirmConnectPage');
  }

  btnClose() {
    this.viewCtrl.dismiss('');
  }

  btnConnect(brandName) {
    console.log('connect:' + brandName);
    let me=this;
    if(brandName=='Apple'){
      //get permission
        me.apple.checkPermission(function(status){
          console.log('status permission ',status);
          me.viewCtrl.dismiss(status);
        })
    }else{
      me.dCCProvider.getLink(brandName).then(data=>{
        console.log('xxx %%%%%',data);
         me.viewCtrl.dismiss(data);
      })
    }
  }

}
