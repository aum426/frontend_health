import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, App } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { HealthProvider } from '../../providers/health/health';
import { HealthPage } from '../health/health';
import { ConfigProvider } from '../../providers/config/config';

@Component({
  selector: 'health-add',
  templateUrl: 'health-add.html',
})

export class ModalPage {

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage ');
    this.getApiHealthData();
    //this.bindLeaderboardData();
  }

  userId: any;
  HealthScore: any;
  userImg: any;

  heartRateArray: any = [];

  steps: any
  exercises: any
  bloodPressure1: string;
  bloodPressure2: string;
  BMI: any
  cholesterol: any
  bloodSugar: any
  height: any
  weight: any
  HDL: any
  Age:any
  mydate:any

  // style inactive,active
  segmentSwitch1: any = 'segment_height_inactive';
  segmentSwitch2: any = 'segment_weight_inactive';
  segmentSwitch3: any = 'segment_bloodPressure_inactive';
  segmentSwitch4: any = 'segment_bloodPressure_1_inactive';
  segmentSwitch5: any = 'segment_cholesterol_inactive';
  segmentSwitch6: any = 'segment_HDL_inactive';
  segmentSwitch7: any = 'segment_bloodSugar_inactive';


  constructor(
    public navCtrl: NavController,
    private healthProvider: HealthProvider,
    private loadingCtrl: LoadingController,
    private config : ConfigProvider,
    private alertCtrl: AlertController,
    private app: App,
  ) {
    this.HealthScore = "100";
    this.userId= this.config.userNo;
    // this.userId= "730659"

    this.steps = "0"
    this.exercises = "0"
    this.bloodPressure1 = "0"
    this.bloodPressure2 = "0"
    this.BMI = "0"
    this.cholesterol = "0"
    this.bloodSugar = "0"
    this.height = "0"
    this.weight = "0"
    this.HDL = "0"
    this.Age = "0"
    
  }

  // btnBack() {
  //   this.navCtrl.setRoot(
  //     HealthPage,
  //     {},
  //     { animate: true, direction: "back" }
  //   );
  // }
  btnBack() {

    console.log('empty')
      let alert = this.alertCtrl.create({
        title: 'Your changes will not be saved',
        message: 'Do you want to continue?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            },
            cssClass: 'profalert'
          },
          {
            text: 'YES',
            handler: () => {
              console.log('Cancel clicked');
              this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
            },
          },
        ]
      });
      alert.present();
  }

  doSomething(date) {
    console.log('date', this.mydate); // 2019-04-22
  }


  getApiHealthData() {
    let me = this;
    const loader = me.loadingCtrl.create({
      spinner: 'crescent'
    });
    loader.present();
    me.healthProvider.getHealthDate(me.userId).then((data: any) => {
      console.log('getHealthDate ', data);
      console.log('getHealthDate health_data_list ', data.data.health_data_list);
      me.heartRateArray = data.data.health_data_list;
      me.paintPeriodGraph(me.heartRateArray);
      loader.dismiss();
    })
  }
  paintPeriodGraph(heartRateArray) {
        let n = 1;

    if (heartRateArray.length == 1){  //  ยังไม่เคยมีข้อมูล
      for (let i = 0; i < heartRateArray.length; i++) {
        let value1 = this.heartRateArray[i].value1;
        let value2 = this.heartRateArray[i].value2;
        switch (n) {
          case 1: { this.Age = value1;  this.heartRateArray[i]; break; }
  
        }
        n++;
      }
    }else{ 

      for (let i = 0; i < heartRateArray.length; i++) {
        let value1 = this.heartRateArray[i].value1;
        let value2 = this.heartRateArray[i].value2;
        switch (n) {
          // case 1: { this.steps = value1;  this.heartRateArray[i]; break; }
          // case 2: { this.exercises = value1;  this.heartRateArray[i]; break; }
          case 1: { this.bloodPressure1 = value1;
                    this.bloodPressure2 = value2;
                    this.heartRateArray[i];; break; }
          case 2: { this.BMI = value1;  this.heartRateArray[i]; break; }
          case 3: { this.cholesterol = value1;  this.heartRateArray[i]; break; }
          case 4: { this.bloodSugar = value1;  this.heartRateArray[i]; break; }
          case 5: { this.height = value1;  this.heartRateArray[i]; break; }
          case 6: { this.weight = value1;  this.heartRateArray[i]; break; }
          case 7: { this.HDL = value1;  this.heartRateArray[i]; break; }
          case 8: { this.Age = value1;  this.heartRateArray[i]; break; }
  
        }
        n++;
      }

    }
    

  }
  btnSegment(even){
    if(even == 1){
      this.segmentSwitch1 = 'segment_height_active';
    }
    else if(even == 2){
      this.segmentSwitch2 = 'segment_weight_active';
    }
    else if(even == 3){
      this.segmentSwitch3 = 'segment_bloodPressure_active';
    }
    else if(even == 4){
      this.segmentSwitch4 = 'segment_bloodPressure_1_active';
    }
    else if(even == 5){
      this.segmentSwitch5 = 'segment_cholesterol_active';
    }
    else if(even == 6){
      this.segmentSwitch6 = 'segment_HDL_active';
    }
    else if(even == 7){
      this.segmentSwitch7 = 'segment_bloodSugar_active';
    }
    
  }
  btnSaveChallenge() {
    let me=this;
    console.log('height',this.height,'weight',this.weight,'bloodPressure'
    ,this.bloodPressure1+'/'+this.bloodPressure2,'cholesterol'
    ,this.cholesterol,'HDL',this.HDL,'bloodSugar',this.bloodSugar)
    //let me = this;
    //this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
    // if(this.steps!=0){
    // } else {
    //   console.log('empty')
      let alert = me.alertCtrl.create({
        title: 'Your changes will be saved',
        message: 'Do you want to continue?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            },
            cssClass: 'profalert'
          },
          {
            text: 'Continue',
            handler: () => {
              console.log('Continue');
              
                let loader = me.loadingCtrl.create({
                  spinner: 'crescent'
                });
              loader.present();
              if(this.mydate != null) {
                me.healthProvider.insertHealthUserProfile(me.userId,me.mydate)
              }
              me.healthProvider.insertHealthData(me.userId,me.height,me.weight,
              me.bloodPressure1,me.bloodPressure2,me.cholesterol,me.HDL,me.bloodSugar).then((data: any) => {
              console.log('btnSaveChallenge ',data);
              console.log('btnSaveChallenge ',data.response.status.code,':',data.response.status.description);
              loader.dismiss();
              //me.btnBack();
              if (data.response.status.code == 0) {
                me.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
                  //this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
              }
         })

              // loader.dismiss();
            }
          }
        ]
      });
      alert.present();
    // }
  }

}