import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ActionSheetController,
  LoadingController,
  AlertController,
  normalizeURL,
  PopoverController,
  ModalController
} from "ionic-angular";
import { TabsPage } from "../tabs/tabs";
import { FriendsPage } from "../friends/friends";

import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { ConfigProvider } from "../../providers/config/config";
import { PopoverSettingComponent } from "../../components/popover-setting/popover-setting";
import { DeviceConnectedPage } from "../device-connected/device-connected";
import { DeviceSyncPage } from "../device-sync/device-sync";
import { LoginPage } from "../login/login";
import { TermAndConditionPage } from "../term-and-condition/term-and-condition";
import { RewardedSmailePointsPage } from "../rewarded-smaile-points/rewarded-smaile-points";
import { WeeklyAchivementPage } from "../weekly-achivement/weekly-achivement";
import { SettingsProvider } from "../../providers/settings/settings";
import {ProfileSettingProvider} from "../../providers/profile-setting/profile-setting";

@Component({
  selector: "page-profile-setting",
  templateUrl: "profile-setting.html"
})
export class ProfileSettingPage {
  userImg: any;
  segmentSwitch1: any = "segment_button_active_history";
  segmentSwitch2: any = "segment_button_inactive_history";

  userName:string="-";
  totalSmilePoint:string="0";
  currentPoint:string="0";
  totalFriends:string="0";
  userStatus:string="-";
  userBadges:string="0";
  userEvents:string="0";
  

  constructor(
    private profileSettingProvider:ProfileSettingProvider,
    private config: ConfigProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    public loadingCtrl: LoadingController,
    public actionSheetController: ActionSheetController,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    private settings: SettingsProvider,
  ) {
    let me=this;
    //this.userImg = "assets/imgs/avatar/Default.jpg";
    this.profileSettingProvider.getProfileSetting().then((data:any)=>{
      console.log(data);
      me.userName=data.userName;
      me.userImg=data.userImg;
      me.userStatus=data.userStatus;
      me.userBadges=data.currentLevel;
      me.totalFriends=data.totalFriend;
      me.totalSmilePoint=data.totalSmilePoint;
      me.userEvents=data.totalEvent;
      me.currentPoint=data.currentPoint;
    })
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProfileSettingPage");
  }

  btnBack() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: "back" });
  }

  goWeeklyAchivement() {
    this.navCtrl.setRoot(
      WeeklyAchivementPage,
      {},
      { animate: true, direction: "forward" }
    );
  }

  goRewardSmilePoints() {
    this.navCtrl.setRoot(
      RewardedSmailePointsPage,
      {},
      { animate: true, direction: "forward" }
    );
  }

  goTabChallengeView() {

    this.settings.tabIndex = 3;

    this.navCtrl.setRoot(
      TabsPage,
      { currentTab: "challenge_view" },
      { animate: true, direction: "back" }
    );
  }

  goFriendsView() {
    console.log("go NotificationView");
    this.navCtrl.setRoot(
      FriendsPage,
      {},
      { animate: true, direction: "forward" }
    );
  }

  async btnSelectPhoto() {
    const actionSheet = await this.actionSheetController.create({
      title: "Select method take photo",
      buttons: [
        {
          text: "Take Photo",
          role: "destructive",
          handler: () => {
            console.log("Take Photo xxx");
            this.openCamera();
          }
        },
        {
          text: "Select from Gallery",
          handler: () => {
            console.log("Select from Gallery");
            this.openPhotoGallery();
          }
        },
        {
          text: "Cancel",
          handler: () => {
            console.log("Cancel");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    await actionSheet.present();
  }

  openCamera() {
    let me = this;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 150,
      targetHeight: 150,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(
      imageData => {
        let filename = imageData.substring(imageData.lastIndexOf("/") + 1);
        let path = imageData.substring(0, imageData.lastIndexOf("/") + 1);
        me.file.readAsDataURL(path, filename).then(data => {
          console.log("xx ", data);
          me.userImg = data;
        });
        console.log("src image ", imageData);
        me.uploadImageProfile(imageData);
      },
      err => {
        console.log(err);
      }
    );
  }

  openPhotoGallery() {
    let me = this;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 150,
      targetHeight: 150,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(
      imageData => {
        console.log(imageData);
        console.log(normalizeURL(imageData));
        let path = imageData.substring(0, imageData.lastIndexOf("/") + 1);
        let filename = imageData.substring(imageData.lastIndexOf("/") + 1);
        let index = filename.indexOf("?");
        if (index > -1) {
          filename = filename.substring(0, index);
        }
        me.file.readAsDataURL(path, filename).then(data => {
          console.log("### ", data);
          me.userImg = data;
        });
        me.uploadImageProfile(imageData);
      },
      err => {
        console.log(err);
      }
    );
  }

  uploadImageProfile(imageData: any) {
    let me = this;
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: this.config.userNo,
      fileName: "profile", //'\\profile\\' + this.config.userNo,// + '.jpg',
      httpMethod: "POST",
      headers: {
        "Content-Type": undefined
      }
    };
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    fileTransfer
      .upload(imageData, encodeURI(this.config.serverUploadAPI), options)
      .then(
        data => {
          loader.dismiss();
        },
        err => {
          loader.dismiss();

          let _alert = this.alertCtrl.create({
            title: "Fail Upload",
            buttons: ["Dismiss"]
          });

          _alert.present();

          console.log(err);
        }
      );
  }

  btnSegment(index) {
    if (index == 1) {
      this.segmentSwitch1 = "segment_button_active_history";
      this.segmentSwitch2 = "segment_button_inactive_history";
    } else if (index == 2) {
      this.segmentSwitch1 = "segment_button_inactive_history";
      this.segmentSwitch2 = "segment_button_active_history";
    }
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverSettingComponent);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(data => {
      console.log(data, "  config ", this.config.objProfile);
      if (data === "deviceSync") {
        //check device sync? goto deviceConnected : DeviceSync
        if (this.config.deviceConnected == true) {
          this.navCtrl.setRoot(
            DeviceConnectedPage,
            {},
            { animate: true, direction: "forward" }
          );
        } else {
          this.navCtrl.setRoot(
            DeviceSyncPage,
            {},
            { animate: true, direction: "forward" }
          );
        }
      } else if (data === "editProfile") {
        console.log("editProfile");
      } else if (data === "termAndCondition") {
        const termAndConditionModal = this.modalCtrl.create(
          TermAndConditionPage,
          {}
        );
        termAndConditionModal.present();
      } else if (data === "logout") {
        let confirm = this.alertCtrl.create({
          title: "Logout System",
          buttons: [
            {
              text: "No"
            },
            {
              text: "Yes",
              handler: () => {
                this.navCtrl.setRoot(
                  LoginPage,
                  {},
                  { animate: true, direction: "forward" }
                );
              }
            }
          ]
        });

        confirm.present();
      }
    });
  }
}
