import { Component,NgZone } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-congratulation-achievment',
  templateUrl: 'congratulation-achievment.html',
})
export class CongratulationAchievmentPage {

  levelNo:string;
  levelName:string;
  levelImage:string;
  levelInfo:string;
  // lsInfo:any;
  smilePoint:string;

  constructor(
    private zone:NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    console.log(this.navParams.get('levelInfo'))
    console.log('json ',JSON.parse(this.navParams.get('levelInfo')))
    this.levelNo= this.navParams.get('levelNo');
    this.levelName=this.navParams.get('levelName');
    this.levelImage=this.navParams.get('levelImage');
    this.levelInfo=JSON.parse(this.navParams.get('levelInfo'))
    this.smilePoint=this.navParams.get('smilePoint');
    // this.zone.run(()=>{});
  }

  ionViewDidLoad() {    
    console.log('ionViewDidLoad CongratulationAchievmentPage');
  }

  btnClose() {
    this.viewCtrl.dismiss();
  }

}
