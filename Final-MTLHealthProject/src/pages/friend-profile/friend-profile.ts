import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, LoadingController, AlertController, normalizeURL } from 'ionic-angular';
import { FriendsPage } from '../friends/friends';
import { ConfigProvider } from '../../providers/config/config';
import { FriendProfileProvider } from '../../providers/friend-profile/friend-profile';
import { FriendsProvider } from '../../providers/friends/friends';

@Component({
  selector: 'page-friend-profile',
  templateUrl: 'friend-profile.html',
})
export class FriendProfilePage {
  userImg: string;
  totalSmilePoint: string;
  segmentSwitch1: any = 'segment_button_active_history';
  segmentSwitch2: any = 'segment_button_inactive_history';
  friendStatus: string;
  frinedNo: string;
  profileType: string;

  constructor(
    private friendProvider: FriendsProvider,
    private friendProfileProvider: FriendProfileProvider,
    private config: ConfigProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public actionSheetController: ActionSheetController,
    public alertCtrl: AlertController,
  ) {
    // this.userImg = 'assets/imgs/avatar/avatar_04.png';

    // this's the mockup status get it from back-end services
    //this.friendStatus = 'addFriend';
    console.log('type ', this.navParams.data.profileType)

    this.profileType = this.navParams.data.profileType;
    if(this.profileType=='F'){
      this.friendStatus = 'unFriend';
    }else if(this.profileType=='S'){
      this.friendStatus = 'requestFriend'; 
    }else{
      this.friendStatus='addFriend';
    }
    

    console.log('==> ', this.navParams.data.friendNo)
    this.frinedNo = this.navParams.data.friendNo;
    let me = this;
    this.friendProfileProvider.getFriendProfile(this.frinedNo).then((data: any) => {
      console.log('profile ', data)
      console.log('img ', data.userImg)
      me.userImg = data.userImg;
      me.totalSmilePoint = data.totalSmilePoint;
    });


    //check params == friend? call profile 
    //button text un friend : add friend

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendProfilePage');
  }

  btnBack() {
    console.log('go btnBack');
    this.navCtrl.setRoot(FriendsPage, {}, { animate: true, direction: 'back' });
  }

  goFriendsView(status) {

    if (status === 'addFriend') {
      let confirm = this.alertCtrl.create({
        title: 'Confirm',
        message: "Un Friend",
        buttons: [
          {
            text: 'No'
          },
          {
            text: 'Yes',
            handler: () => {
              let friendStatus = 'B';
              let me = this;
              this.friendProvider.updateFriendStatus(this.frinedNo, friendStatus).then((data: any) => {
                me.friendStatus = status;

                console.log(data);
              })
            }
          }
        ]
      });
      confirm.present();

    } else {

      let confirm = this.alertCtrl.create({
        title: 'Confirm',
        message:this.profileType == 'S'? "Request Friend":'Add Friend',
        buttons: [
          {
            text: 'No'
          },
          {
            text: 'Yes',
            handler: () => {
              let me = this;
              if (me.profileType == 'S') {
                //request friend
                console.log('request friend')
                me.friendProvider.requestFriend(this.frinedNo).then((data:any)=>{
                  me.friendStatus='requestFriend';
                })
              } else {
                //add friend
                console.log('add friend')
                this.friendProvider.addFriend(this.frinedNo).then((data: any) => {
                  console.log(data);
                  me.friendStatus = status;
                })
              }


            }
          }
        ]
      });
      confirm.present();

    }
  }

  btnSegment(index) {

    console.log(index);

    if (index == 1) {
      this.segmentSwitch1 = 'segment_button_active_history';
      this.segmentSwitch2 = 'segment_button_inactive_history';
    } else if (index == 2) {
      this.segmentSwitch1 = 'segment_button_inactive_history';
      this.segmentSwitch2 = 'segment_button_active_history';
    }
  }

}
