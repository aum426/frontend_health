import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, NavParams, Content, LoadingController, ActionSheetController, AlertController, normalizeURL } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';

import { LeaderBoardProvider } from '../../providers/leader-board/leader-board';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../providers/config/config';

@Component({
  selector: 'page-leader-board',
  templateUrl: 'leader-board.html',
})
export class LeaderBoardPage {

  @ViewChild('pageTop') pageTop: Content;
  @ViewChild(Content) content: Content;

  section: string = 'two';
  somethings: any = new Array(10);
  footerStatus: boolean = true;
  mockupData_days: any;
  mockupData_avg: any;
  showCardPadding: any;
  magicFooter: any;
  segmentSwitch1: any = 'segment_button_inactive';
  segmentSwitch2: any = 'segment_button_active';
  showCardData: any;
  scrollStatus: any;
  scrollPosition: any;
  showCardRadius: any = 'cardRadiusTrue';
  //***********
  objCurrentRankOfAll: any;
  objRankOfAll: any;
  objCurrentRankOfWeek: any;
  objRankOfWeek: any;
  userName: string;
  userImg: any;
  smilePoint: string;
  highLight: string;
  rankNo: string;
  startIndex: number = 1;
  endIndex: number = 1;
  mode: string = 'current';

  actionSheetSelected: any;
  actionSheetGroup: any;
  actionSheetSportIcon:string;

  constructor(
    private config: ConfigProvider,
    private alertCtrl: AlertController,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private leaderboard: LeaderBoardProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public zone: NgZone,
    public loadingCtrl: LoadingController,
    public nav: NavController,
    private settings: SettingsProvider,
    public actionSheetController: ActionSheetController
  ) {

    let me = this;
    this.showCardPadding = 'cardPaddingTrue';
    this.magicFooter = 'magicFooterShow';
    this.scrollStatus = false;
    this.userImg = 'assets/imgs/avatar/Default.jpg';
    
    this.actionSheetGroup = 'Public';//"Friends";
    this.actionSheetSelected = "Steps";
    this.actionSheetSportIcon = 'steps.svg';
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter LeaderBoardPage')
    //check WeeklyAchivementPag มันจะเข้้าหน้า ionViewDidLoad อีกครั้ง ทุกหน้าที่ เป็น popup
    // if (this.config.refreshLeaderboard == true && this.objRankOfWeek != undefined) {
    //   console.log('call service get data')
    //   this.config.refreshLeaderboard = false;
    //   this.bindLeaderboardData();
    // }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaderBoardPage ');
    this.bindLeaderboardData();

  }

  updateActionSheetSelected(value) {
    this.pageTop.scrollToTop(100);
    if (value === 1) {
      this.actionSheetSelected = 'Steps';
      this.actionSheetSportIcon = 'steps.svg';
    } else if (value === 2) {
      this.actionSheetSelected = 'Exercise (Overall)';
      this.actionSheetSportIcon = 'exercise.svg';
    } else if (value === 3) {
      this.actionSheetSelected = 'Cycling';
      this.actionSheetSportIcon = 'bike.svg';
    } else if (value === 4) {
      this.actionSheetSelected = 'Running';
      this.actionSheetSportIcon = 'running.svg';
    } else if (value === 5) {
      this.actionSheetSelected = 'Swimming';
      this.actionSheetSportIcon = 'swimming.svg';
    } else if (value === 6) {
      this.actionSheetSelected = 'Others';
      this.actionSheetSportIcon = 'other.svg';
    }

    this.bindLeaderboardData();

  }

  updateActionSheetGroup(value) {
    if (value === 1) {
      this.actionSheetGroup = 'Friends';
    } else if (value === 2) {
      this.actionSheetGroup = 'Public';
    } 
    this.bindLeaderboardData();

  }

  async presentActionSheetGroup() {
    const actionSheet = await this.actionSheetController.create({
      title: 'Select Group',
      buttons: [
        {
        text: 'Friends',
        role: 'destructive',
        icon: 'friends',
        handler: () => {
          this.updateActionSheetGroup(1);
        }
      }, 
      {
        text: 'Public',
        icon: 'group',
        handler: () => {
          this.updateActionSheetGroup(2);
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentActionSheetSports() {
    const actionSheet = await this.actionSheetController.create({
      title: 'Select catagories',
      buttons: [{
        text: 'Steps',
        role: 'destructive',
        icon: 'steps',
        handler: () => {
          this.updateActionSheetSelected(1);
        }
      }, 
      {
        text: 'Exercise (Overall)',
        icon: 'exercise',
        handler: () => {
          this.updateActionSheetSelected(2);
        }
      }, 
      {
        text: 'Running',
        icon: 'running',
        handler: () => {
          this.updateActionSheetSelected(4);
        }
      },
      {
        text: 'Cycling',
        icon: 'bike',
        handler: () => {
          this.updateActionSheetSelected(3);
        }
      }, 
      {
        text: 'Swimming',
        icon: 'swimming',
        handler: () => {
          this.updateActionSheetSelected(5);
        }
      }, 
      {
        text: 'Others',
        icon: 'other',
        handler: () => {
          this.updateActionSheetSelected(6);
        }
      }, 
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }



  bindLeaderboardData() {
    let me = this;
    const loader = this.loadingCtrl.create({
      content: 'Loading...',
      spinner: 'crescent'
    });

    loader.present();
    this.leaderboard.getLeaderboard().then((data: any) => {
      loader.dismiss();
      console.log('leaderboard ', data ,'mode ',me.mode,' type ',me.actionSheetSelected);
      me.userImg = data.profile[0].userImg;//'assets/imgs/avatar/Default.jpg';
      me.userName = data.profile[0].userName;
      me.highLight=data.profile[0].userNo;
      me.rankNo =  '-';
      me.smilePoint = data.smilePointInfo.smilePoint;  
      // me.segmentSwitch1 = 'segment_button_inactive';
      // me.segmentSwitch2 = 'segment_button_active';
      // me.showCardData = data.rankOfWeek;
      if(me.mode=='current'){
        if(this.actionSheetGroup == 'Friends'){
          switch (me.actionSheetSelected){
            case 'Steps':{me.showCardData=data.lsStepFriendThisWeek;me.rankNo=data.rankFriendThisWeek.rankOfStep;break;}
            case 'Exercise (Overall)':{me.showCardData=data.lsOverAllFriendThisWeek;me.rankNo=data.rankFriendThisWeek.rankOfOverAll;break;}
            case 'Cycling':{me.showCardData=data.lsCyclingFriendThisWeek;me.rankNo=data.rankFriendThisWeek.rankOfCycling;break;}
            case 'Running':{me.showCardData=data.lsRunningFriendThisWeek;me.rankNo=data.rankFriendThisWeek.rankOfRunning;break;}
            case 'Swimming':{me.showCardData=data.lsSwimmingFriendThisWeek;me.rankNo=data.rankFriendThisWeek.rankOfSwimming;break;}
            case 'Others':{me.showCardData=data.lsOtherFriendThisWeek;me.rankNo=data.rankFriendThisWeek.rankOfOther;break;}
          }
        }else{
          switch (me.actionSheetSelected){
            case 'Steps':{me.showCardData=data.lsStepThisWeek;me.rankNo=data.rankThisWeek.rankOfStep;break;}
            case 'Exercise (Overall)':{me.showCardData=data.lsOverAllThisWeek;me.rankNo=data.rankThisWeek.rankOfOverAll;break;}
            case 'Cycling':{me.showCardData=data.lsCyclingThisWeek;me.rankNo=data.rankThisWeek.rankOfCycling;break;}
            case 'Running':{me.showCardData=data.lsRunningThisWeek;me.rankNo=data.rankThisWeek.rankOfRunning;break;}
            case 'Swimming':{me.showCardData=data.lsSwimmingThisWeek;me.rankNo=data.rankThisWeek.rankOfSwimming;break;}
            case 'Others':{me.showCardData=data.lsOtherThisWeek;me.rankNo=data.rankThisWeek.rankOfOther;break;}
          }
        }
        
      }else{
        if(this.actionSheetGroup == 'Friends'){
          switch (me.actionSheetSelected){
            case 'Steps':{me.showCardData=data.lsStepFriendLastWeek;me.rankNo=data.rankFriendLastWeek.rankOfStep;break;}
            case 'Exercise (Overall)':{me.showCardData=data.lsOverAllFriendLastWeek;me.rankNo=data.rankFriendLastWeek.rankOfOverAll;break;}
            case 'Cycling':{me.showCardData=data.lsCyclingFriendLastWeek;me.rankNo=data.rankFriendLastWeek.rankOfCycling;break;}
            case 'Running':{me.showCardData=data.lsRunningFriendLastWeek;me.rankNo=data.rankFriendLastWeek.rankOfRunning;break;}
            case 'Swimming':{me.showCardData=data.lsSwimmingFriendLastWeek;me.rankNo=data.rankFriendLastWeek.rankOfSwimming;break;}
            case 'Others':{me.showCardData=data.lsOtherFriendLastWeek;me.rankNo=data.rankFriendLastWeek.rankOfOther;break;}
          }
        }else{
          switch (me.actionSheetSelected){
            case 'Steps':{me.showCardData=data.lsStepLastWeek;me.rankNo=data.rankLastWeek.rankOfStep;break;}
            case 'Exercise (Overall)':{me.showCardData=data.lsOverAllLastWeek;me.rankNo=data.rankLastWeek.rankOfOverAll;break;}
            case 'Cycling':{me.showCardData=data.lsCyclingLastWeek;me.rankNo=data.rankLastWeek.rankOfCycling;break;}
            case 'Running':{me.showCardData=data.lsRunningLastWeek;me.rankNo=data.rankLastWeek.rankOfRunning;break;}
            case 'Swimming':{me.showCardData=data.lsSwimmingLastWeek;me.rankNo=data.rankLastWeek.rankOfSwimming;break;}
            case 'Others':{me.showCardData=data.lsOtherLastWeek;me.rankNo=data.rankLastWeek.rankOfOther;break;}
          }
        }
      }
    });
  }


  ngAfterViewInit() {

    this.content.ionScrollEnd.subscribe((e) => {
      this.scrollStatus = false;
    });

    this.content.ionScroll.subscribe((data) => {
      this.scrollStatus = true;
    });

  }

  paddingCard(data) {

    if (data.scrollTop == 0) {
      this.zone.run(() => {
        this.showCardPadding = 'cardPaddingTrue';
        this.showCardRadius = 'cardRadiusTrue';
      });
    } else {
      this.zone.run(() => {
        this.showCardPadding = 'cardPaddingFalse';
        this.showCardRadius = 'cardRadiusFalse';
      });
    }
  }

  pageToTop() {
    this.content.scrollToTop(30);
  }

  pageToBottom() {
    this.pageTop.scrollTo(0, 223, 30);
  }

  // swipe(e) {

  //   if (e.direction == 4) {

  //     console.log(e.direction);

  //     if (this.scrollStatus == false) {
  //       this.nav.parent.select(1);
  //     }
  //   } else if (e.direction == 2) {

  //     console.log(e.direction);

  //     if (this.scrollStatus == false) {
  //       this.nav.parent.select(3);
  //     }
  //   }
  // }

  btnSegment(index) {
    let me=this;
    if (this.scrollStatus == false) {
      // const loader = this.loadingCtrl.create({
      //   content: 'Loading...',
      //   spinner: 'crescent'
      // });

      // loader.present();

      if (index == 1) {
        console.log('last')
        this.mode = 'last';
        this.segmentSwitch1 = 'segment_button_active';
        this.segmentSwitch2 = 'segment_button_inactive';
        // this.showCardData = this.objRankOfAll;
        // console.log('show ', this.showCardData)
        // if (this.objCurrentRankOfAll.length > 0) {
        //   this.rankNo = this.objCurrentRankOfAll[0].rankNo;
        //   this.highLight = this.objCurrentRankOfAll[0].userNo;
        // } else {
        //   this.rankNo = '-';
        // }
        // console.log('current rank ', this.objCurrentRankOfAll)

        this.pageTop.scrollToTop(100);

      } else {
        console.log('current')
        this.mode = 'current';
        this.segmentSwitch1 = 'segment_button_inactive';
        this.segmentSwitch2 = 'segment_button_active';
        // this.showCardData = this.objRankOfWeek;
        // console.log('show ', this.showCardData)
        // if (this.objCurrentRankOfWeek.length > 0) {
        //   this.rankNo = this.objCurrentRankOfWeek[0].rankNo;
        //   this.highLight = this.objCurrentRankOfWeek[0].userNo;
        // } else {
        //   this.rankNo = '-';
        // }
        // console.log('rank ', this.rankNo, ' ==> ', this.highLight)

        // console.log('chk week ', this.objCurrentRankOfWeek)
        this.pageTop.scrollToTop(100);

      }
      this.zone.run(() => {
        me.bindLeaderboardData();
       });
      this.startIndex = 1;
      this.endIndex = 1;
    }

  }

  onLoadRank(infiniteScroll) {
    console.log('e ', infiniteScroll)
    // let me = this;
    // this.startIndex = this.endIndex + 1;
    // infiniteScroll.complete();
    // if (this.mode == 'week') {
    //   this.leaderboard.getRankOfWeek(this.startIndex).then((data: any) => {
    //     console.log('week ', data)
    //     me.objRankOfWeek = data;
    //     me.showCardData = me.objRankOfWeek;
    //     me.endIndex = data.length;
    //     console.log('end index ', me.endIndex)
    //   })
    // } else {
    //   this.leaderboard.getRankOfAll(this.startIndex).then((data: any) => {
    //     console.log('all ', data);
    //     me.objRankOfAll = data;
    //     me.showCardData = me.objRankOfAll;
    //     me.endIndex = data.length;
    //   })
    // }
  }

  async btnSelectPhoto() {
    let me = this;
    let actionSheet = await this.actionSheetController.create({
      title: 'Select catergories',
      buttons: [
        {
          text: 'Take Photo',
          handler: () => {
            console.log('Take Photo na ja');
            this.openCamera();
          }
        }, {
          text: 'Select from Gallery',
          handler: () => {
            console.log('Select from Gallery');
            this.openPhotoGallery();
          }
        },{
          text: 'Cancel',
          handler: () => {
            console.log('Cancel');
          }
        }, {
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  openCamera() {
    let me = this;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 150,
      targetHeight: 150,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };


    this.camera.getPicture(options).then((imageData) => {
      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      me.file.readAsDataURL(path, filename).then(data => {
        console.log('xx ', data)
        me.userImg = data;
      });
      console.log('src image ', imageData)
      me.uploadImageProfile(imageData);
    }, (err) => {
      console.log(err);
    });
  }
  openPhotoGallery() {
    let me = this;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 150,
      targetHeight: 150,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };


    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData)
      console.log(normalizeURL(imageData))
      let path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let index = filename.indexOf('?');
      if (index > -1) {
        filename = filename.substring(0, index);
      }
      me.file.readAsDataURL(path, filename).then(data => {
        console.log('### ', data)
        me.userImg = data;
      });
      me.uploadImageProfile(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  uploadImageProfile(imageData: any) {
    let me = this;
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: this.config.userNo,
      fileName: 'profile', //'\\profile\\' + this.config.userNo,// + '.jpg',
      httpMethod: 'POST',
      headers: {
        'Content-Type': undefined
      }
    };
    let loader = this.loadingCtrl.create({
      content: "Uploading...",
    });
    loader.present();


    fileTransfer.upload(imageData, encodeURI(this.config.serverUploadAPI), options)
      .then((data) => {
        loader.dismiss();
      }, (err) => {
        loader.dismiss();

        let _alert = this.alertCtrl.create({
          title: 'Fail Upload',
          buttons: ['Dismiss']
        });

        _alert.present();

        console.log(err);
      });
  }

  // btnJoinQrcode(){
  //   console.log('====================================');
  //   console.log('join qr code');
  //   console.log('====================================');
  // }
}
