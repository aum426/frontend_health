import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { HealthKit, HealthKitOptions } from '@ionic-native/health-kit';
import { HeartratePage } from '../heartrate/heartrate';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  selectDate: any;
  height: number;
  heartRate: any;
  energy: any = 0;
  walk_distance: any = 0;
  cycling_distance: any = 0;
  currentHeight = 'No Data';
  stepcount = 'No Data';
  workouts = [];

  constructor(private healthKit: HealthKit, private platform: Platform, public navCtrl: NavController) {

    var start = new Date();
    start.setDate(start.getDate())
    start.setHours(0, 0, 0, 0);

    var end = new Date();
    end.setHours(23, 59, 59, 0); // next midnight

    this.selectDate = {
      myStartDate: start,
      myEndDate: end
    }

    console.log(this.selectDate);

    if (this.platform.is('cordova')) {

      this.platform.ready().then(() => {
        this.healthKit.available().then(available => {
          if (available) {
            // Request all permissions up front if you like to
            var options: HealthKitOptions = {
              readTypes: [
                'HKQuantityTypeIdentifierStepCount',
                'HKWorkoutTypeIdentifier',
                'HKQuantityTypeIdentifierActiveEnergyBurned',
                'HKQuantityTypeIdentifierDistanceCycling',
                'HKQuantityTypeIdentifierHeartRate',
                'HKQuantityTypeIdentifierDistanceWalkingRunning']
            }
            this.healthKit.requestAuthorization(options).then(_ => {
              this.loadHealthData();
            })
          }
        });
      });

    }
  }


  onDaySelect(event) {

    let start = new Date(event.year, event.month, event.date, 0, 0, 0, 0);
    var end = new Date(event.year, event.month, event.date, 23, 59, 59, 0);

    this.selectDate = {
      myStartDate: start,
      myEndDate: end
    }

    console.log('start-end date ', this.selectDate);

    this.loadHealthData();

  }

  loadHealthData() {

    var stepOptions = {
      'startDate': this.selectDate.myStartDate,
      'endDate': this.selectDate.myEndDate,
      'aggregation': 'day',
      'sampleType': 'HKQuantityTypeIdentifierStepCount',
      'unit': 'count'
    }

    this.healthKit.querySampleTypeAggregated(stepOptions).then(data => {
      console.log('step ', data);
      this.stepcount = data[0].quantity;
    }, err => {
      console.log('No steps: ', err);
    });

    var heartRateOptions = {
      'startDate': this.selectDate.myStartDate,
      'endDate': this.selectDate.myEndDate,
      'aggregation': 'day',
      'sampleType': 'HKQuantityTypeIdentifierHeartRate',
      'unit': 'count/min',
    }

    this.healthKit.querySampleTypeAggregated(heartRateOptions).then(data => {
      console.log('HR ', data);
      this.heartRate = Math.round(data[0].quantity);
    }, err => {
      console.log('No heart rate: ', err);
    });

    var energyOptions = {
      'startDate': this.selectDate.myStartDate,
      'endDate': this.selectDate.myEndDate,
      'aggregation': 'day',
      'sampleType': 'HKQuantityTypeIdentifierActiveEnergyBurned',
      'unit': 'kcal',
    }

    this.healthKit.querySampleTypeAggregated(energyOptions).then(data => {
      console.log('Active Energy ', data);
      this.energy = Math.round(data[0].quantity);
    }, err => {
      console.log('No energy: ', err);
    });

    var walkDistanceOptions = {
      'startDate': this.selectDate.myStartDate,
      'endDate': this.selectDate.myEndDate,
      'aggregation': 'day',
      'sampleType': 'HKQuantityTypeIdentifierDistanceWalkingRunning',
      'unit': 'm',
    }

    this.healthKit.querySampleTypeAggregated(walkDistanceOptions).then(data => {
      console.log('distance walking & running ', data, ' ####');

      this.walk_distance = Number(data[0].quantity / 1000).toFixed(1);
      // this.walk_distance = Math.round(data[0].quantity / 100);
    }, err => {
      console.log('No energy: ', err);
    });

    var cyclingDistanceOptions = {
      'startDate': this.selectDate.myStartDate,
      'endDate': this.selectDate.myEndDate,
      'aggregation': 'day',
      'sampleType': 'HKQuantityTypeIdentifierDistanceCycling',
      'unit': 'm',
    }

    this.healthKit.querySampleTypeAggregated(cyclingDistanceOptions).then(data => {
      console.log('distance cycling ', data);
      this.cycling_distance = Number(data[0].quantity / 1000).toFixed(1);
    }, err => {
      console.log('No energy: ', err);
    });

    // var workoutOptions = {
    //   'startDate': this.selectDate.myStartDate,
    //   'endDate': this.selectDate.myEndDate,
    //   'aggregation': 'day',
    //   'sampleType': 'HKWorkoutTypeIdentifier',
    //   'activity': 'activityTypem',
    // }

    // this.healthKit.querySampleType(workoutOptions).then(data => {
    //   console.log('workout ==> ',data);
    //   console.log(data.length);
    // }, err => {
    //   console.log('No Workout Data: ', err);
    // });
    console.log('work')
    // this.healthKit.findWorkouts().then(data => {
    this.healthKit.findWorkouts().then(x => {
      console.log('workout ==> ', x, ' !!!!');
      this.workouts = x;
    }, err => {
      console.log(err);
      // Sometimes the result comes in here, very strange.
      this.workouts = err;
    });

  }

  showHeartrate() {
    console.log('===> ', this.workouts);
    //this.navCtrl.setRoot(HeartratePage, { passDate: this.selectDate }, { animate: true, direction: 'forward' });
  }

}
