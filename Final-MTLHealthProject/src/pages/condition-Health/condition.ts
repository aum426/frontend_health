import { TabsPage } from '../tabs/tabs';
import { Component } from '@angular/core';
import { NavController, NavParams ,LoadingController,AlertController} from 'ionic-angular';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

import { LoginProvider } from '../../providers/login/login';
import { ConfigProvider } from '../../providers/config/config';
import { WebSqlProvider } from "../../providers/web-sql/web-sql";
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-Condition',
  templateUrl: 'Condition.html',
})
export class ConditionPage {
  txtUsername: any;
  txtPassword: any;
  isenabled:boolean=false;


  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private SQLite: WebSqlProvider,
    private config: ConfigProvider, 
    public navCtrl: NavController, private loginPvd: LoginProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    // this.txtUsername='mtl88866';
    // this.txtPassword='P@ssw0rd';
  }

  btnLogin() {
    
    let me = this;
    if (this.txtUsername.length > 0 && this.txtPassword.length > 0) {
      const loader = me.loadingCtrl.create({
        spinner: 'crescent'
      });
      loader.present();
      this.loginPvd.login(this.txtUsername, this.txtPassword).then((data: any) => {
        console.log('login ',data);
        loader.dismiss();
        if (data != null && data.errMsg==null) {
          // me.config.userNo='89107';//for test

          me.config.userNo = data.userNo;
          let sql = "INSERT INTO tb_config(user_no)VALUES('" + me.config.userNo + "')";
              console.log('sql', sql)
              me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status: any) {
                me.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'forward' });
              });
        }else{
          let alertMsg = this.alertCtrl.create({
            title: 'Login Message',
            message: data.errMsg,
            buttons: [
              {
                text: 'Yes'
              }
            ]
          });
          alertMsg.present();
        }
      })
    }
  }

  createAAA(){
    console.log('AAAAA')
  }
  
  createWaste() {
    // this.service.save(this.setDump())
    //     .then(() => {
    //       alert('salvo com sucesso')
    //     })
    console.log(this.isenabled)
  }

  btnBack() {
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
  }
  btnForgotPassword() {
    this.navCtrl.setRoot(ForgotPasswordPage, {}, { animate: true, direction: 'forward' });
  }

}
