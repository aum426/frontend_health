import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, Tabs, PopoverController, AlertController } from 'ionic-angular';

import { ThisWeekPage } from '../this-week/this-week';
import { HistoryPage } from './../history/history';
import { LeaderBoardPage } from './../leader-board/leader-board';
import { WeeklyAchivementPage } from '../weekly-achivement/weekly-achivement';
import { SettingsProvider } from '../../providers/settings/settings';
import { Events } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { PopoverSettingComponent } from '../../components/popover-setting/popover-setting';
import { DeviceSyncPage } from '../device-sync/device-sync';
import { LoginPage } from '../login/login';
import { ChallengeDashboardPage } from '../challenge-dashboard/challenge-dashboard';
import { DeviceConnectedPage } from '../device-connected/device-connected';
import { HealthPage } from './../health/health';

import { ConfigProvider } from '../../providers/config/config';
import { NotificationPage } from '../notification/notification';
import { ProfileSettingPage } from '../profile-setting/profile-setting';
import { TabsProvider } from '../../providers/tabs/tabs';
import {FriendsProvider} from '../../providers/friends/friends';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('mainTabs') mainTabs: Tabs;
  selectedTheme: String;
  rootThisWeekPage: any = ThisWeekPage;
  rooteHistoryPage: any = HistoryPage;
  rootLeaderBoardPage: any = LeaderBoardPage;
  rootChallengeDashboardPage: any = ChallengeDashboardPage;
  rootHealthPage: any = HealthPage;
  tabIndex: any = 1;
  userImg:string='';
  showNoti:boolean=false;
  notificationMessages:any=0;
  

  constructor(
    private friend:FriendsProvider,
    private tabsProvider:TabsProvider,
    private config: ConfigProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public nav: NavController,
    private settings: SettingsProvider,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public events: Events,
    private zone: NgZone
  ) { 
    // this.settings.tabIndex=3;//For Dev
    let me=this;
    //TODO: subscribe event from app.component, assume as push from FCM events
    events.subscribe("notification:changes", () => {
      // alert('subscribe received.');
      // if(this.showNoti){
      //   this.showNoti = false;
      //   this.notificationMessages=0;
      // }else{
      //   this.showNoti = true;
      //   this.notificationMessages=9;
      // }

      // this.zone.run(() => {});
      this.zone.run(() => {
        console.log('notification total '+me.config.totalNotification)
        if(me.config.totalNotification>0){
          me.showNoti=true;
        }else{
          me.showNoti=false;
        }
        me.notificationMessages=me.config.totalNotification;
      });

    });
  }

  ionViewDidLoad() {
    let me=this;
    console.log('tabs page')
    me.friend.getFriendRequestMe().then((data:any)=>{
      console.log('tab friend request ',data.length)
      me.config.objFriendReques=data;
      if(data.length>0){
        me.showNoti=true;
      }else{
        me.showNoti=false;
      }
      me.notificationMessages=data.length;
    });
    
    console.log('chk  xxxx',this.settings.tabIndex)
    this.mainTabs.select(this.settings.tabIndex);
    this.tabsProvider.getProfile().then((data:any)=>{
      console.log('@@@@@@@ ',data);
      me.userImg=data[0].userImg;
    })
  }

  goWeeklyAchievement() {
    this.navCtrl.setRoot(WeeklyAchivementPage, {}, { animate: true, direction: 'forward' });
  }

  goNotificationView() {
    this.navCtrl.setRoot(NotificationPage, {}, { animate: true, direction: 'forward' });
  }

  goProfileSettingView(){
    this.navCtrl.setRoot(ProfileSettingPage, {}, { animate: true, direction: 'forward' });
    
  }

  onTabSelect(index) {
    console.log('index ',index)
    this.settings.tabIndex = index;
  }

  presentPopover(myEvent) {

    let popover = this.popoverCtrl.create(PopoverSettingComponent);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(data => {
      console.log(data, '  config ', this.config.objProfile);
      if (data == 'deviceSync') {
        //check device sync? goto deviceConnected : DeviceSync
        if (this.config.deviceConnected == true) {
          this.navCtrl.setRoot(DeviceConnectedPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.navCtrl.setRoot(DeviceSyncPage, {}, { animate: true, direction: 'forward' });
        }
      } else if (data == 'logout') {

        let confirm = this.alertCtrl.create({
          title: 'Logout System',
          buttons: [
            {
              text: 'No'
            },
            {
              text: 'Yes',
              handler: () => {
                this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
              }
            }
          ]
        });

        confirm.present();

      }
    });

  }

}
