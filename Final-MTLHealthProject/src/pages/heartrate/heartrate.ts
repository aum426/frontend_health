import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HealthKit, HealthKitOptions } from '@ionic-native/health-kit';

@Component({
  selector: 'page-heartrate',
  templateUrl: 'heartrate.html',
})
export class HeartratePage {

  selectDate: any;
  showDateTitle: any;
  heartRateArray: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private healthKit: HealthKit
    ) {
    this.selectDate = this.navParams.get('passDate');
    console.log(this.selectDate);
    this.showDateTitle = this.selectDate.myStartDate.toLocaleDateString("en-US");

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HeartratePage');
    this.showHeartrate();
  }

  showHeartrate() {

    var options = {
      startDate: this.selectDate.myStartDate,
      endDate: this.selectDate.myEndDate,
      sampleType: 'HKQuantityTypeIdentifierHeartRate',
      unit: 'count/min',
      limit: 1000
    }

    this.healthKit.querySampleType(options).then(data => {
      console.log(data);
      console.log(data.length);
      // console.log(data[0]);
      // console.log(data[0].quantity);
      this.heartRateArray = data;
    }, err => {
      console.log('No Heartrate Data: ', err);
    });

  }

  back() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }

}
