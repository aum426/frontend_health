import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FriendsPage } from '../friends/friends';
import { FriendsProvider } from '../../providers/friends/friends';

@Component({
  selector: 'page-pending-request',
  templateUrl: 'pending-request.html',
})
export class PendingRequestPage {

  showCardData: any;
  constructor(
    private friendProvider:FriendsProvider,
    public navCtrl: NavController, public navParams: NavParams) {
    console.log(this.navParams.data.data)
    if(this.navParams.data.data.length>0)this.showCardData=this.navParams.data.data;

    // this.showCardData = [
    //   {
    //     rankNo: 1,
    //     total: 569,
    //     totalTxt: "569 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/89106_156.jpg",
    //     userName: "Elise",
    //     userNo: "89106",
    //   },
    //   {
    //     rankNo: 2,
    //     total: 570,
    //     totalTxt: "570 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/730394_63.jpg",
    //     userName: "Lisa",
    //     userNo: "730394",
    //   },
    //   {
    //     rankNo: 3,
    //     total: 571,
    //     totalTxt: "571 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/84742_734.jpg",
    //     userName: "Stephanie",
    //     userNo: "84742",
    //   },
    //   {
    //     rankNo: 4,
    //     total: 572,
    //     totalTxt: "572 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/82815_482.jpg",
    //     userName: "Stephanie",
    //     userNo: "82815",
    //   }
    // ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingRequestPage');
  }

  btnBack(){
    console.log('go btnBack');
    this.navCtrl.setRoot(FriendsPage,{}, { animate: true, direction: 'back' });
  }

  onAccept(el){
    console.log(el,el.userNo);
    let me=this;
    this.friendProvider.addFriend(el.friendNo).then((data:any)=>{
      console.log('---- ',data)
        me.friendProvider.getFriendRequestMe().then((data:any)=>{
          me.showCardData=data;
        })
    })
  }

  onReject(el){
    console.log(el);
    let me=this;
    this.friendProvider.rejectFriendRequest(el.friendNo).then((data:any)=>{
      console.log('### ',data);
      me.friendProvider.getFriendRequestMe().then((data:any)=>{
        me.showCardData=data;
      })
    })
  }

}
