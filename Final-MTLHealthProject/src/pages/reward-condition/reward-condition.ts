import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController} from 'ionic-angular';


@Component({
  selector: 'page-reward-condition',
  templateUrl: 'reward-condition.html',
})
export class RewardConditionPage {

  annualPremium:string='0';
  constructor(
    private viewCtrl: ViewController,
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardConditionPage',this.navParams.get('annualPremium'));
    this.annualPremium=this.navParams.get('annualPremium');
  }
  btnClose() {
    this.viewCtrl.dismiss();
  }
}
