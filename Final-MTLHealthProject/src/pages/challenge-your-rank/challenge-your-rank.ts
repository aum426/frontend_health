import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-challenge-your-rank',
  templateUrl: 'challenge-your-rank.html',
})
export class ChallengeYourRankPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChallengeYourRankPage');
  }

}
