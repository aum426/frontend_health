import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PendingRequestPage } from '../pending-request/pending-request';
import { ProfileSettingPage } from '../profile-setting/profile-setting';
import { FriendProfilePage } from '../friend-profile/friend-profile';
import { FriendsProvider } from '../../providers/friends/friends';

@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {

  segmentSwitch1: any = 'segment_button_active';
  segmentSwitch2: any = 'segment_button_inactive';
  friendData:any;
  showCardData: any;
  searchType: string = '1';
  friendRequestMe:any=0;

  constructor(
    private friendProvider: FriendsProvider,
    public navCtrl: NavController, public navParams: NavParams) {
      // check friend list >0 search Type =1 : 0 and segmentSwitch2 = active
      let me=this;
      me.friendProvider.getFriendList().then((data:any)=>{
        console.log('friend list ',data)        
        me.friendData=data; 
        me.showCardData=data.friendList;  
        me.friendRequestMe=data.friendRequestMe.length;     
      });

      

    // this.showCardData = [
    //   {
    //     rankNo: 1,
    //     total: 569,
    //     totalTxt: "569 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/89106_156.jpg",
    //     userName: "Elise",
    //     userNo: "89106",
    //   },
    //   {
    //     rankNo: 2,
    //     total: 570,
    //     totalTxt: "570 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/730394_63.jpg",
    //     userName: "Lisa",
    //     userNo: "730394",
    //   },
    //   {
    //     rankNo: 3,
    //     total: 571,
    //     totalTxt: "571 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/84742_734.jpg",
    //     userName: "Stephanie",
    //     userNo: "84742",
    //   },
    //   {
    //     rankNo: 4,
    //     total: 572,
    //     totalTxt: "572 mins",
    //     userImg: "http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/profile/82815_482.jpg",
    //     userName: "Stephanie",
    //     userNo: "82815",
    //   }
    // ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendsPage');
  }

  goPendingRequestView() {
    console.log('go goPendingRequestView ',this.friendData.friendRequestMe);
    this.navCtrl.setRoot(PendingRequestPage, {data:this.friendData.friendRequestMe}, { animate: true, direction: 'forward' });
  }

  goFriendProfileView(el) {
    console.log('go goFriendProfileView' ,el.userNo);
    let _profileType= this.searchType == '1'?'F':'S';
    this.navCtrl.setRoot(FriendProfilePage, {friendNo:el.userNo,profileType:_profileType}, { animate: true, direction: 'forward' });
  }

  btnBack() {
    console.log('go btnBack');
    this.navCtrl.setRoot(ProfileSettingPage, {}, { animate: true, direction: 'back' });
  }

  btnSegment(index) {

    if (index == 1) {
      this.searchType = '1';
      this.segmentSwitch1 = 'segment_button_active';
      this.segmentSwitch2 = 'segment_button_inactive';
      this.showCardData=this.friendData.friendList;
    } else {
      this.searchType = '2';
      this.segmentSwitch1 = 'segment_button_inactive';
      this.segmentSwitch2 = 'segment_button_active';
      this.showCardData=this.friendData.suggestFriendList;
    }

  }

  searchFriend(e) {
    console.log(e.value);
    let me = this;
    //check segment == yourfriend or suggested
    if (me.searchType == '1') {
      me.friendProvider.searchFriendList(e.value).then((data: any) => {
        console.log(data)
        me.showCardData = data;
      })
    } else {
      me.friendProvider.searchSuggestFriend(e.value).then((data:any)=>{
        console.log(data);
        me.showCardData=data;
      });
    }
  }

}
