import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-device-confirm-unlink',
  templateUrl: 'device-confirm-unlink.html',
})
export class DeviceConfirmUnlinkPage {

  brandId: any;
  brandName: any;
  barndDescription: any;
  brandDevicePhoto: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController
  ) {

    let record = this.navParams.get('record');

    this.brandId = record.deviceNo;
    this.brandName = record.deviceBrand;
    this.brandDevicePhoto = record.deviceImage;
    this.barndDescription = record.deviceInfo;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeviceConfirmUnlinkPage');
  }

  btnUnlink(data) {
    console.log('##### ',data);
    this.viewCtrl.dismiss(data);
    // this.viewCtrl.dismiss('unlink');
  }

  btnClose() {
    this.viewCtrl.dismiss('');
  }

}
