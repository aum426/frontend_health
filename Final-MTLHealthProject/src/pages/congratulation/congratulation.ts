import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-congratulation',
  templateUrl: 'congratulation.html',
})
export class CongratulationPage {

  achievedType: any;
  iconImage: string;
  textAchieved: string;
  textResult: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {

    this.achievedType = this.navParams.get('achievedType');

    if (this.achievedType == 'step') {
      this.iconImage =  'icon_step.png';
      this.textAchieved = 'You achieved walking';
      this.textResult = 'over '+this.navParams.get('stepAchivement')+' steps.';
    } else {
      this.iconImage = 'icon_workout.png';
      this.textAchieved = 'You achieved working out';
      this.textResult = 'over 30 minutes per day.';
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CongratulationPage');
  }

  btnClose() {
    this.viewCtrl.dismiss(this.achievedType);
  }

}
