import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-qr-code',
  templateUrl: 'qr-code.html',
})
export class QrCodePage {
  urlQRCode:string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController) {
      this.urlQRCode=this.navParams.get('urlQRCode');
      console.log('url qrcode ',this.urlQRCode)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QrCodePage');
  }

  btnClose() {
    this.viewCtrl.dismiss();
  }
}
