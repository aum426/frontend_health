import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ConfigProvider } from '../../providers/config/config';
//import { HealthPage } from '../health/health';
import { Storage } from '@ionic/storage';



@Component({
  selector: 'health-welcome',
  templateUrl: 'health-welcome.html',
})

export class HealthWelcomePage {

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage ');
    //console.log(this.storage)
    //this.bindLeaderboardData();
  }

  count: number = 1;
  textWelcom ='Your health score shows how healthy you are based on your data. Its based on scientific research and global health recommendations.';


  constructor(
    public navCtrl: NavController,
    private storage: Storage,
  ) {
  
  }

  btnBack() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
  }
  btnNext(){
    console.log('count',this.count++)
    
    console.log('textWelcom=====',this.textWelcom)
    if(this.count==2){
      this.textWelcom = 'Starting by adding your health data  on the next screen'
    }else if(this.count==3){
      this.storage.set('welcomePage', true);
      this.btnBack();
    }
    console.log('textAAAAA',this.textWelcom)
  }
  

}