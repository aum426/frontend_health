import { Component } from "@angular/core";
import { NavController, NavParams, ViewController } from "ionic-angular";

@Component({
  selector: "page-term-and-condition",
  templateUrl: "term-and-condition.html"
})
export class TermAndConditionPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad TermAndConditionPage");
  }

  btnClose() {
    this.viewCtrl.dismiss();
  }
}
