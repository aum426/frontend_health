import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-excercise-detail',
  templateUrl: 'excercise-detail.html',
})
export class ExcerciseDetailPage {

  activityDate:string;
  exerciseZone2Minute: string;
  exerciseZone2Percent: string;
  exerciseZone3Minute: string;
  exerciseZone3Percent: string;
  exerciseTotalMinute: string;
  exerciseSwimDistance: string;
  exereciseSwimPercent: string;
  exerciseTotalPercent: string;
  exerciseTotalCalories:string;

  constructor(
    private viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExcerciseDetailPage', this.navParams.get('objData'));
    let obj:any=this.navParams.get('objData');
    console.log('xxx ',obj)
    this.activityDate=obj.activityDate;
    this.exerciseSwimDistance=obj.exerciseSwimDistance;
    this.exerciseTotalMinute=obj.exerciseTotalMinute;
    this.exerciseTotalPercent=obj.exerciseTotalPercent;
    this.exerciseZone2Minute=obj.exerciseZone2Minute;
    this.exerciseZone2Percent=obj.exerciseZone2Percent;
    this.exerciseZone3Minute=obj.exerciseZone3Minute;
    this.exerciseZone3Percent=obj.exerciseZone3Percent;
    this.exereciseSwimPercent=obj.exereciseSwimPercent;
    this.exerciseTotalCalories=obj.exerciseTotalCalories;
  }

  btnClose() {
    this.viewCtrl.dismiss();
  }

}
