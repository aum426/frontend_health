import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, AlertController, LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

import { ChallengeCreateProvider } from '../../providers/challenge-create/challenge-create';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../providers/config/config';

@Component({
  selector: 'page-challenge-create',
  templateUrl: 'challenge-create.html',
})
export class ChallengeCreatePage {
  txtChallengeName: string = '';
  // txtConditionStep: string = '';
  // txtConditionHeartRate: string = '';
  txtCondition: string = '';
  txtReward: string = '';
  startDate: any;
  endDate: any;

  imgChallenge: string = 'assets/imgs/icons/group_challenge.png';
  objImageData: any;
  objCondition: string = '';
  actionSheetSelected: any;

  constructor(
    private config: ConfigProvider,
    public loadingCtrl: LoadingController,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private challengeCreate: ChallengeCreateProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetController: ActionSheetController,
    private alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChallengeCreatePage');
    this.actionSheetSelected = "Steps";

    // today
    this.startDate = new Date().toISOString();
    console.log(this.startDate);

    // tomorrow
    this.endDate = this.startDate

  }

  btnBack() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
  }

  btnSaveChallenge() {
    console.log(this.startDate)
    let me = this;
    //this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
    if (this.txtChallengeName != '' && this.startDate != undefined && this.endDate != undefined
      && this.objCondition != '' && this.txtCondition != '') {
      let conditionStep = '0';
      let conditionHR = '0';
      let conditionDistance = '0';
      switch (me.objCondition) {
        case 'HR': {
          conditionHR = this.txtCondition;
          break;
        }
        case 'Distance': {
          conditionDistance = this.txtCondition;
        }
        case 'Steps': {
          conditionStep = this.txtCondition;
        }
      }
      this.txtReward = this.txtReward == '' ? '-' : this.txtReward;
      this.challengeCreate.create(this.txtChallengeName, conditionStep, conditionHR, conditionDistance,
        this.txtReward, this.startDate, this.endDate).then((data: any) => {
          console.log('result create challenge ', data)
          //check change image ? call upload : nothing
          //call upload image
          if (data != '' && me.objImageData != undefined) me.uploadImageChallenge(data);
          if (data != '') me.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
        })
    } else {
      console.log('empty')
      let alert = this.alertCtrl.create({
        title: 'Create Challenge',
        message: 'please insert information',
        buttons: [
          {
            text: 'YES'
          }
        ]
      });
      alert.present();
    }
  }

  updateActionSheetSelected(value) {
    if (value === 1) {
      this.actionSheetSelected = 'Steps';
    } else if (value === 2) {
      this.actionSheetSelected = 'Exercise (Overall)';
    } else if (value === 3) {
      this.actionSheetSelected = 'Biking';
    } else if (value === 4) {
      this.actionSheetSelected = 'Running';
    } else if (value === 5) {
      this.actionSheetSelected = 'Swimming';
    } else if (value === 6) {
      this.actionSheetSelected = 'Other';
    }

  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      title: 'Select catergories',
      buttons: [{
        text: 'Steps',
        role: 'destructive',
        icon: 'steps',
        // cssClass: "share actionSheet_withIcomoon",
        handler: () => {
          this.updateActionSheetSelected(1);
        }
      }, {
        text: 'Exercise (Overall)',
        icon: 'exercise',
        handler: () => {
          this.updateActionSheetSelected(2);
        }
      }, {
        text: 'Biking',
        icon: 'bike',
        handler: () => {
          this.updateActionSheetSelected(3);
        }
      }, {
        text: 'Running',
        icon: 'running',
        handler: () => {
          this.updateActionSheetSelected(4);
        }
      }, {
        text: 'Swimming',
        icon: 'swimming',
        handler: () => {
          this.updateActionSheetSelected(5);
        }
      }, {
        text: 'Other',
        icon: 'other',
        handler: () => {
          this.updateActionSheetSelected(6);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  async btnSelectPhoto() {
    const actionSheet = await this.actionSheetController.create({
      title: 'Select method take photo',
      buttons: [{
        text: 'Take Photo',
        role: 'destructive',
        handler: () => {
          console.log('Take Photo xxx');
          this.openCamera();
        }
      }, {
        text: 'Select from Gallery',
        handler: () => {
          console.log('Select from Gallery');
          this.openPhotoGallery();
        }
      }, {
        text: 'Cancel',
        handler: () => {
          console.log('Cancel');
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  openCamera() {
    let me = this;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 150,
      targetHeight: 150,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };


    this.camera.getPicture(options).then((imageData) => {
      me.objImageData = imageData;

      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      me.file.readAsDataURL(path, filename).then(data => {
        console.log('xx ', data)
        me.imgChallenge = data;
      });
      // console.log('src image ', imageData)
      // me.uploadImageProfile(imageData);
    }, (err) => {
      console.log(err);
    });
  }
  openPhotoGallery() {
    let me = this;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 150,
      targetHeight: 150,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };


    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData)
      me.objImageData = imageData;

      let path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let index = filename.indexOf('?');
      if (index > -1) {
        filename = filename.substring(0, index);
      }
      me.file.readAsDataURL(path, filename).then(data => {
        console.log('### ', data)
        me.imgChallenge = data;
      });
      // me.uploadImageProfile(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  uploadImageChallenge(challengeNo: string) {
    let me = this;
    console.log('upload challenge ', me.objImageData)
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: challengeNo,
      fileName: 'challenge',
      httpMethod: 'POST',
      headers: {
        'Content-Type': undefined
      }
    };
    let loader = this.loadingCtrl.create({
      content: "Uploading...",
    });
    loader.present();


    fileTransfer.upload(me.objImageData, encodeURI(this.config.serverUploadAPI), options)
      .then((data) => {
        loader.dismiss();
      }, (err) => {
        loader.dismiss();

        let _alert = this.alertCtrl.create({
          title: 'Fail Upload',
          buttons: ['Dismiss']
        });

        _alert.present();

        console.log(err);
      });
  }

  onConditionChange(e) {
    console.log('c', e)
    this.objCondition = e;
  }
}
