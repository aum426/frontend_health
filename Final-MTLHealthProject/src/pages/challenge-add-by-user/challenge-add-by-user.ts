import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-challenge-add-by-user',
  templateUrl: 'challenge-add-by-user.html',
})
export class ChallengeAddByUserPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChallengeAddByUserPage');
  }

}
