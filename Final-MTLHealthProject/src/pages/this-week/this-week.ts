import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, NavParams, Content, ModalController, Platform, FabContainer, LoadingController } from 'ionic-angular';
import { CongratulationPage } from '../congratulation/congratulation';
import { CongratulationAchievmentPage } from '../congratulation-achievment/congratulation-achievment';

import { ConfigProvider } from '../../providers/config/config';
import { DeviceSyncPage } from '../../pages/device-sync/device-sync';
import { ThisWeekProvider } from '../../providers/this-week/this-week';

import { InAppBrowser, InAppBrowserEvent } from '@ionic-native/in-app-browser';
import { DeviceSyncProvider } from '../../providers/device-sync/device-sync';
import { WebSqlProvider } from "../../providers/web-sql/web-sql";

import { AppleProvider } from '../../providers/apple/apple';
import { ExcerciseDetailPage } from '../excercise-detail/excercise-detail';
import { HowToChallengesWorkPage } from '../how-to-challenges-work/how-to-challenges-work';
import { WeeklyAchivementPage } from '../weekly-achivement/weekly-achivement';

@Component({
  selector: 'page-this-week',
  templateUrl: 'this-week.html',
})
export class ThisWeekPage {

  @ViewChild('pageTop') pageTop: Content;

  version: string;

  progress: any = 0;
  progressCss: string;
  viewHeight: any;
  fabStatus: boolean;
  positionTopRightCorner: any;
  timeRemain: string = '--';
  stepDesc: string = '';
  stepGoal: string = '75,000';
  stepComplete: string = '0';
  stepLeft: string = '0';
  // currentRank: number = 0;
  totalRank: number = 0;
  exerciseDesc: string = '';
  exerciseGoal: string = '3 days';
  exerciseComplete: string = '';
  exerciseRemain: string = '';


  cycle1: string;
  cycle2: string;
  cycle3: string;
  // cycle4: string;
  // cycle5: string;
  // cycle6: string;
  // cycle7: string;

  cycle1Value: string;
  cycle2Value: string;
  cycle3Value: string;
  // cycle4Value: string;
  // cycle5Value: string;
  // cycle6Value: string;
  // cycle7Value: string;

  cycle1Status: string;
  cycle2Status: string;
  cycle3Status: string;

  dateName1: string;
  dateName2: string;
  dateName3: string;

  cycle1Detail: string;
  cycle2Detail: string;
  cycle3Detail: string;

  currentIndex: number = 0;

  lastSync: string;

  iconSyncStatus: string = 'iconSync';

  // objDebug: any;

  constructor(
    private apple: AppleProvider,
    private SQLite: WebSqlProvider,
    private deviceSyncProvider: DeviceSyncProvider,
    private iab: InAppBrowser,
    private loadingCtrl: LoadingController,
    private zone: NgZone,
    private thisWeek: ThisWeekProvider,
    private config: ConfigProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public nav: NavController,
    public platform: Platform
  ) {
    this.version = this.config.appVersion;

    let me = this;
    console.log('Width: ' + platform.width());
    console.log('Height: ' + platform.height());

    this.cycle1 = 'c100 p0 color1';
    this.cycle2 = 'c100 p0 color1';
    this.cycle3 = 'c100 p0 color1';
    this.cycle1Value = '0%';
    this.cycle2Value = '0%';
    this.cycle3Value = '0%';
    this.cycle1Status = '';
    this.cycle2Status = '';
    this.cycle3Status = '';
    this.dateName1 = 'Mon';
    this.dateName2 = 'Tue';
    this.dateName3 = 'Wen';

    // if (platform.height() <= 700) {
    //   console.log('iphone5');
    //   this.fabStatus = false;
    // } else {
    //   console.log('iphone X');
    this.fabStatus = true;
    // }

    if (platform.width() <= 320) {
      this.positionTopRightCorner = 'top-right-corner-small';
    } else {
      this.positionTopRightCorner = 'top-right-corner-normal';
    }
    // me.thisWeek.getDateRemain().then((data: any) => {
    //   console.log(data);
    //   me.timeRemain = data == 0 ? 'last ' : data;
    // })
    const loader = me.loadingCtrl.create({
      spinner: 'crescent'
    });
    loader.present();
    me.thisWeek.getDataInitial().then((data: any) => {
      loader.dismiss();
      console.log('@@@@@ ', data);
      me.timeRemain = data.dateRemain;//== 1 ? 'last ' : data.dateRemain;
      // me.exerciseDesc = data.exerciseDesc;
      // me.stepDesc = data.stepDesc;
    })

  }

  // testData: any = new Array(20);

  ionViewDidLoad() {

    // this.cycle4 = 'c100 p0 color1';
    // this.cycle5 = 'c100 p0 color1';
    // this.cycle6 = 'c100 p0 color1';
    // this.cycle7 = 'c100 p0 color1';
    // this.cycle4Value = '30m';
    // this.cycle5Value = '30m';
    // this.cycle6Value = '30m';
    // this.cycle7Value = '30m';

    console.log('ionViewDidLoad ThisWeekPage');
    let me = this;
    if(me.config.tokenDevice!=''){
      me.thisWeek.updateToken().then((data:any)=>{
        console.log('update token',data)
      })
    }

    let sql = "SELECT * FROM tb_config";
    me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result: any) {
      if (result != null && result.rows.length > 0) {
        console.log('get userNo from db ', result.rows.item(0).user_no);

        me.config.userNo = result.rows.item(0).user_no;
        console.log('this.config.objThisWeek ', me.config.objThisWeek != undefined, '=>', me.config.objThisWeek)
        //clear graph
        me.progress = 0;
        me.updateStepProgress();
        // me.updateExerciseProgress(0, 0, 0);
        if (me.config.objThisWeek != undefined) {
          me.bindData(me.config.objThisWeek);
        } else {

          me.exerciseGoal = '3 days';
          me.exerciseComplete = '0 day';
          me.exerciseRemain = '0 day';
          me.zone.run(() => { });
        }

        me.config.deviceConnected = me.checkDeviceConnected();
        console.log('device status ', me.config.deviceConnected)
        if (me.config.deviceConnected == true && me.config.objThisWeek == undefined) {
          //data last sync
          console.log('load last sync', me.config.objThisWeek)
          const loader = me.loadingCtrl.create({
            spinner: 'crescent'
          });
          loader.present();
          me.thisWeek.getSyncData().then((data: any) => {
            loader.dismiss();
            console.log('### ', data)
            me.config.objThisWeek = data;
            let _day = data.dateNo - 1;
            me.currentIndex = _day > 4 ? 4 : _day;
            console.log('current Index ', me.currentIndex, ' ', _day)
            me.bindData(data);
          });
          // me.thisWeek.getLastSync().then((data: any) => {
          //   loader.dismiss();
          //   console.log('last sync data ', data)

          //   if (data.stepGoal > 0) {
          //     me.bindData(data);
          //   } else {
          //     me.progress = 0;
          //     me.updateStepProgress();
          //     // me.updateExerciseProgress(0, 0, 0);
          //     me.exerciseGoal = '3 day';
          //     me.exerciseComplete = '0 day';
          //     me.exerciseRemain = '0 day';
          //     me.zone.run(() => { });
          //   }
          // })


        }
      }
    });
  }

  updateStepProgress() {
    if (this.progress > 0) {
      this.progressCss = 'progressInner';
    } else {
      this.progressCss = 'progressInnerZero';
    }
  }

  updateExerciseProgress(exerciseComplete: number, exerciseMinute: number, exerciseProgress: number) {
    console.log('xxx ', exerciseComplete)
    if (exerciseComplete > 0) {
      this.zone.run(() => {
        for (let i = 1; i <= exerciseComplete; i++) {
          console.log('i ', i)
          if (i == 1) {
            this.cycle1 = 'c100 p100 color1';
            this.cycle1Value = '30m';
            this.cycle1Status = 'assets/imgs/icons/checkbox_oval.png';
            if (exerciseMinute > 0 && exerciseMinute < 30) {
              this.cycle2 = 'c100 p' + exerciseProgress + ' color1';
              this.cycle2Value = exerciseMinute + 'm';
            }
          } else if (i == 2) {
            this.cycle1 = 'c100 p100 color1';
            this.cycle1Value = '30m';
            this.cycle1Status = 'assets/imgs/icons/checkbox_oval.png';
            this.cycle2 = 'c100 p100 color1';
            this.cycle2Value = '30m';
            this.cycle2Status = 'assets/imgs/icons/checkbox_oval.png';
            if (exerciseMinute > 0 && exerciseMinute < 30) {
              this.cycle3 = 'c100 p' + exerciseProgress + ' color1';
              this.cycle3Value = exerciseMinute + 'm';
            }
          } else {
            this.cycle1 = 'c100 p100 color1';
            this.cycle1Value = '30m';
            this.cycle1Status = 'assets/imgs/icons/checkbox_oval.png';
            this.cycle2 = 'c100 p100 color1';
            this.cycle2Value = '30m';
            this.cycle2Status = 'assets/imgs/icons/checkbox_oval.png';
            this.cycle3 = 'c100 p100 color1';
            this.cycle3Value = '30m';
            this.cycle3Status = 'assets/imgs/icons/checkbox_oval.png';
          }
        }
      });


    } else {
      if (exerciseMinute > 0) {
        this.cycle1 = 'c100 p' + exerciseProgress + ' color1';
        this.cycle1Value = exerciseMinute + 'm';
      } else {
        this.cycle1 = 'c100 p0 color1';
        this.cycle2 = 'c100 p0 color1';
        this.cycle3 = 'c100 p0 color1';
        this.cycle1Value = '0m';
        this.cycle2Value = '0m';
        this.cycle3Value = '0m';
        this.cycle1Status = '';
        this.cycle2Status = '';
        this.cycle3Status = '';
      }

    }
  }

  swipe(e) {
    console.log(e.direction);
    if (e.direction == 2) {
      this.nav.parent.select(1);
    }
  }

  checkDeviceConnected() {
    console.log('check device connected ', this.config.objProfile);
    let status: boolean = false;
    if (this.config.objProfile == undefined) {
      console.log('not connect')
      status = false;
    } else if (this.config.objProfile.rows.item(0).brand == null || this.config.objProfile.rows.item(0).brand == '') {
      console.log('not connect')
      status = false;
    } else {
      console.log('connected')
      status = true;
    }
    return status;
  }

  bindData(data: any) {
    console.log('bind data')
    this.lastSync = data.lastSync;
    this.progress = data.stepProgress;
    this.updateStepProgress();
    this.exerciseGoal = data.exerciseGoal + ' days';
    // this.exerciseDesc = data.exerciseDesc;
    this.exerciseComplete = parseInt(data.exerciseComplete) > 1 ? data.exerciseComplete + ' days' : data.exerciseComplete + ' day';
    this.exerciseRemain = parseInt(data.exerciseRemain) > 1 ? data.exerciseRemain + ' days' : data.exerciseRemain + ' day';
    // this.updateExerciseProgress(data.exerciseComplete, data.exerciseMinute, data.exerciseProgress);
    this.updateExerciseProgressPercent(data.lsExercise);
    this.stepGoal = data.stepGoalDisplay;
    // this.stepDesc = data.stepDesc;
    this.stepComplete = data.stepCompleteDisplay;
    this.stepLeft = data.stepRemainDisplay;
    // this.currentRank = data.currentRank;
    this.totalRank = data.totalRank;
    this.config.currentLevel = data.levelNo;
    this.zone.run(() => { });
  }

  checkTokenExpiry(brand, func) {
    let me = this;
    me.thisWeek.checkTokenExpiry(brand).then((status: string) => {
      console.log('token status ', status == '' ? 'good' : status);
      if (status != '') {
        let browser = me.iab.create(status, "_blank", "location=no");
        browser.on('loadstop').subscribe((event: InAppBrowserEvent) => {
          console.log('event ', event.url);
          let currentURL = new URL(event.url);
          console.log('url ', currentURL)
          let code = currentURL.searchParams.getAll('code');
          console.log('code ', code, ' ', code.length)
          if (code.length > 0) {
            me.deviceSyncProvider.registerUser(brand, code).then((result: any) => {
              console.log('register result ', result)
              browser.close();
              func();
            })
          }
        });
      } else {
        func();
      }
    })
  }

  chkSyncSDK(brand, func) {
    let me = this;
    if (brand == 'Apple') {
      let lsWorkout: any = [];

      me.apple.getSyncDate('Apple', 'step').then((objSyncDate: any) => {
        console.log('start date ', objSyncDate.startDate, ' end date ', objSyncDate.endDate)
        var start = new Date(objSyncDate.startDate);
        start.setHours(0, 0, 0, 0);
        console.log('check start date ', start)
        var end = new Date(objSyncDate.endDate);
        end.setHours(23, 59, 59, 0);
        console.log('check end date ', end)
        let objDate = {
          startDate: start,
          endDate: end
        }

        console.log('obj date ', objDate)
        me.apple.getStep(objDate, function (objStep) {
          console.log('lsStep ', objStep)
          // me.objDebug = 'step ' + JSON.stringify(objStep);
          me.apple.getActivity(function (obj: any = []) {
            let objWorkOut = obj.filter(data => new Date(data.startDate) >= start);
            console.log('filter workout ', objWorkOut)
            // me.objDebug = me.objDebug + ' workout ' + JSON.stringify(objWorkOut);
            me.apple.mapActivityHR(objWorkOut, lsWorkout, function (objWorkout) {
              console.log('workout & HR ---------> ', objWorkout)
              me.apple.syncAppleRawData(objStep, objWorkout).then(result => {
                console.log('xxxxx ', result)
                // me.objDebug = me.objDebug + ' sync raw data ' + result;
                func(1);
              });
            })
          })
        });
      });
    } else {
      func(0);
    }
  }



  syncData(type, fab: FabContainer) {
    console.log('sync ', type)
    console.log(type);
    console.log(fab);
    let me = this;

    if (type == 'sync') {
      console.log('sync ..... ')
      fab != undefined ? fab.close() : '';
      console.log('config in this week page ', me.config.objProfile)
      //check device connected?
      if (me.config.deviceConnected == true) {
        console.log('get value from db')
        let userNo = me.config.objProfile.rows.item(0).user_no;
        let brand = me.config.objProfile.rows.item(0).brand;
        console.log(userNo, '  ', brand)
        //if brand == Garmin check have token register ? go to call sync webservice call device connect
        //check token expiry *** Garmin token never expiry
        // me.checkTokenExpiry(brand, function () {
        //call webservice sync
        const loader = me.loadingCtrl.create({
          spinner: 'dots',
          content: 'Syncing your data...'
        });
        
        // Start animation sync icon
        me.iconSyncStatus = 'iconSyncing';

        loader.present();
        //check brand
        me.chkSyncSDK(brand, function (x) {
          me.thisWeek.syncData(brand).then((data: any) => {
            loader.dismiss();
            
            // Stop animation sync icon
            me.iconSyncStatus = 'iconSync';

            console.log('@@@ ', data);
            // let data={stepAchivement:'50000',exerciseAchievement:'1',levelAchievement:'0',
            // levelNo:'0',levelName:'',levelImage:'http://apixdev.muangthai.co.th/Resource/MTLHealthyProgram/level/monkey.png',levelInfo:'[{"info":"- 3 days exercise"},{"info":"- 50k steps and 2 days exercise"},{"info":"- 75k steps and 1 day exercise"}]',smilePoint:''};
            if (data != null) {
              me.config.refreshHistory = true;
              me.config.refreshLeaderboard = true;
              me.config.refreshChallengeDashboard = true;
              me.config.objThisWeek = data;
              let _day = data.dateNo - 1;
              me.currentIndex = _day > 4 ? 4 : _day;
              me.bindData(me.config.objThisWeek);
              //open dialog step,activity,level                   
              if ((data.stepAchivement != "0" && data.stepAchivement != null) || (data.exerciseAchievement != "0" && data.exerciseAchievement != null)) {

                const stepModal = me.modalCtrl.create(CongratulationPage, { achievedType: 'step', stepAchivement: data.stepAchivement });
                const exerciseModal = me.modalCtrl.create(CongratulationPage, { achievedType: 'workout' });
                const levelModal = me.modalCtrl.create(CongratulationAchievmentPage, { levelNo: data.levelNo, levelName: data.levelName, levelImage: data.levelImage, levelInfo: data.levelInfo, smilePoint: data.smilePoint }
                );
                if (data.stepAchivement != "0") {
                  console.log('case 1')
                  stepModal.present();
                }
                if (data.stepAchivement == "0" && data.exerciseAchievement != "0") {
                  console.log('case 2')
                  exerciseModal.present();
                }
                stepModal.onDidDismiss(typePopup => {
                  console.log('type popup ', typePopup);
                  if (data.exerciseAchievement != "0") {
                    console.log('step case 1')
                    exerciseModal.present();
                  } else if (data.levelAchievement != '-1') {
                    console.log('step case 2');
                    levelModal.present();
                  }
                });
                exerciseModal.onDidDismiss(typePopup => {
                  console.log('type popup ', typePopup);
                  if (data.levelAchievement != '-1') {
                    console.log('workout case 1')
                    levelModal.present();
                  }
                })
              }
            }
          });
        });

        // });
      } else {
        console.log('new open app')
        const profileModal = this.modalCtrl.create(DeviceSyncPage, { achievedType: type });
        profileModal.present();
      }
    }

  }

  updateExerciseProgressPercent(data: any) {
    console.log('==> ', data, ' #### ', this.currentIndex)
    let me = this;
    this.zone.run(() => {
      me.cycle1 = 'c100 p' + data[me.currentIndex].exerciseProgress + ' color1';
      me.cycle1Value = data[me.currentIndex].exerciseTotalPercent + '%';
      me.dateName1 = data[me.currentIndex].dateName;
      if (data[me.currentIndex].exerciseProgress == 100) {
        me.cycle1Status = 'assets/imgs/icons/checkbox_oval.png'
      } else {
        me.cycle1Status = '';
      }

      me.cycle2 = 'c100 p' + data[me.currentIndex + 1].exerciseProgress + ' color1';
      me.cycle2Value = data[me.currentIndex + 1].exerciseTotalPercent + '%';
      me.dateName2 = data[me.currentIndex + 1].dateName;
      if (data[me.currentIndex + 1].exerciseProgress == 100) {
        me.cycle2Status = 'assets/imgs/icons/checkbox_oval.png'
      } else {
        me.cycle2Status = '';
      }

      me.cycle3 = 'c100 p' + data[me.currentIndex + 2].exerciseProgress + ' color1';
      me.cycle3Value = data[me.currentIndex + 2].exerciseTotalPercent + '%';
      me.dateName3 = data[me.currentIndex + 2].dateName;
      if (data[me.currentIndex + 2].exerciseProgress == 100) {
        me.cycle3Status = 'assets/imgs/icons/checkbox_oval.png'
      } else {
        me.cycle3Status = '';
      }
    });



  }

  getSyncData() {
    console.log('xxxx')
    let me = this;
    this.thisWeek.getSyncData().then((data: any) => {
      console.log('data ', data)
      me.config.objThisWeek = data;

      me.updateExerciseProgressPercent(me.config.objThisWeek.lsExercise);
    })
  }

  backward() {
    if (this.config.objThisWeek != undefined) {
      console.log('left ', this.currentIndex)
      if (this.currentIndex > 0) this.currentIndex = this.currentIndex - 1;
      console.log('@@ ', this.currentIndex)
      this.updateExerciseProgressPercent(this.config.objThisWeek.lsExercise);
    }
  }

  forward() {
    if (this.config.objThisWeek != undefined) {
      console.log('right ', this.currentIndex)
      if (this.currentIndex < 4) this.currentIndex = this.currentIndex + 1;
      console.log('## ', this.currentIndex, ' ', this.config.objThisWeek)
      this.updateExerciseProgressPercent(this.config.objThisWeek.lsExercise);
    }
  }

  getDetail(el: number) {
    if (this.config.objThisWeek != undefined) {
    console.log(el, this.currentIndex)
    console.log('detail ', this.config.objThisWeek.lsExercise[this.currentIndex + el]);
    const stepModal = this.modalCtrl.create(ExcerciseDetailPage, { objData: this.config.objThisWeek.lsExercise[this.currentIndex + el] });
    stepModal.present();
    }
  }

  goHowToChallengesWorkPageView() {
    // console.log('go goHowToChallengesWorkPageView');
    const profileModal = this.modalCtrl.create(HowToChallengesWorkPage, {});
    profileModal.present();
  }

  getAchievement(){
    console.log(' achievement')
    const profileModal = this.modalCtrl.create(WeeklyAchivementPage, {sourcePage:'thisWeek'});
    profileModal.present();
  }

}
