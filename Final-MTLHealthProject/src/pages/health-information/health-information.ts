import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ConfigProvider } from '../../providers/config/config';

@Component({
  selector: 'health-information',
  templateUrl: 'health-information.html',
})

export class HealthInformationPage {

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage ');
    //console.log(this.storage)
    //this.bindLeaderboardData();
  }

  count: number = 1;

  constructor(
    public navCtrl: NavController,
    private config: ConfigProvider, 
  ) {
  
  }


  btnBack() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
  }

}