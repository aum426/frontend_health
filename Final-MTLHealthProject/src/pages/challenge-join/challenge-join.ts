import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { PopoverShareComponent } from '../../components/popover-share/popover-share';
import { QrCodePage } from './../qr-code/qr-code';

import { ChallengeJoinProvider } from '../../providers/challenge-join/challenge-join';

@Component({
  selector: 'page-challenge-join',
  templateUrl: 'challenge-join.html',
})
export class ChallengeJoinPage {

  record: any;
  mockupData: any;
  objChallengeDetail: any;
  joinStatus: any;
  showType: string;
  txtDate:string;
  segmentSwitch1: any = 'segment_button_active';
  segmentSwitch2: any = 'segment_button_inactive';

  constructor(
    private challengeJoin: ChallengeJoinProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController
  ) {

    let me = this;
    this.showType = this.navParams.get('showType');
    console.log('show type ',this.showType)

    // this for test button joined
    // this.joinStatus = true;
    // this.joinStatus = false;
    this.joinStatus = this.navParams.get('statusJoin');
    

    this.record = this.navParams.get('passRecord');
    console.log('status ', this.joinStatus)
    console.log('record ', this.record)
    if (this.showType == 'UpComimg') {
      this.txtDate='Beginning in';
      this.challengeJoin.getChallengeDetail(this.record.challengeNo).then((data: any) => {
        console.log('xx ', data)
        // debugger;
        me.objChallengeDetail = data;
      })
    } else {
      this.txtDate='Remaining';
      this.challengeJoin.getCompetitionDetail(this.record.challengeNo).then((data: any) => {
        console.log('xxx ', data)
        me.objChallengeDetail = data;
      })
    }


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChallengeJoinPage');

  }

  btnBack() {
    console.log('b')
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
  }

  btnReject() {
    console.log('btnReject');

    let confirm = this.alertCtrl.create({
      title: 'Reject this challenge',
      message: this.record.challengeName,
      buttons: [
        {
          text: 'No'
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('btnReject');
            this.challengeJoin.rejectChallenge(this.record.challengeNo).then((data: any) => {
              console.log(data)
              if (data == 1) {
                this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
              }
            });
          }
        }
      ]
    });
    confirm.present();

  }

  btnJoin() {
    console.log('btnJoin', this.record);
    let me = this;
    let confirm = this.alertCtrl.create({
      title: 'Join this challenge',
      message: this.record.challengeName,
      buttons: [
        {
          text: 'No'
        },
        {
          text: 'Yes',
          handler: () => {
            this.challengeJoin.joinChallenge(this.record.challengeNo).then((data: any) => {
              console.log(data)
              if (data == 1) {
                this.joinStatus = true;
                this.challengeJoin.getChallengeDetail(this.record.challengeNo).then((data: any) => {
                  console.log('xx ', data)
                  me.objChallengeDetail = data;
                })
              }
            });
          }
        }
      ]
    });
    confirm.present();
  }

  btnCancelJoin() {
    // this.joinStatus = false;
  }

  // btnShare(myEvent) {

  //   let popover = this.popoverCtrl.create(PopoverShareComponent);
  //   popover.present({
  //     ev: myEvent
  //   });
  //   let _urlQRCode = this.record.qrCode;
  //   popover.onDidDismiss(data => {
  //     console.log(data);
  //     if (data == 'qr') {
  //       console.log('QR ', _urlQRCode);

  //       const profileModal = this.modalCtrl.create(QrCodePage, { achievedType: 'generate_qr', urlQRCode: _urlQRCode });
  //       profileModal.present();

  //     } else if (data == 'email') {
  //       console.log('Email');
  //     }
  //   });

  // }

  btnShare(myEvent) {
    let _urlQRCode = this.record.qrCode;
    const profileModal = this.modalCtrl.create(QrCodePage, { achievedType: 'generate_qr', urlQRCode: _urlQRCode });
    profileModal.present();

  }

  btnSegment(index) {
      if (index == 1) {
        this.segmentSwitch1 = 'segment_button_active';
        this.segmentSwitch2 = 'segment_button_inactive';
      } else {
        this.segmentSwitch1 = 'segment_button_inactive';
        this.segmentSwitch2 = 'segment_button_active';
      }
  }

}
