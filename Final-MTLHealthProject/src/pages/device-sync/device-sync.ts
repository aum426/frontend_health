

// declare var cordova: any;
import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController,Platform } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DeviceConfirmConnectPage } from '../device-confirm-connect/device-confirm-connect';
import { DeviceConnectedPage } from '../device-connected/device-connected';

import { DeviceSyncProvider } from '../../providers/device-sync/device-sync';
import { WebSqlProvider } from "../../providers/web-sql/web-sql";
import { ConfigProvider } from '../../providers/config/config';

import { InAppBrowser, InAppBrowserEvent } from '@ionic-native/in-app-browser';
import { ProfileSettingPage } from '../profile-setting/profile-setting';

@Component({
  selector: 'page-device-sync',
  templateUrl: 'device-sync.html',
})
export class DeviceSyncPage {

  mockupData: any;

  deviceData: any;

  constructor(
    private platform: Platform,
    private loadingCtrl: LoadingController,
    private iab: InAppBrowser,
    // private zone: NgZone,
    private config: ConfigProvider,
    private SQLite: WebSqlProvider,
    private deviceSyncProvider: DeviceSyncProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
    let me = this;    
    me.deviceSyncProvider.getDeviceConfig().then((data: any) => {
      console.log('getDeviceConfig ',data);
      me.deviceData = data;
      me.config.deviceData = data;
      // console.log('find apple ',data.findIndex(x=>x.deviceBrand=='Apple'))
      // if(me.platform.is('android')) {
      //   let indexApple =data.findIndex(x=>x.deviceBrand=='Apple');
      //   console.log('android')
      //   data.removeAt(0)
      //   console.log('chk ',data)
      // }else{
      //   console.log('ios')
      // }
      console.log('deviceData ', me.config.deviceData)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeviceSyncPage');

  }

  btnSkip() {
    this.navCtrl.setRoot(ProfileSettingPage, {}, { animate: true, direction: 'back' });
  }

  btnConnectDevice(data) {
    let me = this;
    console.log('btnConnectDevice',data);
    const deviceConfirmModal = this.modalCtrl.create(DeviceConfirmConnectPage, { record: data });
    deviceConfirmModal.present();

    deviceConfirmModal.onDidDismiss(url => {
      console.log('url ####', url ,'chk ',url=='');
      if (data.deviceBrand!='Apple' && url != '') {
        console.log('url ', url)
        let browser = me.iab.create(url, "_blank", "location=no");
        browser.on('exit').subscribe((event: InAppBrowserEvent) => {
          console.log('browser closed',event);
        }, err => {
          console.error(err);
        });
        browser.on('loadstop').subscribe((event: InAppBrowserEvent) => {
          console.log('event ', event.url);
          let currentURL = new URL(event.url);
          console.log('url ', currentURL)
          let code = currentURL.searchParams.getAll('code');
          console.log('code ', code, ' ', code.length)
          if (code.length > 0) {
            browser.close();
            me.deviceSyncProvider.registerUser(data.deviceBrand, code).then((result: any) => {
              console.log('register authen', result)
              console.log(data.deviceBrand)
              data.status = 'Y';
              //save in brand , dateConnect in db
              let sql = "INSERT OR REPLACE INTO tb_config(user_no,brand,date_connect)VALUES('" + me.config.userNo + "','" + data.deviceBrand + "',DATE())";
              console.log('sql', sql)
              this.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status: any) {
                console.log(status)
                sql = "SELECT * FROM tb_config WHERE user_no='" + me.config.userNo + "'";
                console.log('### ', sql);
                me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result: any) {
                  console.log(result)
                  me.config.objProfile = result;
                  data.dateConnect = result.rows.item(0).date_connect;//[0]["date_connect"];
                  me.navCtrl.setRoot(DeviceConnectedPage, {}, { animate: true, direction: 'forward' });
                })
              })
            })

          }
        });
      }else{
        if(data.deviceBrand=='Apple'){
          if(url=='allow')data.status = 'Y';
           //save in brand , dateConnect in db
           let sql = "INSERT OR REPLACE INTO tb_config(user_no,brand,date_connect)VALUES('" + me.config.userNo + "','" + data.deviceBrand + "',DATE())";
           console.log('sql', sql)
           this.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status: any) {
             console.log(status)
             sql = "SELECT * FROM tb_config WHERE user_no='" + me.config.userNo + "'";
             console.log('### ', sql);
             me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result: any) {
               console.log(result)
               me.config.objProfile = result;
               data.dateConnect = result.rows.item(0).date_connect;
               me.navCtrl.setRoot(DeviceConnectedPage, {}, { animate: true, direction: 'forward' });
             })
           })
        }
      }
    });

  }

}
