import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { HealthKit, HealthKitOptions } from '@ionic-native/health-kit';

import { ConfigProvider } from '../../providers/config/config';
import { WebSqlProvider } from "../../providers/web-sql/web-sql";
import {DeviceSyncProvider} from '../../providers/device-sync/device-sync';
import {ThisWeekProvider} from '../../providers/this-week/this-week';

@Component({
  selector: 'page-applewatch',
  templateUrl: 'applewatch.html',
})
export class ApplewatchPage {

  workoutData:any;
  objDate:any;

  constructor(
    private thisWeek:ThisWeekProvider,
    private deviceSync:DeviceSyncProvider,
    private config: ConfigProvider,
    private SQLite: WebSqlProvider,
    private healthKit: HealthKit,
    private platform: Platform,
    public navCtrl: NavController) {

      // var start = new Date();
      // start.setDate(start.getDate() - 7)
      // start.setHours(0, 0, 0, 0);
      // console.log('check start date ', start)
  
      // var end = new Date();
      // //end.setDate(end.getDate());
      // end.setHours(23, 59, 59, 0); // next midnight
  
      //  this.objDate = {
      //   startDate: start,
      //   endDate: end
      // }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApplewatchPage');
  }

  checkPermission(func): any {
    let me = this;
    this.platform.ready().then(() => {
      me.healthKit.available().then(available => {
        if (available) {
          // Request all permissions up front if you like to
          var options: HealthKitOptions = {
            readTypes: [
              'HKQuantityTypeIdentifierStepCount',
              'HKWorkoutTypeIdentifier',
              'HKQuantityTypeIdentifierActiveEnergyBurned',
              'HKQuantityTypeIdentifierDistanceCycling',
              'HKQuantityTypeIdentifierHeartRate',
              'HKQuantityTypeIdentifierDistanceWalkingRunning']
          }
          me.healthKit.requestAuthorization(options).then(_ => {
            console.log('allow permisstion')
            func("allow");
          })
        } else {
          console.log('ios can not access health function')
          func("denied");
        }
      });
    });
  }


  requestPermission() {
    this.checkPermission(function(result){
      console.log('check permission ',result);
    });
  }
  checkDeviceConnect(func) {
    let me = this;
    let sql = "SELECT * FROM tb_config";
    let status='';
    console.log('db ',me.config.dbMaster)
    me.SQLite.executeQuery(me.config.dbMaster, sql, [], function (result: any) {
      console.log('get userNo from db ', result.rows);
      if(result.rows.length>0)status=result.rows.item(0).brand;
      func(status);
    });
  }

  deviceConnect() {
    console.log('connect')
    let me = this;
    this.checkDeviceConnect(function (statusConnect) {
      console.log('status Connect ', statusConnect)
      if (statusConnect == undefined || statusConnect == '') {
        console.log('disconnect');
        // let statusPermission = me.checkPermission();
        me.checkPermission(function(statusPermission){
          console.log('status permission ', statusPermission)
          me.config.userNo='81276';
          if (statusPermission == 'allow') {
            console.log('change status device connected')
            let sql = "INSERT OR REPLACE INTO tb_config(user_no,brand,date_connect)VALUES('" + me.config.userNo + "','Apple',DATE())";
            console.log('sql', sql)
            me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status: any) {
              console.log('query status ', status)
            })
          }
        });
      } else {
        console.log('connect ', statusConnect)
      }
    })
  }

  deviceDisconnect() {
    console.log('disconnect')
    let me = this;
    let sql = "UPDATE tb_config SET brand='' WHERE user_no='" + me.config.userNo + "'";
    console.log('sql update ', sql)
    me.SQLite.executeNonQuery(me.config.dbMaster, sql, [], function (status) {
      console.log('status update ', status)
    });
  }

  syncData() {
    //call webservice get startDate-endDate
    
    console.log('objDate ', this.objDate)
    // this.getStep(this.objDate);
    // this.getWorkOut();
  }

  getStep(objDate: any,func:any) {
    console.log('step')
    var stepOptions = {
      'startDate': objDate.startDate,
      'endDate': objDate.endDate,
      'aggregation': 'day',
      'sampleType': 'HKQuantityTypeIdentifierStepCount',
      'unit': 'count'
    }

    this.healthKit.querySampleTypeAggregated(stepOptions).then(data => {
      // console.log('objStep ', data, ' step ', data[0].quantity);
      func(data);
    }, err => {
      console.log('No steps: ', err);
      func(err);
    });
  }

  getActivity(func) {
    let me = this;
    if (this.platform.is('cordova')) {
      this.platform.ready().then(() => {
        me.healthKit.available().then(available => {
          if (available) {
            // Request all permissions up front if you like to
            var options: HealthKitOptions = {
              readTypes: [
                'HKQuantityTypeIdentifierStepCount',
                'HKWorkoutTypeIdentifier',
                'HKQuantityTypeIdentifierActiveEnergyBurned',
                'HKQuantityTypeIdentifierDistanceCycling',
                'HKQuantityTypeIdentifierHeartRate',
                'HKQuantityTypeIdentifierDistanceWalkingRunning']
            }
            me.healthKit.requestAuthorization(options).then(_ => {
              console.log('allow permisstion')
              this.healthKit.findWorkouts().then(x => {
                console.log('workout ==> ', x, ' !!!!');
                func(x);
                //this.workoutData = x;
              }, err => {
                console.log(err);
                // Sometimes the result comes in here, very strange.
                func(err);
                // this.workoutData = err;
              });
            })
          } else {
            console.log('ios can not access health function')
          }
        });
      });
    }
  }

  mapActivityHR(objActivity:Array<any>,lsWorkout:Array<object>,func:any){
    let me=this;
    if(objActivity.length>0){
      var obj:any=objActivity.shift();
      this.getHeartRate(new Date(obj.startDate),new Date(obj.endDate),function(objHR){
        console.log('obj HR ',objHR);
        obj["lsHR"]=objHR;
        lsWorkout.push(obj);
        me.mapActivityHR(objActivity,lsWorkout,func);
      })
    }else{
      func(lsWorkout);
    }
  }

  getWorkOutx() {
  

    let me = this;
    // let objAppleWatch={};
    let lsWorkout:any=[];
    me.config.userNo='81276';
    console.log('ls ',lsWorkout)
    me.deviceSync.getSyncDate('Apple','step').then((syncDate:any)=>{      
      console.log('sync Date ',syncDate)
      var start = new Date(syncDate);
      // start.setDate(start.getDate() - 7)
      start.setHours(0, 0, 0, 0);
      console.log('check start date ', start)
      var end = new Date();
      end.setHours(23, 59, 59, 0);
      console.log('check end date ', end)
      me.objDate = {
        startDate: start,
        endDate: end
      }

      console.log('obj date ',me.objDate)
      me.getStep(me.objDate,function(objStep){
        console.log('lsStep ',objStep)
        // console.log('objStep.lsStep ',objStep.lsStep);
        // objAppleWatch['lsStep']=objStep;
        // console.log('chk ',objAppleWatch)
        // lsWorkout.push(objStep);
        // console.log('chk ls ',lsWorkout);
        me.getActivity(function (obj:any=[]) {
          me.workoutData =JSON.parse(JSON.stringify(obj));//clone
          console.log('lsWorkout ',obj)
          let newWorkOut=obj.filter(data => new Date(data.startDate) >=start);
          console.log('filter workout ',newWorkOut)
          me.mapActivityHR(newWorkOut,lsWorkout,function(objWorkout){
            console.log('workout & HR ---------> ',objWorkout)
            me.deviceSync.syncAppleRawData(objStep,objWorkout).then(result=>{
                console.log('xxxxx ',result)
                if(result==1){
                  console.log('sync')
                  me.thisWeek.syncData('Apple').then((objSync:any)=>{
                    console.log('====> ',objSync)
                  });
                }
            });
          })
        }) 
      });
    })
    
       
    // console.log('workout ==> ', data, ' ===>',data.length);
    // for(var i=0;i<data.length;i++){
    //   console.log(data[i]);
    // }
    // for (var d = new Date(2012, 0, 1); d <= now; d.setDate(d.getDate() + 1)) {

    // }


  }

  getHeartRate(start:Date,end:Date,func:any) {
    console.log('HR ')
    // var start = new Date();
    //   start.setDate(start.getDate() - 2)
    //   start.setHours(0, 0, 0, 0);
    //   console.log('check start date ', start)
  
    //   var end = new Date();
    //   end.setDate(end.getDate()-2);
    //   end.setHours(23, 59, 59, 0); // next midnight
    console.log('start ',start ,' end ',end)

    var options = {
      startDate: start,
      endDate: end,
      sampleType: 'HKQuantityTypeIdentifierHeartRate',
      unit: 'count/min',
      limit: 3000
    }

    this.healthKit.querySampleType(options).then(data => {
      console.log(data);
      console.log(data.length);
      func(data);

    }, err => {
      console.log('No Heartrate Data: ', err);
      func(err);
    });
  }

  getQuery() {
    console.log('query')
    

    var stepOptions = {
      startDate: this.objDate.startDate,
      endDate: this.objDate.endDate,
      sampleType: 'HKQuantityTypeIdentifierStepCount',
      unit: 'count'
    }
    this.healthKit.querySampleType(stepOptions).then(data => {
      console.log('data ', data)
    })
  }



}
