import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-how-to-challenges-work',
  templateUrl: 'how-to-challenges-work.html',
})
export class HowToChallengesWorkPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HowToChallengesWorkPage');
  }

  btnBack() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'back' });
  }

}
