import { Component } from "@angular/core";
import { NavController, ViewController, AlertController } from "ionic-angular";

@Component({
  selector: "popover-setting",
  templateUrl: "popover-setting.html"
})
export class PopoverSettingComponent {
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController
  ) {}

  btnEditProfile() {
    this.viewCtrl.dismiss("editProfile");
  }

  btnDeviceSetting() {
    this.viewCtrl.dismiss("deviceSync");
  }

  btnTermsAndCondition() {
    this.viewCtrl.dismiss("termAndCondition");
  }

  btnLogout() {
    this.viewCtrl.dismiss("logout");
  }
}
