import { Component } from '@angular/core';
import { NavController, ViewController, AlertController } from 'ionic-angular';

@Component({
  selector: 'popover-share',
  templateUrl: 'popover-share.html'
})
export class PopoverShareComponent {

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController
  ) { }

  btnQR() {
    this.viewCtrl.dismiss('qr');
  }

  btnEmail() {
    this.viewCtrl.dismiss('email');
  }

}
