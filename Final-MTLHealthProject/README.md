<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p>

<h3 align="center">MTL Healthkit</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()

</div>

---

<p align="center"> Healthkit with Hybrid application
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](../TODO.md)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

Exciting project.

## 🏁 Getting Started <a name = "getting_started"></a>

Yep

### Git command

Basic git command

```
1) Clone repository to local machine
    $ git clone repo

2) Check all branch from local & origin
    $ git branch -a

3) Change branch to master
    $ git checkout master
    $ git branch (check your current branch)
    $ git pull (pull all commit from master first)

4) Create new branch and continue Fix or Feature
    $ git checkout -b fix/CREATE_YOUR_BRANCH

5) Add modified files with (.) for select your files by yourself
    $ git status (check modified files)
    $ git diff (check different files in current branch)
    $ git log (check history of commit & commit id)    
    $ git add .
    $ git add header.js body.js

6) Commit to staging 
    $ git commit -m "Your commit description"

7) Push to origin (Internet: Github.com)
    $ git push (new branch will call up-stream by default just copy & paste)

8) Merge branch (e.g. Stay at master branch)
    $ git merge fix/CREATE_YOUR_BRANCH
    $ git push

9) Fetch from Origin (update all data from origin)
    $ git fetch --prune    

10) Pull data from origin BRANCH to current branch
    $ git pull origin master

```

### Installing

A step by step series of examples that tell you how to get a development env running.

```
Cd ionic folder & run 
$ npm install

Start development
$ ionic serve

Build production
$ ionic cordova build android/ios --prod
```

End with an example of getting some data out of the system or using it for a little demo.

## 🎈 Usage <a name="usage"></a>

Add notes about how to use the system.

## 🚀 Deployment <a name = "deployment"></a>

Add additional notes about how to deploy this on a live system.

## ⛏️ Built Using <a name = "built_using"></a>

- [Ionic3](https://www.ionic.com/) - Hybrid Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ✍️ Authors <a name = "authors"></a>

- [@senchabox](https://github.com/senchaboxdev) - Idea & Initial work
